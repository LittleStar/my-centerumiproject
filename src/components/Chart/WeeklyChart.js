import ReactEcharts from 'echarts-for-react'

//直接传入 thisWeekSalesPriceDetail [{dayOfWeek, data}]
const WeeklyChart = ({ data }) => {

  const weekMenu = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

  const RowDataToSorted = (rowData) => {
    if (rowData.length === 0) return []
    let result = []
    weekMenu.forEach((item) => {
      const selectData = rowData.find((dataItem) => {
        return dataItem.dayOfWeek === item
      })
      const value = selectData === undefined ? 0 : selectData.data
      result.push(value)
    })
    return result
  }

  const showData = data || []

  const sortedData = RowDataToSorted(showData)

  const option = {
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
    },
    yAxis: {
      type: 'value',
      axisLabel: {
        formatter: '{value} 元'
      },
    },
    series: [
      {
        type: 'line',
        label: {
          normal: {
            show: true,
            position: 'top'
          }
        },
        data: sortedData
      },
    ]
  }

  return (
    <ReactEcharts
      option={option}
      style={{ height: 300, width: '100%', margin: 'auto' }}
    />
  )

}

export default WeeklyChart
