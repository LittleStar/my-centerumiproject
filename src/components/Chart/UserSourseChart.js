import ReactEcharts from 'echarts-for-react'

//传入用户来源list [{name,value}]
const UserSourseChart = ({data})=>{

  const showData = data||[]

	const sourseLegend = showData.map((item)=>{
		return item.name
	})

	const option = {
		title : {
	        text: '客户来源分析',
	        x:'center'
	    },
	    tooltip : {
	        trigger: 'item',
	        formatter: "{b} : {d}%"
	    },
	    legend: {
	        orient: 'vertical',
	        left: 'left',
	        data: sourseLegend
	    },
	    series : [
	        {
	            type: 'pie',
	            radius : '55%',
              data:showData,
              label:{
                formatter: '{b}: {d}%',
              },
	            itemStyle: {
	                emphasis: {
	                    shadowBlur: 10,
	                    shadowOffsetX: 0,
	                    shadowColor: 'rgba(0, 0, 0, 0.5)'
	                }
	            }
	        }
	    ]
	}

	return (
        <ReactEcharts
            option={option}
            style={{ height: 400, width: '100%', margin: 'auto' }}
        />
    )

}

export default UserSourseChart
