import React, { PureComponent } from 'react';
import { Tabs } from 'antd';
import styles from './DetailPage.css'
import { Link } from 'dva/router';

const TabPane = Tabs.TabPane

export default class DetailPage extends PureComponent {
  constructor(props) {
    super(props)
    this.list = props.list
  }

  /**
   * 判断是否是http链接.返回 Link 或 a
   * Judge whether it is http link.return a or Link
   * @memberof SiderMenu
   */
  getTabItemPath = item => {
    const itemPath = item.path
    const { tabName } = item

    return (
      <Link className={styles.tabLinkStyle}
        to={itemPath}
      >{tabName}
      </Link>
    )
  }

  render() {

    // 遍历生成tab栏
    var tabList = this.list.map((item) => {
      return (
        <TabPane tab={this.getTabItemPath(item)} key={item.path}></TabPane>
      )
    })

    return (
      <Tabs
        activeKey={this.props.location.pathname}
      >
        {tabList}
      </Tabs>
    )
  }
}