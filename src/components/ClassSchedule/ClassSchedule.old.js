import React, { PureComponent } from 'react';
import { Table, Button } from 'antd'
import { spawn } from '../../../node_modules/redux-saga/effects';

const GetClassroomData = ({ room, teacher, classId }) => {
  const size = 'small'
  function handleClick(classId) {
    console.log(classId)
  }
  return (
    <div>
      <div>{room}</div>
      <div>{teacher}</div>
      <div>{classId}</div>
      <Button type="primary" size={size} onClick={()=>handleClick(classId)}>more</Button>
    </div>
  )
  // }
}
export default class ClassSchedule extends PureComponent {

  constructor(props) {
    super(props)
    this.time = props.time
    this.classroom = props.classroom
    this.classSchedule = props.classSchedule
    this.clickMore = props.onClickMore
    this.locate = this.locate.bind(this)

    this.addTimeIndex()
    this.addClassroomIndex()

    this.fillData()
  }

  addTimeIndex() {
    for (let i = 0; i < this.time.length; i++) {
      let timeArr = this.time[i].start.split(':')
      this.time[i].timeSortIndex = timeArr[0] * 10000 + timeArr[1] * 100 + Number(timeArr[2])
    }
    this.time.sort((a, b) => {
      return a.timeSortIndex > b.timeSortIndex
    })
    this.time.forEach((item,index)=>item.timeSortIndex = index)
  }

  addClassroomIndex() {
    for (let i = 0; i < this.classSchedule.length; i++) {
      let room = this.classroom.indexOf(this.classSchedule[i].room)
      this.classSchedule[i].roomSortIndex = room
    }
    this.classSchedule.sort((a, b) => {
      return a.roomSortIndex > b.roomSortIndex
    })
  }

  locate(item) {
    let col = item.roomSortIndex
    let time = this.time.find(element=> element.key=== item.time)
    let row = time.timeSortIndex
    this._data[row][`classroom${col}`] = <GetClassroomData room={item.room} teacher={item.teacher} classId={item.classId} />
  }

  fillData() {
    const data = []

    const columns = [
      { title: 'time', width: 100, dataIndex: 'time', key: 'time', fixed: 'left' },
    ]
    for (let j = 0; j < this.classroom.length; j++) {
      columns.push(
        {
          title: `教室 ${this.classroom[j]}`,
          dataIndex: 'classroom' + j,
          key: `${j}`,
          width: 150,
        }
      )
    }

    const len = this.time.length
    for (let i = 0; i < len; i++) {
      data.push({
        key: i,
        time: `${this.renderTime(this.time, i)}`,
      })
    }

    this._data = data
    this.classSchedule.forEach(this.locate)

    this._columns = columns
  }

  // 左边时间栏
  renderTime(time, index) {
    return `${time[index].start}-${time[index].end}`
  }

  render() {
    return (
      <Table columns={this._columns} dataSource={this._data} scroll={{ x: 1500, y: 500 }} bordered />
    )
  }

}
