import React, { PureComponent } from 'react'
import { Table, Button, Select, Card, Icon, Avatar, Row, Col } from 'antd'
import moment from 'moment'
import styles from './classSchedule.less'
import { isExistAtCurrentPermission, GetCurrentPermission } from '../../utils/filterPermission'

const { Meta } = Card;
const Option = Select.Option
const columnWidth = 250

const ClassList = ({ classList, onSelectClass }) => {
  if (classList !== undefined) {
    return (
      classList.map((classItem, index) => {

        const { id, teacher, course, subjects } = classItem
        const courseInfo = course ? course.title : '未指定'
        const teacherInfo = teacher ? teacher.name : '未指定'
        const subjectsInfo = subjects.length + '人'

        const classAvatar = classItem.coursePlanId ? <Avatar shape="square" style={{ color: '#000', backgroundColor: '#87d068' }}>{courseInfo}</Avatar> : <Avatar shape="square" style={{ color: '#000', backgroundColor: '#fde3cf' }}>{courseInfo}</Avatar>


        if (index !== classList.length - 1) {
          return (
            <React.Fragment key={id} >
              <div className={styles.classListStyle}>
                <Card
                  hoverable
                  style={{ width: columnWidth - 90 }}
                  cover={classAvatar}
                  onClick={() => onSelectClass(id)}
                >
                  <Meta
                    description={teacherInfo + '-' + subjectsInfo}
                  // style={{padding: 100}}
                  />
                </Card>
                <br />

              </div>
            </React.Fragment>
          )
        }
        else {
          return (
            <React.Fragment key={id} >
              <div className={styles.classListStyle}>
                <Card
                  hoverable
                  cover={classAvatar}
                  style={{ width: columnWidth - 90 }}
                  onClick={() => onSelectClass(id)}
                >
                  <Meta
                    description={teacherInfo + '-' + subjectsInfo}
                  />
                </Card>
              </div>
            </React.Fragment>
          )
        }
      })
    )
  }
  else {
    return null
  }
}

const ClassSchedule = ({ timeList, classroomList, classScheduleList, onSelect, onClickAdd, loading }) => {

  // 排序时间段
  const sortTime = (time) => {
    const temp = time
    temp.sort((a, b) => {
      return (moment(a.startTime, 'HH:mm:ss').isBefore(moment(b.startTime, 'HH:mm:ss'))) ? -1 : 1
    })
    return temp
  }

  const renderTimeColumn = (text, record, index) => {
    return (
      <p>{record.time}</p>
    )
  }

  const renderClassColumn = (text, record, index, room) => {
    const classListProps = {
      classList: text,
      onSelectClass(id) {
        const classItem = text.find((item) => {
          return item.id === id
        })
        onSelect(classItem)
      }
    }

    return (
      <div className={styles.wrap}>
        <Card>
          <ClassList {...classListProps} />
        </Card>
      </div>
    )
  }

  const scheduleListToObj = (schedule) => {
    const result = {}

    schedule.forEach((item) => {
      const { classroomId } = item
      if (!Array.isArray(result[classroomId])) {
        result[classroomId] = []
      }
      result[classroomId].push({ ...item })
    })

    return result
  }

  const getShowTime = (time) => {
    return time.startTime + '-' + time.endTime
  }

  const sortedTime = sortTime(timeList)

  const showData = sortedTime.map((timeItem) => {
    const schedule = classScheduleList.filter((classItem) => {
      return classItem.startCoursePeriodId === timeItem.id
    })

    const scheduleObj = scheduleListToObj(schedule)
    const showTime = getShowTime(timeItem)
    return { key: showTime, timeKey: timeItem.key, time: showTime, ...scheduleObj }
  })


  const textAlign = styles['column-textalign']
  // 头部列
  const columns = [
    { title: '时间', width: 100, className: textAlign, key: 'time', fixed: 'left', render: (text, record, index) => renderTimeColumn(text, record, index) },
  ]

  const scrollX = classroomList.length * columnWidth + 100

  classroomList.forEach((item, index) => {
    const { name, id } = item
    columns.push({
      title: '教室 ' + name,
      dataIndex: id,
      key: id,
      width: columnWidth,
      className: textAlign,
      render: (text, record, index) => renderClassColumn(text, record, index, item)
    })
  })

  return (
    <div>
      <Row type="flex" justify="end" style={{ marginBottom: 10 }}>
        <Col className={styles.temp}></Col>临时
        <Col className={styles.plan}></Col>长期
      </Row>
      <Table
        loading={loading}
        columns={columns}
        dataSource={showData}
        bordered
        scroll={{ x: scrollX, y: 600 }}
        pagination={false}
      />
    </div>
  )
}

export default ClassSchedule
