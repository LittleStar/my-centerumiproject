import React from 'react'
import { connect } from 'dva'
import styles from './CardInfo.css'

const CardInfo = ({ cardData }) => {
  return (
    <div>
      {
        cardData.map((item, index) => {
          return (
            <span className={styles.cardItem} key={item.name}>
              <span className={`${styles.cardText} ${styles.cardTitle}`}>{item.name}: </span>
              <span className={styles.cardText}>{item.itemVal}</span>
            </span>
          )
        })
      }
    </div>
  )
}
export default CardInfo
