import styles from './footer.css';

const GlobalFooter = ({ links, copyright }) => {

	return (
		<div className = {styles.footer}>
		    <div className={styles.links}>
		      {links && (
		        <div className={styles.links}>
		          {links.map(link => (
		            <a className={styles.a} key={link.key} target={link.blankTarget ? '_blank' : '_self'} href={link.href}>
		              {link.title}
		            </a>
		          ))}
		        </div>
		      )}
		      {copyright && <div className={styles.copyright}>{copyright}</div>}
		    </div>
		</div>
		);
};

export default GlobalFooter;
