import React from 'react'
import { Popconfirm, message, Icon } from 'antd'

const PopConfirm = () => {
  return (
    <div>
      <Popconfirm title="Are you sure？" icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}>
        <a href="#">Delete</a>
      </Popconfirm>
    </div>
  )
}

export default PopConfirm

