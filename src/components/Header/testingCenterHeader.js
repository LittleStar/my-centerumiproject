import { Menu, Dropdown, Button, Row, Col } from 'antd'
import { Link } from 'dva/router';
import styles from './Header.css'
import cogLeapLogo from './img/CogLeapLogo.png'
import down from './img/down.png';

const testingCenterHeader = ({currentUser, locations, onSignOut, onLocationChange}) => {

  const {user,organizationUnit,application} = currentUser

  const showLocations = locations.filter((item)=>{
    return item.id!==organizationUnit.id
  })

  if(user===undefined){
    return(
      <div className={styles.commonHeader}></div>
      )
  }

  const handleMenuClick = (e) =>{
    if(e.key==='2'){
      onSignOut()
    }
  }

  const menu = (
    <Menu onClick={handleMenuClick} className={styles.header_menu}>
      <Menu.Item key="2" className={styles.header_menu_item}>退出登录</Menu.Item>
    </Menu>
  )

  const locationMenu = (
    <Menu onClick={(e)=>onLocationChange(e.key)} className={styles.header_menu}>
    {
      showLocations.map((item)=>{
        return(
          <Menu.Item key={item.id} className={styles.header_menu_item}>{item.displayName}</Menu.Item>
          )
      })
    }
    </Menu>
  )

  return (
    <div className={styles.commonHeader}>
      <Row>
        <Col span={8}><div className={styles.leftLogo}><Link to='/'> <img src={cogLeapLogo} className={styles.logo}/></Link></div></Col>
        <Col span={8}><div className={styles.centerDashboardTitle}>{organizationUnit.displayName}</div></Col>
        <Col span={8}>
          <div className={styles.rightLogo}>
            {
              showLocations.length>0?
              <Dropdown overlay={locationMenu} className={styles.infoMenu}>
                <Button className={styles.userNameBtn}>
                  {organizationUnit.displayName}<img src={down} className={styles.downImg}/>
                </Button>
              </Dropdown>
              :null
            }
            <Dropdown overlay={menu} className={styles.infoMenu}>
              <Button className={styles.userNameBtn}>
                {user.name}<img src={down} className={styles.downImg}/>
              </Button>
            </Dropdown>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default testingCenterHeader;
