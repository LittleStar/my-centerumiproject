import React from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

class Wysiwyg extends React.Component{

	constructor(props) {
		super(props);

		const html = this.props.value||'<p>initial...</p>'
		const contentBlock = htmlToDraft(html);
      	const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      	const editorState = EditorState.createWithContent(contentState);
		this.state = {
		    editorState: editorState,
		};
	}

	handleEditorChange = (editorState)=>{
		const onChange = this.props.onChange
		this.setState({editorState});
    	
    	const saveData = draftToHtml(convertToRaw(editorState.getCurrentContent()))
    	onChange(saveData)
	}

	render(){

	    const { editorState } = this.state

		return(
			<Editor
				editorState = {editorState}
				onEditorStateChange={this.handleEditorChange}
			/>
			)
	}
}

export default Wysiwyg