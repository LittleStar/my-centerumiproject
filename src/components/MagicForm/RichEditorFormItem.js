import { Input, Form } from 'antd';
//import Editor from './TinyEditor'
import Editor from './WysiwygEditor'
const FormItem = Form.Item


const RichEditorFormItem = ({propItem, getFieldDecorator,formItemLayout}) => {
	const {ShowTitle, FormKey, Value, Setting, Description} = propItem
	const {Required} = Setting

	const initialValue = Value?Value:''
    return (
			<FormItem
			{...formItemLayout}
		      label={ShowTitle}
		      extra = {Description}
		      key = {FormKey}
		      >
		      {getFieldDecorator(FormKey, {
		      	initialValue: initialValue,
		        rules: [{required: Required, message: `${ShowTitle} 是必填项`},],
		      })(<Editor />)}
		    </FormItem>
    )
}

export default RichEditorFormItem
