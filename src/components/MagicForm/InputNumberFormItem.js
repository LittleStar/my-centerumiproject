import { Input, Form, InputNumber } from 'antd';
const FormItem = Form.Item

const InputNumberFormItem = ({propItem, getFieldDecorator,formItemLayout}) => {

	const {ShowTitle, FormKey, Value, Setting, Description} = propItem
	const {Required,NumberSetting, Disabled} = Setting

	const initialValue = Value?Value:0

    return (
			<FormItem
			{...formItemLayout}
		      label={ShowTitle}
		      extra={Description}
		      key = {FormKey}
		    >
		      {getFieldDecorator(FormKey, {
		      	initialValue: initialValue,
		        rules: [{required: Required, message: `${ShowTitle} 是必填项`},],
		      })(<InputNumber {...NumberSetting} disabled = {Disabled}/>)}
		    </FormItem>
    )
}

export default InputNumberFormItem
