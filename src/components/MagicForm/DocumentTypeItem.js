import { Form, Button, Spin } from 'antd'
import { propertyToFormItem } from './PropertyToFormItem'

//根据DocumentType的每一个property自动生成表单，每一个DocumentType就是一个表单
//每一个property的可能的类型为：
//时间类： DatePicker, RangePicker,
//输入类： Input, RichEditor, NumberInput,
//选择类： ContentSelector, Bool, Dropdown,
//容器类： EditableList, SortList
//其他： Upload

//注意： EditableList是可以编辑的List，每一个元素都是documentType，这个列表只存在于该Content;
//       SortList是选择已有的DocumentType, 不能编辑每一个元素，只能排序，添加，删除
//注意： DocumentType具有提交表单的功能, 所以每一个表单提交都是一个documentType
//       DocumentType里面各个元素的值通过properties传入，其他的类型值均通过Value传入

//content: {ShowTitle, FormKey, EditorType, LastEditBy, LastEditTime, Properties, Value, Setting, Description}
//Setting: {
//	Required,
//	SelectorOptions(ContentSelector 要用到), item:{Id:, Value:}
//	DateType('time'/'date'/'month' DatePicker要用)
//	DropdownOptions(Dropdown 要用到)  item:{Id:, Value:}
//  NumberSetting(InputNumber要用到) {min, max, step}
//	UploadSetting(Upload要用到) {maxFileCount,allowedMIMEType,allowedFileExtensions,maxFileSizeKB}
//	ListElement(EditableListFormItem要用到)
//	SortListSetting
//}


const DocumentTypeItem = ({ content, onSubmit, onCancel, loading, form: { getFieldDecorator, validateFieldsAndScroll, resetFields } }) => {
  // const { Properties, FormKey } = content
  let Properties = []

  if(content){
    Properties = content.Properties
  }

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };


  const showFormItems = Properties.map((propItem) => {
    return propertyToFormItem(propItem, getFieldDecorator, formItemLayout, loading)
  })

  const onClickSubmit = () => {
    validateFieldsAndScroll((err, value) => {
      if (err)
        return
      onSubmit(value)
      // resetFields()
    })
  }

  return (
    <React.Fragment>
      {
        loading ?
          <Spin /> :
          <Form>
            {showFormItems}
            <Button size="large" onClick={onCancel} style={{ marginRight: 10 }}>取消</Button>
            <Button size="large" type="primary" onClick={onClickSubmit}>确认</Button>
          </Form>
      }
    </React.Fragment>
  )

}

export default Form.create()(DocumentTypeItem)
