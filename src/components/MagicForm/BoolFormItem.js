import { DatePicker, Form, Switch } from 'antd';
import moment from 'moment'

const FormItem = Form.Item

const BoolFormItem = ({	propItem, getFieldDecorator,formItemLayout}) => {

	const {ShowTitle, FormKey, Value, Setting, Description} = propItem
	const {Required, Disabled} = Setting

	const initialValue = Value?Value:null

    return (
			<FormItem
				{...formItemLayout}
		      label={ShowTitle}
		      extra = {Description}
		      key = {FormKey}
		      >
		      {getFieldDecorator(FormKey, {
		      	initialValue: initialValue,
		      	valuePropName: 'checked',
		        rules: [{required: Required, message: `${ShowTitle} 是必填项`},],
		      })(<Switch disabled = {Disabled}/>)}
		    </FormItem>
    )
}

export default BoolFormItem
