import { Input, Form, Select  } from 'antd';

const FormItem = Form.Item
const Option = Select.Option;

const ContentSourceFormItem = ({propItem, getFieldDecorator,formItemLayout}) => {


	const {ShowTitle, FormKey, Value, Setting, Description} = propItem
	const {Required, SelectorOptions, Disabled} = Setting
	const initialValue = Value?Value:[]

	const showOptions = SelectorOptions.map((item)=>{
		return(
			<Option key = {item.Id} value = {item.Id}>{item.Value}</Option>
			)
	})

    return (
			<FormItem
			{...formItemLayout}
		      label={ShowTitle}
		      extra = {Description}
		      key = {FormKey}
		      >
		      {getFieldDecorator(FormKey, {
		      	initialValue: initialValue,
		        rules: [{required: Required, message: `${ShowTitle} 是必填项`},],
		      })(
		      <Select disabled = {Disabled}
		      	mode="multiple"
		      >
		      	{showOptions}
		      </Select>
		      )}
		    </FormItem>
    )
}

export default ContentSourceFormItem
