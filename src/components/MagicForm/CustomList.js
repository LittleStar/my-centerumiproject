import React from 'react';
import { Input, Form, Collapse, Button } from 'antd';
import DocumentTypeItem from './DocumentTypeItem'

const Panel = Collapse.Panel;

class ListItem extends React.Component {

  constructor(props) {
    super(props);

    const value = props.value || [];
    this.state = {
      value: value,
    };
  }

  componentWillReceiveProps(nextProps) {
    // Should be a controlled component.
    if ('value' in nextProps) {
      const value = nextProps.value;
      this.setState(value);
    }
  }

  updateValue = (data)=>{
    const onChange = this.props.onChange
    this.setState({value:data});
    onChange(data)
  }

  handleAdd = ()=>{
    let addData = {}
    const properties = this.props.element.Properties
    for(let property in properties){
      Object.assign(addData,{[property.FormKey]:null})
    }
    let tempValue = this.props.value
    tempValue.push(addData)
    this.updateValue(tempValue)
  }

  handleDelete = (index) => {
    let tempValue = this.props.value
    tempValue.splice(index,1)
    this.updateValue(tempValue)
  }


  handleUpdate = (newValue, index)=>{
    let tempValue = this.props.value
    tempValue.splice(index,1,newValue)
    this.updateValue(tempValue)
  }


  handleCancel = ()=>{
    const data = this.props.value
    //不知道该干啥，重新刷新一下好了
    this.updateValue(data)
  }


  render(){

  	const { element, formKey} = this.props
    const { value } = this.state

    const elementFormKey = element.FormKey

    //将一个valueItem放入documentType中
    const insertValue=(element,valueItem)=>{
      //copy一份
      let documentType = element
      //获取properties
      let properties = documentType.Properties

      for(let propertyItem of properties){
        if(valueItem.hasOwnProperty(propertyItem.FormKey)){
          propertyItem.Value = valueItem[propertyItem.FormKey]
        }
      }
      documentType.Properties = properties
      return documentType
    }

    const showDynamic = value.map((valueItem,index)=>{
      const documentTypeItem = insertValue(element,valueItem)

      const documentTypeProps = {
        content:documentTypeItem,
        onSubmit:(newValue)=>{
          this.handleUpdate(newValue,index)
        },
        onCancel:this.handleCancel
      }

      return(
        <Collapse key={formKey+index}>
          <Panel header={elementFormKey}>
            <Button onClick = {()=>this.handleDelete(index)}>Delete</Button>
            <DocumentTypeItem {...documentTypeProps}/>
          </Panel>
        </Collapse>
        )
    })

  	return(
      <Collapse>
        <Panel header={formKey} key = {formKey}>
          {showDynamic}
          <Button onClick={this.handleAdd}>Add</Button>
        </Panel>
      </Collapse>
  		)
  }

}

export default ListItem;