import { DatePicker, Form } from 'antd';
import moment from 'moment'

const FormItem = Form.Item
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

const dateFormat = 'MM/DD/YYYY';
const monthFormat = 'MM/YYYY';
const timeFormat = 'MM-DD-YYYY, HH:mm:ss'

const DateFormItem = ({propItem, getFieldDecorator,formItemLayout }) => {

	const {ShowTitle, FormKey, Value, Setting, Description} = propItem
	const {Required, DateType, Disabled} = Setting

	const initialValue = Value?moment(Value):null


	const ShowComponent = (Type)=>{
		switch(Type){
			case 'time':
				return <DatePicker showTime format = {timeFormat} disabled = {Disabled}/>
			case 'date':
				return <DatePicker format = {dateFormat} disabled = {Disabled}/>
			case 'month':
				return <MonthPicker format = {monthFormat} disabled = {Disabled}/>
			default:
				return <DatePicker showTime format = {timeFormat} disabled = {Disabled}/>
		}
	}


    return (
			<FormItem
			{...formItemLayout}
		      label={ShowTitle}
		      extra = {Description}
          key = {FormKey}
		      >
		      {getFieldDecorator(FormKey, {
		      	initialValue: initialValue,
		        rules: [{required: Required, message: `${ShowTitle} 是必填项`},],
		      })(ShowComponent(DateType))}
		    </FormItem>
    )
}

export default DateFormItem
