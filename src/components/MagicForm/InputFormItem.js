import { Input, Form } from 'antd';
const FormItem = Form.Item

const InputFormItem = ({ propItem, getFieldDecorator, formItemLayout }) => {

	const { ShowTitle, FormKey, Value, Setting, Description } = propItem
	const { Required, Disabled } = Setting

	const initialValue = Value ? Value : null

	return (
		<FormItem
			{...formItemLayout}
			label={ShowTitle}
			extra={Description}
      key={FormKey}
		>
			{getFieldDecorator(FormKey, {
				initialValue: initialValue,
				rules: [{ required: Required,  message: `${ShowTitle} 是必填项`},],
			})(<Input disabled = {Disabled}/>)}
		</FormItem>
	)
}

export default InputFormItem
