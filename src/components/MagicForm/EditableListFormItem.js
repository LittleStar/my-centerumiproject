import { Input, Form, Collapse, Button } from 'antd';
import CustomList from './CustomList'

const Panel = Collapse.Panel;
const FormItem = Form.Item
const EditableListFormItem = ({propItem, getFieldDecorator,formItemLayout})=>{

  const { ShowTitle, FormKey, Value, Setting, Description } = propItem
  const { Required, ListElement } = Setting

  const initialValue = Value?Value:[]

  const listProps = {
    element:ListElement,
    formKey:FormKey
  }

  return (
      <FormItem
      {...formItemLayout}
          label={ShowTitle}
          extra = {Description}
          key = {FormKey}
          >
          {getFieldDecorator(FormKey, {
            initialValue: initialValue,
            rules: [{required: Required, message: `${ShowTitle} 是必填项`},],
          })(<CustomList {...listProps}/>)}
        </FormItem>
    )
}

export default EditableListFormItem;
