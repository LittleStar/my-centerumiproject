import { Form, Upload, Button, Icon, message } from 'antd';
import { config,getAuthHeader } from '../../utils'

const { baseURL } = config
const UploadFile = '/api/File/ContractUpload'

const FormItem = Form.Item

const UploadFormItem = ({propItem, getFieldDecorator,formItemLayout}) => {

	const {ShowTitle, FormKey, Value, Setting, Description} = propItem
	const {Required, UploadSetting} = Setting

	const initialValue = Value?Value:[]

	const valueToShowPic = (pics)=>{
		return pics.map((item)=>{
			return{
				...item,
				name:item.fileName,
				status:'done',
				uid:item.id,
			}
		})
	}

	const uploadProps = {
		...UploadSetting,
    action: baseURL+UploadFile,
    accept: "image/*",
		headers: {
			...getAuthHeader()
		},
		listType:'picture'
	}

	const normFile = (e) => {
	    if (Array.isArray(e)) {
	      return e;
	    }
	    return e && e.fileList;
	  }


    return (
			<FormItem
			{...formItemLayout}
		      label={ShowTitle}
		      extra = {Description}
		      key = {FormKey}
		    >
		      {getFieldDecorator(FormKey, {
		      	initialValue: valueToShowPic(initialValue),
		      	valuePropName: 'fileList',
            	getValueFromEvent: normFile,
		        rules: [{required: Required, message: `${ShowTitle} 是必填项`},],
		      })(
				<Upload {...uploadProps}>
	              <Button>
	                <Icon type="upload" /> 上传
	              </Button>
	            </Upload>
		      )}
		    </FormItem>
    )
}

export default UploadFormItem
