import { DatePicker, Form } from 'antd';
import moment from 'moment'

const FormItem = Form.Item
const RangePicker = DatePicker.RangePicker;


const RangeDateFormItem = ({propItem, getFieldDecorator,formItemLayout}) => {

	const {ShowTitle, FormKey, Value, Setting, Description} = propItem
	const {Required, Disabled} = Setting
	const initialValue = Value?Value:[]

    return (
			<FormItem
			{...formItemLayout}
		      label={ShowTitle}
		      extra = {Description}
		      key = {FormKey}
		      >
		      {getFieldDecorator(FormKey, {
		      	initialValue: initialValue,
		        rules: [{required: Required, message: `${ShowTitle} 是必填项`},],
		      })(<RangePicker showTime format="MM-DD-YYYY HH:mm:ss" disabled = {Disabled}/>)}
		    </FormItem>
    )
}

export default RangeDateFormItem
