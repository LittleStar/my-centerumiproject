import DateFormItem from './DateFormItem'
import RangeDateFormItem from './RangeDateFormItem'
import InputFormItem from './InputFormItem'
import RichEditorFormItem from './RichEditorFormItem'
import InputNumberFormItem from './InputNumberFormItem'
import ContentSourceFormItem from './ContentSourceFormItem'
import BoolFormItem from './BoolFormItem'
import DropDownFormItem from './DropDownFormItem'
import EditableListFormItem from './EditableListFormItem'
import SortExistedListFormItem from './SortExistedListFormItem'
import UploadFormItem from './UploadFormItem'

function propertyToFormItem(propItem,getFieldDecorator,formItemLayout){

	const {EditorType,FormKey} = propItem

	const data = {
		key:FormKey,
		propItem,
		getFieldDecorator,
    formItemLayout,
	}

	switch(EditorType){
		case 'DatePicker':
			return(<DateFormItem {...data}/>)
		case 'RangePicker':
			return (<RangeDateFormItem {...data}/>)
		case 'Input':
			return (<InputFormItem {...data}/>)
		case 'RichEditor':
			return (<RichEditorFormItem {...data}/>)
		case 'NumberInput':
			return (<InputNumberFormItem {...data}/>)
		case 'ContentSelector':
			return (<ContentSourceFormItem {...data}/>)
		case 'Bool':
			return (<BoolFormItem {...data}/>)
		case 'Dropdown':
			return (<DropDownFormItem {...data}/>)
		case 'EditableList':
			return (<EditableListFormItem {...data}/>)
		case 'SortList':
			return (<SortExistedListFormItem {...data}/>)
		case 'Upload':
			return (<UploadFormItem {...data}/>)
	}
}

export {propertyToFormItem}
