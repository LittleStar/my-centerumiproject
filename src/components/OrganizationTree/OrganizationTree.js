import { Tree, Card, Button, Divider, Icon, Tooltip } from 'antd';

const TreeNode = Tree.TreeNode;


const OrganizationTree = ({ organizationList, onGoToLocationDetail, onGoToRegionDetail, onAddLocationToRegion, onDeleteLocationOrRegion, onMoveLocation }) => {
  const listToTree = (list) => {
    let tree = [],
      mappedArr = {},
      arrElem,
      mappedElem;

    // First map the nodes of the array to an object -> create a hash table.
    for (let i = 0, len = list.length; i < len; i++) {
      arrElem = list[i];
      mappedArr[arrElem.id] = arrElem;
      mappedArr[arrElem.id]['childrens'] = [];
    }

    for (let id in mappedArr) {
      if (mappedArr.hasOwnProperty(id)) {
        mappedElem = mappedArr[id];
        // If the element is not at the root level, add it to its parent array of children.
        if (mappedElem.parentId&&mappedArr[mappedElem.parentId]!==undefined) {
          mappedArr[mappedElem['parentId']]['childrens'].push(mappedElem);
        }
        // If the element is at the root level, add it to first level elements array.
        else {
          tree.push(mappedElem);
        }
      }
    }
    return tree;
  }

  const clickToDetail = (id, organizationUnitType) => {
    if (organizationUnitType === 1) {
      onGoToRegionDetail(id)
    }
    if (organizationUnitType === 0) {
      onGoToLocationDetail(id)
    }
  }
  const clickAdd = (id) => {
    onAddLocationToRegion(id)
  }
  const clickDelete = (id) => {
    onDeleteLocationOrRegion(id)
  }
  const clickMove = (id) => {
    onMoveLocation(id)
  }

  const tips = {
    detail: '查看详情',
    move: '移动',
    add: '添加',
    delete: '删除'
  }

  const itemToCard = (item) => {
    const { displayName, childrens, id, organizationUnitType } = item
    //结点为区域，允许添加新的location/Region
    if (organizationUnitType === 1) {
      return (
        <span>
          <span>{displayName}</span>
          <Divider type="vertical" />
          <Tooltip placement="topLeft" title={tips.add}>
            <Button size={'small'} onClick={() => clickAdd(id)}><Icon type="plus" /></Button>
          </Tooltip>
          <Divider type="vertical" />
          <Tooltip placement="topLeft" title={tips.detail}>
            <Button size={'small'} onClick={() => clickToDetail(id, organizationUnitType)}><Icon type="file-search" /></Button>
          </Tooltip>
          <Tooltip placement="topLeft" title={tips.move}>
            <Button size={'small'} onClick={() => clickMove(id, organizationUnitType)}><Icon type="swap" /></Button>
          </Tooltip>
          <Tooltip placement="topLeft" title={tips.delete}>
            <Button size={'small'} onClick={() => clickDelete(id, organizationUnitType)}><Icon type="delete" /></Button>
          </Tooltip>
        </span>
      )
    }
    //结点为location，只允许进入详情页
    else if (organizationUnitType === 0) {
      return (
        <span>
          <span>{displayName}</span>
          <Divider type="vertical" />
          <Tooltip placement="topLeft" title={tips.detail}>
            <Button size={'small'} onClick={() => clickToDetail(id, organizationUnitType)}><Icon type="file-search" /></Button>
          </Tooltip>
          <Tooltip placement="topLeft" title={tips.move}>
            <Button size={'small'} onClick={() => clickMove(id, organizationUnitType)}><Icon type="swap" /></Button>
          </Tooltip>
          <Tooltip placement="topLeft" title={tips.delete}>
            <Button size={'small'} onClick={() => clickDelete(id, organizationUnitType)}><Icon type="delete" /></Button>
          </Tooltip>
        </span>
      )
    }
  }

  const typeToIcon = (type) => {
    if (type === 0)
      return <Icon type="home" theme="twoTone" />
    else if (type === 1) {
      return <Icon type="cluster" />
    }
  }

  const showTree = listToTree(organizationList)

  const treeToNode = (tree) => {
    return tree.map((item) => {
      const { displayName, childrens, id, organizationUnitType } = item
      if (childrens.length > 0) {
        return (
          <TreeNode selectable={false} icon={typeToIcon(organizationUnitType)} title={itemToCard(item)} key={id}>
            {treeToNode(childrens)}
          </TreeNode>
        )
      }
      else {
        return (
          <TreeNode selectable={false} icon={typeToIcon(organizationUnitType)} title={itemToCard(item)} key={id} />
        )

      }
    })
  }



  return (
    <Tree showIcon>
      {
        treeToNode(showTree)
      }
    </Tree>
  )
}

export default OrganizationTree
