import React from 'react'
import { Card, Row, Col, Button, Spin } from 'antd'

const BasicInfoCard = ({ titleInfo, infoList, btnInfo, clickBtn, loading }) => {
  const title = titleInfo || ''
  const list = infoList || []
  const btn = btnInfo || false

  const arr = list.map((item, index) => {
    return (
      <Col span={6} style={{ fontSize: 15 }} key={item.name}>
        <span>{item.name}: </span>
        <span>{item.itemVal}</span>
      </Col>
    )
  })
  return (
    <Card style={{ marginBottom: 10 }}>
      <h2>{title}</h2>
      <Spin spinning={loading}>
      <Row type="flex" justify="space-around" style={{textAlign: 'center'}}>
        {arr}
        <Col span={8}>
          {btn !== false ? <Button onClick={clickBtn}>{btn}</Button> : null}
        </Col>
      </Row>
      </Spin>
    </Card>
  )
}

export default BasicInfoCard
