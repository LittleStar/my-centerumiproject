import React, { PureComponent } from 'react';
import { Button, Divider } from 'antd';
import styles from './navButton.css'
import { Link } from 'dva/router';


const NavButton = ({ location, list }) => {

  const { pathname, search } = location

  const btnList = list.map((item) => {

    const { path, tabName } = item
    const locationPath = path+search

    //搜索当前location是否有list里面的path
    const type = pathname.search(path) === 0 ? 'primary' : ''
    return (
      <Link className={styles.buttonLinkStyle} to={locationPath} key={path}>
        <Button type = {type} size = 'large'>{tabName}</Button>
      </Link>
    )
  })

  return (
    <div>
      {btnList}
      <Divider />
    </div>
  )
}

export default NavButton
