import React, { PureComponent } from 'react';
import { Tabs, Divider } from 'antd';
import styles from './navTab.css'
import { Link } from 'dva/router';

const TabPane = Tabs.TabPane

const NavTab = ({ location, list, type }) => {

  const { pathname, search } = location
  const tabsType = type || ''

  const tabList = list.map((item) => {

    const { path } = item
    const locationPath = path + search
    const getTabItemPath = (item) => {

      return (
        <Link className={styles.tabLinkStyle}
          to={locationPath}
        >{item.tabName}
        </Link>
      )
    }
    const tabPanePath = pathname.search(path) === 0 ? pathname : path

    return (
      <TabPane tab={getTabItemPath(item)} key={tabPanePath}></TabPane>
    )
  })

  return (
    <div>
      <Tabs
        activeKey={pathname}
        type={tabsType}
        className={styles.tabWrap}
      >
        {tabList}
      </Tabs>
    </div>
  )

}

export default NavTab
