import { Table, Button, Icon, Modal, Divider, Spin, Pagination } from 'antd'
import moment from 'moment';

const confirm = Modal.confirm

//需要显示的具体内容
//listData里面的Key需要对应到listSupport里面的key
//listData:[{
//	key1:qwerweqr,
//	key2:wewewe,
//}]

//控制每一列以何种方式显示，是否可以sorter以及filter
//listSupport:{
// 	key1:{showText,showType,filter,sorter,addtional,visible},
// 	key2:{},
// 	...
// }
//showType: Time / Text(Number/String) / Status / Bool

//sorter: {
//	hasSorter:true/false
//}
//filter: {
//	hasFilter:true/false
//  filterOptions: [
//    { text: 'Male', value: 'male' },
//    { text: 'Female', value: 'female' },
//  ],
//}

//addtional: Time->format(string) 表示显示时间的精度
//			 Status->statusArray(array) 表示每一个数值的显示内容
//			 Bool-> boolArray(array) array[0] ->false array[1]->true
//

//list里面的页码信息
//pagination:{current,pageSize,total}

//onTableChange:(pagination, filters, sorter)=>{}

//Actions:[{
//	showText:'erer',
//	onClick:(record)=>{}
//}]
const MagicTable = ({ listData, listSupport, pagination, onTableChange, Actions, loading, rowSelection, filters={}, sorter={} }) => {

  const showKeys = Object.keys(listSupport)

  const { total, pageSize, current } = pagination

  const renderActions = (text, record, index) => {

    return (
      <span>
        {
          Actions.map((item, index) => {
            const onClickAction = item.onClick
            if (index === Actions.length - 1)
              return (
                <a key={item.showText} onClick={() => onClickAction(record)}>{item.showText}</a>
              )
            else
              return (
                <React.Fragment key={item.showText}>
                  <a onClick={() => onClickAction(record)}>{item.showText}</a>
                  <Divider type="vertical" />
                </React.Fragment>
              )
          })
        }
      </span>
    )
  }

  const renderColumns = (text, showItem) => {

    const {showType,addtional} = showItem

      switch (showType) {
        case 'Time':
          const defaultFormat = 'YYYY-MM-DD'
          const timeFormat = addtional.format || defaultFormat
          const showText = text?moment(text).local().format(timeFormat) : ''
          return <span>{showText}</span>
        case 'Status':
          const statusArray = addtional.statusArray || []
          const showStatus = (Number.isInteger(text)&&(text < statusArray.length))?statusArray[text]:''
          return <span>{showStatus}</span>
        case 'Bool':
          const boolArray = addtional.boolArray || ['No', 'Yes']
          const showBool = text?boolArray[1] : boolArray[0]
          return <span>{showBool}</span>
        // add Emma
        case 'Photo':
          return text.map((item, index) => {
            return <img style={{ width: 50, height: 50, marginRight: 5 }} src={item} alt={index} key={index} />
          })
        default:
          return <span>{text}</span>
      }

  }

  const columns = showKeys.map((item, index) => {
    const showItem = listSupport[item]
    const filterItem = filters[item]||null
    const sortOrder = sorter.columnKey === item && sorter.order
    const canSort = showItem.sorter === undefined ? false : showItem.sorter.hasSorter
    const { hasFilter, filterOptions } = showItem.filter === undefined ? {} : showItem.filter
    const showFilter = hasFilter === undefined ? [] : filterOptions
    return {
      key: item,
      title: showItem.showText,
      dataIndex: item,
      sorter: canSort,
      filters: showFilter,
      filteredValue:filterItem,
      sortOrder: sortOrder,
      render: text => renderColumns(text, showItem)
    }
  })

  //判断传入的actions是[]/undefined的情况
  if (Array.isArray(Actions)&&Actions.length>0) {
    columns.push({
      key: 'Actions',
      title: '操作',
      render: (text, record, index) => renderActions(text, record, index)
    })
  }

  const showListData = listData
  // add Emma
  const showThumbnail = (thumbnailItem) => {
    return thumbnailItem.map((item, index) => {
      return <img style={{ width: 50, height: 50, marginRight: 5 }} src={item} alt={index} key={index} />
    })
  }

  const showData = showListData.map((item, index) => {
    const { id } = item
    return { key: id || index, ...item }
  })

  const showExtra = showListData.length > 0 && showListData[0].hasOwnProperty('extra')

  const expandedData = (record) => {
    return (
      <p>{record.extra}</p>
    )
  }

  const pageChange = (page, pageSize) => {
    const pagination = { current: page, pageSize }
    onTableChange(pagination)
  }

  return (
    <div>
      <Table
        bordered
        loading={loading}
        columns={columns}
        dataSource={showData}
        pagination={false}
        onChange={onTableChange}
        rowSelection={rowSelection}
        expandedRowRender={showExtra ? expandedData : null}
        expandRowByClick={true}
        locale={{emptyText:'暂无数据'}}
      />
      <Pagination
        style={{margin: 10}}
        showQuickJumper
        showTotal={total => `总计 ${total} 条`}
        onChange={pageChange}
        pageSize={pageSize}
        current={current}
        total={total} />
    </div>
  )
}

export default MagicTable
