import {Button, Modal, Form, Input,Radio,DatePicker,Row,Col,Cascader  } from 'antd'
const FormItem = Form.Item;

const ILSModal = ({title, visible, onCancel, onOk, courseOptions, userValue, loading, selectedILS, form: { getFieldDecorator, validateFieldsAndScroll, resetFields }}) =>{

  const showILSOptions = (courseOptions)=>{
		const result = courseOptions.map((item)=>{
			const {displayName,children,id} = item
			return{
				value:id,
				label:displayName,
				children:showILSOptions(children)
			}
		})
		return result
	}

  const options = showILSOptions(courseOptions)

	const formItemLayout = {
		labelCol: {
		  xs: { span: 24 },
		  sm: { span: 6 },
		},
		wrapperCol: {
		  xs: { span: 24 },
		  sm: { span: 16 },
		},
	};

	const onClickSubmit = () => {
		validateFieldsAndScroll((err, value) => {
		  if (err)
		    return
		  onOk(value)
		  // resetFields()
		})
	}

	return(

		<Modal
		  	visible={visible}
		  	title = {title}
		  	width = {800}
			footer = {null}
	        onCancel = {onCancel}
	        destroyOnClose = {true}
		>
			<Form>
				<FormItem
		          {...formItemLayout}
		          label="ILS课程"
		        >
		          {getFieldDecorator('ils', {
		            initialValue: [],
		            rules: [{ required: true, message: '请选择ILS课程进度' }],
		          })(
		            <Cascader placeholder={selectedILS?selectedILS:'请选择'} options={options} />
		          )}
		        </FormItem>
		        <Button size="large" onClick={onCancel} style={{ marginRight: 10 }}>取消</Button>
            	<Button size="large" type="primary" onClick={onClickSubmit}>确认</Button>
	        </Form>
		</Modal>
		)
}

export default Form.create()(ILSModal)
