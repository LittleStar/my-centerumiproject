import { Modal, Form, Input,Radio,DatePicker,Row,Col } from 'antd'
import MagicForm from '../MagicForm/DocumentTypeItem'

const MagicFormModal = ({title, visible, onCancel, onOk, content, loading}) =>{

	const magicFormProps = {
    loading,
		content,
		onSubmit:onOk,
		onCancel,
	}

	return(

		<Modal
		  	visible={visible}
		  	title = {title}
		  	width = {800}
				footer = {null}
        onCancel = {onCancel}
        destroyOnClose = {true}
		>
			<MagicForm {...magicFormProps}/>
		</Modal>
		)
}

export default MagicFormModal
