import { routerRedux } from 'dva/router'
import { PostAuthenticate } from '../services/centerService/User'
import { logOut,logIn } from '../utils/auth.js'

export default {

	namespace: 'login',

	state: {
    loginStatus: null,
	},

	reducers: {

	},

	effects: {

		*login({payload}, {call,put,select}) {
        const res = yield call (PostAuthenticate,payload)

			if(res.success){
				const {result} = res
        		const {accessToken,userId} = result
				logIn(accessToken)
				yield put({type:'user/fetch'});
			}

		}

	},

	subscriptions: {
	    setup ({ dispatch, history }) {

	    },

	},
}
