import { GetCurrentUserInfo,GetCurrentUserPermissions,GetMenuPermission,GetAllPermissions,GetCurrentUserOrganizationUnits,ChangeOrganizationUnit} from 'services/centerService/User'
import { GetUserNotifications,SetAllNotificationsAsRead,SetNotificationAsRead } from 'services/centerService/Notification'
import { routerRedux } from 'dva/router'
import { stringify } from 'qs'
import { logOut,logIn } from '../utils/auth.js'
import { filterMenu,CheckUrlPermission } from 'utils/utils';

const menuList = [
  {
    name: '首页',
    icon: 'home',
    path: '/system/manage/dashboard',
    permissionKey: 'Location.Dashboard',
  },
  {
    name: '报表中心',
    icon: 'line-chart',
    path: '/system/manage/report',
    permissionKey: 'Report',
    children: [
      {
        name: '客户数据报表',
        path: '/system/manage/report/sa',
        permissionKey: 'Report.Sales',
        children: [
          {
            name: '客户来源',
            path: '/system/manage/report/sa/us',
          },
          {
            name: '课程顾问统计',
            path: '/system/manage/report/sa/sales',
          },
        ],
      },
      {
        name: '教务报表',
        path: '/system/manage/report/te',
        permissionKey: 'Report.Teaching',
        children: [
          {
            name: '月度耗课统计',
            path: '/system/manage/report/te/mc',
          },
          {
            name: '教师排课课时统计',
            path: '/system/manage/report/te/tc',
          },
          {
            name: '会员耗课统计',
            path: '/system/manage/report/te/ap',
          },
        ],
      },
      {
        name: '营收报表',
        path: '/system/manage/report/pa',
        permissionKey: 'Report.Revenue',
        children: [
          {
            name: '月度营收',
            path: '/system/manage/report/pa/mp',
          },
        ],
      },
    ],
  },
  {
    name: '营销中心',
    icon: 'bank',
    path: '/system/manage/sales',
    permissionKey: 'Location.Sales',
    children: [
      {
        permissionKey: 'Location.Sales.PotentialUser',
        name: '储备用户',
        path: '/system/manage/sales/user',
      },
    ],
  },
  {
    name: '会员中心',
    icon: 'user',
    path: '/system/manage/user',
    permissionKey: 'Location.Vip',
    children: [
      {
        name: '会员列表',
        path: '/system/manage/user/vip',
        permissionKey: 'Location.Vip.List',
      },
      {
        name: '合同管理',
        path: '/system/manage/user/contract',
        permissionKey: 'Location.Vip.Contract',
      },
    ],
  },
  {
    name: '教务中心',
    icon: 'schedule',
    path: '/system/manage/edu',
    permissionKey: 'Location.TeachingCenter',
    children: [
      {
        name: '课程表',
        path: '/system/manage/edu/class',
        permissionKey: 'Location.TeachingCenter.Schedule',
      },
      {
        name: '课程计划',
        path: '/system/manage/edu/plan',
        permissionKey: 'Location.TeachingCenter.Plan',
      },
    ],
  },
  {
    name: '测评中心',
    icon: 'compass',
    path: '/system/manage/test',
    permissionKey: 'Location.TestingCenter',
    children: [
      {
        name: 'VRAT测评',
        path: '/system/manage/test/vrat',
      },
    ],
  },
  {
    name: '审核中心',
    icon: 'key',
    path: '/system/manage/verify',
    permissionKey: 'Location.VerifyCenter',
    children: [
      {
        name: '合同审核',
        path: '/system/manage/verify/contract',
        permissionKey: 'Location.VerifyCenter.Contract',
      },
      {
        name: '退费审核',
        path: '/system/manage/verify/refund',
        permissionKey: 'Location.VerifyCenter.Refund',
      },
      {
        name: '赠课审核',
        path: '/system/manage/verify/gift',
        permissionKey: 'Location.VerifyCenter.Gift',
      },
    ],
  },
  {
    name: '配置中心',
    icon: 'setting',
    path: '/system/manage/setting',
    permissionKey: 'Location.SettingCenter',
    children: [
      {
        name: '人事管理',
        path: '/system/manage/setting/user',
        permissionKey: 'Location.SettingCenter.Hr',
      },
      {
        name: '店铺管理',
        path: '/system/manage/setting/account',
        permissionKey: 'Location.SettingCenter.Location',
      },
      {
        name: '课程管理',
        path: '/system/manage/setting/course',
        permissionKey: 'Location.SettingCenter.Course',
      },
      {
        name: '教室管理',
        path: '/system/manage/setting/room',
        permissionKey: 'Location.SettingCenter.Classroom',
      },
      {
        name: '其他',
        path: '/system/manage/setting/other',
        permissionKey: 'Location.SettingCenter.Others',
      },
    ],
  },
  {
    name: '管理中心',
    icon: 'global',
    path: '/system/manage/manage',
    permissionKey: 'Management.Tenant',
    children: [
      {
        name: '架构管理',
        path: '/system/manage/manage/organization',
      },
      {
        name: '店铺管理',
        path: '/system/manage/manage/center',
        permissionKey: 'Management.Location.Tenant',
      },
      {
        name: '区域管理',
        path: '/system/manage/manage/region',
      },
      {
        name: '课程管理',
        path: '/system/manage/manage/course',
        permissionKey: 'CourseManagement.Tenant',
      },
    ],
  },
];

export default {
  namespace: 'user',

  state: {
    currentUser:{},
    permission:[],
    locations:[],
    notifications:[],
    showMenu:[]
  },

  effects: {

    *fetch({payload}, { call, put, take,select }) {
      const userInfo = yield call(GetCurrentUserInfo)

      if(userInfo.success){
        const currentUser = userInfo.result
        yield put({type:'updateState',payload:{currentUser}})
        //yield put({type:'queryNotification'})

        const permissionRes = yield call(GetCurrentUserPermissions)
        const menuRes = yield call(GetMenuPermission)
        const locationRes = yield call(GetCurrentUserOrganizationUnits)
        if(permissionRes.success&&menuRes.success&&locationRes.success){
          const {grantedPermissionNames} = permissionRes.result
          const menu = menuRes.result
          const {items:locations} = locationRes.result
          const showMenu = menu ? filterMenu(menuList, menu.items) : []
          const defaultLink = showMenu[0].path;
          yield put({type:'updateState',payload:{permission:grantedPermissionNames,showMenu,locations}})
          const {fromUrl} = payload===undefined?{}:payload
          //判断是否允许访问fromUrl，如果允许访问则跳转到fromUrl
          const bolValue = CheckUrlPermission(fromUrl,grantedPermissionNames)
          // urlPermission.js里面没有定义的权限字段，都要走else
          if(fromUrl!==undefined&&bolValue){
            // console.log(fromUrl)
            // yield put(routerRedux.push(fromUrl))
          }
          //判断能访问的第一个Url是哪里，跳转过去
          else{
            console.log(2222)
            // const toUrl = DefaultRedirectUrl(grantedPermissionNames)
            // yield put(routerRedux.push(toUrl))
            yield put(routerRedux.push(defaultLink))
          }
        }
      }
    },

    // *queryPermission(_,{call, put,take,select}){
    //   const permissionRes = yield call(GetCurrentUserPermissions)
    //   const menuRes = yield call(GetMenuPermission)
    //   const locationRes = yield call(GetCurrentUserOrganizationUnits)
    //   if(permissionRes.success&&menuRes.success&&locationRes.success){
    //     const {grantedPermissionNames} = permissionRes.result
    //     const menu = menuRes.result
    //     const {items:locations} = locationRes.result
    //     yield put({type:'updateState',payload:{permission:grantedPermissionNames,menu,locations}})
    //   }
    // },

    *queryNotification(_,{call, put,take,select}){
      const notiRes = yield call(GetUserNotifications)
    },

    *changeLocation({payload:id}, { call, put, take, select }) {
      const changeRes = yield call(ChangeOrganizationUnit,id)
      if(changeRes.success){
        yield put({type:'updateState',payload:{menu:null}})
        yield put({ type: 'report/updateState', payload: { firstUnit:'', unitId:'' } })
        const {accessToken} = changeRes.result
        logIn(accessToken)
        yield put(routerRedux.push('/'))
      }
    },

    *logout(_, { call, put,take,select }){
      logOut();
      yield put({type:'updateState',payload:{menu:null}})
      yield put(routerRedux.push('/login'))
    },

  },

  reducers: {

    saveCurrentUser(state, payload) {
      return {
        ...state,
        currentUser: payload,
      };
    },

    updateState(state,{payload}){
        return{
          ...state,
          ...payload,
        }
      },

  },

  subscriptions: {

      setupHistory ({ dispatch, history }) {
        history.listen((location) => {
          if(location.pathname==='/'||location.pathname==='/system/manage'){
            dispatch({type: 'fetch'})
          }
        })
      },
  },

};
