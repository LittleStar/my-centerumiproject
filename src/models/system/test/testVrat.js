import { routerRedux } from 'dva/router'
import { pageConfig } from '../../../utils'

const { detail } = pageConfig
const { vratDetail,vratDetailBasicInfo} = detail

export default {
  namespace: 'testVrat',
  state: {
    list: [
      {
        "testId": "00000",
        "createdDate": "2018-09-09",
        "createdTime": "14:00",
        "tester": 'Maple',
        "status": "0",
        "report": "0",
      }, {
        "testId": "11111",
        "createdDate": "2018-09-09",
        "createdTime": "14:00",
        "tester": 'Maple',
        "status": "1",
        "report": "1",
      }
    ],
    listSupport: {
      createdDate: {
        showText: '创建日期',
        showType: 'Text',
      },
      createdTime: {
        showText: '创建时间',
        showType: 'Text',
      },
      tester: {
        showText: '测试人',
        showType: 'Text',
      },
      status: {
        showText: '测试状态',
        showType: 'Status',
        addtional: {
          statusArray: ['未测试', '测试中', '测试结束', '已出报告', '失败']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '未测试', value: 0 },
            { text: '测试中', value: 1 },
            { text: '测试结束', value: 2 },
            { text: '已出报告', value: 3 },
            { text: '失败', value: 4 },
          ],
        }
      },
      report: {
        showText: '报告',
        showType: 'Status',
        addtional: {
          statusArray: ['未生成', '在线查看']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '未生成', value: 0 },
            { text: '在线查看', value: 1 },
          ],
        }
      },
    },
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *goToVratDetail({ payload: id }, { take, put, call, select }) {
      yield put(routerRedux.push({
        pathname: vratDetailBasicInfo,
        search: '?testId=' + id
      }))
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
    },
  }
}
