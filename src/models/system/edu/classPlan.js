import router from 'umi/router'
import { pageConfig, matchDynamicForm } from 'utils'
import queryString from 'query-string';
import { message } from 'antd';
import { GetCoursePlanForEdit, GetAllActiveCoursePeriod, CreateOrUpdateCoursePlan, GetCoursePlan, FindCoursePlan, DeleteCoursePlan, ManuallySchedule } from '../../../services/centerService/Teaching'
import { FindAllCourse } from 'services/centerService/Manage'

const { edu, detail } = pageConfig
const { plan } = edu
const { classPlanDetail, classPlanSubjectList } = detail

const weekDayMenu = {
  "0": '星期日',
  "1": '星期一',
  "2": '星期二',
  "3": '星期三',
  "4": '星期四',
  "5": '星期五',
  "6": '星期六',
}

const NumberToWeekDayKey = {
  "0": 'Sunday',
  "1": 'Monday',
  "2": 'Tuesday',
  "3": 'Wednesday',
  "4": 'Thursday',
  "5": 'Friday',
  "6": 'Saturday',
}

const filterableItem = {
  isActive: "isActive",
}

const initClassPlanForm = (insertData) => {

  const newPlanForm = {
    Properties: [{
      "EditorType": "Dropdown",
      "Value": null,
      "Setting": {
        "DropdownOptions": [],
        "Required": false
      },
      "ShowTitle": "课程",
      "FormKey": "courseId",
      "Description": null,
    }, {
      "EditorType": "Dropdown",
      "Value": null,
      "Setting": {
        "DropdownOptions": [],
        "Required": true
      },
      "ShowTitle": "上课时间",
      "FormKey": "startCoursePeriodId",
      "Description": null,
    }, {
      "EditorType": "Dropdown",
      "Value": null,
      "Setting": {
        "DropdownOptions": [],
        "Required": true
      },
      "ShowTitle": "教室",
      "FormKey": "classroomId",
      "Description": null
    }, {
      "EditorType": "Dropdown",
      "Value": null,
      "Setting": {
        "DropdownOptions": [],
        "Required": true
      },
      "ShowTitle": "排课周期",
      "FormKey": "planType",
      "Description": null
    }, {
      "EditorType": "Dropdown",
      "Value": null,
      "Setting": {
        "DropdownOptions": [],
        "Required": false
      },
      "ShowTitle": "教师",
      "FormKey": "teacherUserId",
      "Description": null
    }, {
      "EditorType": "Bool",
      "Value": true,
      "Setting": {
        "Required": false
      },
      "ShowTitle": "是否激活该课程计划",
      "FormKey": "isActive",
      "Description": null
    }, {
      "EditorType": "Input",
      "ShowTitle": "描述",
      "FormKey": "description",
      "AdditionalData": null,
      "Value": null,
      "Setting": {
        "Required": false,
      },
      "Description": null
    }]
  }

  const { courseList, teacherList, classrooms, availablePlanType, periodList } = insertData

  const courseIndex = newPlanForm.Properties.findIndex((item) => {
    return item.FormKey === 'courseId'
  })
  const teacherIndex = newPlanForm.Properties.findIndex((item) => {
    return item.FormKey === 'teacherUserId'
  })
  const roomIndex = newPlanForm.Properties.findIndex((item) => {
    return item.FormKey === 'classroomId'
  })
  const planTypeIndex = newPlanForm.Properties.findIndex((item) => {
    return item.FormKey === 'planType'
  })
  const periodIndex = newPlanForm.Properties.findIndex((item) => {
    return item.FormKey === 'startCoursePeriodId'
  })
  if (courseIndex !== -1) {
    const courseOptions = courseList.map((item) => {
      return { "Value": `${item.title} ( ${item.subTitle} )`, "Id": item.id }
    })
    newPlanForm.Properties[courseIndex].Setting.DropdownOptions = courseOptions
  }
  if (teacherIndex !== -1) {
    const teacherOptions = teacherList.map((item) => {
      return { "Value": item.name, "Id": item.id.toString() }
    })
    newPlanForm.Properties[teacherIndex].Setting.DropdownOptions = teacherOptions
  }
  if (roomIndex !== -1) {
    const roomOptions = classrooms.map((item) => {
      return { "Value": item.name + '( ' + item.description + ' )', "Id": item.id }
    })
    newPlanForm.Properties[roomIndex].Setting.DropdownOptions = roomOptions
  }
  if (planTypeIndex !== -1) {
    const planTypeOptions = availablePlanType.map((item) => {
      return { "Value": item.value, "Id": item.id }
    })
    newPlanForm.Properties[planTypeIndex].Setting.DropdownOptions = planTypeOptions
  }
  if (periodIndex !== -1) {
    const periodOptions = periodList.map((item) => {
      return { "Value": item.startTime + '--' + item.endTime, "Id": item.id }
    })
    newPlanForm.Properties[periodIndex].Setting.DropdownOptions = periodOptions
  }

  return newPlanForm
}

const sortableItem = {
  "coursePeriod": 'CoursePeriod.SortOrder',
}

const sortOrderItem = {
  'ascend': '',
  'descend': 'DESC'
}

export default {

  namespace: 'classPlan',

  state: {
    planList: [],
    planListSupport: {
      coursePeriod: {
        showText: '上课时间（/天）',
        showType: 'Text',
        sorter: {
          hasSorter: true
        },
      },
      courseName: {
        showText: '课程',
        showType: 'Text',
        filter: {
          hasFilter: true,
          filterOptions: [],
        }
      },
      roomName: {
        showText: '教室',
        showType: 'Text',
        filter: {
          hasFilter: true,
          filterOptions: [],
        }
      },
      teacherName: {
        showText: '教师',
        showType: 'Text',
        filter: {
          hasFilter: true,
          filterOptions: [],
        }
      },
      isActive: {
        showText: '班级状态',
        showType: 'Bool',
        addtional: {
          boolArray: ['未激活', '激活']
        },
        // filter: {
        //   hasFilter: false,
        //   filterOptions: [
        //     { text: '未激活', value: false },
        //     { text: '激活', value: true },
        //   ],
        // }
      },
      subjectCount: {
        showText: '长期学生人数',
        showType: 'Text',
        filter: {
          hasFilter: true,
          filterOptions: [],
        }
      },

    },
    tabPlanListSupport: {
      planValueName: {
        showText: '星期',
        showType: 'Text',
        filter: {
          hasFilter: true,
          filterOptions: [],
        }
      },
      coursePeriod: {
        showText: '上课时间（/天）',
        showType: 'Text',
        sorter: {
          hasSorter: true
        },
      },
      courseName: {
        showText: '课程',
        showType: 'Text',
        filter: {
          hasFilter: true,
          filterOptions: [],
        }
      },
      roomName: {
        showText: '教室',
        showType: 'Text',
        filter: {
          hasFilter: true,
          filterOptions: [],
        }
      },
      teacherName: {
        showText: '教师',
        showType: 'Text',
        filter: {
          hasFilter: true,
          filterOptions: [],
        }
      },
      isActive: {
        showText: '班级状态',
        showType: 'Bool',
        addtional: {
          boolArray: ['未激活', '激活']
        },
        // filter: {
        //   hasFilter: false,
        //   filterOptions: [
        //     { text: '未激活', value: false },
        //     { text: '激活', value: true },
        //   ],
        // }
      },
      subjectCount: {
        showText: '长期学生人数',
        showType: 'Text',
        filter: {
          hasFilter: true,
          filterOptions: [],
        }
      },

    },
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    modalType: 'add',
    modalVisible: false,
    content: null,
    selectWeekday: 'Monday',
    dropdownSelect: 'Monday',
    editId: '',
    manualVisible: false,
    manualContent: null,
    tabSelect: 'Monday',
    searchField:'',
    sorter: {},
    filter: {},
  },

  reducers: {

    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },

    updateModalVisible(state, { payload: modalVisible }) {
      return {
        ...state,
        modalVisible,
      }
    },

    updateManualVisible(state, { payload: manualVisible }) {
      return {
        ...state,
        manualVisible,
      }
    }
  },

  effects: {

    *query(_, { take, put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      const defaultDay = 'Monday'
      yield put({ type: 'updateState', payload: { pagination: defaultPagination, selectWeekday: defaultDay } })
      yield put({ type: 'queryPlan' })
    },

    *queryPlan(_, { put, call, select }) {
      const { pagination, selectWeekday, sorter, filter, searchField} = yield select(state => state.classPlan)
      let queryData = { skipCount: 0, maxResultCount: 10 }
      let currentPageIndex = pagination.current || 1
      let currentPageSize = pagination.pageSize || 10
      queryData = { ...queryData, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize, planValue: [selectWeekday] }
      if (selectWeekday === 'all') {
        queryData = { ...queryData, planValue: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'] }
      }

      if (searchField !== undefined) {
        queryData = { ...queryData, filter: searchField }
      }

      if (sorter !== undefined) {
        const { columnKey, order } = sorter
        if (columnKey !== undefined) {
          queryData = { ...queryData, sorting: sortableItem[columnKey] + ' ' + sortOrderItem[order] }
        }
      }

      const coursePlanRes = yield call(FindCoursePlan, queryData)
      if (coursePlanRes.success) {
        const { items, totalCount } = coursePlanRes.result
        const list = items.map((planItem) => {
          const { classroom, course, planType, startCoursePeriod, teacher, planValue, subjectCount } = planItem
          return {
            ...planItem,
            teacherName: teacher ? teacher.name : '',
            planValueName: weekDayMenu[planValue],
            coursePeriod: startCoursePeriod ? startCoursePeriod.startTime + '-' + startCoursePeriod.endTime : '',
            roomName: classroom ? classroom.name : '',
            courseName: course ? course.title + ' (' + course.subTitle + ')' : '',
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { planList: list, pagination: newPagination, sorter, filter, searchField} })
      }
    },

    *changeTab({ payload: key }, { put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { pagination: defaultPagination, selectWeekday: key, dropdownSelect: key, tabSelect: key, sorter:{}, filter:{} } })
      yield put({ type: 'queryPlan' })
    },

    *queryWeekDay({ payload: weekDay }, { put, call, select }) {
      yield put({ type: 'updateState', payload: { dropdownSelect: weekDay } })
      const periodRes = yield call(GetAllActiveCoursePeriod, weekDay)
      if (periodRes.success) {
        const periodList = periodRes.result || []
        const { content: updateForm } = yield select(state => state.classPlan)

        //每次修改时间之后都会清空选择的
        const periodIndex = updateForm.Properties.findIndex((item) => {
          return item.FormKey === 'startCoursePeriodId'
        })

        if (periodIndex !== -1) {
          //黑科技为了刷掉原来填写的value
          //todo 清空某一个填写的值
          updateForm.Properties[periodIndex].FormKey = 'changePeriod'
          yield put({ type: 'updateState', payload: { content: updateForm } })

          const periodOptions = periodList.map((item) => {
            return { "Value": item.startTime + '--' + item.endTime, "Id": item.id }
          })
          updateForm.Properties[periodIndex].Setting.DropdownOptions = periodOptions
          updateForm.Properties[periodIndex].FormKey = 'startCoursePeriodId'
          updateForm.Properties[periodIndex].Value = null
        }
        yield put({ type: 'updateState', payload: { content: updateForm } })
      }
    },

    *clickAdd(_, { take, put, call, select }) {
      const { dropdownSelect: dropdownSelectInfo } = yield select(state => state.classPlan)
      const dropdownSelect = dropdownSelectInfo === 'all' ? 'Monday' : dropdownSelectInfo
      const periodRes = yield call(GetAllActiveCoursePeriod, dropdownSelect)
      const infoRes = yield call(GetCoursePlanForEdit, {})
      const courseRes = yield call(FindAllCourse, { skipCount: 0, maxResultCount: 200 })
      if (periodRes.success && infoRes.success && courseRes.success) {
        const periodList = periodRes.result || []
        const { classrooms, teachers: { items: teacherList }, availablePlanType } = infoRes.result || {}
        const { items: courseList } = courseRes.result || {}
        const newPlanForm = initClassPlanForm({ courseList, teacherList, classrooms, availablePlanType, periodList })
        yield put({ type: 'updateState', payload: { modalType: 'add', content: newPlanForm, modalVisible: true, dropdownSelect } })
      }
    },

    *onSubmitModal({ payload: data }, { put, call, select }) {
      const { modalType, editId, dropdownSelect } = yield select(state => state.classPlan)

      switch (modalType) {
        case 'add':
        case 'edit':
          const postData = modalType === 'edit' ? { ...data, id: editId, planValue: dropdownSelect } : { ...data, planValue: dropdownSelect }
          const res = yield call(CreateOrUpdateCoursePlan, postData)
          if (res.success) {
            yield put({ type: 'updateState', payload: { selectWeekday: dropdownSelect } })
            yield put({ type: 'updateModalVisible', payload: false })
            yield put({ type: 'queryPlan' })
          }
          break
        case 'manual':
          const manualRes = yield call(ManuallySchedule, data)
          if (manualRes.success) {
            message.success('开始排课！更新至课程表中大约需要20秒，请稍等...')
            yield put({ type: 'updateManualVisible', payload: false })
          }
          else {
            message.error('排课失败');
          }
          break
        default:
          break
      }
    },

    *clickEdit({ payload: id }, { put, call, select }) {
      const infoRes = yield call(GetCoursePlanForEdit, { Id: id })
      const { classrooms, teachers: { items: teacherList }, availablePlanType, coursePlan } = infoRes.result || {}
      const { planValue } = coursePlan
      const dropdownSelect = NumberToWeekDayKey[planValue]
      const periodRes = yield call(GetAllActiveCoursePeriod, dropdownSelect)
      const courseRes = yield call(FindAllCourse, { skipCount: 0, maxResultCount: 200 })
      if (periodRes.success && infoRes.success && courseRes.success) {
        const periodList = periodRes.result || []
        const { items: courseList } = courseRes.result || {}

        const emptyPlanForm = initClassPlanForm({ courseList, teacherList, classrooms, availablePlanType, periodList })
        const editPlanForm = matchDynamicForm(emptyPlanForm, coursePlan)
        yield put({ type: 'updateState', payload: { modalType: 'edit', content: editPlanForm, editId: id, dropdownSelect } })
      }
    },

    *clickDelete({ payload: id }, { put, call, select }) {
      const res = yield call(DeleteCoursePlan, id)
      if (res.success) {
        yield put({ type: 'queryPlan' })
      }
    },

    *clickManualSchedule(_, { take, put, call, select }) {
      const manualScheduleForm = {
        Properties: [{
          "EditorType": "NumberInput",
          "ShowTitle": "排课周数",
          "FormKey": "weekCount",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true,
            'NumberSetting': {
              min: 0,
              step: 1,
            }
          },
          "Description": null
        }, {
          "EditorType": "Dropdown",
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "MergeReplace",
              "Id": "MergeReplace"
            }, {
              "Value": "MergeJump",
              "Id": "MergeJump"
            }, {
              "Value": "Override",
              "Id": "Override"
            }],
            "Required": true
          },
          "ShowTitle": "排课模式",
          "FormKey": "mode",
          "Description": null,
        }, {
          "EditorType": "DatePicker",
          "ShowTitle": "排课开始时间",
          "FormKey": "startTime",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": false,
            DateType: 'date'
          },
          "Description": '从选择的日期开始排课或者更新'
        }]
      }

      yield put({ type: 'updateState', payload: { modalType: 'manual', manualContent: manualScheduleForm, manualVisible: true } })
    },

    *changeTable({ payload: data }, { put, call, select }) {
      yield put({ type: 'updateState', payload: data })
      yield put({ type: 'queryPlan' })
    },

    *changeSearchField({ payload: data }, { put, call, select }) {
      yield put({ type: 'updateState', payload: data })
      yield put({ type: 'queryPlan' })
    },

    *clearFilter(_, { put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField: '' } })
      yield put({ type: 'queryPlan' })
    },

    *goToClassPlanDetail({ payload: id }, { put, call, select }) {
      router.push({
        pathname: classPlanSubjectList,
        search: '?ck=' + id
      })
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { searchField: '', pagination: defaultPagination } })
    }

  },

  subscriptions: {
    setup({ dispatch, history }) {

      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(plan) === 0) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })

    },

  },
}
