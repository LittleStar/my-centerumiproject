import { routerRedux } from 'dva/router'
import router from 'umi/router'
import moment from 'moment'
import { pageConfig } from '../../../utils'
import queryString from 'query-string';
import { GetSchedule, GetAllActiveCoursePeriod, CreateOrUpdateCourseInstance, GetCourseInstanceForEdit } from '../../../services/centerService/Teaching'

const { edu, detail } = pageConfig
const { classList } = edu
const { classDetail,classSubjectList } = detail

const initCourseInstanceForm = ({ courseList, teacherList, classrooms, periodList }) => {
  const newPlanForm = {
    Properties: [{
      "EditorType": "Dropdown",
      "Value": null,
      "Setting": {
        "DropdownOptions": [],
        "Required": true
      },
      "ShowTitle": "课程",
      "FormKey": "courseId",
      "Description": null,
    }, {
      "EditorType": "Dropdown",
      "Value": null,
      "Setting": {
        "DropdownOptions": [],
        "Required": true
      },
      "ShowTitle": "上课时间",
      "FormKey": "startCoursePeriodId",
      "Description": null,
    }, {
      "EditorType": "Dropdown",
      "Value": null,
      "Setting": {
        "DropdownOptions": [],
        "Required": true
      },
      "ShowTitle": "教室",
      "FormKey": "classroomId",
      "Description": null
    }, {
      "EditorType": "Dropdown",
      "Value": null,
      "Setting": {
        "DropdownOptions": [],
        "Required": false
      },
      "ShowTitle": "教师",
      "FormKey": "teacherUserId",
      "Description": null
    }]
  }

  const courseIndex = newPlanForm.Properties.findIndex((item) => {
    return item.FormKey === 'courseId'
  })
  const teacherIndex = newPlanForm.Properties.findIndex((item) => {
    return item.FormKey === 'teacherUserId'
  })
  const roomIndex = newPlanForm.Properties.findIndex((item) => {
    return item.FormKey === 'classroomId'
  })
  const periodIndex = newPlanForm.Properties.findIndex((item) => {
    return item.FormKey === 'startCoursePeriodId'
  })

  if (courseIndex !== -1) {
    const courseOptions = courseList.map((item) => {
      return { "Value": `${item.title} ( ${item.subTitle} )`, "Id": item.id }
    })
    newPlanForm.Properties[courseIndex].Setting.DropdownOptions = courseOptions
  }
  if (teacherIndex !== -1) {
    const teacherOptions = teacherList.map((item) => {
      return { "Value": item.name, "Id": item.id }
    })
    newPlanForm.Properties[teacherIndex].Setting.DropdownOptions = teacherOptions
  }
  if (roomIndex !== -1) {
    const roomOptions = classrooms.map((item) => {
      return { "Value": item.name + '( ' + item.description + ' )', "Id": item.id }
    })
    newPlanForm.Properties[roomIndex].Setting.DropdownOptions = roomOptions
  }

  if (periodIndex !== -1) {
    const periodOptions = periodList.map((item) => {
      return { "Value": item.startTime + '--' + item.endTime, "Id": item.id }
    })
    newPlanForm.Properties[periodIndex].Setting.DropdownOptions = periodOptions
  }
  return newPlanForm
}

export default {

  namespace: 'classes',

  state: {
    selected: {},
    timeList: [],
    classroomList: [],
    classScheduleList: [],

    addClassVisible: false,
    content: null,

    selectDate: null
  },

  reducers: {

    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
    updateSelect(state, { payload: data }) {
      return {
        ...state,
        selected: data,
      }
    }

  },

  effects: {

    *query({ payload: queryData }, { take, put, call, select }) {
      const { date } = queryData
      const queryDate = date ? moment(date).format() : moment().format()
      const res = yield call(GetSchedule, { date: queryDate })
      const { coursePeriods, courseInstances, classrooms } = res.result
      yield put({ type: 'updateState', payload: { timeList: coursePeriods, classroomList: classrooms, classScheduleList: courseInstances, selectDate: queryDate } })
    },

    *goToClassDetail({ payload: classItem }, { take, put, call, select }) {
      const { id } = classItem
      router.push({
        pathname: classSubjectList,
        search: '?ck=' + id
      })
    },

    *clickAdd(_, { take, put, call, select }) {
      const { timeList } = yield select(state => state.classes)
      const courseInstancesRes = yield call(GetCourseInstanceForEdit, {})
      if (courseInstancesRes.success) {
        const { availableTeachers, classrooms, courses } = courseInstancesRes.result
        const newCourseForm = initCourseInstanceForm({ courseList: courses, teacherList: availableTeachers.items, classrooms, periodList: timeList })
        yield put({ type: 'updateState', payload: { content: newCourseForm, addClassVisible: true } })
      }
    },

    *addClass({ payload: data }, { take, put, call, select }) {
      const { selectDate } = yield select(state => state.classes)
      const postData = { ...data, actualTime: moment(selectDate) }

      const addRes = yield call(CreateOrUpdateCourseInstance, postData)
      if (addRes.success) {
        yield put({ type: 'query', payload: { date: selectDate } })
        yield put({ type: 'updateState', payload: { addClassVisible: false } })
      }
    },

    *clearModel(_, { take, put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { classScheduleList: [], pagination: defaultPagination } })
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {

      history.listen((location) => {
        if (location.pathname === classList) {
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })

    },

  },
}
