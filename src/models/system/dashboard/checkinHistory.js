import { routerRedux } from 'dva/router'
import { pageConfig,enumSetting } from 'utils'
import queryString from 'query-string'
import { CheckInManageList } from 'services/centerService/Teaching'
import moment from 'moment'

const { dashboard } = pageConfig
const { checkinHistory } = dashboard
const { vipType } = enumSetting


const sorterWay = {
  descend: 'DESC',
  ascend: 'ASC'
}

export default {
  namespace: 'checkinHistory',
  state: {
    listCheckinHistory: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query(_, { take, put, call, select }) {
      const { searchField,selectDate } = yield select(state => state.checkin)
      const { pagination } = yield select(state => state.checkinHistory)

      let query = { date: selectDate, filter: searchField, couseCourseInstanceUserStatuses: ['CheckedIn'] }
      let currentPageIndex = pagination.current
      let currentPageSize = pagination.pageSize
      query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }

      const res = yield call(CheckInManageList, query)
      if (res.success) {
        const { items: notCheckinItem, totalCount } = res.result
        const list = notCheckinItem.map((subjectItem) => {
          const { courseInstance, subject, userStatus,checkRestUnplannedCourseCount } = subjectItem
          const { startCoursePeriod, teacher, course, classroom } = courseInstance
          const firstGuardians = subject.guardians[0]
          return {
            ...subjectItem,
            subjectName: subject?subject.name:'',
            guardiansPhone: firstGuardians?firstGuardians.phoneNumber:'',
            startTime: startCoursePeriod?startCoursePeriod.startTime:'',
            endTime: startCoursePeriod?startCoursePeriod.endTime:'',
            teacher: teacher?teacher.name:'',
            userStatus: userStatus,
            courseTitle: course?course.title:'',
            classroom: classroom?classroom.name:'',
            type: subject?vipType[subject.type]['cn']:'',
            checkRestUnplannedCourseCount: checkRestUnplannedCourseCount?'否':'是',
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, listCheckinHistory: list } })
      }
    },

    *updatePagination({ payload: pagination }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { pagination } })
      yield put({ type: 'query'})
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { listCheckinHistory: [], pagination: defaultPagination } })
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === checkinHistory) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query' })
        }
      })
    },
  }
}
