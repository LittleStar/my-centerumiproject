import { routerRedux } from 'dva/router'
import { pageConfig } from 'utils'
import queryString from 'query-string';
import { GetSchedule} from '../../../services/centerService/Teaching'
import moment from 'moment'

const { dashboard,detail } = pageConfig
const { courseSchedule } = dashboard
const { classDetail } = detail


export default {
  namespace: 'courseManage',
  state: {
    selected: {},
    timeList: [],
    classroomList: [],
    classScheduleList: [],
    addClassVisible: false,
    content:null
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },

    updateSelect(state, { payload: data }) {
      return {
        ...state,
        selected: data,
      }
    }

  },
  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {
      const { searchField } = queryData
      const date = moment().format()
      let query={ date }
      if(searchField !== undefined){
        query = { ...query, filter: searchField }
      }
      const res = yield call(GetSchedule,query)
      const {coursePeriods, courseInstances, classrooms} = res.result
      yield put({type:'updateState',payload:{timeList:coursePeriods,classroomList:classrooms,classScheduleList:courseInstances}})
    },

    *goToClassDetail({ payload: classItem }, { take, put, call, select }) {
      const {id} = classItem
      yield put(routerRedux.push({
        pathname: classDetail,
        search: '?ck=' + id
      }))
    },

    *clickAdd({ payload: data }, { take, put, call, select }) {
      const addFormContent =  {
        "Id": "c6012122-84c4-425e-8e40-a119860780a7",
        "CreateBy": 0,
        "CreateTime": "0001-01-01T00:00:00",
        "ShowTitle": "Base",
        "Alias": "5天正念入门",
        "Icon": null,
        "Version": "00000000-0000-0000-0000-000000000000",
        "LastEditTime": "0001-01-01T00:00:00",
        "LastEditBy": 0,
        "IsDeleted": false,
        "DeletedDate": null,
        "SortOrder": 0,
        "DocumentTypeId": "00000000-0000-0000-0000-000000000000",
        "DocumentTypeVersion": "00000000-0000-0000-0000-000000000000",
        "Description": null,
        "Language": null,
        "ReleaseDate": null,
        "ExpireDate": null,
        "Status": "Unpublished",
        "Depth": 0,
        "ParentContent": "00000000-0000-0000-0000-000000000000",
        "Properties": [{
          "Id": "d99f3b45-8bf9-4a2c-ba69-0a036802f036",
          "ParentDocument": "33c6538c-7c2d-4b5c-b12f-88acd6307cd8",
          "EditorType": "Dropdown",
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "未开班",
              "Id": "未开班"
            }, {
              "Value": "开班中",
              "Id": "开班中"
            },{
              "Value": "已冻结",
              "Id": "已冻结"
            }],
            "Required": true
          },
          "ShowTitle": "班级状态",
          "FormKey": "classStatus",
          "Description": null,
          "SortOrder": 0
        },{
          "Id": "0d1f1d8b-8c2d-469b-b51f-6227f7df7b04",
          "ParentDocument": "33c6538c-7c2d-4b5c-b12f-88acd6307cd8",
          "EditorType": "Dropdown",
          "ShowTitle": "任课老师",
          "FormKey": "Teacher",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "Alen",
              "Id": "Alen"
            }, {
              "Value": "Maple",
              "Id": "Maple"
            },{
              "Value": "Ada",
              "Id": "Ada"
            }],
            "Required": true
          },
          "Description": null,
        },
        {
          "Id": "0d1f1d8b-8c2d-469b-b51f-6227f7df7b05",
          "ParentDocument": "33c6538c-7c2d-4b5c-b12f-88acd6307cd8",
          "EditorType": "Input",
          "ShowTitle": "最大可容纳人数",
          "FormKey": "MaxPopulation",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null,
        },
        {
          "Id": "0d1f1d8b-8c2d-469b-b51f-6227f7df7b09",
          "ParentDocument": "33c6538c-7c2d-4b5c-b12f-88acd6307cd8",
          "EditorType": "Dropdown",
          "ShowTitle": "教室",
          "FormKey": "Room",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "A1",
              "Id": "A1"
            }, {
              "Value": "A2",
              "Id": "A2"
            },{
              "Value": "B1",
              "Id": "B1"
            }],
            "Required": true
          },
          "Description": null,
        },]
       }
      yield put({type:'updateState',payload:{selected:data,content:addFormContent,addClassVisible:true}})
    },

    *addClass({ payload: data }, { take, put, call, select }) {
      const {selected} = yield select(state=>state.courseManage)
    },

    *clearModel(_, { take, put, call, select }){
      yield put({ type: 'updateState', payload: { classScheduleList: [] } })
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
        history.listen((location) => {
          if (location.pathname === courseSchedule) {
            dispatch({ type: 'clearModel' })
            dispatch({ type: 'query',payload:{...queryString.parse(location.search)}})
          }
        })
    },
  }
}
