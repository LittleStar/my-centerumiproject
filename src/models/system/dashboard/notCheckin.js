import { pageConfig, enumSetting } from 'utils'
import queryString from 'query-string'
import { message } from 'antd'
import { CheckInManageList, ChangeUserCourseInstance } from 'services/centerService/Teaching'

const { dashboard } = pageConfig
const { notCheckin } = dashboard
const { vipType } = enumSetting

export default {
  namespace: 'notCheckin',
  state: {
    listNocheckin: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query(_, { take, put, call, select }) {
      const { searchField,selectDate } = yield select(state => state.checkin)
      const { pagination } = yield select(state => state.notCheckin)
      let query = {date: selectDate, filter: searchField, couseCourseInstanceUserStatuses: ['None', 'Absenteeism'] }
      let currentPageIndex = pagination.current
      let currentPageSize = pagination.pageSize
      query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      const res = yield call(CheckInManageList, query)
      if (res.success) {
        const { items: notCheckinItem, totalCount } = res.result
        const list = notCheckinItem.map((subjectItem) => {
          const { courseInstance, subject, userStatus, checkRestUnplannedCourseCount } = subjectItem
          const { startCoursePeriod, teacher, course, classroom } = courseInstance
          const firstGuardians = subject.guardians[0]
          return {
            ...subjectItem,
            subjectName: subject?subject.name:'',
            guardiansPhone: firstGuardians?firstGuardians.phoneNumber:'',
            startTime: startCoursePeriod?startCoursePeriod.startTime:'',
            endTime: startCoursePeriod?startCoursePeriod.endTime:'',
            teacher: teacher?teacher.name:'',
            userStatus: userStatus,
            courseTitle: course?course.title:'',
            classroom: classroom?classroom.name:'',
            type: subject?vipType[subject.type]['cn']:'',
            checkRestUnplannedCourseCount: checkRestUnplannedCourseCount?'否':'是',

          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, listNocheckin: list } })
      }
    },

    *updatePagination({ payload: pagination }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { pagination } })
      yield put({ type: 'query'})
    },

    *onCheckIn({payload: data}, {take, put, call,select}) {
      const postData = { action: "CheckIn", ...data }
      const res = yield call(ChangeUserCourseInstance, postData)
      if(res.success){
        message.success('签到成功!')
        yield put({ type: 'query'  })
      }else {
        message.error('添加操作失败!')
      }
    },

    *onLeave({payload: data}, {take, put, call,select}) {
      const postData = { action: "Leave", ...data }
      const res = yield call(ChangeUserCourseInstance, postData)
      if(res.success){
        message.success('请假成功!')
        yield put({ type: 'query' })
      }else {
        message.error('请假操作失败!')
      }
    },

    *onAbsent ({payload: data}, {take, put, call,select}) {
      const postData = { action: "Absent", ...data }
      const res = yield call(ChangeUserCourseInstance, postData)
      if(res.success){
        message.success('旷课成功!')
        yield put({ type: 'query' })
      }else {
        message.error('旷课操作失败!')
      }
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { listNocheckin: [], pagination: defaultPagination } })
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === notCheckin) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },
  }
}
