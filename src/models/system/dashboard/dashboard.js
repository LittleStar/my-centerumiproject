import router from 'umi/router';
import { filterList } from 'utils/utils';
import {
  pageConfig,
  matchDynamicForm,
  CheckUrlPermission,
  DefaultRedirectUrl,
  permissionConfig,
} from 'utils';

const { location: locationPermission } = permissionConfig;
const { CheckIn, TeachingSchedule } = locationPermission;
const { dashboard } = pageConfig;
const { dashboard: dashboardPage } = dashboard;

const list = [
  {
    tabName: '运营报表',
    path: '/system/manage/dashboard/operationalReport',
    // permissionKey: OperationalReport
  },
  {
    tabName: '签到管理',
    path: '/system/manage/dashboard/checkin',
    permissionKey: CheckIn,
  },
  {
    tabName: '今日课表',
    path: '/system/manage/dashboard/courseSchedule',
    permissionKey: TeachingSchedule,
  },
];

export default {
  namespace: 'dashboard',
  state: {
    showList: [],
    defaultLink: '',
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
  effects: {

    *getPermission(_, { take, put, call, select }) {
      const { permission } = yield select(state => state.user)
      //todo
      const showList = permission.length!==0?filterList(list, permission):list
      yield put({ type: 'updateState', payload: { showList } })

      if(location.pathname===dashboardPage){
        const defaultLink = showList[0].path
        router.push(defaultLink)
      }
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        //如果pathname里面找到了
        if (location.pathname.search(dashboardPage) !== -1) {
          dispatch({ type: 'getPermission' });
        }
      });
    },
  },
};
