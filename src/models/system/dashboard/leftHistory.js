import { pageConfig, enumSetting  } from 'utils'
import { message } from 'antd'
import { CheckInManageList, ChangeUserCourseInstance } from 'services/centerService/Teaching'

const { dashboard } = pageConfig
const { leftHistory } = dashboard
const { vipType } = enumSetting

export default {
  namespace: 'leftHistory',
  state: {
    list: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query(_, { take, put, call, select }) {
      const { searchField,selectDate } = yield select(state => state.checkin)
      const { pagination } = yield select(state => state.leftHistory)

      let query = { date: selectDate, filter: searchField, couseCourseInstanceUserStatuses: ['Left'] }
      let currentPageIndex = pagination.current
      let currentPageSize = pagination.pageSize
      query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }

      const res = yield call(CheckInManageList, query)
      if (res.success) {
        const { items: notCheckinItem, totalCount } = res.result
        const list = notCheckinItem.map((subjectItem) => {
          const { courseInstance, subject, userStatus, checkRestUnplannedCourseCount } = subjectItem
          const { startCoursePeriod, teacher, course, classroom } = courseInstance
          const firstGuardians = subject.guardians[0]
          return {
            ...subjectItem,
            subjectName: subject?subject.name:'',
            guardiansPhone: firstGuardians?firstGuardians.phoneNumber:'',
            startTime: startCoursePeriod?startCoursePeriod.startTime:'',
            endTime: startCoursePeriod?startCoursePeriod.endTime:'',
            teacher: teacher?teacher.name:'',
            userStatus: userStatus,
            courseTitle: course?course.title:'',
            classroom: classroom?classroom.name:'',
            type: subject?vipType[subject.type]['cn']:'',
            checkRestUnplannedCourseCount: checkRestUnplannedCourseCount?'否':'是',
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list } })
      }
    },

    *updatePagination({ payload: pagination }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { pagination } })
      yield put({ type: 'query'})
    },

    *onCancelLeft({payload: data}, {take, put, call,select}) {
      const postData = { action: "CancelLeave", ...data }
      const res = yield call(ChangeUserCourseInstance, postData)
      if(res.success){
        message.success('取消请假成功!')
        yield put({ type: 'query' })
      }
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { list: [], pagination: defaultPagination } })
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === leftHistory) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query' })
        }
      })
    },
  }
}
