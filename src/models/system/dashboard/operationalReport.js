import {pageConfig, matchDynamicForm } from 'utils'
import queryString from 'query-string'
import { OperationalStatistics } from 'services/centerService/LocationStatistics'

const { dashboard } = pageConfig
const { operationalReport } = dashboard

export default {
  namespace: 'operationalReport',
  state: {
    currentMonthInfo: {},
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query(_, { take, put, call, select }) {
      const res = yield call(OperationalStatistics)
      if (res.success) {
        yield put({ type: 'updateState', payload: { currentMonthInfo: res.result } })
      }
    },

    *clearModel(_, { take, put, call, select }){
      yield put({ type: 'updateState', payload: { currentMonthInfo: {} } })
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(operationalReport) === 0) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query' })
        }
      })
    }
  }
}
