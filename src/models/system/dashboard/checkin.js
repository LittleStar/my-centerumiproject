import { routerRedux } from 'dva/router'
import { pageConfig } from 'utils'
import queryString from 'query-string'
import { message } from 'antd'
import { CheckInManageList, ChangeUserCourseInstance } from 'services/centerService/Teaching'
import moment from 'moment'

const { dashboard } = pageConfig
const { checkin:checkinPage, notCheckin:notCheckInPage, checkinHistory: checkinHistoryPage, leftHistory:leftHistoryPage} = dashboard


const locationMenu = {
  [notCheckInPage]:'notCheckIn',
  [checkinHistoryPage]:'checkIn',
  [leftHistoryPage]:'left'
}

export default {
  namespace: 'checkin',
  state: {
    selectDate: moment(),
    searchField: '',
    checkInType:'notCheckIn',
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *queryPage(_, { take, put, call, select }) {
      const { checkInType} = yield select(state => state.checkin)
      if(checkInType==='notCheckIn'){
        yield put({ type: 'notCheckin/query' })
      }
      else if(checkInType==='left'){
        yield put({ type: 'leftHistory/query'})
      }
      else{
        yield put({ type: 'checkinHistory/query'})
      }
    },

    *updateSearchField({ payload: searchField }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField } })
      yield put({ type: 'queryPage'})
    },

    *updateDate({ payload: selectDate }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { selectDate } })
      yield put({ type: 'queryPage'})
    },

    //checkInType: notCheckIn/checkIn
    *switchTab({ payload: checkInType }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { checkInType } })
      if(checkInType==='notCheckIn'){
        yield put(routerRedux.push({pathname: notCheckInPage}))
      }
      else if(checkInType==='left'){
        yield put(routerRedux.push({pathname: leftHistoryPage}))
      }
      else{
        yield put(routerRedux.push({pathname: checkinHistoryPage}))
      }
      yield put({ type: 'queryPage'})
    },

    *clearSearch(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField:'' } })
      yield put({ type: 'queryPage'})
    },

    *todayCheckIn(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField:'', selectDate:moment() } })
      yield put({ type: 'queryPage'})
    },

    *checkLocation(_, { take, put, call, select }) {
      const { checkInType} = yield select(state => state.checkin)
      if(locationMenu[location.pathname]!==checkInType){
        yield put({ type: 'updateState', payload: { checkInType:locationMenu[location.pathname]}})
      }
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname.search(checkinPage) !== -1) {
          dispatch({ type: 'checkLocation' })
        }
      })
    },
  }
}
