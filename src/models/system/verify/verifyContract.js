import { routerRedux } from 'dva/router'
import { pageConfig } from '../../../utils'
import { message } from 'antd';
import queryString from 'query-string'

import { ApplyForContract, CommitContract, GetUserAvailableContract, UserContractList } from '../../../services/centerService/Contract'

const { verify, detail } = pageConfig
const { contract } = verify
const { contractDetail } = detail

const currencyMenu = {
  '0': '元'
}

const filterableItem = {
  status: 'userContractStatuses',
  contractType: 'contractTypes',
  packageType: 'coursePackageTypes',
}

export default {
  namespace: 'verifyContract',
  state: {
    list: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    verifyType: 'current',
    currentFilter: {},
    historyFilter: {},
    sorter: {},
    searchField: null,
    unchangeModelLoading: true,
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {

    *query(_, { take, put, call, select }) {

      const { pagination, verifyType, currentFilter, historyFilter, searchField } = yield select(state => state.verifyContract)

      const userContractStatuses = verifyType === 'current' ? ['Commited'] : ['Deny', 'Confirmed', 'Cancelled', 'Expired', 'None']

      let query = { userContractStatuses, skipCount: 0, maxResultCount: 10 }

      //check pagination
      const currentPageIndex = pagination.current
      const currentPageSize = pagination.pageSize
      query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }

      if (searchField !== null) {
        query = { ...query, filter: searchField, skipCount: 0, maxResultCount: 10 }
      }

      if (verifyType === 'current') {
        for (let item in currentFilter) {
          if (currentFilter[item] !== null) {
            query[filterableItem[item]] = currentFilter[item]
          }
        }
      }
      else {
        for (let item in historyFilter) {
          if (historyFilter[item] !== null) {
            query[filterableItem[item]] = historyFilter[item]
          }
        }
      }

      const res = yield call(UserContractList, query)
      if (res.success) {
        const { items, totalCount } = res.result
        const list = items.map((contractItem) => {
          const { subject, responseUser, discount, paymentPrice, currency, coursePackage, price } = contractItem
          const { coursePackageType, count } = coursePackage
          return {
            ...contractItem,
            subjectName: subject ? subject.name : '',
            createPerson: responseUser ? responseUser.name : '',
            discountPrice: discount + currencyMenu[currency],
            realPayment: paymentPrice + currencyMenu[currency],
            originPrice: price + currencyMenu[currency],
            packageCount: count,
            packageType: coursePackageType,
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list, unchangeModelLoading: false } })
      }
    },

    //tabKey: current/history
    *switchTab({ payload: tabKey }, { call, put, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { verifyType: tabKey, pagination: defaultPagination, searchField:null,unchangeModelLoading: true } })
      yield put({ type: 'query' })
    },

    *changeTable({ payload: data }, { call, put, select }) {
      const { verifyType } = yield select(state => state.verifyContract)
      const { pagination, filters, sorter } = data
      if (verifyType === 'current') {
        yield put({ type: 'updateState', payload: { currentFilter: filters, pagination, sorter } })
      }
      else {
        yield put({ type: 'updateState', payload: { historyFilter: filters, pagination, sorter } })
      }
      yield put({ type: 'query' })
    },

    *goToContractDetail({ payload: id }, { take, put, call, select }) {
      yield put(routerRedux.push({
        pathname: contractDetail,
        search: '?contractId=' + id
      }))
    },

    *clearCurrentFilter(_, { call, put, select }) {
      yield put({ type: 'updateState', payload: { currentFilter: {}, searchField: null } })
      yield put({ type: 'query' })
    },

    *clearHistoryFilter(_, { call, put, select }) {
      yield put({ type: 'updateState', payload: { historyFilter: {}, searchField: null } })
      yield put({ type: 'query' })
    },

    *updateSearchField({ payload: data }, { call, put, select }) {
      const { pagination, currentFilter, historyFilter, sorter, searchField } = data
      yield put({ type: 'updateState', payload: { pagination, currentFilter, historyFilter, sorter, searchField, unchangeModelLoading: true } })
      yield put({ type: 'query' })
    },

    *clear(_, { take, put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      const defaultVerifyType = 'current'
      yield put({ type: 'updateState', payload: { pagination: defaultPagination, verifyType: defaultVerifyType, list: [], currentFilter: {}, historyFilter: {} } })
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === contract) {
          dispatch({ type: 'clear' })
          dispatch({ type: 'query' })
        }
      })
    },
  }
}
