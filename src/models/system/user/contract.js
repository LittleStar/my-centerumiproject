import { routerRedux } from 'dva/router'
import { pageConfig } from '../../../utils'
import { ApplyForContract,CommitContract,GetUserAvailableContract, UserContractList } from 'services/centerService/Contract'
import queryString from 'query-string'
const { detail,user } = pageConfig
const { contractDetail } = detail
const { contract } = user

const currencyMenu = {
  '0':'元'
}

const filterableItem = {
  status: 'userContractStatuses',
  contractType: 'contractTypes',
  coursePackageType: 'coursePackageTypes',
}


export default {
  namespace: 'contract',
  state: {
    list: [],
    listSupport: {
      creationTime:{
        showText: '申请时间',
        showType: 'Time',
        addtional: {
          format: 'MM/DD/YYYY HH:mm'
        }
      },
      subjectName: {
        showText: '学生姓名',
        showType: 'Text',
      },
      contractType: {
        showText: '合同类型',
        showType: 'Status',
        addtional: {
          statusArray: ['新合同', '升包', '课包更改', ' 退费']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '新合同', value: 'New' },
            { text: '升包', value: 'Upgrade' },
            { text: '课包更改', value: 'ModifyPackage' },
          ],
        }
      },
      status: {
        showText: '合同状态',
        showType: 'Status',
        addtional: {
          statusArray: ['合同草稿', '待审核', '未通过', '已通过', '过期', '已关闭']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '合同草稿', value: 'None' },
            { text: '待审核', value: 'Commited' },
            { text: '未通过', value: 'Deny' },
            { text: '已通过', value: 'Confirmed' },
            { text: '过期', value: 'Expired' },
            { text: '已关闭', value: 'Cancelled' },
          ],
        }
      },
      createPerson: {
        showText: '申请人',
        showType: 'Text',
      },
      discountPrice:{
        showText: '折扣金额',
        showType: 'Text',
      },
      realPayment:{
        showText: '实付金额',
        showType: 'Text',
      },
      coursePackageType: {
        showText: '课包类型',
        showType: 'Status',
        addtional: {
          statusArray: ['一对多', '一对一']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '一对多', value: 'OneToMany' },
            { text: '一对一', value: 'OneToOne' },
          ],
        }
      },
      price: {
        showText: '课包原价',
        showType: 'Text',
      },
      period: {
        showText: '有效期',
        showType: 'Text',
      },
      // expireTime: {
      //   showText: '到期时间',
      //   showType: 'Time',
      //   addtional: {
      //     format: 'MM/DD/YYYY HH:mm'
      //   }
      // }
    },
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    filter:{}
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {
      const dateTypeMenu = {
        0: '年',
        1: '月',
        2: '周',
        3: '天',
      }
      const { pagination, filters, sorter, searchField } = queryData || {}

      // let query = {userContractStatuses:['Confirmed'], skipCount: 0, maxResultCount: 10 }
      let query = { skipCount: 0, maxResultCount: 10 }
      let currentPageIndex = 1
      let currentPageSize = 10

      if(pagination!==undefined){
        currentPageIndex = pagination.current || 1
        currentPageSize = pagination.pageSize || 10
        query = {...query,skipCount:(currentPageIndex-1)*currentPageSize, maxResultCount:currentPageSize}
      }

      if (searchField !== undefined) {
        query = { ...query, filter: searchField, skipCount: 0, maxResultCount: 10 }
      }

      if(filters!==undefined){
        for(let item in filters){
          if(filters[item]!==null){
            query[filterableItem[item]]=filters[item]
          }
        }
      }

      const res = yield call(UserContractList,query)

      if(res.success){
        const { items, totalCount } = res.result
        const list = items.map((contractItem)=>{
          const{subject,responseUser,discount,paymentPrice,currency, coursePackage,price,expireTime} = contractItem
          return{
            ...contractItem,
            subjectName:subject?subject.name:'',
            createPerson:responseUser?responseUser.name:'',
            discountPrice:discount+currencyMenu[currency],
            realPayment:paymentPrice+currencyMenu[currency],
            coursePackageType: coursePackage?coursePackage.coursePackageType:'',
            price,
            period: coursePackage?coursePackage.period+dateTypeMenu[coursePackage.displayDateType]:'',
            expireTime,
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list, filter: filters } })
      }
    },

    *goToContractDetail({ payload: id }, { take, put, call, select }) {
      yield put(routerRedux.push({
        pathname: contractDetail,
        search: '?contractId=' + id
      }))
    },

    *clearFilter(_, { take, put, call, select }){
      yield put({ type: 'updateState', payload: { filter: {} } })
      yield put({ type: 'query' })
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { list: [], pagination:defaultPagination } })
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === contract) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },
  }
}
