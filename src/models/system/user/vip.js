import { routerRedux } from 'dva/router'
import { pageConfig, matchDynamicForm } from 'utils'
import queryString from 'query-string'
import { FindVip, GetCurrentSubjectForSales, GetCurrentSubjectForHeadteacher, CreateOrUpdateSubject, GetSubjectEditInfo, LinkSubjectHeadTeacher } from 'services/centerService/User'

const { user, detail } = pageConfig
const { vip } = user
const { userDetail,basicInfo } = detail


const dateTypeMenu = {
  0: '年',
  1: '月',
  2: '周',
  3: '天',
}
const sortableItem = {
  "restAllCourseCount":'Status.RestAllCourseCount',
  'expireTime':'Status.ExpireTime',
  'startTime':'Status.StartTime'
}

const orderToBackEnd = {
  'ascend':'',
  'descend':'DESC'
}

const filterableItem = {
  type: "type",
  coursePackageType: "coursePackageType",
}



export default {
  namespace: 'vip',
  state: {
    list: [],
    listSupport: {
      subjectName: {
        showText: '儿童姓名',
        showType: 'Text',
      },
      birthDay: {
        showText: '儿童生日',
        showType: 'Time',
        addtional: {
          format: 'YYYY/MM/DD'
        }
      },
      guardianName: {
        showText: '家长姓名',
        showType: 'Text',
      },
      guardianPhone: {
        showText: '家长电话',
        showType: 'Text',
      },
      type: {
        showText: '会员状态',
        showType: 'Status',
        addtional: {
          statusArray: ['非会员', '会员', '过期会员']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            // { text: '非会员', value: 'None' },
            { text: '会员', value: 'Vip' },
            { text: '过期会员', value: 'ExpiredVip ' },
          ],
        }
      },
      assignedSales: {
        showText: '负责课程顾问',
        showType: 'Text',
      },
      headteacher: {
        showText: '班主任',
        showType: 'Text',
      },
      coursePackageType: {
        showText: '课包类型',
        showType: 'Status',
        addtional: {
          statusArray: ['一对多', '一对一']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '一对多', value: 'OneToMany' },
            { text: '一对一', value: 'OneToOne' },
          ],
        }
      },
      restAllCourseCount: {
        showText: '剩余课时数',
        showType: 'Text',
        sorter: {
          hasSorter: true
        }
      },
      period: {
        showText: '有效期',
        showType: 'Text',
      },
      startTime: {
        showText: '申请时间',
        showType: 'Time',
        addtional: {
          format: 'YYYY/MM/DD'
        },
        sorter: {
          hasSorter: true,
        }
      },
      expireTime: {
        showText: '到期日期',
        showType: 'Time',
        addtional: {
          format: 'YYYY/MM/DD'
        },
        sorter: {
          hasSorter: true,
        }
      },
    },
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0,
    },
    modalVisible: false,
    content: null,
    modalType: 'edit',
    editId: '',
    guardianId: '',
    sorter: {},
    filters:{}
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {

      let { pagination, filters, sorter, searchField } = queryData || {}

      let query = { skipCount: 0, maxResultCount: 10, type: ['Vip', 'ExpiredVip'] }
      let currentPageIndex = 1
      let currentPageSize = 10
      if (pagination !== undefined) {
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      if (searchField !== undefined) {
        query = { ...query, filter: searchField, skipCount: 0, maxResultCount: 10 }
      }

      if(filters!==undefined){
        for(let item in filters){
          if(filters[item]!==null){
            query[filterableItem[item]] = filters[item]
          }
        }
      }

      if(sorter!==undefined){
        const {columnKey,order} = sorter
        if(columnKey!==undefined){
          query = {...query, sorting: sortableItem[columnKey]+' '+orderToBackEnd[order]}
        }
      }

      // 需要加sorter 和 filters ?
      const res = yield call(FindVip, query)
      if (res.success) {
        const { items, totalCount } = res.result
        const list = items.map((userItem) => {
          const { guardians, headteacher, name: subjectName, source, sales, restAllCourseCount, currentCoursePackage, startTime, expireTime } = userItem

          if (guardians.length === 0) {
            return {
              ...userItemm,
              assignedSales: sales ? sales.name : '',
              subjectName: subjectName,
            }
          } else {
            const firstGuardian = guardians[0]
            const { name, role, phoneNumber } = firstGuardian
            return {
              ...userItem,
              assignedSales: sales ? sales.name : '',
              headteacher: headteacher ? headteacher.name : '',
              subjectName: subjectName,
              guardianPhone: phoneNumber,
              guardianName: name,
              coursePackageType: currentCoursePackage ? currentCoursePackage.coursePackageType : '',
              restAllCourseCount: restAllCourseCount,
              period: currentCoursePackage ? currentCoursePackage.period + dateTypeMenu[currentCoursePackage.displayDateType] : '',
              startTime,
              expireTime,
            }
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }

        yield put({ type: 'updateState', payload: { pagination: newPagination, list, sorter, filters } })
      }
    },
    // 编辑
    *onEditBasicInfo({ payload: data }, { take, call, put, select }) {
      const editRes = yield call(GetSubjectEditInfo, data.id)
      const sSalesRes = yield call(GetCurrentSubjectForSales, data.id)
      const sTeacherRes = yield call(GetCurrentSubjectForHeadteacher, data.id)
      if (editRes.success && sSalesRes.success && sTeacherRes.success) {

        const { guardianSubjectRoleDefinitions, subject, sources, guardians } = editRes.result
        const { available: salesAvailable, current: salesCurrent } = sSalesRes.result
        const { items: salesItems } = salesAvailable

        const { available: teacherAvailable, current: teacherCurrent } = sTeacherRes.result
        const { items: teacherItems } = teacherAvailable

        let editForm = {
          Properties: [{
            "EditorType": "Input",
            "ShowTitle": "儿童姓名",
            "FormKey": "subjectName",
            "AdditionalData": null,
            "Value": '',
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "DatePicker",
            "ShowTitle": "儿童生日",
            "FormKey": "subjectBirthDay",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true,
              'DateType': 'date'
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "男",
                "Id": "1"
              }, {
                "Value": "女",
                "Id": "2"
              }, {
                "Value": "其他",
                "Id": "0"
              }],
              "Required": true
            },
            "ShowTitle": "儿童性别",
            "FormKey": "subjectGender",
            "Description": null,
          }, {
            "EditorType": "Input",
            "ShowTitle": "家长姓名",
            "FormKey": "guardianName",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
            },
            "Description": null
          }, {
            "EditorType": "Input",
            "ShowTitle": "家长电话",
            "FormKey": "guardianPhone",
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
            },
            "ShowTitle": "亲子关系",
            "FormKey": "guardianRole",
            "Description": null,
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "用户来源",
            "FormKey": "subjectSource",
            "Description": null,
          }
          ]
        }

        //找到需要添加内容的位置,似乎可以写成公共方法
        const guardianIndex = editForm.Properties.findIndex((item) => {
          return item.FormKey === 'guardianRole'
        })
        const sourceIndex = editForm.Properties.findIndex((item) => {
          return item.FormKey === 'subjectSource'
        })

        //如果找到了
        if (guardianIndex !== -1) {
          const guardianOptions = guardianSubjectRoleDefinitions.map((item) => {
            return { "Value": item.displayName, "Id": item.name }
          })
          editForm.Properties[guardianIndex].Setting.DropdownOptions = guardianOptions
        }
        if (sourceIndex !== -1) {
          const sourceOptions = sources.map((item) => {
            return { "Value": item.value, "Id": item.id }
          })
          //要具体看是什么组件
          editForm.Properties[sourceIndex].Setting.DropdownOptions = sourceOptions
        }

        const firstGuardian = guardians[0]

        const values = {
          subjectName: subject.name,
          subjectSource: subject.sourceId,
          subjectSurName: subject.surname,
          subjectGender: subject.gender,
          guardianName: firstGuardian.name,
          guardianSurName: firstGuardian.surname,
          subjectBirthDay: subject.birthDay,
          guardianPhone: firstGuardian.phoneNumber,
          guardianRole: firstGuardian.role,
          subjectSource: subject.sourceId
        }

        const editUserInfoForm = matchDynamicForm(editForm, values)
        yield put({ type: 'updateState', payload: { content: editUserInfoForm, modalType: 'edit', modalVisible: true, editId: data.id, guardianId: firstGuardian.id } })

      }
    },

    // 分配班主任
    *onAssignTeacher({ payload: id }, { take, call, put, select }) {
      const res = yield call(GetCurrentSubjectForHeadteacher, id)
      if (res.success) {
        const { available: { items: teacherItems }, current } = res.result

        let assignTeacherForm = {
          "Properties": [{
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "班主任列表",
            "FormKey": "headteacherId",
            "Description": null,
          },]
        }

        const teacherIndex = assignTeacherForm.Properties.findIndex((item) => {
          return item.FormKey === 'headteacherId'
        })
        if (teacherIndex !== -1) {
          const teacherOptions = teacherItems.map((item) => {
            return { "Value": item.name, "Id": item.id.toString() }
          })
          assignTeacherForm.Properties[teacherIndex].Setting.DropdownOptions = teacherOptions
        }

        if (current) {
          const matchValue = { headteacherId: current.id }
          assignTeacherForm = matchDynamicForm(assignTeacherForm, matchValue)
        }
        yield put({ type: 'updateState', payload: { modalType: 'assignTeacher', content: assignTeacherForm, modalVisible: true, editId: id } })
      }
    },

    *onSubmitModal({ payload: data }, { take, put, call, select }) {
      const { modalType, editId, guardianId } = yield select(state => state.vip)
      //注意在add的时候不能加id，edit的时候需要加id
      switch (modalType) {
        case 'edit':
          let postData = data
          if (modalType === 'edit') {
            const { subject, guardians } = data
            const guardianItem = { ...guardians[0], id: guardianId }
            postData = { guardians: [guardianItem], subject: { ...subject, id: editId } }
          }

          const res = yield call(CreateOrUpdateSubject, postData)
          if (res.success) {
            yield put({ type: 'query' })
            yield put({ type: 'updateState', payload: { modalVisible: false } })
          }
          break
        case 'assignTeacher':
          const assignTeacherPostData = { ...data, subjectIds: [editId] }
          const htRes = yield call(LinkSubjectHeadTeacher, assignTeacherPostData)
          if (htRes.success) {
            yield put({ type: 'query' })
            yield put({ type: 'updateState', payload: { modalVisible: false } })
          }
          break
        default:
          break
      }
    },

    *goToUserDetail({ payload: data }, { take, put, call, select }) {
      yield put(routerRedux.push({
        pathname: basicInfo,
        search: '?userId=' + data.id
      }))
    },

    *clearFilter(_, { take, put, call, select }){
      yield put({ type: 'updateState', payload: { filters: {} } })
      yield put({ type: 'query' })
    },

    *clearModel(_, { take, put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { list: [], pagination: defaultPagination } })
    }
  },


  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === vip) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },
  }
}
