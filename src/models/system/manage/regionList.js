import { routerRedux } from 'dva/router'
import { pageConfig } from 'utils'
import { FindOrganizationUnits } from 'services/centerService/OrganizationUnit'


const { manage, detail } = pageConfig
const { regionList } = manage
const { regionDetail } = detail


export default {
  namespace: 'regionList',

  state: {
    list: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    modalVisible: false,
    modalType: 'add',
    content: null,
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {

    *query({ payload: data }, { take, put, call, select }) {
      let { pagination, filters, sorter, searchField } = data || {}
      let query = { skipCount: 0, maxResultCount: 10, type: ['Region'] }
      let currentPageIndex = 1
      let currentPageSize = 50
      if (pagination !== undefined) {
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      const res = yield call(FindOrganizationUnits, query)
      if (res.success) {
        const { items, totalCount } = res.result
        const list = items.map(item => {
          const { creationTime, displayName } = item
          return {
            ...item
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list } })
      }

    },
    // 添加区域
    *onAddRegion(_, { take, put, call, select }) {
      // 从后端获取可选的员工 TODO  是否可以放在最前面来做
      const addRegionForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "区域名",
          "FormKey": "regionName",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        }, {
          "EditorType": "Dropdown",
          "ShowTitle": "区域经理	",
          "FormKey": "regionalManager",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "某位区域经理",
              "Id": "1"
            },],
            "Required": false,
          },
          "Description": null
        }, {
          "EditorType": "ContentSelector",
          "ShowTitle": "管辖门店",
          "FormKey": "jurisdiction",
          "AdditionalData": null,
          "Value": '',
          "Setting": {
            "SelectorOptions": [{
              "Value": "可选择无管辖归属门店1",
              "Id": "1"
            }, {
              "Value": "可选择无管辖归属门店1",
              "Id": "2"
            },],
            "Required": false
          },
          "Description": null
        },]
      }
      // 找到需要添加内容的索引位置 TODO
      // 如果找到就赋值

      yield put({ type: 'updateState', payload: { content: addRegionForm, modalType: 'add', modalVisible: true } })
    },
    // 编辑区域
    *onEditRegion(_, { take, put, call, select }) {
      // 从后端获取可选的员工 TODO  是否可以放在最前面来做
      const emptyRegionForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "区域名",
          "FormKey": "regionName",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        }, {
          "EditorType": "Dropdown",
          "ShowTitle": "区域经理	",
          "FormKey": "regionalManager",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [],
            "Required": false,
          },
          "Description": null
        }, {
          "EditorType": "ContentSelector",
          "ShowTitle": "管辖门店",
          "FormKey": "jurisdiction",
          "AdditionalData": null,
          "Value": '',
          "Setting": {
            "SelectorOptions": [],
            "Required": false
          },
          "Description": null
        },]
      }
      // 找到需要添加内容的索引位置 TODO
      // 如果找到就赋值

      yield put({ type: 'updateState', payload: { content: emptyRegionForm, modalType: 'eidt', modalVisible: true } })
    },

    *goToRegionDetail({ payload: id }, { take, put, call, select }) {
      yield put(routerRedux.push({
        pathname: regionDetail,
        search: '?orgId=' + id
      }))
    }


  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === regionList) {
          dispatch({ type: 'query' })
        }
      })
    },
  }
}
