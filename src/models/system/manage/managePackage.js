import { routerRedux } from 'dva/router'
import { pageConfig, matchDynamicForm } from '../../../utils'
import queryString from 'query-string'
import moment from 'moment'
import { CreateOrUpdatePackage, FindAllPackage, GetPackageDetail, PackageDelete } from '../../../services/centerService/Manage'

const { manage } = pageConfig
const { packageList } = manage

const currencyMenu = {
  '0':'元'
}

const displayDateTypeMenu = {
  '0':'年',
  '1':'月',
  '2':'周',
  '3':'天',
}

export default {
  namespace: 'managePackage',
  state: {
    packageList: [],
    packageListSupport: {
      creationTime: {
        showText: '创建日期',
        showType: 'Time',
        addtional: {
          format: 'MM/DD/YYYY HH:mm'
        }
      },
      count: {
        showText: '课时数',
        showType: 'Text',
      },
      effectiveTime: {
        showText: '有效期',
        showType: 'Text',
      },
      coursePackageType: {
        showText: '类型',
        showType: 'Status',
        addtional: {
          statusArray: ['一对多','一对一']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '一对一', value: 1 },
            { text: '一对多', value: 0 },
          ],
        }
      },
      costMoney:{
        showText: '价钱',
        showType: 'Text',
      },
      isActive:{
        showText: '类型',
        showType: 'Bool',
        addtional: {
          boolArray: ['未激活', '激活']
        },
      }
    },
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    modalVisible: false,
    content: null,
    modalType: 'add',
    editId: '',
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
    // updateModalVisible(state, { payload: modalVisible }) {
    //   return {
    //     ...state,
    //     modalVisible
    //   }
    // }
  },
  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {
      const { pagination, filters, sorter, searchField } = queryData || {}

      let query = { skipCount: 0, maxResultCount: 10 }
      let currentPageIndex = 1
      let currentPageSize = 10

      if (pagination !== undefined) {
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = { skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      const res = yield call(FindAllPackage, query)
      if (res.success) {
        const { items, totalCount } = res.result

        const showList = items.map(item=>{
          const {displayDateType,period,currency,price} = item

          const effectiveTime = period+displayDateTypeMenu[displayDateType]
          const costMoney = price+currencyMenu[currency]
          return {...item,effectiveTime,costMoney}
        })

        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, packageList: showList } })
      }

    },

    *onAddPackage(_, { put, call, select }) {
      const newPackageForm = {
        ShowTitle: "新课包",
        Properties: [{
          "EditorType": "NumberInput",
          "ShowTitle": "课时数",
          "FormKey": "count",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true,
            'NumberSetting': {
              min: 0,
              step: 1,
            }
          },
          "Description": null
        }, {
          "EditorType": "NumberInput",
          "ShowTitle": "有效期数字",
          "FormKey": "period",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true,
            'NumberSetting': {
              min: 0,
            }
          },
          "Description": null
        }, {
          "EditorType": "Dropdown",
          "ShowTitle": "有效期单位",
          "FormKey": "displayDateType",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "天",
              "Id": "3"
            },{
              "Value": "周",
              "Id": "2"
            }, {
              "Value": "月",
              "Id": "1"
            }, {
              "Value": "年",
              "Id": "0"
            }],
            "Required": false
          },
          "Description": null
        }, {
          "EditorType": "Dropdown",
          "ShowTitle": "课包类型",
          "FormKey": "coursePackageType",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "一对一",
              "Id": "1"
            }, {
              "Value": "一对多",
              "Id": "0"
            }],
            "Required": false
          },
          "Description": null
        },{
          "EditorType": "Dropdown",
          "ShowTitle": "货币单位",
          "FormKey": "currency",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "人民币",
              "Id": "0"
            }],
            "Required": false
          },
          "Description": null
        },{
          "EditorType": "NumberInput",
          "ShowTitle": "价格",
          "FormKey": "price",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true,
            'NumberSetting': {
              min: 0,
              precision: 2
            }
          },
          "Description": null
        },{
          "EditorType": "Bool",
          "ShowTitle": "是否激活",
          "FormKey": "isActive",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true,
          },
          "Description": null
        }]
      }
      yield put({ type: 'updateState', payload: { content: newPackageForm, modalType: 'add', modalVisible: true, editId: '' } })
    },

    *onEditPackage({ payload: id }, { put, call, select }) {
      const res = yield call(GetPackageDetail, id)
      if (res.success) {
        var { result } = res
        const emptyPackageForm = {
          ShowTitle: "新课包",
          Properties: [{
            "EditorType": "NumberInput",
            "ShowTitle": "课时数",
            "FormKey": "count",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true,
              'NumberSetting': {
                min: 0,
                step: 1,
              }
            },
            "Description": null
          }, {
            "EditorType": "NumberInput",
            "ShowTitle": "有效期数字",
            "FormKey": "period",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true,
              'NumberSetting': {
                min: 0,
              }
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "ShowTitle": "有效期单位",
            "FormKey": "displayDateType",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "天",
                "Id": "3"
              },{
                "Value": "周",
                "Id": "2"
              }, {
                "Value": "月",
                "Id": "1"
              }, {
                "Value": "年",
                "Id": "0"
              }],
              "Required": false
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "ShowTitle": "课包类型",
            "FormKey": "coursePackageType",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "一对一",
                "Id": "1"
              }, {
                "Value": "一对多",
                "Id": "0"
              }],
              "Required": false
            },
            "Description": null
          },{
            "EditorType": "Dropdown",
            "ShowTitle": "货币单位",
            "FormKey": "currency",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "人民币",
                "Id": "0"
              }],
              "Required": false
            },
            "Description": null
          },{
            "EditorType": "NumberInput",
            "ShowTitle": "价格",
            "FormKey": "price",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true,
              'NumberSetting': {
                min: 0,
                precision: 2,
              }
            },
            "Description": null
          },{
            "EditorType": "Bool",
            "ShowTitle": "是否激活",
            "FormKey": "isActive",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true,
            },
            "Description": null
          }]
        }
        const editPackageForm = matchDynamicForm(emptyPackageForm, result)
        yield put({ type: 'updateState', payload: { content: editPackageForm, modalType: 'edit', modalVisible: true, editId: id } })
      }
    },

    *onDeletePackage({ payload: id }, { put, call, select }) {
      const res = yield call(PackageDelete, id)
      if (res.success) {
        yield put({ type: 'query' })
      }
    },

    *onSubmitModal({ payload: data }, { put, call, select }) {
      const { modalType, editId } = yield select(state => state.managePackage)
      const postData = modalType === 'edit' ? { ...data, id: editId } : data
      const res = yield call(CreateOrUpdatePackage, postData)
      if (res.success) {
        yield put({ type: 'query' })
        yield put({ type: 'updateState', payload:{ modalVisible: false } })
      }
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === packageList) {
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },
  }
}
