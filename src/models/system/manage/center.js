import { routerRedux } from 'dva/router'
import { pageConfig } from '../../../utils'
import { FindOrganizationUnits, CreateOrganizationUnit, DeleteOrganizationUnit, MoveOrganizationUnit } from 'services/centerService/OrganizationUnit'
const { detail,manage } = pageConfig
const { centerDetail } = detail
const { center:centerPage } = manage


export default {
  namespace: 'center',

  state: {
    list: [],
    modalVisible: false,
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    content: null,
    modalType: 'add',
    searchField: '',
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {

      let { pagination, filters, sorter, searchField } = queryData || {}
      let query = { skipCount: 0, maxResultCount: 10, type: ['Location'] }
      let currentPageIndex = 1
      let currentPageSize = 50
      if (pagination !== undefined) {
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      if(searchField!==undefined){
        query = { ...query, filter: searchField }
      }

      // if(filters!==undefined){
      //   for(let item in filters){
      //     if(filters[item]!==null){
      //       query[filterableItem[item]] = filters[item]
      //     }
      //   }
      // }

      // if(sorter!==undefined){
      //   const {columnKey,order} = sorter
      //   if(columnKey!==undefined){
      //     query = {...query, sorting: sortableItem[columnKey]+' '+orderToBackEnd[order]}
      //   }
      // }

      // 需要加sorter 和 filters ?
      const res = yield call(FindOrganizationUnits, query)
      if(res.success){
        const {items,totalCount} = res.result
        const newPagination = {current:currentPageIndex,pageSize:currentPageSize,total:totalCount}

        const showLocationList = items.map((locationItem)=>{
          const{address1,address2} = locationItem
          const showAddress1 = address1?address1:''
          const showAddress2 = address2?address2:''
          return{
            ...locationItem,
            totalAddress:showAddress1+' '+ showAddress2
          }
        })
        yield put({ type: 'updateState', payload: {pagination:newPagination,list:showLocationList}})
      }
    },

    // 添加新店铺
    *newCenter(_, { take, put, call, select }) {
      // 从后端获取可选的员工 TODO
      const addCenterForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "店铺名称",
          "FormKey": "centerName",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        }, {
          "EditorType": "Input",
          "ShowTitle": "所在城市",
          "FormKey": "city",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        }, {
          "EditorType": "Input",
          "ShowTitle": "地址",
          "FormKey": "address",
          "AdditionalData": null,
          "Value": '',
          "Setting": {
            "Required": false
          },
          "Description": null
        }, {
          "EditorType": "Dropdown",
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "某位员工",
              "Id": "1"
            },],
            "Required": true
          },
          "ShowTitle": "选择店长",
          "FormKey": "selectCD",
          "Description": null,
        },]
      }
      // 找到需要添加内容的索引位置 TODO
      // 如果找到就赋值


      yield put({ type: 'updateState', payload: { content: addCenterForm, modalType: 'add', modalVisible: true } })
    },

    *onSubmitModal({ payload: data }, { take, put, call, select }) {

    },

    *goToCenterDetail({ payload: id }, { take, put, call, select }) {
      yield put(routerRedux.push({
        pathname: centerDetail,
        search: '?orgId=' + id
      }))
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      if (location.pathname === centerPage) {
        dispatch({ type: 'query' })
      }
    },
  }
}
