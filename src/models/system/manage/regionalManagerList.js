import { routerRedux } from 'dva/router'
import { pageConfig } from 'utils'

const { manage, detail } = pageConfig
const { regionalManagerList } = manage
const { regionManagerDetail } = detail


export default {
  namespace: 'regionalManagerList',

  state: {
    list: [
      {
        startDate: '2018-02-01',
        assignDate: '2018-03-03',
        employeeNo: '22',
        employeeName: 'Cindy',
        controlRegion: '古北',
        phoneNo: '18922223333',
        sex:'1',
        status: '1'
      }
    ],
    modalVisible: false,
    content: null,
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    filters: {},
    sorter: {},

  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {

    *query({ payload: data }, { take, put, call, select }) {

    },

    *onAddRegionManager(_, { take, put, call, select }) {
      // 从后端获取可选的员工 TODO  是否可以放在最前面来做
      const addRegionManagerForm = {
        Properties: [{
          "EditorType": "Dropdown",
          "ShowTitle": "员工姓名",
          "FormKey": "employeeName",
          "AdditionalData": null,
          "Value": '',
          "Setting": {
            "DropdownOptions": [{
              "Value": "姓名1",
              "Id": "1"
            }, {
              "Value": "姓名2",
              "Id": "2"
            },],
            "Required": true
          },
          "Description": null
        }, {
          "EditorType": "Input",
          "ShowTitle": "手机号",
          "FormKey": "phoneNo",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": false
          },
          "Description": null
        }, {
          "EditorType": "Dropdown",
          "ShowTitle": "性别	",
          "FormKey": "sex",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "男",
              "Id": "1"
            }, {
              "Value": "女",
              "Id": "2"
            },],
            "Required": false,
          },
          "Description": null
        },]
      }
      // 找到需要添加内容的索引位置 TODO
      // 如果找到就赋值

      yield put({ type: 'updateState', payload: { content: addRegionManagerForm, modalVisible: true } })
    },

    *goToRegionDetail({ payload: id }, { take, put, call, select }) {
      yield put(routerRedux.push({
        pathname: regionManagerDetail,
        search: '?rmKey=' + id
      }))
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === regionalManagerList) {
          dispatch({ type: 'query' })
        }
      })
    },
  }
}
