import { routerRedux } from 'dva/router'
import { pageConfig,matchDynamicForm } from '../../../utils'
import queryString from 'query-string';
import { CreateOrUpdateCourse,FindAllCourse,GetCourseDetail,CourseDelete} from '../../../services/centerService/Manage'

const { manage,detail } = pageConfig
const { courseList } = manage
const { courseDetail } = detail


const filterableItem = {
  courseType: "courseType",
}

export default {
  namespace: 'manageCourse',
  state: {
    courseList: [],
    courseListSupport: {
      title: {
        showText: '课程标题',
        showType: 'Text',
      },
      subTitle: {
        showText: '课程副标题',
        showType: 'Text',
      },
      courseType: {
        showText: '课包类型',
        showType: 'Status',
        addtional: {
          statusArray: ['一对多','一对一']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '一对多', value: 'OneToMany' },
            { text: '一对一', value: 'OneToOne' },
          ],
        }
      },
      suggestedCapacity: {
        showText: '建议容纳人数',
        showType: 'Text',
      },
      description: {
        showText: '课程描述',
        showType: 'Text',
      },
    },
    pagination:{
      current: 1,
      pageSize: 10,
      total: 0
    },
    modalVisible:false,
    content:null,
    modalType:'add',
    editId:'',
    filters:{},
    sorter:{},
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },

    updateModalVisible(state,{payload:modalVisible}){
      return{
        ...state,
        modalVisible
      }
    }

  },
  effects: {
    *query(_,{take, put, call, select}){
      const { pagination, filters, sorter, searchField } = yield select(state=>state.manageCourse)

      let query = {skipCount:0,maxResultCount:10}
      let currentPageIndex = 1
      let currentPageSize = 10

      if(pagination!==undefined){
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = {skipCount:(currentPageIndex-1)*currentPageSize, maxResultCount:currentPageSize}
      }

      if(filters!==undefined){
        for(let item in filters){
          if(filters[item]!==null){
            query[filterableItem[item]] = filters[item]
          }
        }
      }

      const res = yield call(FindAllCourse,query)
      if(res.success){
        const {items,totalCount} = res.result
        const newPagination = {current:currentPageIndex,pageSize:currentPageSize,total:totalCount}
        yield put({ type: 'updateState', payload: {pagination:newPagination,courseList:items}})
      }

    },

    *onAddCourse(_,{put, call, select}){
      const newCourseForm = {
        ShowTitle: "新教室",
        Properties:[{
          "EditorType": "Input",
          "ShowTitle": "课程标题",
          "FormKey": "title",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        },{
          "EditorType": "Input",
          "ShowTitle": "课程副标题",
          "FormKey": "subTitle",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": false
          },
          "Description": null
        },{
          "EditorType": "Dropdown",
          "ShowTitle": "课包类型",
          "FormKey": "courseType",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "一对多",
              "Id": "0"
            },{
              "Value": "一对一",
              "Id": "1"
            }],
            "Required": false
          },
          "Description": null
        },{
          "EditorType": "Input",
          "ShowTitle": "课程描述",
          "FormKey": "description",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": false
          },
          "Description": null
        },{
          "EditorType": "NumberInput",
          "ShowTitle": "建议容纳人数",
          "FormKey": "suggestedCapacity",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": false,
            'NumberSetting':{
              min:0,
              step:1,
            }
          },
          "Description": null
        }]
      }
      yield put({ type: 'updateState', payload: {content:newCourseForm, modalType:'add', modalVisible:true, editId:''}})
    },

    *onEditCourse({payload:id},{put, call, select}){
      const res = yield call(GetCourseDetail,id)
      if(res.success){
        const {result} = res
        const emptyCourseForm = {
          ShowTitle: "新教室",
          Properties:[{
            "EditorType": "Input",
            "ShowTitle": "课程标题",
            "FormKey": "title",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          },{
            "EditorType": "Input",
            "ShowTitle": "课程副标题",
            "FormKey": "subTitle",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": false
            },
            "Description": null
          },{
            "EditorType": "Dropdown",
            "ShowTitle": "课包类型",
            "FormKey": "courseType",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "一对多",
                "Id": "0"
              },{
                "Value": "一对一",
                "Id": "1"
              }],
              "Required": false
            },
            "Description": null
          },{
            "EditorType": "Input",
            "ShowTitle": "课程描述",
            "FormKey": "description",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": false
            },
            "Description": null
          },{
            "EditorType": "NumberInput",
            "ShowTitle": "建议容纳人数",
            "FormKey": "suggestedCapacity",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": false,
              'NumberSetting':{
                min:0,
                step:1,
              }
            },
            "Description": null
          }]
        }
        const editCourseForm = matchDynamicForm(emptyCourseForm,result)
        yield put({ type: 'updateState', payload: {content:editCourseForm, modalType:'edit', modalVisible:true,editId:id}})
      }
    },

    *onDeleteCourse({payload:id},{put, call, select}){
      const res = yield call(CourseDelete,id)
      if(res.success){
        yield put({ type: 'query'})
      }
    },

    *onSubmitModal({payload:data},{put,call,select}){
      const {modalType,editId} = yield select(state=>state.manageCourse)
      const postData = modalType==='edit'?{...data,id:editId}:data
      const res = yield call(CreateOrUpdateCourse,postData)
      if(res.success){
        yield put({ type: 'query'})
        yield put({ type: 'updateState', payload: { modalVisible:false } })
      }
    },

    *changeTable({payload: data}, {put,call,select}){
      const { pagination, filters } = data
      yield put({ type: 'updateState', payload: { pagination, filters } })
      yield put({ type: 'query' })
    },

    *goToCourseDetail({ payload: id }, { take, put, call, select }) {
      yield put(routerRedux.push({
        pathname: courseDetail,
        search: '?courseKey=' + id
      }))
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === courseList) {
          dispatch({ type: 'query' })
        }
      })
    },
  }
}
