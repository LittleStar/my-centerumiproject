import { routerRedux } from 'dva/router'
import { pageConfig } from '../../../utils'
import { CreateOrUpdateRole } from '../../../services/centerService/Manage'

const { manage } = pageConfig
const { permission } = manage

export default {
  namespace: 'permission',
  state: {
    list:[],
    listSupport: {
      roleName: {
        showText: '角色名',
        showType: 'Text',
      }
    },
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    modalVisible: false,
    content: null,
    modalType: null,
    editId: null,
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {

    },
    *onAddRole(_, { take, put, call, select }) {
      const addRoleForm = {
        ShowTitle: "新角色",
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "角色名",
          "FormKey": "roleName",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true,
          },
          "Description": null
        },]
      }
      yield put({ type: 'updateState', payload: { content: addRoleForm, modalType: 'add', modalVisible: true } })
    },

    *onSubmitModal({ payload: data }, { put, call, select }) {
      const { modalType } = yield select(state => state.permission)
      const postData = modalType === 'edit' ? { ...data, id: editId } : data
      const res = yield call(CreateOrUpdateRole, postData)
      if (res.success) {
        yield put({ type: 'query' })
        yield put({ type: 'updateState', payload: { modalVisible: false } })
      }
    },

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === permission) {
          dispatch({ type: 'query' })
        }
      })
    },
  }
}
