import { routerRedux } from 'dva/router'
import { pageConfig } from '../../../utils'
import { message } from 'antd'
import { FindOrganizationUnits, CreateOrganizationUnit, DeleteOrganizationUnit, MoveOrganizationUnit } from 'services/centerService/OrganizationUnit'

const { detail, manage } = pageConfig
const { centerDetail, regionDetail } = detail
const { organization: organizationPage } = manage

export default {
  namespace: 'organization',

  state: {
    organizationList: [],
    modalVisible: false,
    content: null,
    modalType: 'addOutside',
    parentId: null,
    currentClickId: null,
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query(_, { take, put, call, select }) {
      const res = yield call(FindOrganizationUnits, {})
      if (res.success) {
        const { items } = res.result
        yield put({ type: 'updateState', payload: { organizationList: items } })
      }
    },

    // 添加新店铺或者区域
    *onNewLocationOrRegion(_, { take, put, call, select }) {
      // 从后端获取可选的员工 TODO
      const addForm = {
        Properties: [{
          "EditorType": "Dropdown",
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "店铺",
              "Id": "0"
            }, {
              "Value": "区域",
              "Id": "1"
            },],
            "Required": true
          },
          "ShowTitle": "选择店铺/区域",
          "FormKey": "organizationUnitType",
          "Description": null,
        }, {
          "EditorType": "Input",
          "ShowTitle": "店铺/区域名称",
          "FormKey": "displayName",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        },
        // {
        //   "EditorType": "Input",
        //   "ShowTitle": "所在城市",
        //   "FormKey": "city",
        //   "AdditionalData": null,
        //   "Value": null,
        //   "Setting": {
        //     "Required": true
        //   },
        //   "Description": null
        // },
        {
          "EditorType": "Input",
          "ShowTitle": "地址",
          "FormKey": "address",
          "AdditionalData": null,
          "Value": '',
          "Setting": {
            "Required": false
          },
          "Description": null
        }, {
          "EditorType": "Bool",
          "Value": null,
          "Setting": {
            "Required": false
          },
          "ShowTitle": "是否激活",
          "FormKey": "isActive",
          "Description": null
        },
        ]
      }
      // 找到需要添加内容的索引位置 TODO
      // 如果找到就赋值

      yield put({ type: 'updateState', payload: { content: addForm, modalType: 'addOutside', modalVisible: true } })
    },

    //为已有区域添加店铺/区域
    *onNewLocation({ payload: id }, { take, put, call, select }) {
      let { organizationList } = yield select(state => state.organization)
      // 从后端获取可选的员工 TODO
      const addLocationForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "所属区域",
          "FormKey": "parentId",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": false,
            "Disabled": true,
          },
          "Description": null
        }, {
          "EditorType": "Dropdown",
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "店铺",
              "Id": "0"
            },{
              "Value": "区域",
              "Id": "1"
            },],
            "Required": true
          },
          "ShowTitle": "选择店铺/区域",
          "FormKey": "organizationUnitType",
          "Description": null,
        }, {
          "EditorType": "Input",
          "ShowTitle": "店铺/区域名称",
          "FormKey": "displayName",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        },
        //  {
        //   "EditorType": "Input",
        //   "ShowTitle": "所在城市",
        //   "FormKey": "city",
        //   "AdditionalData": null,
        //   "Value": null,
        //   "Setting": {
        //     "Required": true
        //   },
        //   "Description": null
        // },
        {
          "EditorType": "Input",
          "ShowTitle": "地址",
          "FormKey": "address",
          "AdditionalData": null,
          "Value": '',
          "Setting": {
            "Required": false
          },
          "Description": null
        }, {
          "EditorType": "Bool",
          "Value": null,
          "Setting": {
            "Required": false
          },
          "ShowTitle": "是否激活",
          "FormKey": "isActive",
          "Description": null
        },]
      }
      // 找到需要添加内容的索引位置 TODO
      const parentIdIndex = addLocationForm.Properties.findIndex((item) => {
        return item.FormKey === 'parentId'
      })
      // 如果找到就赋值
      if (parentIdIndex !== -1) {
        const parentInfo = organizationList.find(item => {
          return item.id === id
        })
        addLocationForm.Properties[parentIdIndex].Value = parentInfo.displayName
      }
      yield put({ type: 'updateState', payload: { content: addLocationForm, modalType: 'addInsid', modalVisible: true, parentId: id } })
    },

    // 删除店铺/区域
    *onDelete({ payload: id }, { take, put, call, select }) {
      const res = yield call(DeleteOrganizationUnit, { id })
      if (res.success) {
        message.success('删除成功!')
        yield put({ type: 'query' })
      } else {
        message.success('删除失败!')
      }
    },

    //移动店铺/区域
    *onMove({ payload: id }, { take, put, call, select }) {

      //获取不包含自己所属区域/店铺
      const res = yield call(FindOrganizationUnits, {notParentId:id})
      let notParentList = []
      if (res.success) {
        notParentList = res.result.items
      }
      notParentList.push({
        id: 'centercenter',
        displayName: "中心管辖区域",
        organizationUnitType: 1
      })

      let { organizationList } = yield select(state=>state.organization)

      const moveForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "现所属区域",
          "FormKey": "parentId",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": false,
            "Disabled": true,
          },
          "Description": null
        }, {
          "EditorType": "Dropdown",
          "Value": null,
          "Setting": {
            "DropdownOptions": [],
            "Required": true
          },
          "ShowTitle": "转区域",
          "FormKey": "regions",
          "Description": null,
        },]
      }

      const parentIdIndex = moveForm.Properties.findIndex((item) => {
        return item.FormKey === 'parentId'
      })
      const regionsIndex = moveForm.Properties.findIndex((item) => {
        return item.FormKey === 'regions'
      })

      // 如果找到就赋值
      let currentInfo
      if (parentIdIndex !== -1) {
        currentInfo = organizationList.find(item => {
          return item.id === id
        })

        const parentInfo = organizationList.find(item => {
          // 如果当前点击的没有parentId
          return currentInfo.parentId === item.id
        })
        const parentsName = parentInfo===undefined?'没有所属区域':parentInfo.displayName
        moveForm.Properties[parentIdIndex].Value = parentsName
      }


      if (regionsIndex !== -1) {

        const regionList = notParentList.filter(item => {
          return item.organizationUnitType === 1
        })
        let regionsOptions = regionList.map(item => {
          return { Value: item.displayName, Id: item.id }
        })
        regionsOptions = regionsOptions.filter(item=>{
          return item.Id !== currentInfo.parentId
        })

        moveForm.Properties[regionsIndex].Setting.DropdownOptions = regionsOptions

      }
      yield put({ type: 'updateState', payload: { content: moveForm, modalType: 'move', modalVisible: true, currentClickId: id } })
    },

    *onSubmitModal({ payload: data }, { take, put, call, select }) {
      const { modalType } = yield select(state => state.organization)
      switch (modalType) {
        case 'addOutside':
        case 'addInsid':
          const createRes = yield call(CreateOrganizationUnit, data)
          if (createRes.success) {
            message.success('添加成功!')
            yield put({ type: 'query' })
            yield put({ type: 'updateState', payload: { modalVisible: false } })
          }else{
            message.success('添加失败!')
          }
        break
        case 'move':
          const{newParentId,id} = data
          let postData = data
          if(newParentId==='centercenter') postData = {id}
          const moveRes = yield call(MoveOrganizationUnit, postData)
          if (moveRes.success) {
            message.success('转区域成功!')
            yield put({ type: 'query' })
            yield put({ type: 'updateState', payload: { modalVisible: false } })
          }else{
            message.success('转区域失败!')
          }
      }
    },

    *goToCenterDetail({ payload: id }, { take, put, call, select }) {
      yield put(routerRedux.push({
        pathname: centerDetail,
        search: '?orgId=' + id
      }))
    },

    *goToRegionDetail({ payload: id }, { take, put, call, select }) {
      yield put(routerRedux.push({
        pathname: regionDetail,
        search: '?orgId=' + id
      }))
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === organizationPage) {
          dispatch({ type: 'query' })
        }
      })
    },
  }
}
