// import { routerRedux } from 'dva/router'
import router from 'umi/router';
import { pageConfig, matchDynamicForm } from '../../../utils'
import { message } from 'antd'
import moment from 'moment'
import queryString from 'query-string'
import { GetSubjectEditInfo, CreateOrUpdateSubject, FindPotentialUser, GetCurrentSubjectForSales, LinkSubjectSales, ExcelBatchImportTemplete, DownloadTempFile } from '../../../services/centerService/User'
import { ApplyForContract, CommitContract, GetUserAvailableContract } from '../../../services/centerService/Contract'
import { UserSourceStatistics } from '../../../services/centerService/LocationStatistics'

const { sale, detail } = pageConfig
const { saleUserList } = sale
const { userDetail,basicInfo } = detail

const currencyMenu = {
  '0': '元'
}

const displayDateTypeMenu = {
  '0': '年',
  '1': '月',
  '2': '周',
  '3': '天',
}

const packageMenu = {
  '0': '一对多',
  '1': '一对一',
}

const tabelKeyByBackEnd = {
  subjectSource: "sourecId",
  latestUserContractStatus: "latestUserContractStatus",
}


export default {
  namespace: 'saleUser',

  state: {
    list: [],
    listSupport: {
      creationTime: {
        showText: '添加日期',
        showType: 'Time',
        addtional: {
          format: 'MM/DD/YYYY HH:mm'
        }
      },
      subjectName: {
        showText: '儿童姓名',
        showType: 'Text',
      },
      birthDay: {
        showText: '儿童生日',
        showType: 'Time',
        addtional: {
          format: 'YYYY/MM/DD'
        }
      },
      guardianName: {
        showText: '家长姓名',
        showType: 'Text',
      },
      guardianPhone: {
        showText: '家长电话',
        showType: 'Text',
      },

      guardianRole: {
        showText: '亲子关系',
        showType: 'Text',
      },
      subjectSource: {
        showText: '用户来源',
        showType: 'Text',
        filter: {
          hasFilter: true,
          filterOptions: [],
        }
      },
      assignedSales: {
        showText: '负责课程顾问',
        showType: 'Text',
      },
      creatorUser: {
        showText: '添加人',
        showType: 'Text',
      },
      latestUserContractStatus: {
        showText: '报名状态',
        showType: 'Status',
        addtional: {
          statusArray: ['未报名', '待审核', '审核未通过', '已通过', '过期', '取消申请']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '未报名', value: 'None' },
            { text: '待审核', value: 'Commited' },
            { text: '审核未通过', value: 'Deny' },
            { text: '取消申请', value: 'Cancelled' },
          ],
        }
      },
    },
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    // add/edit/assign/apply
    modalType: 'add',
    content: null,
    editId: '',
    modalVisible: false,
    guardianId: '',
    filters: {},
    hasSpin:false,
  },

  reducers: {

    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },

  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {
      const { listSupport } = yield select(state => state.saleUser)
      let queryInfo = { startTime: moment().format(), endTime: moment().format() }
      const sourceRes = yield call(UserSourceStatistics, queryInfo)

      let filterOptions = []
      if (sourceRes.success) {
        const itemList = sourceRes.result.data
        itemList.forEach(item => {
          filterOptions.push({ text: item.source.value, value: item.source.value })
        })
      }
      listSupport.subjectSource.filter.filterOptions = filterOptions
      const { pagination, filters, sorter, searchField } = queryData || {}

      let query = { skipCount: 0, maxResultCount: 10, type: ['None'] }
      let currentPageIndex = 1
      let currentPageSize = 10

      if (pagination !== undefined) {
        currentPageIndex = pagination.current || 1
        currentPageSize = pagination.pageSize || 10
        query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      if (searchField !== undefined) {
        query = { ...query, filter: searchField }
      }
      if (filters !== undefined) {
        for (let item in filters) {
          if (filters[item] !== null) {
            query[tabelKeyByBackEnd[item]] = filters[item]
          }
        }
      }

      const res = yield call(FindPotentialUser, query)
      if (res.success) {
        const { items, totalCount } = res.result

        const list = items.map((userItem) => {

          const { guardians, surname: subjectSurname, name: subjectName, source, sales, creatorUser } = userItem

          if (guardians.length === 0)
            return {
              ...userItem,
              assignedSales: sales ? sales.name : '',
              subjectName: subjectName,
              subjectSource: source.value,
            }
          else {
            const firstGuardian = guardians[0]
            const { name, surname, role, phoneNumber } = firstGuardian
            return {
              ...userItem,
              assignedSales: sales ? sales.name : '',
              guardianName: name,
              guardianPhone: phoneNumber,
              guardianRole: role ? role.displayName : '',
              subjectName: subjectName,
              subjectSource: source ? source.value : '',
              creatorUser: creatorUser ? creatorUser.name : '',
            }
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list, listSupport, filters } })
      }
    },
    // 点击添加新用户
    *onAddUser(_, { take, put, call, select }) {

      //后端获取可选择的guardianRole以及source
      const res = yield call(GetSubjectEditInfo)

      if (res.success) {
        const { guardianSubjectRoleDefinitions, sources } = res.result

        //初始化表单组件，需要走后端拿数据的地方先赋值空
        const addUserForm = {
          Properties: [{
            "EditorType": "Input",
            "ShowTitle": "家长姓名",
            "FormKey": "GuardianName",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
            },
            "Description": null
          }, {
            "EditorType": "Input",
            "ShowTitle": "家长电话",
            "FormKey": "GuardianPhone",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Input",
            "ShowTitle": "儿童姓名",
            "FormKey": "SubjectName",
            "AdditionalData": null,
            "Value": '',
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "男",
                "Id": "1"
              }, {
                "Value": "女",
                "Id": "2"
              }, {
                "Value": "其他",
                "Id": "0"
              }],
              "Required": false
            },
            "ShowTitle": "儿童性别",
            "FormKey": "SubjectGender",
            "Description": null,
          }, {
            "EditorType": "DatePicker",
            "ShowTitle": "儿童生日",
            "FormKey": "SubjectBirthDay",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true,
              'DateType': 'date'
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
            },
            "ShowTitle": "亲子关系",
            "FormKey": "GuardianRole",
            "Description": null,
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "用户来源",
            "FormKey": "SubjectSource",
            "Description": null,
          }, {
            "EditorType": "Input",
            "ShowTitle": "备注",
            "FormKey": "Description",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
            },
            "Description": null
          },]
        }

        //找到需要添加内容的位置,似乎可以写成公共方法
        const guardianIndex = addUserForm.Properties.findIndex((item) => {
          return item.FormKey === 'GuardianRole'
        })

        const sourceIndex = addUserForm.Properties.findIndex((item) => {
          return item.FormKey === 'SubjectSource'
        })

        //如果找到了
        if (guardianIndex !== -1) {
          const guardianOptions = guardianSubjectRoleDefinitions.map((item) => {
            return { "Value": item.displayName, "Id": item.name }
          })
          //要具体看是什么组件
          addUserForm.Properties[guardianIndex].Setting.DropdownOptions = guardianOptions
        }
        if (sourceIndex !== -1) {
          const sourceOptions = sources.map((item) => {
            return { "Value": item.value, "Id": item.id }
          })
          //要具体看是什么组件
          addUserForm.Properties[sourceIndex].Setting.DropdownOptions = sourceOptions
        }
        yield put({ type: 'updateState', payload: { content: addUserForm, modalType: 'add', modalVisible: true } })
      }
    },

    //点击编辑按钮
    *onEditUser({ payload: id }, { take, put, call, select }) {
      const res = yield call(GetSubjectEditInfo, id)
      if (res.success) {
        const { guardianSubjectRoleDefinitions, sources, guardians, subject } = res.result
        const emptyUserForm = {
          Properties: [{
            "EditorType": "Input",
            "ShowTitle": "家长姓名",
            "FormKey": "GuardianName",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
            },
            "Description": null
          }, {
            "EditorType": "Input",
            "ShowTitle": "家长电话",
            "FormKey": "GuardianPhone",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Input",
            "ShowTitle": "儿童姓名",
            "FormKey": "SubjectName",
            "AdditionalData": null,
            "Value": '',
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "男",
                "Id": "1"
              }, {
                "Value": "女",
                "Id": "2"
              }, {
                "Value": "其他",
                "Id": "0"
              }],
              "Required": true
            },
            "ShowTitle": "儿童性别",
            "FormKey": "SubjectGender",
            "Description": null,
          }, {
            "EditorType": "DatePicker",
            "ShowTitle": "儿童生日",
            "FormKey": "SubjectBirthDay",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true,
              'DateType': 'date'
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
            },
            "ShowTitle": "亲子关系",
            "FormKey": "GuardianRole",
            "Description": null,
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "用户来源",
            "FormKey": "SubjectSource",
            "Description": null,
          }, {
            "EditorType": "Input",
            "ShowTitle": "备注",
            "FormKey": "Description",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
            },
            "Description": null
          },]
        }
        //找到需要添加内容的位置,似乎可以写成公共方法
        const guardianIndex = emptyUserForm.Properties.findIndex((item) => {
          return item.FormKey === 'GuardianRole'
        })
        const sourceIndex = emptyUserForm.Properties.findIndex((item) => {
          return item.FormKey === 'SubjectSource'
        })
        //如果找到了
        if (guardianIndex !== -1) {
          const guardianOptions = guardianSubjectRoleDefinitions.map((item) => {
            return { "Value": item.displayName, "Id": item.name }
          })
          //要具体看是什么组件
          emptyUserForm.Properties[guardianIndex].Setting.DropdownOptions = guardianOptions
        }
        if (sourceIndex !== -1) {
          const sourceOptions = sources.map((item) => {
            return { "Value": item.value, "Id": item.id.toString() }
          })
          //要具体看是什么组件
          emptyUserForm.Properties[sourceIndex].Setting.DropdownOptions = sourceOptions
        }

        const firstGuardian = guardians[0]
        const values = {
          GuardianSurName: firstGuardian.surname,
          SubjectSource: subject.sourceId,
          GuardianRole: firstGuardian.role,
          SubjectBirthDay: subject.birthDay,
          SubjectGender: subject.gender,
          SubjectName: subject.name,
          SubjectSurName: subject.surname,
          GuardianPhone: firstGuardian.phoneNumber,
          GuardianName: firstGuardian.name,
          Description: subject.description,
        }
        const editUserForm = matchDynamicForm(emptyUserForm, values)
        yield put({ type: 'updateState', payload: { content: editUserForm, modalType: 'edit', modalVisible: true, editId: id, guardianId: firstGuardian.id } })

      }
    },

    // 点击分配按钮
    *onAssignSalesman({ payload: id }, { take, put, call, select }) {
      const res = yield call(GetCurrentSubjectForSales, id)
      if (res.success) {
        const { available, current } = res.result
        const { items: salesItems } = available
        let assignSalesForm = {
          Properties: [{
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "课程顾问列表",
            "FormKey": "salesId",
            "Description": null,
          }]
        }

        const salesIndex = assignSalesForm.Properties.findIndex((item) => {
          return item.FormKey === 'salesId'
        })

        if (salesIndex !== -1) {
          const salesOptions = salesItems.map((item) => {
            return { "Value": item.name, "Id": item.id.toString() }
          })
          //要具体看是什么组件
          assignSalesForm.Properties[salesIndex].Setting.DropdownOptions = salesOptions
        }

        if (current) {
          const matchValue = { salesId: current.id }
          assignSalesForm = matchDynamicForm(assignSalesForm, matchValue)
        }

        yield put({ type: 'updateState', payload: { content: assignSalesForm, modalType: 'assign', modalVisible: true, editId: id } })
      }
    },

    // 点击付费申请
    *onApply({ payload: id }, { take, put, call, select }) {
      //在储备用户这里只有新合同
      const postSubjectInfo = { subjectId: id, "contractType": "New" }
      const contractInfo = yield call(ApplyForContract, postSubjectInfo)
      //从后端获取该用户是否已经申请了新的合同
      if (!contractInfo.success) {
        return
      }
      const { availableCoursePackages, availableResponseUsers } = contractInfo.result
      const applyForm = {
        Properties: [{
          "EditorType": "Dropdown",
          "ShowTitle": "购买课包类型",
          "FormKey": "coursePackageId",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [],
            "Required": true
          },
          "Description": null
        }, {
          "EditorType": "Dropdown",
          "ShowTitle": "货币类型",
          "FormKey": "currency",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "人民币",
              "Id": "0"
            }],
            "Required": true
          },
          "Description": null
        }, {
          "EditorType": "NumberInput",
          "ShowTitle": "折扣金额",
          "FormKey": "discount",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true,
            'NumberSetting': {
              min: 0,
              step: 1,
              precision:2
            }
          },
          "Description": null
        }, {
          "EditorType": "NumberInput",
          "ShowTitle": "实付金额",
          "FormKey": "paymentPrice",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true,
            'NumberSetting': {
              min: 0,
              step: 1,
              precision:2
            }
          },
          "Description": null
        }, {
          "EditorType": "Upload",
          "Value": null,
          "Setting": {
            "Required": true
          },
          "ShowTitle": "附件图片",
          "FormKey": "contractPics",
          "Description": '如合同原件、付费凭证'
        }, {
          "EditorType": "Input",
          "ShowTitle": "订单ID",
          "FormKey": "orderId",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": false
          },
          "Description": null
        },
        ]
      }

      const coursePackageIndex = applyForm.Properties.findIndex((item) => {
        return item.FormKey === 'coursePackageId'
      })

      if (coursePackageIndex !== -1) {
        const coursePackageOptions = availableCoursePackages.map((item) => {
          const { count, coursePackageType, displayDateType, period, price } = item
          const showValue = '课时数：' + count + '  课包类型：' + packageMenu[coursePackageType] + '  课包有效期:' + period + displayDateTypeMenu[displayDateType] + ` 课包原价: ${price}元`
          return { "Value": showValue, "Id": item.id }
        })
        applyForm.Properties[coursePackageIndex].Setting.DropdownOptions = coursePackageOptions
      }

      yield put({ type: 'updateState', payload: { content: applyForm, modalType: 'apply', modalVisible: true, editId: id } })
    },

    // 提交
    *onSubmitModal({ payload: data }, { take, put, call, select }) {
      const { modalType, editId, guardianId } = yield select(state => state.saleUser)
      //注意在add的时候不能加id，edit的时候需要加id

      switch (modalType) {
        case 'add':
        case 'edit':
          let postData = data
          if (modalType === 'edit') {
            const { subject, guardians } = data
            const guardianItem = { ...guardians[0], id: guardianId }
            postData = { guardians: [guardianItem], subject: { ...subject, id: editId } }
          }
          const res = yield call(CreateOrUpdateSubject, postData)
          if (res.success) {
            yield put({ type: 'updateState', payload: { modalVisible: false } })
            yield put({ type: 'query' })
          }
          break
        case 'assign':
          const assignPostData = { ...data, subjectIds: [editId] }
          const assignRes = yield call(LinkSubjectSales, assignPostData)
          if (assignRes.success) {
            yield put({ type: 'updateState', payload: { modalVisible: false } })
            yield put({ type: 'query' })
          }
          break
        case 'apply':
          const applayPostData = { ...data, subjectId: editId, contractType: 'New' }
          const applayRes = yield call(CommitContract, applayPostData)
          if (applayRes.success) {
            yield put({ type: 'updateState', payload: { modalVisible: false } })
            yield put({ type: 'query' })
            message.success('付费申请成功提交')
          }
          else {
            message.error('付费申请提交失败')
          }
          break
        default:
          break
      }
    },

    *getResponseFile({ payload: data }, { take, put, call, select }) {
      const res = yield call(DownloadTempFile, data)
    },

    *getTemplete(_, { take, put, call, select }) {
      const res = yield call(ExcelBatchImportTemplete);
    },

    // 跳转到详情页
    *goToUserDetail({ payload: id }, { take, put, call, select }) {
      // yield put(routerRedux.push({
      //   pathname: userDetail,
      //   search: '?userId=' + id
      // }))
      // const permissionList = yield select(_=>_.user.permission)
      // const toUrl = DefaultRedirectUrl(permissionList,userDetail)
      router.push({
        pathname: basicInfo,
        query: {userId: id}
      })
    },

    *clearFilter(_, { take, put, call, select }){
      yield put({ type: 'updateState', payload: { filters: {} } })
      yield put({ type: 'query' })
    },

    *clearModel(_, { take, put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { list: [], pagination: defaultPagination } })
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === saleUserList) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },
  },
}
