import { routerRedux } from 'dva/router'
import { pageConfig } from '../../../utils'

const { user } = pageConfig

export default {
  namespace: 'settingCourseManage',
  state: {
    list: [
      {
        "courseId": "002",
        "courseName": "游戏",
        "classHour": "12",
        "status": "0"

      }, {
        "courseId": "001",
        "courseName": "写字",
        "classHour": "20",
        "status": "1"
      }
    ],
    listSupport: {
      courseName: {
        showText: '课程名',
        showType: 'Text',
      },
      classHour: {
        showText: '课时数',
        showType: 'Text',
      },
      status: {
        showText: '课程状态',
        showType: 'Status',
        addtional: {
          statusArray: ['未使用', '使用中']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '未使用', value: 0 },
            { text: '使用中', value: 1 },
          ],
        }
      },
    },
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    // *goToContractDetail({ payload: id }, { take, put, call, select }) {
    //   yield put(routerRedux.push({
    //     pathname: contractInfoDetail,
    //     search: '?contractId=' + id
    //   }))
    // }

  },
  subscriptions: {
    setup({ dispatch, history }) {
    },
  }
}