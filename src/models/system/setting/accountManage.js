import { routerRedux } from 'dva/router'
import { pageConfig, matchDynamicForm } from 'utils'
import { GetDetail, UpdateOrganizationUnit } from 'services/centerService/OrganizationUnit'


const { user, setting } = pageConfig
const { account: accountPage } = setting

export default {
  namespace: 'accountManage',
  state: {
    list: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 1
    },
    centerInfo: {},
    modalType: 'edit',
    modalVisible: false,
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query(_, { take, put, call, select }) {
      const { currentUser } = yield select(state => state.user)
      const { id } = currentUser.organizationUnit
      const res = yield call(GetDetail, id)
      if (res.success) {
        const centerInfo = res.result
        const list = [{ ...centerInfo }]
        yield put({ type: 'updateState', payload: { list, centerInfo } })
      }
    },

    *onEdit(_, { take, put, call, select }) {
      const { centerInfo } = yield select(state => state.accountManage)
      const { displayName, city, address1 } = centerInfo
      const editForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "店铺名称",
          "FormKey": "displayName",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        }, {
          "EditorType": "Input",
          "ShowTitle": "所在城市",
          "FormKey": "city",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        }, {
          "EditorType": "Input",
          "ShowTitle": "地址",
          "FormKey": "address1",
          "AdditionalData": null,
          "Value": '',
          "Setting": {
            "Required": false
          },
          "Description": null
        },]
      }

      const values = { displayName, city, address1 }
      const centerInfoForm = matchDynamicForm(editForm, values)
      yield put({ type: 'updateState', payload: { content: centerInfoForm, modalType: 'edit', modalVisible: true } })
    },

    *onSubmitModal({ payload: data }, { take, put, call, select }) {
      const { modalType, centerInfo } = yield select(state => state.accountManage)
      switch (modalType) {
        case 'edit':
          const { address1, city, displayName } = data
          const putData = { id: centerInfo.id, address1, city, displayName }
          const centerInfoRes = yield call(UpdateOrganizationUnit, putData)
          if (centerInfoRes.success) {
            yield put({ type: 'updateState', payload: { modalVisible: false } })
            yield put({ type: 'query' })
          }
          break
      }
    },

    *clear(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { list: [] } })
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === accountPage) {
          dispatch({ type: 'clear' })
          dispatch({ type: 'query' })
        }
      })
    },
  }
}
