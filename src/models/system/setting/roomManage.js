import { routerRedux } from 'dva/router'
import { pageConfig, matchDynamicForm } from '../../../utils'
import { CreateOrUpdateClassroom, GetClassrooms, GetDetail, DeleteClassroom } from '../../../services/centerService/Classroom'
import queryString from 'query-string';
const { setting } = pageConfig

const { room } = setting

export default {
  namespace: 'roomManage',
  state: {
    list: [],
    listSupport: {
      name: {
        showText: '教室名',
        showType: 'Text',
      },
      description: {
        showText: '描述',
        showType: 'Text',
      },
      classroomType: {
        showText: '教室类型',
        showType: 'Status',
        addtional: {
          statusArray: ['其他', '动态教室', '静态教室']
        },
      },
      capacity: {
        showText: '最大容纳人数',
        showType: 'Text',
      },
    },
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    modalVisible: false,
    content: null,
    modalType: 'add',
    editId: null,
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {

    *query({ payload: queryData }, { take, put, call, select }) {

      const { pagination, filters, sorter, searchField } = queryData || {}

      let query = { skipCount: 0, maxResultCount: 10 }
      let currentPageIndex = 1
      let currentPageSize = 10

      if (pagination !== undefined) {
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = { skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      const res = yield call(GetClassrooms, query)
      if (res.success) {
        const { items, totalCount } = res.result

        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list: items } })
      }

    },

    *onAddRoom(_, { put, call, select }) {

      const newRoomForm = {
        ShowTitle: "新教室",
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "教室名称",
          "FormKey": "name",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        }, {
          "EditorType": "Input",
          "ShowTitle": "教室描述",
          "FormKey": "description",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        }, {
          "EditorType": "Dropdown",
          "ShowTitle": "教室类型",
          "FormKey": "classroomType",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true,
            'DropdownOptions': [{
              Id: '1',
              Value: '动态教室',
            }, {
              Id: '2',
              Value: '静态教室',
            }, {
              Id: '0',
              Value: '其他',
            }]
          },
          "Description": null
        }, {
          "EditorType": "NumberInput",
          "ShowTitle": "最大容纳",
          "FormKey": "capacity",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true,
            'NumberSetting': {
              min: 0,
              step: 1,
            }
          },
          "Description": null
        }]
      }

      yield put({ type: 'updateState', payload: { content: newRoomForm, modalType: 'add', modalVisible: true, editId: '' } })
    },

    *onEditRoom({ payload: id }, { put, call, select }) {
      const res = yield call(GetDetail, id)

      if (res.success) {
        const { result } = res
        const emptyRoomForm = {
          ShowTitle: "新教室",
          Properties: [{
            "EditorType": "Input",
            "ShowTitle": "教室名称",
            "FormKey": "name",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Input",
            "ShowTitle": "教室描述",
            "FormKey": "description",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "ShowTitle": "教室类型",
            "FormKey": "classroomType",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true,
              'DropdownOptions': [{
                Id: '1',
                Value: '动态教室',
              }, {
                Id: '2',
                Value: '静态教室',
              }, {
                Id: '0',
                Value: '其他',
              }]
            },
            "Description": null
          }, {
            "EditorType": "NumberInput",
            "ShowTitle": "最大容纳",
            "FormKey": "capacity",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true,
              'NumberSetting': {
                min: 0,
                step: 1,
              }
            },
            "Description": null
          }]
        }
        const editRoomForm = matchDynamicForm(emptyRoomForm, result)
        yield put({ type: 'updateState', payload: { content: editRoomForm, modalType: 'edit', modalVisible: true, editId: id } })
      }

    },

    *onDeleteRoom({ payload: id }, { put, call, select }) {
      const res = yield call(DeleteClassroom, id)
      console.log('res', res)
      if (res.success) {
        yield put({ type: 'query' })
      }
    },

    *onSubmitModal({ payload: data }, { put, call, select }) {
      const { modalType, editId } = yield select(state => state.roomManage)
      const postData = modalType === 'edit' ? { ...data, id: editId } : data
      const res = yield call(CreateOrUpdateClassroom, postData)
      if (res.success) {
        yield put({ type: 'query' })
        yield put({ type: 'updateState', payload: { modalVisible: false } })
      }
    },

    *clearModel(_, { take, put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { list: [], pagination: defaultPagination } })
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === room) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },
  }
}
