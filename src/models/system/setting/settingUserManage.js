import router from 'umi/router'
import queryString from 'query-string'
import { pageConfig, matchDynamicForm } from 'utils'
import { CreateOrUpdateUser, GetCurrentUserForEdit, GetUsers, GetRolesExceptSubject } from 'services/centerService/User'

const { setting, detail } = pageConfig
const { user } = setting
const { employeeDetail } = detail

const filterableItem = {
  role: 'role',
}

export default {
  namespace: 'settingUserManage',
  state: {
    list: [],
    listSupport: {
      creationTime: {
        showText: '添加时间',
        showType: 'Time',
        addtional: {
          format: 'MM/DD/YYYY HH:mm'
        }
      },
      fullName: {
        showText: '员工姓名',
        showType: 'Text',
      },
      phoneNumber: {
        showText: '手机号',
        showType: 'Text',
      },
      emailAddress: {
        showText: '邮箱',
        showType: 'Text',
      },
      role: {
        showText: '角色',
        showType: 'Text',
        filter: {
          hasFilter: true,
          filterOptions: [],
        }
      },
      status: {
        showText: '员工状态',
        showType: 'Bool',
        addtional: {
          boolArray: ['未激活', '激活']
        },
        filter: {
          // hasFilter: false,
          filterOptions: [
            { text: '未激活', value: 0 },
            { text: '激活', value: 1 },
          ],
        }
      },
    },
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    // add/edit
    modalType: 'add',
    content: null,
    modalVisible: false,
    editId: '',
    filter: {},
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {

    *query({ payload: queryData }, { take, put, call, select }) {
      const { listSupport } = yield select(state => state.settingUserManage)
      const allRoleRes = yield call(GetRolesExceptSubject)
      let filterOptions = []
      if (allRoleRes.success) {
        const itemList = allRoleRes.result.items
        itemList.forEach(item => {
          filterOptions.push({ text: item.displayName, value: item.id })
        })
      }
      listSupport.role.filter.filterOptions = filterOptions


      const { pagination, filter, sorter, searchField } = queryData || {}
      let query = { skipCount: 0, maxResultCount: 10 }
      let currentPageIndex = 1
      let currentPageSize = 10

      if (pagination !== undefined) {
        currentPageIndex = pagination.current || 1
        currentPageSize = pagination.pageSize || 10
        query = { skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      if (searchField !== undefined) {
        query = { ...query, filter: searchField, skipCount: 0, maxResultCount: 10 }
      }

      if (filter !== undefined) {
        for (let item in filter) {
          if (filter[item] !== null) {
            query[filterableItem[item]] = filter[item]
          }
        }
      }

      const res = yield call(GetUsers, query)
      if (res.success) {
        const { items, totalCount } = res.result

        const list = items.map((item) => {
          const { roles, name, isActive } = item
          let RolesText = ''
          roles.forEach((item) => {
            RolesText += item.roleName + ' '
          })
          return {
            ...item,
            role: RolesText,
            fullName: name,
            status: isActive,
          }
        })


        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list, filter } })
      }
    },

    // 点击new按钮
    *onNewWorker(_, { take, put, call, select }) {

      const res = yield call(GetCurrentUserForEdit)

      if (res.success) {
        const { roles } = res.result
        // 初始化表单组件
        const newWorkerForm = {
          ShowTitle: "新员工",
          Properties: [{
            "EditorType": "Input",
            "ShowTitle": "员工姓名",
            "FormKey": "employeName",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Input",
            "ShowTitle": "手机号",
            "FormKey": "phone",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "男",
                "Id": "1"
              }, {
                "Value": "女",
                "Id": "2"
              }],
              "Required": true
            },
            "ShowTitle": "性别",
            "FormKey": "gender",
            "Description": null,
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "角色",
            "FormKey": "role",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "邮箱",
            "FormKey": "emailAddress",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "密码",
            "FormKey": "password",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "再次输入密码",
            "FormKey": "rePassword",
            "Description": null
          }, {
            "EditorType": "Bool",
            "Value": null,
            "Setting": {
              "Required": true
            },
            "ShowTitle": "是否激活该员工",
            "FormKey": "isActive",
            "Description": null
          }]
        }

        // 找到需要添加内容的位置
        const roleIndex = newWorkerForm.Properties.findIndex((item) => {
          return item.FormKey === 'role'
        })
        // 如果找到
        if (roleIndex !== -1) {
          const roleOptions = roles.map((item) => {
            return { "Value": item.roleDisplayName, "Id": item.roleId }
          })
          newWorkerForm.Properties[roleIndex].Setting.DropdownOptions = roleOptions
        }
        yield put({ type: 'updateState', payload: { content: newWorkerForm, modalType: 'add', modalVisible: true } })
      }
    },

    // 点击编辑按钮
    *onEditWorker({ payload: id }, { take, put, call, select }) {
      const res = yield call(GetCurrentUserForEdit, { id })
      if (res.success) {
        const { user, roles } = res.result
        // 获取res.result
        // 后端获取该用户的信息
        const emptyWorkerForm = {
          ShowTitle: "编辑员工",
          Properties: [{
            "EditorType": "Input",
            "ShowTitle": "员工姓名",
            "FormKey": "employeName",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Input",
            "ShowTitle": "手机号",
            "FormKey": "phone",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "男",
                "Id": "1"
              }, {
                "Value": "女",
                "Id": "2"
              }],
              "Required": true
            },
            "ShowTitle": "性别",
            "FormKey": "gender",
            "Description": null,
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "角色",
            "FormKey": "role",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "邮箱",
            "FormKey": "emailAddress",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
            },
            "ShowTitle": "密码",
            "FormKey": "password",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
            },
            "ShowTitle": "再次输入密码",
            "FormKey": "rePassword",
            "Description": null
          }, {
            "EditorType": "Bool",
            "Value": null,
            "Setting": {
              "Required": true
            },
            "ShowTitle": "是否激活该员工",
            "FormKey": "isActive",
            "Description": null
          }]
        }

        // 找到需要添加内容的位置
        const roleIndex = emptyWorkerForm.Properties.findIndex((item) => {
          return item.FormKey === 'role'
        })

        // 如果找到
        if (roleIndex !== -1) {
          const roleOptions = roles.map((item) => {
            return { "Value": item.roleDisplayName, "Id": item.roleId.toString() }
          })
          emptyWorkerForm.Properties[roleIndex].Setting.DropdownOptions = roleOptions
        }
        const roleValue = roles.filter(item => {
          return item.isAssigned === true
        })
        const values = {
          employeSurName: user.surname,
          employeName: user.name,
          phone: user.phoneNumber,
          gender: user.gender,
          role: roleValue[0].roleId.toString(),
          password: user.password,
          rePassword: user.password,
          emailAddress: user.emailAddress
        }
        const onEditWorkerForm = matchDynamicForm(emptyWorkerForm, { ...user, ...values })
        yield put({ type: 'updateState', payload: { content: onEditWorkerForm, modalType: 'edit', modalVisible: true, editId: id } })
      }
    },

    // 提交
    *onSubmitModal({ payload: data }, { take, put, call, select }) {

      const { modalType, editId } = yield select(state => state.settingUserManage)
      switch (modalType) {
        case 'add':
        case 'edit':
          //注意在add的时候不能加id，edit的时候需要加i
          let postData = data
          const { user, assignedRoleIds } = data
          if (modalType === 'edit') {
            postData = { assignedRoleIds, user: { ...user, id: editId } }
          }
          const res = yield call(CreateOrUpdateUser, postData)
          if (res.success) {
            yield put({ type: 'updateState', payload: { modalVisible: false } })
            yield put({ type: 'query' })
          }
      }
    },

    *clearModel(_, { take, put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { list: [], pagination: defaultPagination } })
    },

    *goToEmployeeDetail({ payload: id }, { take, put, call, select }) {
      router.push({
        pathname: employeeDetail,
        search: '?empId=' + id
      })
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === user) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },
  }
}
