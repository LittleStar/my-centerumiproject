import { pageConfig, matchDynamicForm } from 'utils'
import { routerRedux } from 'dva/router'
import queryString from 'query-string'
import moment from 'moment'
import { UserSourceStatistics } from 'services/centerService/LocationStatistics'

const { report } = pageConfig
const { appear: appearPage, appearDetails:appearDetailsPage, appearAdvisor:appearAdvisorPage, appearTeacher: appearTeacherPage, appearSales:appearSalesPage} = report

const locationMenu = {
  [appearDetailsPage]:'details',
  [appearAdvisorPage]:'headTeacher',
  [appearTeacherPage]:'teacher',
  [appearSalesPage]:'sales',
}

export default {
  namespace: 'appear',
  state: {
    selectDate: moment(),
    searchField:'',
    checkInType:'details',
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {
      const { checkInType} = yield select(state => state.appear)

      if(checkInType==='details'){
        yield put({ type: 'appearDetails/query' })
      }
      else if(checkInType==='headTeacher'){
        yield put({ type: 'appearAdvisor/query'})
      }
      else if(checkInType==='teacher'){
        yield put({ type: 'appearTeacher/query'})
      }
      else{
        yield put({ type: 'appearSales/query'})
      }

    },
    *updateDate({ payload: changedDate }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { selectDate: changedDate } })
      yield put({ type: 'query' })
    },
    *updateSearchField({ payload: searchField }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField } })
      yield put({ type: 'query' })
    },
    *clearSearch(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField: '' } })
      yield put({ type: 'query' })
    },
    *switchTab({ payload: checkInType }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { checkInType } })
      if(checkInType==='details'){
        yield put(routerRedux.push({pathname: appearDetailsPage}))
      }
      else if(checkInType==='headTeacher'){
        yield put(routerRedux.push({pathname: appearAdvisorPage}))
      }
      else if(checkInType==='teacher'){
        yield put(routerRedux.push({pathname: appearTeacherPage}))
      }
      else{
        yield put(routerRedux.push({pathname: appearSalesPage}))
      }
    },

    *checkLocation(_, { take, put, call, select }) {
      const { checkInType} = yield select(state => state.appear)
      if(locationMenu[location.pathname]!==checkInType){
        yield put({ type: 'updateState', payload: { checkInType:locationMenu[location.pathname]}})
      }
    },
    *clearDate(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { selectDate: moment() } })
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(appearPage) !== -1) {
          dispatch({ type: 'clearDate' })
          dispatch({ type: 'checkLocation' })
        }
      })
    }
  }
}
