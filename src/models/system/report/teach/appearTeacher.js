import { pageConfig, matchDynamicForm } from 'utils'
import queryString from 'query-string'
import moment from 'moment'
import { SubjectCourseStatictis } from 'services/centerService/LocationStatistics'

const { report } = pageConfig
const { appearTeacher } = report


export default {
  namespace: 'appearTeacher',
  state: {
    appearTeacherList: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    filters: {},
    sorter: {},
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query(_, { take, put, call, select }) {
      const { searchField, selectDate } = yield select(state => state.appear)
      const { unitId } = yield select(state => state.report)
      const { pagination, filters, sorter } = yield select(state => state.appearTeacher)
      let query = { skipCount: 0, maxResultCount: 10 }
      let currentPageIndex = 1
      let currentPageSize = 10
      if (pagination !== undefined) {
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      if (searchField !== undefined) {
        query = { ...query, filter: searchField }
      }

      // if(filters!==undefined){}
      // if(sorter!==undefined){}
      query = { ...query, year: parseInt(selectDate.format('YYYY')), month: parseInt(selectDate.format('MM')), aggregateType: 'Teacher', organizationUnitId: unitId }
      const res = yield call(SubjectCourseStatictis, query)
      if (res.success) {
        const { items, totalCount } = res.result
        const list = items.map(item => {
          const { teacher } = item
          return {
            ...item,
            teacherName: teacher?teacher.name:'',
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, appearTeacherList: list, filters, sorter} })
      }

    },
    *changeTable({ payload: data }, { take, put, call, select }) {
      const { pagination, filters, sorter } = data
      yield put({ type: 'updateState', payload: { pagination, filters, sorter } })
      yield put({ type: 'query' })
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { appearTeacherList: [], pagination:defaultPagination } })
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(appearTeacher) === 0) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query' })
        }
      })
    }
  }
}
