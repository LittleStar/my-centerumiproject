import { pageConfig, matchDynamicForm } from 'utils'
import { routerRedux } from 'dva/router'
import queryString from 'query-string'
import moment from 'moment'
import { GetAvailableOrganizationUnits } from 'services/centerService/LocationStatistics'

const { report } = pageConfig
const { teach: teachPage } = report


export default {
  namespace: 'teach',
  state: {
    organizationUnitsList: [],
    firstUnit: '',
    unitId: null,
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query(_, { take, put, call, select }) {
      let { unitId } = yield select(state => state.teach)
      const res = yield call(GetAvailableOrganizationUnits)
      if (res.success) {
        const organizationUnitsList = res.result
        let firstUnit = organizationUnitsList.length === 0 ? '' : organizationUnitsList[0].displayName
        if(unitId===null){
          unitId = organizationUnitsList.length === 0 ? null : organizationUnitsList[0].id
        }
        yield put({ type: 'updateState', payload: { organizationUnitsList, firstUnit, unitId } })
        yield put({ type: 'monthCost/query' })
        yield put({ type: 'teacherCourse/query' })
        yield put({ type: 'appear/query' })
      }
    },
    *changeUnit({ payload: data }, { take, put, call, select }) {
      const unitId = data.unitId
      yield put({ type: 'updateState', payload: { unitId } })
      yield put({ type: 'query' })
    },


  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(teachPage) !== -1) {
          dispatch({ type: 'query' })
        }
      })
    }
  }
}
