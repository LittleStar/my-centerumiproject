import { pageConfig, matchDynamicForm } from 'utils'
import queryString from 'query-string'
import moment from 'moment'
import { TeacherTeachingStatistics } from 'services/centerService/LocationStatistics'

const { report } = pageConfig
const { teacherCourse } = report

const courseTypeEnum = ['一对多','一对一']

export default {
  namespace: 'teacherCourse',
  state: {
    list: [],
    listSupport:{
    },
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    selectDate: moment(),
    searchField:'',
    filters:{},
    sorter:{},
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {
      const { selectDate, searchField } = yield select(state => state.teacherCourse)
      const { unitId } = yield select(state => state.report)
      let { pagination, filters, sorter } = queryData || {}

      let query = { skipCount: 0, maxResultCount: 10 }
      let currentPageIndex = 1
      let currentPageSize = 10
      if (pagination !== undefined) {
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      if (searchField !== undefined) {
        query = { ...query, filter: searchField }
      }

      query = { ...query, year: parseInt(selectDate.format('YYYY')), month: parseInt(selectDate.format('MM')), organizationUnitId: unitId}
      const res = yield call(TeacherTeachingStatistics,query)

      if(res.success){
        const {data,courses} = res.result
        let coursesObj = {}
        courses.forEach((courseItem)=>{
          const courseTitle = courseItem.title +'('+ courseTypeEnum[courseItem.courseType]+')'
          coursesObj = {
            ...coursesObj,
            [courseItem.id]:{
              'showText': courseTitle,
              'showType': 'Text',
            }
          }
        })

        const listSupport = {
          centerName: {
            showText: '中心名称',
            showType: 'Text',
          },
          teacherName: {
            showText: '老师姓名',
            showType: 'Text',
          },
          ...coursesObj,
          totalCount: {
            showText: '总排课量',
            showType: 'Text',
          },
        }

        const list = data.map((item)=>{
          const {organizationUnit,isTotal,courseCount,teacher,totalCount} = item
          const teacherName = isTotal?'总计':(teacher?teacher.name:'')
          let courseCountObj = {}
          Object.keys(courseCount).forEach((courseCountKey)=>{
            courseCountObj = {...courseCountObj,[courseCountKey]:courseCount[courseCountKey]}
          })
          return{
            ...item,
            centerName:organizationUnit?organizationUnit.displayName:'',
            teacherName,
            ...courseCountObj
          }
        })

        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: data.length-1 }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list, listSupport, filters, sorter} })
      }

    },
    *updateDate({ payload: changedDate }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { selectDate: changedDate } })
      yield put({ type: 'query' })
    },
    *updateSearchField({ payload: searchField }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField } })
      yield put({ type: 'query' })
    },
    *clearSearch(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField: '' } })
      yield put({ type: 'query' })
    },
    *clearDate(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { selectDate: moment() } })
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { list: [], pagination:defaultPagination } })
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(teacherCourse) === 0) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'clearDate' })
          dispatch({ type: 'query' })
        }
      })
    }
  }
}
