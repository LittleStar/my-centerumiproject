import { routerRedux } from 'dva/router'
import { pageConfig, matchDynamicForm } from 'utils'
import moment from 'moment'
import { ClassStatistics } from 'services/centerService/LocationStatistics'

const { report, detail } = pageConfig
const { monthCost } = report
const { userDetail } = detail

const displayDateTypeMenu = {
  '0': '年',
  '1': '月',
  '2': '周',
  '3': '天',
}

export default {
  namespace: 'monthCost',
  state: {
    list: [],
    totalCostCourseCount: '',
    totalCostCoursePrice: '',
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    selectDate: moment(),
    searchField: '',
    sorter: {},
    filters: {},
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {
      const { selectDate, searchField } = yield select(state => state.monthCost)
      const { unitId } = yield select(state => state.report)
      let { pagination, filters, sorter } = queryData || {}

      let query = { skipCount: 0, maxResultCount: 10 }
      let currentPageIndex = 1
      let currentPageSize = 10
      if (pagination !== undefined) {
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      if (searchField !== undefined) {
        query = { ...query, filter: searchField }
      }

      // if(filters!==undefined){}
      // if(sorter!==undefined){}

      query = { ...query, year: parseInt(selectDate.format('YYYY')), month: parseInt(selectDate.format('MM')), organizationUnitId: unitId }
      const res = yield call(ClassStatistics, query)
      if (res.success) {
        const { items, totalCount, totalCostCourseCount, totalCostCoursePrice } = res.result
        const list = items.map(item => {
          const { organizationUnit, subject, userContractAggregateInfo, costCourseCount, startCourseCount, costCoursePrice } = item
          const { startTime, coursePackage, period, displayDateType, totalPaymentPrice, courseTotalCount, averagePaymentPrice } = userContractAggregateInfo
          const subjectGuardian = subject.guardians[0]
          return {
            ...item,
            centerName: organizationUnit ? organizationUnit.displayName : '',
            creationTime: startTime,
            subjectName: subject ? subject.name : '',
            guardiansPhone: subjectGuardian ? subjectGuardian.phoneNumber : '',
            period: period + displayDateTypeMenu[displayDateType],
            realPayment: totalPaymentPrice,
            packageCount: courseTotalCount,
            unitPrice: averagePaymentPrice,
            startCourseCount,
            costCourseCount,
            costCoursePrice,
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list, totalCostCourseCount, totalCostCoursePrice, filters, sorter } })

      }

    },
    *updateDate({ payload: changedDate }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { selectDate: changedDate } })
      yield put({ type: 'query' })
    },
    *updateSearchField({ payload: searchField }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField } })
      yield put({ type: 'query' })
    },
    *clearSearch(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField: '' } })
      yield put({ type: 'query' })
    },
    *clearDate(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { selectDate: moment() } })
    },

    // 跳转到详情页
    *goToUserDetail({ payload: id }, { take, put, call, select }) {
      yield put(routerRedux.push({
        pathname: userDetail,
        search: '?userId=' + id
      }))
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { list: [], pagination:defaultPagination } })
    }


  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(monthCost) === 0) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'clearDate' })
          dispatch({ type: 'query' })
        }
      })
    }
  }
}
