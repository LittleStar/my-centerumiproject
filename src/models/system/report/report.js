import { pageConfig, matchDynamicForm } from 'utils'
import { routerRedux } from 'dva/router'
import queryString from 'query-string'
import moment from 'moment'
import { GetAvailableOrganizationUnits } from 'services/centerService/LocationStatistics'

const { report } = pageConfig
const { report: reportPage } = report


const pathList = {
  '/system/manage/report/sa/us': 'userSource',
  '/system/manage/report/sa/sales': 'salesData',
  '/system/manage/report/te/mc': 'monthCost',
  '/system/manage/report/te/tc': 'teacherCourse',
  '/system/manage/report/te/ap/appear': 'appear',
  '/system/manage/report/te/ap/appearDetails': 'appear',
  '/system/manage/report/te/ap/appearTeacher': 'appear',
  '/system/manage/report/te/ap/appearAdvisor': 'appear',
  '/system/manage/report/te/ap/appearSales': 'appear',
  '/system/manage/report/pa/mp': 'monthPay'
}


export default {
  namespace: 'report',
  state: {
    organizationUnitsList: [],
    firstUnit: '',
    unitId: '',
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *queryOrgUnit(_, { take, put, call, select }) {
      let { unitId } = yield select(state => state.report)
      const res = yield call(GetAvailableOrganizationUnits)
      if (res.success) {
        const organizationUnitsList = res.result
        let firstUnit = organizationUnitsList.length === 0 ? '' : organizationUnitsList[0].displayName
        if(unitId===''){
          unitId = organizationUnitsList.length === 0 ? '' : organizationUnitsList[0].id
        }
        yield put({ type: 'updateState', payload: { organizationUnitsList, firstUnit, unitId:parseInt(unitId) } })
      }
    },
    *changeUnit({ payload: unitId }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { unitId } })
      yield put({ type: `${pathList[location.pathname]}/query`})
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { organizationUnitsList: [], pagination:defaultPagination } })
    }


  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(reportPage) !== -1) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'queryOrgUnit' })
        }
      })
    }
  }
}
