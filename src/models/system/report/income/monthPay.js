import { pageConfig, matchDynamicForm } from 'utils'
import queryString from 'query-string'
import moment from 'moment'
import { MonthlySalesStatistics } from 'services/centerService/LocationStatistics'

const { report } = pageConfig
const { monthPay } = report

const weekBynum = {
  1: '星期一',
  2: '星期二',
  3: '星期三',
  4: '星期四',
  5: '星期五',
  6: '星期六',
  0: '星期日',
}

export default {
  namespace: 'monthPay',
  state: {
    list: [],
    listSupport: {},
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    selectDate: moment(),
    searchField: '',
    filters: {},
    sorter: {},
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query(_, { take, put, call, select }) {
      let { selectDate, searchField, pagination, filters, sorter } = yield select(state => state.monthPay)

      const { unitId } = yield select(state => state.report)
      // let query = { skipCount: 0, maxResultCount: 10 }
      // let currentPageIndex = 1
      // let currentPageSize = 10
      // if (pagination !== undefined) {
      //   currentPageIndex = pagination.current
      //   currentPageSize = pagination.pageSize
      //   query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      // }
      // if (searchField !== undefined) {
      //   query = { ...query, filter: searchField }
      // }

      // if(filters!==undefined){}
      // if(sorter!==undefined){}

      let query = { ...query, year: parseInt(selectDate.format('YYYY')), month: parseInt(selectDate.format('MM')), organizationUnitId: unitId }
      const res = yield call(MonthlySalesStatistics, query)
      if (res.success) {
        const { data, weeksInMonth } = res.result
        let appendListSupport = {}
        weeksInMonth.forEach(item => {
          const { dayInMonths, dayOfWeeks, weekInMonth } = item
          const weeksInMonthTitle = `${dayInMonths[0]}-${dayInMonths[dayInMonths.length - 1]}(${weekBynum[dayOfWeeks[0]]}~${weekBynum[dayOfWeeks[dayOfWeeks.length - 1]]})`
          appendListSupport = {
            ...appendListSupport,
            [weekInMonth]: {
              showText: weeksInMonthTitle,
              showType: 'Text',
            }
          }
        })

        const listSupport = {
          location: {
            showText: '区域',
            showType: 'Text',
          },
          centerName: {
            showText: '中心名称',
            showType: 'Text',
          },
          ...appendListSupport,
          salesTotal: {
            showText: '现金收入',
            showType: 'Text',
          },
          refundTotal: {
            showText: '退课',
            showType: 'Text',
          },
          actualSalesTotal: {
            showText: '实际营业收入',
            showType: 'Text',
          }
        }
        const newPagination = { current: 1, pageSize: 70, total: data.length===0?0:data.length-1 }
        const list = data.map(item => {
          const { organizationUnit, weeklyData, isTotal } = item
          const centerName = isTotal ? '总计' : (organizationUnit ? organizationUnit.displayName : '')
          let weeklyDataObj = {}
          Object.keys(weeklyData).forEach((weeklyKey) => {
            weeklyDataObj = { ...weeklyDataObj, [weeklyKey]: weeklyData[weeklyKey] }
          })
          return {
            ...item,
            centerName,
            ...weeklyDataObj,
          }
        })
        yield put({ type: 'updateState', payload: { list, listSupport, pagination: newPagination } })
      }
    },
    *updateDate({ payload: changedDate }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { selectDate: changedDate } })
      yield put({ type: 'query' })
    },
    *updateSearchField({ payload: searchField }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField } })
      yield put({ type: 'query' })
    },
    *clearSearch(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField: '' } })
      yield put({ type: 'query' })
    },
    *clearDate(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { selectDate: moment() } })
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { list: [], pagination:defaultPagination } })
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(monthPay) === 0) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'clearDate' })
          dispatch({ type: 'query' })
        }
      })
    }
  }
}
