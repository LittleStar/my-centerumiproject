import { pageConfig, matchDynamicForm } from 'utils'
import queryString from 'query-string'
import moment from 'moment'
import { SalesStatistics } from 'services/centerService/LocationStatistics'

const { report } = pageConfig
const { salesData } = report


export default {
  namespace: 'salesData',
  state: {
    list: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    searchField: '',
    dateRange: {
      startTime: moment().startOf('month'),
      endTime: moment().endOf('month'),
    }
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {
      const { dateRange, searchField } = yield select(state => state.salesData)
      const { unitId } = yield select(state => state.report)
      const { pagination, filters, sorter } = queryData || {}
      let query = { skipCount: 0, maxResultCount: 10, filter: searchField }
      query = dateRange === null ? { ...query } : { ...query, startTime: dateRange.startTime.format(), endTime: dateRange.endTime.format(), organizationUnitId: unitId  }
      let currentPageIndex = 1
      let currentPageSize = 10

      if (pagination !== undefined) {
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }
      const res = yield call(SalesStatistics, query)
      if (res.success) {
        const resData = res.result
        const { items, totalCount } = resData
        const list = items.map(item => {
          const { organizationUnit, sales, subjectCount, dealCount, transitPercent } = item
          return {
            ...item,
            salesName: sales ? sales.name : '',
            organizationUnitName: organizationUnit ? organizationUnit.displayName : '',
            userTransitPercent: (transitPercent.toFixed(2))*100 + '%',
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list } })
      }
    },

    *updateSearchField({ payload: searchField }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField } })
      yield put({ type: 'query' })
    },
    *updateDate({ payload: changedDate }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { dateRange: changedDate } })
      yield put({ type: 'query' })
    },
    *clearSearch(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { searchField: '' } })
      yield put({ type: 'query' })
    },
    *clearDate(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { dateRange: { startTime: moment().startOf('month'), endTime: moment().endOf('month') } } })
    },
    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { list: [], pagination:defaultPagination } })
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(salesData) === 0) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'clearDate' })
          dispatch({ type: 'query' })
        }
      })
    }
  }
}
