import { pageConfig, matchDynamicForm } from 'utils'
import queryString from 'query-string'
import moment from 'moment'
import { UserSourceStatistics } from 'services/centerService/LocationStatistics'

const { report } = pageConfig
const { userSource } = report


export default {
  namespace: 'userSource',
  state: {
    list: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    dateRange: {
      startTime: moment().startOf('month'),
      endTime: moment().endOf('month'),
    },
    userSourceInfo: {},
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {
      const { dateRange, pagination } = yield select(state => state.userSource)
      const { unitId } = yield select(state => state.report)
      let query = dateRange === null ? {} : { startTime: dateRange.startTime.format(), endTime: dateRange.endTime.format(), organizationUnitId: unitId }
      const res = yield call(UserSourceStatistics, query)
      if (res.success) {
        const resData = res.result
        const data = resData.data
        const list = data.map(item => {
          const { source, subjectCount, dealCount, transitPercent, percent, organizationUnit } = item
          return {
            ...item,
            sourcePercent: (percent*100).toFixed(2)+'%',
            sourceName: source ? source.value : '',
            organizationUnitName: organizationUnit ? organizationUnit.displayName : '',
            userTransitPercent: (transitPercent.toFixed(2))*100+'%',
          }
        })
        const newPagination = { ...pagination, total: data.length }
        yield put({ type: 'updateState', payload: { userSourceInfo: resData, list, pagination: newPagination } })
      }
    },

    *updateDate({ payload: changedDate }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { dateRange: changedDate } })
      yield put({ type: 'query' })
    },

    *clearDate(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { dateRange: { startTime: moment().startOf('month'), endTime: moment().endOf('month') } } })
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { list: [], pagination:defaultPagination } })
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(userSource) === 0) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'clearDate' })
          dispatch({ type: 'query' })
        }
      })
    }
  }
}
