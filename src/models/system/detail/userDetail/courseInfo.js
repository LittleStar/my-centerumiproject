import { routerRedux } from 'dva/router'
import { pageConfig, matchUserInfo, matchDynamicForm } from 'utils'
import queryString from 'query-string'
import { message } from 'antd'
import { GetCurrentSubjectForHeadteacher, LinkSubjectHeadTeacher } from 'services/centerService/User'
import {
  GetUserCourseInstanceList, FindCourseInstanceFeedback, UpdateCourseInstanceUserFeedback,
  GetCourseInstanceUserFeedbackForEdit, DeleteCourseInstanceFeedback,
} from 'services/centerService/Teaching'

const { detail } = pageConfig
const { userDetail, classDetail,classSubjectList } = detail
const userStatusList = ['未签到', '完成课程', '请假', '旷课']

export default {

  namespace: 'courseInfo',

  state: {
    userId: null,
    modalVisible: false,
    modalType: null,
    fbModalType: null,
    content: null,

    currentTeacher: null,

    courseType: 'finished',
    courseList: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    feedbackPagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    courseLoading: true,
    feedbackModalVisible: false,
    ILSModalVisible: false,
    feedbackList: [],
    feedbackId: null,
    courseInstanceId: null,
    editFeedbackModalVisible: false,
    selectedILS: '',
    contentType: 'text',
    ilsOptions: [],
  },

  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload
      }
    },
  },

  effects: {

    *queryPage({ payload: data }, { call, put, select }) {
      const { userId } = data
      yield put({ type: 'updateState', payload: { userId } })
      yield put({ type: 'queryTeacher' })
      yield put({ type: 'queryCourse' })
    },

    *queryTeacher(_, { call, put, select }) {
      const { userId } = yield select(state => state.courseInfo)
      const thRes = yield call(GetCurrentSubjectForHeadteacher, userId)
      // 班主任
      if (thRes.success) {
        const { current: currentTeacher } = thRes.result
        yield put({ type: 'updateState', payload: { currentTeacher } })
      }
    },

    //tabKey: finished/unfinished
    *switchTab({ payload: tabKey }, { call, put, select }) {
      const defualtPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { courseType: tabKey, pagination: defualtPagination, courseLoading: true } })
      yield put({ type: 'queryCourse' })
    },

    *switchPagination({ payload: pagination }, { call, put, select }) {
      yield put({ type: 'updateState', payload: { pagination } })
      yield put({ type: 'queryCourse' })
    },

    *queryCourse({ payload: queryData }, { call, put, select }) {

      const { pagination, userId, courseType } = yield select(state => state.courseInfo)

      let currentPageIndex = pagination.current
      let currentPageSize = pagination.pageSize
      let query = { skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      query = { ...query, subjectId: userId }
      const isFinish = courseType === 'finished' ? true : false
      //todo courseType
      query = { ...query, isFinish }
      const courseRes = yield call(GetUserCourseInstanceList, query)
      if (courseRes.success) {
        const { items, totalCount } = courseRes.result
        const courseList = items.map((courseItem) => {
          const { courseInstance, userStatus, creatorUser, creationTime } = courseItem
          const { course, classroom, actualTime, startCoursePeriod, teacher } = courseInstance
          return {
            ...courseItem,
            teacherName: teacher ? teacher.name : null,
            coursePeriod: startCoursePeriod ? startCoursePeriod.startTime + '-' + startCoursePeriod.endTime : null,
            classroomName: classroom ? classroom.name : '',
            courseName: course ? course.title : '',
            userStatus: userStatusList[userStatus],
            creatorUser: creatorUser ? creatorUser.name : '',
            actualTime: actualTime,
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { courseList, pagination: newPagination, courseLoading: false } })
      }
    },

    // 分配/重新分配班主任
    *onAssignTeacher({ payload }, { take, call, put, select }) {
      const { userId } = yield select(state => state.courseInfo)
      const res = yield call(GetCurrentSubjectForHeadteacher, userId)
      if (res.success) {
        const { available, current } = res.result
        const { items: teacherItems } = available

        let assignTeacherForm = {
          "Properties": [{
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "班主任列表",
            "FormKey": "headteacherId",
            "Description": null,
          },
          ]
        }

        const teacherIndex = assignTeacherForm.Properties.findIndex((item) => {
          return item.FormKey === 'headteacherId'
        })

        if (teacherIndex !== -1) {
          const teacherOptions = teacherItems.map((item) => {
            return { "Value": item.name, "Id": item.id.toString() }
          })
          //要具体看是什么组件
          assignTeacherForm.Properties[teacherIndex].Setting.DropdownOptions = teacherOptions
        }
        // 重新分配班主任的原因？？？
        if (current) {
          const matchValue = { headteacherId: current.id }
          assignTeacherForm = matchDynamicForm(assignTeacherForm, matchValue)
        }

        yield put({ type: 'updateState', payload: { modalType: 'assignTeacher', content: assignTeacherForm, modalVisible: true } })
      }
    },

    *checkFeedback({ payload: data }, { take, call, put, select }) {
      const { courseInstanceId, contentType } = data
      yield put({ type: 'getFeedback', payload: data })
      yield put({ type: 'updateState', payload: { feedbackModalVisible: true, feedbackList: [], modalType: 'checkFeedback', courseInstanceId, contentType, } })
    },

    *getFeedback({ payload: data }, { take, call, put, select }) {
      const { courseInstanceId, contentType } = data
      const { userId } = yield select(state => state.courseInfo)
      const postData = {
        contentType: [contentType],
        courseInstanceId,
        toUserId: userId
      }

      const res = yield call(FindCourseInstanceFeedback, postData)
      if (res.success) {
        const { items, totalCount } = res.result
        const feedbackList = items.map(item => {
          if (item.contentType === contentType) return { feedbackContent: item.content, feedbackId: item.id, courseInstanceId, contentType }
        })
        const newPagination = { current: 1, pageSize: 10, total: totalCount }
        yield put({ type: 'updateState', payload: { feedbackPagination: newPagination, feedbackList } })
      }
    },

    *editFeedback({ payload: data }, { take, call, put, select }) {
      const { feedbackId, contentType, feedbackContent } = data

      const res = yield call(GetCourseInstanceUserFeedbackForEdit, { ContentType: contentType, Id: feedbackId })
      if (res.success) {
        const resData = res.result
        if (resData.contentType === 'text') {
          const emptyFeedbackForm = {
            Properties: [{
              "EditorType": "Input",
              "ShowTitle": "课堂反馈",
              "FormKey": "feedback",
              "AdditionalData": null,
              "Value": null,
              "Setting": {
                "Required": true
              },
              "Description": null
            }]
          }
          const values = {
            feedback: resData.content
          }
          const editFeedbackForm = matchDynamicForm(emptyFeedbackForm, values)
          yield put({ type: 'updateState', payload: { content: editFeedbackForm, editFeedbackModalVisible: true, fbModalType: 'editFeedback', feedbackId } })
        } else {
          const { format } = resData
          yield put({ type: 'updateState', payload: { ilsOptions: format.children, ILSModalVisible: true, fbModalType: 'ils', selectedILS: feedbackContent, feedbackId } })
        }
      }
    },

    *deleteFeedback({ payload: data }, { take, call, put, select }) {
      const { feedbackId } = data
      const res = yield call(DeleteCourseInstanceFeedback, { Id: feedbackId })
      if (res.success) {
        message.success('删除成功!')
        yield put({ type: 'getFeedback', payload: data })
      }
    },

    *onSubmitModal({ payload: data }, { take, call, put, select }) {
      const { modalType, userId, fbModalType, feedbackId, courseInstanceId, contentType } = yield select(state => state.courseInfo)

      switch (modalType) {
        case 'assignTeacher':
          const assignTeacherPostData = { ...data, subjectIds: [userId] }
          const assignHTRes = yield call(LinkSubjectHeadTeacher, assignTeacherPostData)
          if (assignHTRes.success) {
            yield put({ type: 'queryTeacher' })
            yield put({ type: 'updateState', payload: { modalVisible: false } })
          }
          break
        case 'addFreeCourse':
        default:
          break
      }
      switch (fbModalType) {
        case 'editFeedback':
          const feedbackContent = data.feedback
          const postFeedbackData = { content: feedbackContent, id: feedbackId }
          const res = yield call(UpdateCourseInstanceUserFeedback, postFeedbackData)
          if (res.success) {
            message.success('修改成功!')
            yield put({ type: 'getFeedback', payload: { courseInstanceId, contentType } })
            yield put({ type: 'updateState', payload: { editFeedbackModalVisible: false } })
          } else {
            message.success('修改失败!')
          }
          break
        case 'ils':
          const { ils } = data
          const postILSdata = {
            content: ils.length > 0 ? ils.pop() : '',
            id: feedbackId
          }
          const ILSRes = yield call(UpdateCourseInstanceUserFeedback, postILSdata)
          if (ILSRes.success) {
            message.success('ILS进度修改成功!')
            yield put({ type: 'getFeedback', payload: { courseInstanceId, contentType } })
            yield put({ type: 'updateState', payload: { ILSModalVisible: false } })
          } else {
            message.error('ILS进度修改失败!')
          }
          break
        default:
          break
      }
    },

    *goToCourseDetail({ payload: id }, { call, put, select }) {
      yield put(routerRedux.push({
        pathname: classSubjectList,
        search: '?ck=' + id
      }))
    },

    *clearModel(_, { take, put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { courseList: [], currentTeacher: null, pagination: defaultPagination } })
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname.search(userDetail + '/courseInfo') !== -1) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'queryPage', payload: { ...queryString.parse(location.search) } })
        }
      })
    }

  },
}
