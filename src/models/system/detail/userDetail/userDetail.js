import { routerRedux } from 'dva/router'
import { pageConfig, matchUserInfo, matchDynamicForm } from 'utils'
import queryString from 'query-string'
import { GetSubjectEditInfo, GetCurrentSubjectForSales, GetCurrentSubjectForHeadteacher,GetSubjectDetail } from 'services/centerService/User'

const { detail } = pageConfig
const { userDetail } = detail

export default {

  namespace: 'userDetail',

  state: {
    userInfo:{},
  },

  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload
      }
    },
  },

  effects: {
    *query({ payload: data }, { call, put, select }) {
      const {userId} = data
      const subjectRes = yield call(GetSubjectDetail, userId)
      if(subjectRes.success){
        yield put({ type: 'updateState', payload: { userInfo:subjectRes.result} })
      }
    },

    *clearModel(_, { take, put, call, select }){
      yield put({ type: 'updateState', payload: { userInfo: {} } })
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        // 现在是路径里面包含了userDetail的话就要刷新一下这个组件，不然可能会出现loading不出来的问题，to fix later
        if (location.pathname.search(userDetail)!==-1) {
          dispatch({type:'clearModel'})
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    }
  },
}
