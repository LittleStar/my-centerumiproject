import { routerRedux } from 'dva/router'
import { pageConfig, matchDynamicForm, matchUserInfo } from 'utils'
import queryString from 'query-string'
import { GetCurrentSubjectForSales, LinkSubjectSales, GetCurrentSubjectForHeadteacher, LinkSubjectHeadTeacher, GetSubjectDetail } from 'services/centerService/User'

const { detail } = pageConfig
const { userDetail } = detail

export default {

  namespace: 'basicInfo',

  state: {

    userInfo:null,
    sales:null,
    headteacher:null,
    currentCoursePackage:null,

    modalVisible: false,
    modalType: null,
    content: null,
  },

  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload
      }
    },
  },

  effects: {
    *query({ payload: data }, { call, put, select }) {
      const {userId} = data
      const subjectRes = yield call(GetSubjectDetail, userId)
      if(subjectRes.success){
        const {sales,headteacher,currentCoursePackage} = subjectRes.result
        yield put({ type: 'updateState', payload: { userInfo:subjectRes.result, sales, headteacher, currentCoursePackage} })
      }
    },

    // 分配/重新分配课程顾问
    *onAssignSales({ payload }, { take, call, put, select }) {
      const { userInfo, sales } = yield select(state => state.basicInfo)
      const { id } = userInfo
      const res = yield call(GetCurrentSubjectForSales, id)
      if (res.success) {
        const { available, current } = res.result
        const { items: salesItems } = available

        let assignSalesForm = {
          "Properties": [{
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "课程顾问列表",
            "FormKey": "salesId",
            "Description": null,
          },
          ]
        }

        const salesIndex = assignSalesForm.Properties.findIndex((item) => {
          return item.FormKey === 'salesId'
        })

        if (salesIndex !== -1) {
          const salesOptions = salesItems.map((item) => {
            return { "Value": item.name, "Id": item.id.toString() }
          })

          assignSalesForm.Properties[salesIndex].Setting.DropdownOptions = salesOptions
        }

        if (current) {
          const matchValue = { salesId: current.id }
          assignSalesForm = matchDynamicForm(assignSalesForm, matchValue)
        }

        yield put({ type: 'updateState', payload: { modalType: 'assignSales', content: assignSalesForm, modalVisible: true } })
      }
    },

    // 分配/重新分配班主任
    *onAssignTeacher({ payload }, { take, call, put, select }) {
      const { userInfo } = yield select(state => state.basicInfo)
      const { id:userId } = userInfo

      const res = yield call(GetCurrentSubjectForHeadteacher, userId)
      if (res.success) {
        const { available, current } = res.result
        const { items: teacherItems } = available

        let assignTeacherForm = {
          "Properties": [{
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "班主任列表",
            "FormKey": "headteacherId",
            "Description": null,
          },]
        }

        const teacherIndex = assignTeacherForm.Properties.findIndex((item) => {
          return item.FormKey === 'headteacherId'
        })

        if (teacherIndex !== -1) {
          const teacherOptions = teacherItems.map((item) => {
            return { "Value": item.name, "Id": item.id.toString() }
          })
          assignTeacherForm.Properties[teacherIndex].Setting.DropdownOptions = teacherOptions
        }

        if (current) {
          const matchValue = { headteacherId: current.id }
          assignTeacherForm = matchDynamicForm(assignTeacherForm, matchValue)
        }
        yield put({ type: 'updateState', payload: { modalType: 'assignTeacher', content: assignTeacherForm, modalVisible: true } })
      }
    },

    // 提交
    *onSubmitModal({ payload: data }, { take, call, put, select }) {
      const { modalType, userInfo } = yield select(state => state.basicInfo)
      const { id:userId} = userInfo

      switch (modalType) {
        case 'assignSales':
          const assignSalesPostData = { ...data, subjectIds: [userId] }
          const assignSaleRes = yield call(LinkSubjectSales, assignSalesPostData)
          if (assignSaleRes.success) {
            yield put({ type: 'query', payload: { userId: userId } })
            yield put({ type: 'updateState', payload: { modalVisible: false }})
          }
          break
        case 'assignTeacher':
          const assignTeacherPostData = { ...data, subjectIds: [userId] }
          const assignHTRes = yield call(LinkSubjectHeadTeacher, assignTeacherPostData)
          if (assignHTRes.success) {
            yield put({ type: 'query', payload: { userId: userId } })
            yield put({ type: 'updateState', payload: { modalVisible: false }})
          }
          break
        case 'addFreeCourse':
        default:
          break
      }
    },

    *clearModel(_, { take, put, call, select }){
      yield put({ type: 'updateState', payload: { sales: null, headteacher: null, currentCoursePackage: null } })
    }

  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === userDetail+ '/basicInfo') {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    }
  },
}
