import { routerRedux } from 'dva/router'
import { pageConfig, enumSetting,matchDynamicForm } from 'utils'
import {message} from 'antd'
import queryString from 'query-string'
import { UserContractList,ApplyForContract,CommitContract,GetUserAvailableContract } from '../../../../services/centerService/Contract'
import { GetSubjectDetail } from 'services/centerService/User'

const { detail } = pageConfig
const { contractInfo, contractDetail } = detail

const { displayDateType, coursePackageType,currencyType } = enumSetting

const typeTitle={
  'New':'续费',
  'Upgrade':'升包',
  'ModifyPackage':'更改课包'
}

export default {

  namespace: 'contractInfo',

  state: {
    content: null,
    modalVisible: false,
    modalType: null,
    contractList: [],
    contractListSupport: {
      creationTime: {
        showText: '申请时间',
        showType: 'Time',
        addtional: {
          format: 'MM/DD/YYYY HH:mm'
        }
      },
      subjectName: {
        showText: '学生姓名',
        showType: 'Text',
      },
      contractType: {
        showText: '合同类型',
        showType: 'Status',
        addtional: {
          statusArray: ['新合同', '升包', '课包更改']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '新合同', value: 'New' },
            { text: '升包', value: 'Upgrade' },
            { text: '课包更改', value: 'ModifyPackage' },
          ],
        }
      },
      coursePackageType: {
        showText: '课包类型',
        showType: 'Status',
        addtional: {
          statusArray: ['一对多', '一对一']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '一对多', value: 'OneToMany' },
            { text: '一对一', value: 'OneToOne' },
          ],
        }
      },
      price: {
        showText: '原价',
        showType: 'Text',
      },
      discountPrice: {
        showText: '折扣金额',
        showType: 'Text',
      },
      realPayment: {
        showText: '实付金额',
        showType: 'Text',
      },
      // expireTime: {
      //   showText: '到期日期',
      //   showType: 'Time',
      //   addtional: {
      //     format: 'MM/DD/YYYY HH:mm'
      //   }
      // },
      contractStatus: {
        showText: '合同状态',
        showType: 'Status',
        addtional: {
          statusArray: ['合同草稿', '待审核', '未通过', '已通过', '过期', '已关闭']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '合同草稿', value: 'None' },
            { text: '待审核', value: 'Commited' },
            { text: '未通过', value: 'Deny' },
            { text: '已通过', value: 'Confirmed' },
            { text: '过期', value: 'Expired' },
            { text: '已关闭', value: 'Cancelled' },
          ],
        }
      },
    },
    pagination: {
      current: 1,
      pageSize: 10,
      total: 37
    },
    userId:null,
    userInfo: null,
  },

  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload
      }
    },
  },

  effects: {
    *query({ payload: queryData }, { select, call, put }) {
      const {userId: id} = queryData
      const subjectRes = yield call(GetSubjectDetail, id)

      const { pagination, filters, sorter, searchField, userId} = queryData || {}
      let query = { subjectId: userId, skipCount: 0, maxResultCount: 10 }
      let currentPageIndex = 1
      let currentPageSize = 10
      if (pagination !== undefined) {
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }
      if (searchField !== undefined) {
        query = { ...query, filter: searchField }
      }
      const res = yield call(UserContractList, query)
      if (res.success) {
        const { items: contractItems, totalCount } = res.result
        const list = contractItems.map((contractItem)=>{
          const {subject,discount,paymentPrice,status,coursePackage, price} = contractItem
          return{
            ...contractItem,
            subjectName: subject?subject.name:'',
            discountPrice: discount,
            realPayment: paymentPrice,
            contractStatus: status,
            coursePackageType: coursePackage?coursePackage.coursePackageType:'',
            price: price,
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, userId, contractList: list, userInfo:subjectRes.result } })
      }
    },

    //New/Upgrade/ModifyPackage
    *onNewContract({ payload: type }, { take, put, call, select }) {

      const { userId } = yield select(state => state.contractInfo)
      const postSubjectInfo = { subjectId:userId, "contractType": type}
      const contractInfo = yield call(ApplyForContract,postSubjectInfo)
      //从后端获取该用户是否已经申请了新的合同
      if(!contractInfo.success){
        return
      }
      const {userContract, availableCoursePackages,availableResponseUsers} = contractInfo.result
      const { subject, paymentPrice } = userContract
      const { restAllCourseCount,currentCoursePackage:coursePackage } = subject

      const applyForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "当前课包类型",
          "FormKey": "coursePackageType",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": false,
            "Disabled": true,
          },
          "Description": null
        },{
          "EditorType": "Input",
          "ShowTitle": "剩余课时数/课时数",
          "FormKey": "count",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": false,
            "Disabled": true,
          },
          "Description": null
        },{
          "EditorType": "Input",
          "ShowTitle": "已付金额总计",
          "FormKey": "pay",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": false,
            "Disabled": true,
          },
          "Description": null
        },{
          "EditorType": "Dropdown",
          "ShowTitle": "购买课包类型",
          "FormKey": "coursePackageId",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [],
            "Required": true
          },
          "Description": null
        },{
          "EditorType": "NumberInput",
          "ShowTitle": "折扣金额",
          "FormKey": "discount",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true,
            'NumberSetting':{
              min:0,
              step:1,
              precision: 2,
            }
          },
          "Description": null
        },{
          "EditorType": "NumberInput",
          "ShowTitle": "实付金额",
          "FormKey": "paymentPrice",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true,
            'NumberSetting':{
              min:0,
              step:1,
              precision: 2,
            }
          },
          "Description": null
        }, {
          "EditorType": "Upload",
          "Value": null,
          "Setting": {
            "Required": true
          },
          "ShowTitle": "附件图片",
          "FormKey": "contractPics",
          "Description": '如合同原件、付费凭证'
        }, {
          "EditorType": "Input",
          "ShowTitle": "订单ID",
          "FormKey": "orderId",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": false
          },
          "Description": null
        },
      ]
      }
      //找到需要添加内容的位置
      const coursePackageIndex = applyForm.Properties.findIndex((item) => {
        return item.FormKey === 'coursePackageId'
      })
      //如果找到了
      if(coursePackageIndex!==-1){
        const coursePackageOptions = availableCoursePackages.map((item) => {
          const {count,coursePackageType:PackageType,displayDateType:DateType,period, price} = item
          const showValue = '课时数：'+count+'  课包类型：'+coursePackageType[PackageType]['cn']+ '  课包有效期:'+period+displayDateType[DateType]['cn']+ ` 课包原价: ${price}元`
          return { "Value": showValue , "Id": item.id }
        })
        applyForm.Properties[coursePackageIndex].Setting.DropdownOptions = coursePackageOptions
      }

      const values = {
        coursePackageType:coursePackageType[coursePackage.coursePackageType]['cn'],
        count: restAllCourseCount+'/'+coursePackage.count,
        pay: paymentPrice+'元'
      }
      const coursePackageForm = matchDynamicForm(applyForm, values)
      yield put({ type: 'updateState', payload: { content: coursePackageForm, modalType: type, modalVisible: true} })
    },

    // 提交
    *onSubmitModal({ payload: data }, { take, put, call, select }) {
      const { modalType, userId } = yield select(state => state.contractInfo)
      const applayPostData = {...data,subjectId:userId,contractType:modalType}
      const applayRes = yield call(CommitContract,applayPostData)
      if(applayRes.success){
        message.success(typeTitle[modalType]+'申请成功提交')
        yield put({ type: 'query',payload:{userId}})
        yield put({ type: 'updateState', payload: { modalVisible: false }})
      }
      else{
        message.error(typeTitle[modalType]+'申请提交失败')
      }
    },

    // 跳转到详情页
    *goToContractInfoDetail({payload: id}, {take, put, call, select}){
      yield put(routerRedux.push({
        pathname: contractDetail,
        search: '?contractId=' + id
      }))
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { contractList: [], pagination:defaultPagination } })
    }

  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname.search(contractInfo) !== -1) {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },
  },
}
