import { routerRedux } from 'dva/router'
import { pageConfig,permissionConfig, matchUserInfo, matchDynamicForm } from 'utils'
import queryString from 'query-string'
import { GetCurrentSubjectForSales, LinkSubjectSales,GetSubjectDetail } from 'services/centerService/User'
import { CreateContact,GetContactRecord } from 'services/centerService/Sales'

import {checkPermissionKey} from 'utils/utils'

const {location:locationPermission} = permissionConfig
const {SalesContact} = locationPermission

const { detail } = pageConfig
const { userDetail } = detail


export default {

  namespace: 'saleInfo',

  state: {
    userId: null,
    currentSale: null,
    commList: [],
    commListSupport: {
      creationTime: {
        showText: '沟通日期',
        showType: 'Time',
        addtional: {
          format: 'YYYY/MM/DD HH:mm:ss'
        }
      },
      fromPerson: {
        showText: '沟通人',
        showType: 'Text',
      },
    },
    pagination: {
      current: 1,
      pageSize: 6,
      total: 0
    },

    modalVisible: false,
    modalType: null,
    content: null,
  },

  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload
      }
    },
  },

  effects: {
    *query({ payload: data }, { call, put, select }) {

      const { pagination, filters, sorter, userId } = data || {}

      const subjectRes = yield call(GetSubjectDetail, userId)

      if(subjectRes.success){
        const {sales} = subjectRes.result
        yield put({ type: 'updateState', payload: { userId, currentSale:sales } })
      }

      const { permission } = yield select(state => state.user)
      const showContactList = checkPermissionKey(SalesContact,permission)

      //todo 把这段代码分离出去
      if(showContactList){
        let query = { skipCount: 0, maxResultCount: 6 }
        let currentPageIndex = 1
        let currentPageSize = 6

        if (pagination !== undefined) {
          currentPageIndex = pagination.current
          currentPageSize = pagination.pageSize
          query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
        }
        const getCommData = {...query, ToUserId:userId}
        const commRes= yield call(GetContactRecord,getCommData)
        // 课程顾问
        if (commRes.success) {
          const { items, totalCount } = commRes.result

          const list = items.map((commentItem)=>{
            const {from:fromPerson,content} = commentItem
            return{
              ...commentItem,
              fromPerson:fromPerson?fromPerson.name:'',
              extra:content,
            }
          })
          const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
          yield put({ type: 'updateState', payload: { commList:list, pagination: newPagination } })
        }
      }

    },

    // 分配/重新分配课程顾问
    *onAssignSales({ payload }, { take, call, put, select }) {
      const { userId, hasSalesman } = yield select(state => state.saleInfo)
      const res = yield call(GetCurrentSubjectForSales, userId)
      if (res.success) {
        const { available, current } = res.result
        const { items: salesItems } = available

        let assignSalesForm = {
          "Properties": [{
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "课程顾问列表",
            "FormKey": "salesId",
            "Description": null,
          },
          ]
        }

        let reAssignSalesform = {
          "Properties": [{
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "课程顾问列表",
            "FormKey": "salesId",
            "Description": null,
          }, {
            "EditorType": "Dropdown",
            "ShowTitle": "更换理由",
            "FormKey": "changeReason",
            "Value": "请选择",
            "Setting": {
              "DropdownOptions": [{
                "Value": "不喜欢",
                "Id": "不喜欢"
              }, {
                "Value": "沟通水平不好",
                "Id": "沟通水平不好"
              }, {
                "Value": "服务态度不好",
                "Id": "服务态度不好"
              }, {
                "Value": "离职",
                "Id": "离职"
              }, {
                "Value": "其他",
                "Id": "其他"
              }],
              "Required": true
            },
            "Description": null,
          },
          ]
        }

        const salesIndex = assignSalesForm.Properties.findIndex((item) => {
          return item.FormKey === 'salesId'
        })

        if (salesIndex !== -1) {
          const salesOptions = salesItems.map((item) => {
            return { "Value": item.name, "Id": item.id.toString() }
          })
          //要具体看是什么组件
          assignSalesForm.Properties[salesIndex].Setting.DropdownOptions = salesOptions
          reAssignSalesform.Properties[salesIndex].Setting.DropdownOptions = salesOptions
        }
        // 重新分配课程顾问的原因？？？
        if (current) {
          const matchValue = { salesId: current.id }
          assignSalesForm = matchDynamicForm(assignSalesForm, matchValue)
          reAssignSalesform = matchDynamicForm(reAssignSalesform, matchValue)
        }

        if (hasSalesman) {
          yield put({ type: 'updateState', payload: { modalType: 'assignSales', content: reAssignSalesform, modalVisible: true } })
        } else {
          yield put({ type: 'updateState', payload: { modalType: 'assignSales', content: assignSalesForm, modalVisible: true } })
        }
      }

    },

    // 追加沟通记录
    *onAddRecord({ payload }, { take, call, put, select }) {
      const addRecordForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "沟通记录",
          "FormKey": "content",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        }]
      }
      yield put({ type: 'updateState', payload: { modalType: 'addRecord', content: addRecordForm, modalVisible: true } })
    },

    // 提交
    *onSubmitModal({ payload: data }, { take, put, call, select }) {
      const { modalType, userId } = yield select(state => state.saleInfo)

      switch (modalType) {
        case 'assignSales':
          const assignSalesPostData = { ...data, subjectIds: [userId] }
          const assignSaleRes = yield call(LinkSubjectSales, assignSalesPostData)
          if (assignSaleRes.success) {
            yield put({ type: 'query', payload: { userId } })
            yield put({ type: 'updateState', payload: { modalVisible: false } })
          }
          break
        case 'editRecord':
          break
        case 'addRecord':
          const addRecordData = {...data,toUserId:userId}
          const addRes = yield call(CreateContact,addRecordData)
          if(addRes.success){
            yield put({ type: 'query', payload: { userId } })
            yield put({ type: 'updateState', payload: { modalVisible: false } })
          }
          break
        default:
          console.log(data)
          break
      }
    },

    *clearModel(_, { take, put, call, select }){
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { commList: [], currentSale: null, pagination:defaultPagination  } })
    }

  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === userDetail + '/salesInfo') {
          dispatch({ type: 'clearModel' })
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    }
  },
}
