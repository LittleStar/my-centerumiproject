import { routerRedux } from 'dva/router'
import { pageConfig } from 'utils'

const { detail } = pageConfig


export default {
  namespace: 'business',

  state: {
    list:[],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    }

  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {

    *query({ payload: data }, { take, put, call, select }) {

    },

  },
  subscriptions: {
    setup({ dispatch, history }) {
    },
  }
}
