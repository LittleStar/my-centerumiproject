
import queryString from 'query-string'
import { message } from 'antd'
import { pageConfig, enumSetting, matchDynamicForm } from 'utils'
import {
  GetOrganizationUnitUsers, GetDetail, ChangeOperationType, History, UserRoleRecords,
  UpdateOrganizationUnit, RemoveUserFromOrganizationUnit, FindUsers, AddUsersToOrganizationUnit
} from 'services/centerService/OrganizationUnit'
import { GetUsers, GetRolesExceptSubject, GetCurrentUserForEdit, CreateOrUpdateUser } from 'services/centerService/User'

const { detail } = pageConfig
const { centerDetail: centerDetailPage } = detail

const filterableItem = {
  role: 'role',
}
const { genderType } = enumSetting

const operationList = ['None', 'Opened', 'Closed']
const operationListCN = ['未开业', '已开业', '已停业']

export default {
  namespace: 'centerDetail',

  state: {
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    selectPagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    centerDetailInfo: {},
    changeType: 'employee',
    list: [],
    selectList: [],
    filters: {},
    roleList: [],
    modalVisible: false,
    selectModalVisible: false,
    modalType: 'editOperation',
    content: null,
    userInfo: null,
    checkedId: null,
    checkedRoleId: null,
    selectModalLoading: true,
    searchField: null,

  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
    clear(state) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      const defaultVerifyType = 'employee'
      const payload = { pagination: defaultPagination, changeType: defaultVerifyType, list: [] }
      return {
        ...state,
        ...payload,
      }
    }
  },
  effects: {

    *queryPage({ payload: queryData }, { take, put, call, select }) {
      const { orgId } = queryData
      yield put({ type: 'queryBasicInfo', payload: orgId })
      yield put({ type: 'queryList', payload: orgId })
    },

    *queryBasicInfo({ payload: organizationUnitId }, { take, put, call, select }) {
      const centerInfoRes = yield call(GetDetail, organizationUnitId)
      if (centerInfoRes.success) {
        const centerDetailInfo = centerInfoRes.result
        yield put({ type: 'updateState', payload: { centerDetailInfo } })
      }
    },

    *queryList({ payload: organizationUnitId }, { take, put, call, select }) {
      const { changeType, centerDetailInfo } = yield select(state => state.centerDetail)
      const queryOrgId = organizationUnitId || centerDetailInfo.id
      switch (changeType) {
        case 'employee':
          yield put({ type: 'queryEmployee', payload: queryOrgId })
          break
        case 'business':
          yield put({ type: 'queryBusiness', payload: queryOrgId })
          break
        case 'cdHistory':
          yield put({ type: 'queryHistory', payload: queryOrgId })
          break
        default:
          break
      }
    },

    *queryEmployee({ payload: organizationUnitId }, { take, put, call, select }) {
      const { pagination, filters } = yield select(state => state.centerDetail)

      let query = { skipCount: 0, maxResultCount: 10, organizationUnitId }
      const currentPageIndex = pagination.current
      const currentPageSize = pagination.pageSize
      query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }

      if (Object.keys(filters).length !== 0) {
        for (let item in filters) {
          if (filters[item] !== null) {
            query[filterableItem[item]] = filters[item]
          }
        }
      }

      const res = yield call(GetUsers, query)
      if (res.success) {
        const { items, totalCount } = res.result
        const list = items.map((item) => {
          const { roles, name, isActive, gender } = item
          let RolesText = ''
          roles.forEach((item) => {
            RolesText += item.roleName + ' '
          })
          return {
            ...item,
            gender: genderType[gender]['cn'],
            role: RolesText,
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list } })
        yield put({ type: 'queryRole', payload: organizationUnitId })
      }
    },

    *queryBusiness({ payload: organizationUnitId }, { take, put, call, select }) {
      const { pagination, filters } = yield select(state => state.centerDetail)
      let query = { skipCount: 0, maxResultCount: 10 }
      const currentPageIndex = pagination.current
      const currentPageSize = pagination.pageSize
      query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }


      query = { ...query, id: organizationUnitId }
      const res = yield call(History, query)
      if (res.success) {
        if (res.success) {
          const { items, totalCount } = res.result
          const list = items.map((item, index) => {
            const { changeTime, organizationUnitId, newValue, reason } = item
            return {
              ...item,
              perationType: operationListCN[newValue]
            }
          })
          const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
          yield put({ type: 'updateState', payload: { pagination: newPagination, list } })
        }
      }
    },

    *queryHistory({ payload: organizationUnitId }, { take, put, call, select }) {
      const { pagination, filters } = yield select(state => state.centerDetail)
      let query = { skipCount: 0, maxResultCount: 10 }
      const currentPageIndex = pagination.current
      const currentPageSize = pagination.pageSize
      query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }

      const postData = {
        ...query,
        organizationUnitId,
        onlyManagement: true
      }
      const res = yield call(UserRoleRecords, postData)
      if (res.success) {
        const { items, totalCount } = res.result
        const list = items.map(item => {
          const { creationTime, creatorUser, user } = item
          return {
            creationTime,
            creatorUser: creatorUser ? creatorUser.name : '',
            cdName: user ? user.name : '',
            phoneNumber: user ? user.phoneNumber : ''
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list } })
      }
    },

    *onEditRole({ payload: data }, { take, put, call, select }) {
      const { id } = data
      const { centerDetailInfo } = yield select(state => state.centerDetail)
      const postData = { id, OrganizationUnitId: centerDetailInfo.id }
      const emptyForm = {
        Properties: [{
          "EditorType": "Dropdown",
          "ShowTitle": "变更角色",
          "FormKey": "role",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "DropdownOptions": [],
            "Required": true
          },
          "Description": null
        },
        {
          "EditorType": "Input",
          "ShowTitle": "变更原因",
          "FormKey": "reason",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        },
        ]
      }
      const res = yield call(GetCurrentUserForEdit, postData)
      if (res.success) {
        const { roles } = res.result
        const roleIndex = emptyForm.Properties.findIndex((item) => {
          return item.FormKey === 'role'
        })
        if (roleIndex !== -1) {
          let roleOptions = roles.map(item => {
            return { "Value": item.roleDisplayName, "Id": item.roleId.toString() }
          })
          emptyForm.Properties[roleIndex].Setting.DropdownOptions = roleOptions
        }
      }
      yield put({ type: 'updateState', payload: { content: emptyForm, modalType: 'changeRole', modalVisible: true, userInfo: res.result } })
    },

    *queryRole({ payload: organizationUnitId }, { take, put, call, select }) {
      const allRoleRes = yield call(GetRolesExceptSubject, { organizationUnitId })
      if (allRoleRes.success) {
        const roleList = allRoleRes.result.items
        yield put({ type: 'updateState', payload: { roleList } })
      }
    },

    *changeTable({ payload: data }, { call, put, select }) {
      const { pagination, filters } = data
      yield put({ type: 'updateState', payload: { pagination, filters } })
      yield put({ type: 'queryList' })
    },

    *switchTab({ payload: tableKey }, { take, put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { changeType: tableKey, pagination: defaultPagination, filters: [] } })
      yield put({ type: 'queryList' })
    },

    *changeOperation(_, { take, put, call, select }) {
      const { centerDetailInfo } = yield select(state => state.centerDetail)
      const { organizationUnitOperationType, id } = centerDetailInfo
      const operationForm = {
        Properties: [{
          "EditorType": "Dropdown",
          "Value": null,
          "Setting": {
            "DropdownOptions": [{
              "Value": "未开业",
              "Id": "0"
            }, {
              "Value": "已开业",
              "Id": "1"
            }, {
              "Value": "已停业",
              "Id": "2"
            },],
            "Required": true
          },
          "ShowTitle": "更改运营状态",
          "FormKey": "organizationUnitType",
          "Description": null,
        }, {
          "EditorType": "Input",
          "ShowTitle": "更改原因",
          "FormKey": "reason",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        },]
      }
      // 找到需要添加内容的索引位置 TODO
      const organizationUnitTypeIndex = operationForm.Properties.findIndex((item) => {
        return item.FormKey === 'organizationUnitType'
      })
      // 如果找到就赋值
      if (organizationUnitTypeIndex !== -1) {
        operationForm.Properties[organizationUnitTypeIndex].Value = organizationUnitOperationType.toString()
      }
      yield put({ type: 'updateState', payload: { content: operationForm, modalType: 'editOperation', modalVisible: true } })
      yield put({ type: 'queryBasicInfo', payload: id })

    },

    *editCenterInfo(_, { take, put, call, select }) {
      const { centerDetailInfo } = yield select(state => state.centerDetail)
      const { displayName, city, address1 } = centerDetailInfo
      const emptyForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "店铺名称",
          "FormKey": "displayName",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        },
        {
          "EditorType": "Input",
          "ShowTitle": "所在城市",
          "FormKey": "city",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        },
        {
          "EditorType": "Input",
          "ShowTitle": "地址",
          "FormKey": "address1",
          "AdditionalData": null,
          "Value": '',
          "Setting": {
            "Required": false
          },
          "Description": null
        },
        ]
      }

      const values = { displayName, city, address1 }
      const centerInfoForm = matchDynamicForm(emptyForm, values)
      yield put({ type: 'updateState', payload: { content: centerInfoForm, modalType: 'editInfo', modalVisible: true } })
    },

    // 添加新员工
    *onNewWorker(_, { take, put, call, select }) {
      const { centerDetailInfo } = yield select(state => state.centerDetail)
      const postData = { OrganizationUnitId: centerDetailInfo.id }
      const res = yield call(GetCurrentUserForEdit, postData)

      if (res.success) {
        const { roles } = res.result
        // 初始化表单组件
        const newWorkerForm = {
          ShowTitle: "新员工",
          Properties: [{
            "EditorType": "Input",
            "ShowTitle": "员工姓名",
            "FormKey": "employeName",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Input",
            "ShowTitle": "手机号",
            "FormKey": "phone",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "男",
                "Id": "1"
              }, {
                "Value": "女",
                "Id": "2"
              }],
              "Required": true
            },
            "ShowTitle": "性别",
            "FormKey": "gender",
            "Description": null,
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "角色",
            "FormKey": "role",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "邮箱",
            "FormKey": "emailAddress",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "密码",
            "FormKey": "password",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "再次输入密码",
            "FormKey": "rePassword",
            "Description": null
          }, {
            "EditorType": "Bool",
            "Value": null,
            "Setting": {
              "Required": true
            },
            "ShowTitle": "是否激活该员工",
            "FormKey": "isActive",
            "Description": null
          }]
        }

        // 找到需要添加内容的位置
        const roleIndex = newWorkerForm.Properties.findIndex((item) => {
          return item.FormKey === 'role'
        })
        // 如果找到
        if (roleIndex !== -1) {
          const roleOptions = roles.map((item) => {
            return { "Value": item.roleDisplayName, "Id": item.roleId }
          })
          newWorkerForm.Properties[roleIndex].Setting.DropdownOptions = roleOptions
        }
        yield put({ type: 'updateState', payload: { content: newWorkerForm, modalType: 'add', modalVisible: true } })
      }
    },

    *onSubmitModal({ payload: data }, { take, put, call, select }) {
      const { modalType, centerDetailInfo, userInfo, checkedId, checkedRoleId } = yield select(state => state.centerDetail)
      const organizationUnitId = centerDetailInfo.id
      const { id } = centerDetailInfo
      switch (modalType) {
        case 'editOperation':
          const { organizationUnitType } = data
          const { reason } = data
          const postData = { id, operationType: operationList[organizationUnitType], reason }
          const operationRes = yield call(ChangeOperationType, postData)
          if (operationRes.success) {
            yield put({ type: 'updateState', payload: { modalVisible: false } })
            yield put({ type: 'queryBasicInfo', payload: organizationUnitId })
          }
          break
        case 'editInfo':
          const { address1, city, displayName } = data
          const putData = { id, address1, city, displayName }
          const centerInfoRes = yield call(UpdateOrganizationUnit, putData)
          if (centerInfoRes.success) {
            yield put({ type: 'updateState', payload: { modalVisible: false } })
            yield put({ type: 'queryBasicInfo', payload: organizationUnitId })
          }
          break
        case 'changeRole':
          const { role } = data
          const { user } = userInfo
          const rolePostData = { user: { ...user }, assignedRoleIds: [role], organizationUnitId }
          const res = yield call(CreateOrUpdateUser, rolePostData)
          if (res.success) {
            message.success('更改成功!')
            yield put({ type: 'updateState', payload: { modalVisible: false } })
            yield put({ type: 'queryEmployee', payload: organizationUnitId })
          } else {
            message.error('更改失败!')
          }
          break
        case 'add':
          const { employeName, phone, gender, role: roleInfo, password, emailAddress, isActive } = data
          const userData = { name: employeName, phoneNumber: phone, gender, password, emailAddress, isActive }
          const assignedRoleIds = [roleInfo]
          const postAddData = { user: userData, assignedRoleIds, organizationUnitId }
          const addRes = yield call(CreateOrUpdateUser, postAddData)
          if (addRes.success) {
            message.success('添加成功!')
            yield put({ type: 'updateState', payload: { modalVisible: false } })
            yield put({ type: 'queryEmployee', payload: organizationUnitId })
          }
          break
        case 'addSelect':
          if (!checkedId || !checkedRoleId) {
            message.error('请选择员工并为其添加角色')
            break
          }
          const postSelectData = { userIds: [checkedId], organizationUnitId, roleIds: [checkedRoleId] }
          const currentUserRes = yield call(AddUsersToOrganizationUnit, postSelectData)
          if (currentUserRes.success) {
            message.success('添加成功!')
            yield put({ type: 'updateState', payload: { selectModalVisible: false } })
            yield put({ type: 'queryEmployee', payload: organizationUnitId })
          } else {
            message.success('添加失败!')
          }
          break
      }
    },

    // 从当前店铺中移除员工
    *onRemoveUser({ payload: data }, { take, put, call, select }) {
      const { centerDetailInfo } = yield select(state => state.centerDetail)
      const organizationUnitId = centerDetailInfo.id
      const deleteData = { organizationUnitId, UserId: data.id }
      const res = yield call(RemoveUserFromOrganizationUnit, deleteData)
      if (res.success) {
        message.success('移除成功!')
        yield put({ type: 'queryEmployee', payload: organizationUnitId })
      } else {
        message.error('移除失败!')
      }
    },

    *onSearch({ payload: data }, { take, put, call, select }) {
      const { searchField } = data || {}
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { selectPagination: defaultPagination, searchField } })
      yield put({ type: 'onSelectQuery' })
    },

    // 搜索现有员工
    *onSelectQuery(_, { take, put, call, select }) {
      const { centerDetailInfo, selectPagination, searchField } = yield select(state => state.centerDetail)
      let query = { skipCount: 0, maxResultCount: 10, organizationUnitId: centerDetailInfo.id }
      const currentPageIndex = selectPagination.current
      const currentPageSize = selectPagination.pageSize
      query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }

      if(searchField !== undefined){
        query = { ...query, filter: searchField }
      }

      const res = yield call(FindUsers, query)
      if (res.success) {
        const { items, totalCount } = res.result
        const selectList = items.map(item => {
          return {
            ...item
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { selectPagination: newPagination, selectList, selectModalLoading: false } })
      }
    },

    *onTabelChange({ payload: data }, { take, put, call, select }) {
      const { selectPagination } = data
      yield put({ type: 'updateState', payload: { selectPagination } })
      yield put({ type: 'onSelectQuery' })
    },

    *onCheckboxChange({ payload: data }, { take, put, call, select }) {
      const id = data[0].id
      yield put({ type: 'updateState', payload: { checkedId: id } })
    },

    // 选择现有员工
    *onSelectWorker(_, { take, put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { selectModalVisible: true, modalType: 'addSelect', selectPagination: defaultPagination } })
      yield put({ type: 'onSelectQuery' })
    },

    *onSelectedRole({ payload: roleId }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { checkedRoleId: roleId } })
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname.search(centerDetailPage) !== -1) {
          dispatch({ type: 'clear' })
          dispatch({ type: 'queryPage', payload: { ...queryString.parse(location.search) } })
        }
      })
    },
  }
}
