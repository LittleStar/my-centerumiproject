
import queryString from 'query-string'
import { pageConfig, matchDynamicForm } from 'utils'
import { UserRoleChange } from 'services/centerService/User'


const { detail } = pageConfig
const { employeeDetail: employeeDetailPage } = detail

export default {
  namespace: 'employeeDetail',
  state: {
    list: [],
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query({ payload: queryData }, { take, put, call, select }) {

      const { pagination } = yield select(state => state.employeeDetail)

      let query = { skipCount: 0, maxResultCount: 10 }
      const currentPageIndex = pagination.current
      const currentPageSize = pagination.pageSize
      query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }

      const res = yield call(UserRoleChange, { userId: queryData.empId })
      if (res.success) {
        const { items, totalCount } = res.result
        const list = items.map(item => {
          const { user, creatorUser, newRoles, oldRoles } = item
          return {
            ...item,
            oldRole: oldRoles[0].displayName,
            newRole: newRoles[0].displayName,
            creatorUser: creatorUser.name,
            userName: user.name
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list } })
      }
    },


    *tableChange({ payload: queryData }, { take, put, call, select }) {
      const { pagination } = queryData
      yield put({ type: 'updateState', payload: { pagination }})
    },



  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname.search(employeeDetailPage) !== -1) {
          // dispatch({ type: 'clear' })
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },
  }
}
