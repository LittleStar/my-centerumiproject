import { routerRedux } from 'dva/router'
import { pageConfig, matchUserInfo, matchDynamicForm } from 'utils'
import queryString from 'query-string';
import { GetCoursePlanInfo } from 'services/centerService/Teaching'

const { edu, detail } = pageConfig
const { classList } = edu
const { classPlanDetail } = detail


export default {

  namespace: 'classPlanBasicInfo',

  state: {
    planInfo: [],
    editClassModalVisible: false,
    editFormContent: {
      "Properties": [{
        "EditorType": "Dropdown",
        "Value": "当日计划",
        "Setting": {
          "DropdownOptions": [{
            "Value": "当日计划",
            "Id": "当日计划"
          }, {
            "Value": "长期计划",
            "Id": "长期计划"
          }],
          "Required": true
        },
        "ShowTitle": "计划",
        "FormKey": "plan",
        "Description": null,
        "SortOrder": 0
      }, {
        "EditorType": "Dropdown",
        "Value": "未开班",
        "Setting": {
          "DropdownOptions": [{
            "Value": "未开班",
            "Id": "未开班"
          }, {
            "Value": "开班中",
            "Id": "开班中"
          }, {
            "Value": "已冻结",
            "Id": "已冻结"
          }],
          "Required": true
        },
        "ShowTitle": "班级状态",
        "FormKey": "classStatus",
        "Description": null,
        "SortOrder": 0
      }, {
        "EditorType": "Dropdown",
        "ShowTitle": "课程类型",
        "FormKey": "ClassNo",
        "AdditionalData": null,
        "Value": "类型B",
        "Setting": {
          "DropdownOptions": [{
            "Value": "类型A",
            "Id": "类型A"
          }, {
            "Value": "类型B",
            "Id": "类型B"
          }, {
            "Value": "类型C",
            "Id": "类型C"
          }],
          "Required": true
        },
        "Description": null,
      }, {
        "EditorType": "Dropdown",
        "ShowTitle": "任课老师",
        "FormKey": "Teacher",
        "AdditionalData": null,
        "Value": "Lisa",
        "Setting": {
          "DropdownOptions": [{
            "Value": "Alen",
            "Id": "Alen"
          }, {
            "Value": "Maple",
            "Id": "Maple"
          }, {
            "Value": "Ada",
            "Id": "Ada"
          }],
          "Required": true
        },
        "Description": null,
      },
      {
        "EditorType": "Input",
        "ShowTitle": "最大可容纳人数",
        "FormKey": "MaxPopulation",
        "AdditionalData": null,
        "Value": "20",
        "Setting": {
          "Required": true
        },
        "Description": null,
      },
      {
        "EditorType": "Dropdown",
        "ShowTitle": "教室",
        "FormKey": "Room",
        "AdditionalData": null,
        "Value": "A1",
        "Setting": {
          "DropdownOptions": [{
            "Value": "A1",
            "Id": "A1"
          }, {
            "Value": "A2",
            "Id": "A2"
          }, {
            "Value": "B1",
            "Id": "B1"
          }],
          "Required": true
        },
        "Description": null,
      },]
    }

  },

  reducers: {

    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },

  },

  effects: {

    *query({ payload: queryData }, { take, put, call, select }) {
      const res = yield call(GetCoursePlanInfo, queryData.ck)
      if (res.success) {
        yield put({ type: 'updateState', payload: { planInfo: res.result } })
      }
    },

    *clickEdit(_, { take, put, call, select }) {
      const editFormContent = {
        "Properties": [{
          "EditorType": "Dropdown",
          "Value": "当日计划",
          "Setting": {
            "DropdownOptions": [{
              "Value": "当日计划",
              "Id": "当日计划"
            }, {
              "Value": "长期计划",
              "Id": "长期计划"
            }],
            "Required": true
          },
          "ShowTitle": "计划",
          "FormKey": "plan",
          "Description": null,
          "SortOrder": 0
        }, {
          "EditorType": "Dropdown",
          "Value": "未开班",
          "Setting": {
            "DropdownOptions": [{
              "Value": "未开班",
              "Id": "未开班"
            }, {
              "Value": "开班中",
              "Id": "开班中"
            }, {
              "Value": "已冻结",
              "Id": "已冻结"
            }],
            "Required": true
          },
          "ShowTitle": "班级状态",
          "FormKey": "classStatus",
          "Description": null,
          "SortOrder": 0
        }, {
          "EditorType": "Dropdown",
          "ShowTitle": "课程类型",
          "FormKey": "ClassNo",
          "AdditionalData": null,
          "Value": "类型B",
          "Setting": {
            "DropdownOptions": [{
              "Value": "类型A",
              "Id": "类型A"
            }, {
              "Value": "类型B",
              "Id": "类型B"
            }, {
              "Value": "类型C",
              "Id": "类型C"
            }],
            "Required": true
          },
          "Description": null,
        }, {
          "EditorType": "Dropdown",
          "ShowTitle": "任课老师",
          "FormKey": "Teacher",
          "AdditionalData": null,
          "Value": "Lisa",
          "Setting": {
            "DropdownOptions": [{
              "Value": "Alen",
              "Id": "Alen"
            }, {
              "Value": "Maple",
              "Id": "Maple"
            }, {
              "Value": "Ada",
              "Id": "Ada"
            }],
            "Required": true
          },
          "Description": null,
        },
        {
          "EditorType": "Input",
          "ShowTitle": "最大可容纳人数",
          "FormKey": "MaxPopulation",
          "AdditionalData": null,
          "Value": "20",
          "Setting": {
            "Required": true
          },
          "Description": null,
        },
        {
          "EditorType": "Dropdown",
          "ShowTitle": "教室",
          "FormKey": "Room",
          "AdditionalData": null,
          "Value": "A1",
          "Setting": {
            "DropdownOptions": [{
              "Value": "A1",
              "Id": "A1"
            }, {
              "Value": "A2",
              "Id": "A2"
            }, {
              "Value": "B1",
              "Id": "B1"
            }],
            "Required": true
          },
          "Description": null,
        },]
      }
      yield put({ type: 'updateState', payload: { editFormContent, editClassModalVisible: true } })
    },

    *submitEdit({ payload: data }, { take, put, call, select }) {
      // const { classInfo } = yield select(state => state.classBasicInfo)
      // console.log(data, classInfo)
    }

  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(classPlanDetail) === 0) {
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },

  },
}
