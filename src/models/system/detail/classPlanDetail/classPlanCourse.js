import { routerRedux } from 'dva/router'
import { pageConfig } from 'utils'
import queryString from 'query-string';

const { detail } = pageConfig
const { classPlanCourseInfo } = detail

export default {

	namespace: 'classPlanCourse',

	state: {

	},

	reducers: {

		updateState(state, { payload }) {
			return {
				...state,
				...payload,
			}
		},

	},

	effects: {

	    *query({ payload: queryData }, { take, put, call, select }) {
	    	console.log(queryData)
	    },

	},

	subscriptions: {
		setup({ dispatch, history }) {

			history.listen((location) => {
			  //如果pathname里面找到了
	          if (location.pathname.search(classPlanCourseInfo) === 0) {
	            dispatch({ type: 'query',payload:{...queryString.parse(location.search)}})
	          }
	        })

		},

	},
}
