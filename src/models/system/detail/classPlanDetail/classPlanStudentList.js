import { routerRedux } from 'dva/router'
import { pageConfig, matchDynamicForm } from 'utils'
import { message } from 'antd'
import queryString from 'query-string'
import { FindSubjects } from 'services/centerService/User'
import { AddToCoursePlan, GetCoursePlanUsers,RemoveFromCoursePlan } from 'services/centerService/Teaching'

const { detail } = pageConfig
const { classPlanSubjectList } = detail

export default {

  namespace: 'classPlanStudentList',

  state: {
    modalVisible: false,
    modalType: 'add',
    studentList: [],
    studentListSupport: {
      type: {
        showText: '用户状态',
        showType: 'Status',
        addtional: {
          statusArray: ['非会员', '会员', '过期会员']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            // { text: '非会员', value: 0 },
            { text: '会员', value: 1 },
            { text: '过期会员', value: 2 },
          ],
        }
      },
      subjectName: {
        showText: '儿童姓名',
        showType: 'Text',
      },
      birthDay: {
        showText: '儿童生日',
        showType: 'Time',
        addtional: {
          format: 'YYYY/MM/DD'
        }
      },
      restAllCourseCount: {
        showText: '剩余课时数',
        showType: 'Text',
      },
      assignedSales: {
        showText: '负责课程顾问',
        showType: 'Text',
      },
      guardianName: {
        showText: '家长姓名',
        showType: 'Text',
      },
      guardianPhone: {
        showText: '家长电话',
        showType: 'Text',
      },
      creatorUser: {
        showText: '操作人',
        showType: 'Text',
      },
    },
    vipList: [],
    vipListSupport: {
      type: {
        showText: '用户状态',
        showType: 'Status',
        addtional: {
          statusArray: ['非会员', '会员', '过期会员']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '非会员', value: 0 },
            { text: '会员', value: 1 },
            { text: '过期会员', value: 2 },
          ],
        }
      },
      subjectName: {
        showText: '儿童姓名',
        showType: 'Text',
      },
      birthDay: {
        showText: '儿童生日',
        showType: 'Time',
        addtional: {
          format: 'YYYY/MM/DD'
        }
      },
      restAllCourseCount: {
        showText: '剩余课时数',
        showType: 'Text',
        sorter: {
          hasSorter: true
        }
      },
      assignedSales: {
        showText: '负责课程顾问',
        showType: 'Text',
      },
      guardianName: {
        showText: '家长姓名',
        showType: 'Text',
      },
      guardianPhone: {
        showText: '家长电话',
        showType: 'Text',
      },
      // headteacher: {
      //   showText: '班主任',
      //   showType: 'Text',
      // },
    },
    pagination: {
      current: 1,
      pageSize: 10,
      total: 37
    },
    editId: '',
    checkedId: null,
    coursePlanId:null,
  },

  reducers: {

    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },

  },

  effects: {

    *query({ payload: queryData }, { take, put, call, select }) {
      const {ck} = queryData
      const { pagination, filters, sorter, searchField } = queryData || {}
      // const { coursePlanId } = yield select(state => state.classPlanStudentList)

      let query = { skipCount: 0, maxResultCount: 10, CoursePlanId: ck }
      let currentPageIndex = 1
      let currentPageSize = 10


      if (pagination !== undefined) {
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      // if (searchField !== undefined) {
      //   query = { ...query, filter: searchField }
      // }
      // 需要加sorter 和 filters ?
      const res = yield call(GetCoursePlanUsers, query)
      if (res.success) {
        const { items, totalCount } = res.result
        const list = items.map((userItem) => {
          const { guardians, headteacher, name: subjectName, source, sales, restAllCourseCount, creatorUser } = userItem

          if (guardians.length === 0) {
            return {
              ...userItemm,
              assignedSales: sales ? sales.surname + ' ' + sales.name : '',
              subjectName: subjectName,
            }
          } else {
            const firstGuardian = guardians[0]
            const { name, surname, role, phoneNumber } = firstGuardian
            return {
              ...userItem,
              assignedSales: sales ? sales.name : '',
              // headteacher: headteacher ? headteacher.surname + ' ' + headteacher.name : '',
              subjectName: subjectName,
              guardianPhone: phoneNumber,
              guardianName: name,
              restAllCourseCount: restAllCourseCount,
            }
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, studentList: list, coursePlanId: ck } })
      }
    },

    *modalQuery({ payload: queryData }, { take, put, call, select }) {
      const dateTypeMenu = {
        0: '年',
        1: '月',
        2: '周',
        3: '天',
      }
      const { pagination, filters, sorter, searchField } = queryData || {}

      let query = { skipCount: 0, maxResultCount: 10, type: ['Vip', 'ExpiredVip'] }
      let currentPageIndex = 1
      let currentPageSize = 10


      if (pagination !== undefined) {
        currentPageIndex = pagination.current
        currentPageSize = pagination.pageSize
        query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      if (searchField !== undefined) {
        query = { ...query, filter: searchField }
      }
      // 需要加sorter 和 filters ?
      const res = yield call(FindSubjects, query)
      console.log(res)
      if (res.success) {
        const { items, totalCount } = res.result
        const list = items.map((userItem) => {
          const { guardians, headteacher, name: subjectName, source, sales, restAllCourseCount, currentCoursePackage } = userItem

          if (guardians.length === 0) {
            return {
              ...userItemm,
              assignedSales: sales ? sales.name : '',
              subjectName: subjectName,
            }
          } else {
            const firstGuardian = guardians[0]
            const { name, role, phoneNumber } = firstGuardian
            return {
              ...userItem,
              assignedSales: sales ? sales.name : '',
              headteacher: headteacher ? headteacher.name : '',
              subjectName: subjectName,
              guardianPhone: phoneNumber,
              guardianName: name,
              coursePackageType: currentCoursePackage?currentCoursePackage.coursePackageType:'',
              restAllCourseCount: restAllCourseCount,
              period: currentCoursePackage?currentCoursePackage.period + dateTypeMenu[currentCoursePackage.displayDateType]:''
            }
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, vipList: list } })
      }
    },

    *onCheckboxChange({ payload: data }, { take, put, call, select }) {
      const subjectsList = []
      data.forEach((item)=> {
        subjectsList.push(item.id)
      })
      yield put({ type: 'updateState', payload: { checkedId: subjectsList } })
    },

    *onAddStudent(_, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { modalType: 'add', vipList: [], modalVisible: true } })
    },

    *onRemove({payload: subjectsId},{take,put,call,select}){
      const { coursePlanId } = yield select(state=>state.classPlanStudentList)
      const res = yield call(RemoveFromCoursePlan, {coursePlanId,subjectsId})
      if(res.success){
        message.success('移除成功!')
        yield put({ type: 'query', payload: { ...queryString.parse(location.search)}})
      }else{
        message.error('移除失败!')
      }

    },

    *onSubmitModal(_, { take, put, call, select }) {
      const { modalType, editId, checkedId, coursePlanId } = yield select(state => state.classPlanStudentList)
      // 注意在add的时候不能加id，edit的时候需要加id
      switch (modalType) {
        case 'add':
          const postData = {
            subjectsId: checkedId,
            coursePlanId: coursePlanId,
          }
          const res = yield call(AddToCoursePlan, postData)
          if (res.success) {
            message.success('添加成功!')
            yield put({ type: 'query' , payload: { ...queryString.parse(location.search) }})
            yield put({type: 'updateState', payload: {modalVisible: false}})

          }else {
            message.error('添加失败!')
          }
          break
        case 'edit':
          break
        default:
          //console.log(data)
          break
      }
    },

  },

  subscriptions: {
    setup({ dispatch, history }) {

      history.listen((location) => {
        // 如果pathname里面找到了
        if (location.pathname.search(classPlanSubjectList) === 0) {
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },

  },
}
