import { routerRedux } from 'dva/router'
import { pageConfig, enumSetting, matchDynamicForm } from 'utils'
import queryString from 'query-string'
import { message } from 'antd'
import { GetContractForEdit, Confirm, Deny, Cancel, UpdateContract, ChangeHistory } from 'services/centerService/Contract'

const { displayDateType, coursePackageType, currencyType } = enumSetting

const { detail, verify } = pageConfig
const { contractDetail, userDetail } = detail
const { contract } = verify

export default {
  namespace: 'contractInfoDetail',
  state: {
    modalVisible: false,
    editId: '',
    content: null,
    modalType: null,
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    userContract: {},
    contractId: '',
    historyList: [],
    historyListSupport: {
      changeTime: {
        showText: '修改日期',
        showType: 'Time',
        addtional: {
          format: 'MM/DD/YYYY HH:mm'
        }
      },
      modifier: {
        showText: '操作人',
        showType: 'Text',
      },
      reason: {
        showText: '原因',
        showType: 'Text',
      },
      // before: {
      //   showText: '改前到期日',
      //   showType: 'Time',
      //   addtional: {
      //     format: 'MM/DD/YYYY HH:mm'
      //   }
      // },
      // after: {
      //   showText: '改后到期日',
      //   showType: 'Time',
      //   addtional: {
      //     format: 'MM/DD/YYYY HH:mm'
      //   }
      // },
      actionTrigger: {
        showText: '操作行为',
        showType: 'Status',
        addtional: {
          statusArray: ['合同草稿', '申请', '被拒', '申请通过', '合同过期', '取消合同']
        },
      },
    },
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *query({ payload: queryData }, { take, call, put, select }) {
      const { contractId } = queryData
      const contractInfo = yield call(GetContractForEdit, contractId)
      const contractHistoryInfo = yield call(ChangeHistory, { id: contractId })

      const totalCount = contractHistoryInfo.result.length
      const newPagination = { current: 1, pageSize: 40, total: totalCount }
      if (contractInfo.success) {
        const { userContract } = contractInfo.result
        yield put({ type: 'updateState', payload: { userContract, contractId, } })
      }
      if (contractHistoryInfo.success) {
        const historyInfo = contractHistoryInfo.result
        const historyList = historyInfo.map((item) => {
          return {
            actionTrigger: item.actionTrigger,
            changeTime: item.changeTime,
            modifier: item.user.name,
            reason: item.reason !== undefined && item.reason ? item.reason : ''
          }
        })
        yield put({ type: 'updateState', payload: { pagination: newPagination, historyList, contractId, } })
      }
    },

    *confirmContract(_, { take, call, put, select }) {
      const { contractId } = yield select(state => state.contractInfoDetail)
      const confirmRes = yield call(Confirm, contractId)
      //审核完毕之后，直接跳回审核中心
      if (confirmRes.success) {
        yield put(routerRedux.push({
          pathname: contract,
        }))
      }
    },

    *onDenyContract(_, { take, call, put, select }) {
      const denyForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "拒绝原因",
          "FormKey": "reason",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": '请简述拒绝申请的原因'
        }]
      }

      yield put({ type: 'updateState', payload: { content: denyForm, modalVisible: true, modalType: 'deny' } })
    },

    *onCencelContract(_, { take, call, put, select }) {
      const cancelForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "取消原因",
          "FormKey": "reason",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": '请简述取消申请的原因'
        }]
      }

      yield put({ type: 'updateState', payload: { content: cancelForm, modalVisible: true, modalType: 'cancel' } })
    },

    *onModifyContract(_, { take, call, put, select }) {

      const { contractId } = yield select(state => state.contractInfoDetail)
      const contractInfo = yield call(GetContractForEdit, contractId)
      if (contractInfo.success) {
        const { userContract } = contractInfo.result
        const { coursePackage, pictures } = userContract
        const { coursePackageType: PackageType, count, displayDateType: DateType, period, price } = coursePackage
        const emptyForm = {
          Properties: [{
            "EditorType": "Input",
            "ShowTitle": "购买课包类型",
            "FormKey": "coursePackageShow",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true,
              "Disabled": true,
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "ShowTitle": "货币类型",
            "FormKey": "currency",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "人民币",
                "Id": "0"
              }],
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "NumberInput",
            "ShowTitle": "折扣金额",
            "FormKey": "discount",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true,
              'NumberSetting': {
                min: 0,
                step: 1,
                precision:2
              }
            },
            "Description": null
          }, {
            "EditorType": "NumberInput",
            "ShowTitle": "实付金额",
            "FormKey": "paymentPrice",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true,
              'NumberSetting': {
                min: 0,
                step: 1,
                precision: 2
              }
            },
            "Description": null
          }, {
            "EditorType": "Upload",
            "Value": null,
            "Setting": {
              "Required": true
            },
            "ShowTitle": "上传合同",
            "FormKey": "contractPics",
          }, {
            "EditorType": "Input",
            "ShowTitle": "订单ID",
            "FormKey": "orderId",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": false
            },
            "Description": null
          },
          ]
        }
        const coursePackageShow = '课时数：' + count + '  课包类型：' + coursePackageType[PackageType]['cn'] + '  课包有效期:' + period + displayDateType[DateType]['cn'] + ` 课包原价: ${price}元`
        const applyForm = matchDynamicForm(emptyForm, { ...userContract, coursePackageShow, contractPics: pictures })
        yield put({ type: 'updateState', payload: { content: applyForm, modalVisible: true, modalType: 'edit' } })
      }
    },

    *cancelContract(_, { take, call, put, select }) {
      const { contractId } = yield select(state => state.contractInfoDetail)
      const confirmRes = yield call(Cancel, contractId)
      //取消之后，原地刷新
      if (confirmRes.success) {
        yield put({ type: 'query', payload: { contractId } })
      }
    },

    *onSubmitModal({ payload: data }, { take, put, call, select }) {
      const { modalType, contractId, userContract } = yield select(state => state.contractInfoDetail)

      switch (modalType) {
        case 'deny':
          const denyPostData = { ...data, userContractId: contractId }
          const denyRes = yield call(Deny, denyPostData)

          if (denyRes.success) {
            yield put(routerRedux.push({
              pathname: contract,
            }))
          }
          break
        case 'cancel':
          const cancelPostData = { ...data, userContractId: contractId }
          const cancelRes = yield call(Cancel, cancelPostData)

          if (cancelRes.success) {
            yield put(routerRedux.push({
              pathname: contract,
            }))
          }
          break
        case 'edit':
          const { subject } = userContract
          const putData = { ...data, id: contractId }
          const editRes = yield call(UpdateContract, putData)
          if (editRes.success) {
            message.success('合同修改成功，已经重新提交申请')
            yield put(routerRedux.push({
              pathname: userDetail,
              search: '?userId=' + subject.id
            }))
          }
          break
      }

    },

  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === contractDetail) {
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },
  }
}
