import { routerRedux } from 'dva/router'
import { pageConfig, matchDynamicForm  } from 'utils'
import queryString from 'query-string'
import { GetCourseInstanceInfo, GetAllActiveCoursePeriod, GetCourseInstanceForEdit,CreateOrUpdateCourseInstance} from 'services/centerService/Teaching'

const { edu, detail } = pageConfig
const { classList } = edu
const { classDetail } = detail

const initCourseInstanceForm = ({teacherList, classrooms})=>{
  const newPlanForm = {
      Properties: [{
        "EditorType": "Dropdown",
        "Value": null,
        "Setting": {
          "DropdownOptions": [],
          "Required": true
        },
        "ShowTitle": "教室",
        "FormKey": "classroomId",
        "Description": null
      }, {
        "EditorType": "Dropdown",
        "Value": null,
        "Setting": {
          "DropdownOptions": [],
          "Required": false
        },
        "ShowTitle": "教师",
        "FormKey": "teacherUserId",
        "Description": null
      }]
    }

  const teacherIndex = newPlanForm.Properties.findIndex((item) => {
    return item.FormKey === 'teacherUserId'
  })
  const roomIndex = newPlanForm.Properties.findIndex((item) => {
    return item.FormKey === 'classroomId'
  })

  if (teacherIndex !== -1) {
    const teacherOptions = teacherList.map((item) => {
      return { "Value": item.name, "Id": item.id.toString() }
    })
    newPlanForm.Properties[teacherIndex].Setting.DropdownOptions = teacherOptions
  }
  if (roomIndex !== -1) {
    const roomOptions = classrooms.map((item) => {
      return { "Value": item.name + '( ' + item.description + ' )', "Id": item.id }
    })
    newPlanForm.Properties[roomIndex].Setting.DropdownOptions = roomOptions
  }

    return newPlanForm
}

export default {

  namespace: 'classBasicInfo',

  state: {
    classInfo: {},
    modalVisible: false,
    content: null,
  },

  reducers: {

    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },

  },

  effects: {

    *query({ payload: queryData }, { take, put, call, select }) {
      const res = yield call(GetCourseInstanceInfo, queryData.ck)
      if (res.success) {
        yield put({ type: 'updateState', payload: { classInfo: res.result } })
      }
    },

    *clickEdit(_, { take, put, call, select }) {
      const { classInfo } = yield select(state=>state.classBasicInfo)
      const id = classInfo.id
      const res = yield call(GetCourseInstanceForEdit, {id:id})
      if(res.success){
        const {availableTeachers,classrooms,courseInstance} = res.result
        const {teacher,classroom} = courseInstance
        const emptyCourseForm = initCourseInstanceForm({teacherList:availableTeachers?availableTeachers.items:[], classrooms})
        const editCourseForm = matchDynamicForm(emptyCourseForm, {classroomId:classroom.id,teacherUserId:teacher?teacher.id:null})
        yield put({ type: 'updateState', payload: { content: editCourseForm, modalVisible: true } })
      }
    },

    *onSubmitModal({ payload: data }, { take, put, call, select }) {
      const { classInfo } = yield select(state => state.classBasicInfo)
      const {startCoursePeriod,course,id,actualTime} = classInfo
      const postData = {...data,courseId:course.id,startCoursePeriodId:startCoursePeriod.id,actualTime,id}
      const editRes = yield call(CreateOrUpdateCourseInstance,postData)
      if(editRes.success){
        yield put({type:'query',payload:{ck:id}})
        yield put({ type: 'updateState', payload: { modalVisible: false } })
      }
    }

  },

  subscriptions: {
    setup({ dispatch, history }) {

      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(classDetail) === 0) {
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })

    },

  },
}
