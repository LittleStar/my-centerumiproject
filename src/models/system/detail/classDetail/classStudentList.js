import { routerRedux } from 'dva/router'
import router from 'umi/router'
import { pageConfig } from 'utils'
import { message } from 'antd'
import queryString from 'query-string'
import { GetCourseInstanceUsers, AddUserToCourseInstance, CreateCourseInstanceUserFeedback, GetCourseInstanceUserFeedbackForEdit } from 'services/centerService/Teaching'
import { FindSubjects } from 'services/centerService/User'

const { detail } = pageConfig
const { classSubjectList, userDetail,basicInfo } = detail

export default {

  namespace: 'classStudentList',

  state: {
    studentList: [],
    studentListSupport: {
      userStatus: {
        showText: '上课状态',
        showType: 'Status',
        addtional: {
          statusArray: ['未签到', '已签到', '请假', '旷课']
        },
        filter: {
          // hasFilter: true,
          filterOptions: [
            { text: '未签到', value: 0 },
            { text: '已签到', value: 1 },
            { text: '请假', value: 2 },
            { text: '旷课', value: 3 },
          ],
        }
      },
      userOrigin: {
        showText: '用户来源',
        showType: 'Status',
        addtional: {
          statusArray: ['长期添加', '当日添加']
        },
        filter: {
          // hasFilter: true,
          filterOptions: [
            { text: '长期添加', value: 0 },
            { text: '当日添加', value: 1 },
          ],
        }
      },
      checkRestUnplannedCourseCount: {
        showText: '是否试听',
        showType: 'Text',
      },
      type: {
        showText: '用户状态',
        showType: 'Status',
        addtional: {
          statusArray: ['非会员', '会员', '过期会员']
        },
        filter: {
          // hasFilter: true,
          filterOptions: [
            { text: '非会员', value: 0 },
            { text: '会员', value: 1 },
            { text: '过期会员', value: 2 },
          ],
        }
      },
      subjectName: {
        showText: '儿童姓名',
        showType: 'Text',
      },
      birthDay: {
        showText: '儿童生日',
        showType: 'Time',
        addtional: {
          format: 'YYYY/MM/DD'
        }
      },
      restAllCourseCount: {
        showText: '剩余课时数',
        showType: 'Text',
      },
      assignedSales: {
        showText: '负责课程顾问',
        showType: 'Text',
      },
      guardianName: {
        showText: '家长姓名',
        showType: 'Text',
      },
      guardianPhone: {
        showText: '家长电话',
        showType: 'Text',
      },
      creatorUser: {
        showText: '操作人',
        showType: 'Text',
      },
      ILSFeedback: {
        showText: 'ILS反馈',
        showType: 'Text',
      },
    },
    userList: [],
    userListSupport: {
      type: {
        showText: '用户状态',
        showType: 'Status',
        addtional: {
          statusArray: ['非会员', '会员', '过期会员']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '非会员', value: 0 },
            { text: '会员', value: 1 },
            { text: '过期会员', value: 2 },
          ],
        }
      },
      subjectName: {
        showText: '儿童姓名',
        showType: 'Text',
      },
      birthDay: {
        showText: '儿童生日',
        showType: 'Time',
        addtional: {
          format: 'YYYY/MM/DD'
        }
      },
      restAllCourseCount: {
        showText: '剩余课时数',
        showType: 'Text',
        sorter: {
          hasSorter: true
        }
      },
      assignedSales: {
        showText: '负责课程顾问',
        showType: 'Text',
      },
      guardianName: {
        showText: '家长姓名',
        showType: 'Text',
      },
      guardianPhone: {
        showText: '家长电话',
        showType: 'Text',
      },
    },
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    modalPagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    addModalVisible: false,
    commonModalVisible: false,
    ilsModalVisible: false,
    modalType: 'add',
    checkedId: null,
    courseInstanceId: null,
    searchObj: {},
    isChecked: false,
    content: null,
    editId: null,
    ilsOptions: [],
  },

  reducers: {

    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },

  },

  effects: {

    *query({ payload: queryData }, { take, put, call, select }) {
      const { ck } = queryData
      const res = yield call(GetCourseInstanceUsers, ck)
      const totalCount = res.result.length
      const newPagination = { current: 1, pageSize: 40, total: totalCount }
      if (res.success) {
        const items = res.result
        const list = items.map((item, index) => {
          const { courseInstance, subject, type, checkRestUnplannedCourseCount, creatorUser, feedbacks } = item
          const { sales } = subject
          const ILSFeedback = feedbacks.map(item => {
            if (item.contentType === 'ils') {
              return item.content
            }
          })
          if (subject.guardians.length === 0) {
            return {
              ...item,
              subjectName: subject.name,
            }
          } else {
            const firstGuardian = subject.guardians[0]
            const { name, phoneNumber } = firstGuardian
            return {
              ...item,
              // headteacher: teacher ? teacher.name : '',
              assignedSales: sales ? sales.name : '',
              subjectName: subject ? subject.name : '',
              guardianPhone: phoneNumber,
              guardianName: name,
              type: subject ? subject.type : '',
              birthDay: subject ? subject.birthDay : '',
              restAllCourseCount: subject ? subject.restAllCourseCount : '',
              userOrigin: type,
              checkRestUnplannedCourseCount: !checkRestUnplannedCourseCount ? '试听' : '非试听',
              creatorUser: creatorUser ? creatorUser.name : '',
              ILSFeedback,
            }
          }
        })
        yield put({ type: 'updateState', payload: { pagination: newPagination, studentList: list, courseInstanceId: ck } })
      }
    },

    *modalQuery({ payload: queryData }, { take, put, call, select }) {
      const { modalPagination, filters, sorter, searchField } = queryData || {}

      let query = { skipCount: 0, maxResultCount: 10, type: ['None', 'Vip', 'ExpiredVip'] }
      let currentPageIndex = 1
      let currentPageSize = 10

      if (modalPagination !== undefined) {
        currentPageIndex = modalPagination.current
        currentPageSize = modalPagination.pageSize
        query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }
      }

      if (searchField !== undefined) {
        query = { ...query, filter: searchField }
      }
      // 需要加sorter 和 filters ?
      const res = yield call(FindSubjects, query)

      if (res.success) {
        const { items, totalCount } = res.result
        const list = items.map((userItem) => {
          const { guardians, headteacher, name: subjectName, sales } = userItem

          if (guardians.length === 0) {
            return {
              ...userItemm,
              assignedSales: sales ? sales.name : '',
              subjectName,
            }
          } else {
            const firstGuardian = guardians[0]
            const { name, role, phoneNumber } = firstGuardian
            return {
              ...userItem,
              assignedSales: sales ? sales.name : '',
              // headteacher: headteacher ? headteacher.name : '',
              subjectName,
              guardianPhone: phoneNumber,
              guardianName: name,
            }
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { modalPagination: newPagination, userList: list } })
      }
    },

    *onAddStudent(_, { take, put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { modalType: 'add', userList: [], addModalVisible: true, modalPagination: defaultPagination } })
    },

    *onCheckboxChange({ payload: data }, { take, put, call, select }) {
      const id = data[0].id
      yield put({ type: 'updateState', payload: { checkedId: id } })
    },

    *onTrialLesson({ payload: boolVal }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { isChecked: boolVal } })
    },

    *onCreateFeedback({ payload: data }, { take, put, call, select }) {
      const { subject } = data
      const emptyFeedbackForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "课堂反馈",
          "FormKey": "feedback",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        }]
      }
      yield put({ type: 'updateState', payload: { content: emptyFeedbackForm, modalType: 'feedback', commonModalVisible: true, editId: subject.id } })
    },

    *onOpenILS({ payload: data }, { take, put, call, select }) {
      const { subject, courseInstance } = data
      const res = yield call(GetCourseInstanceUserFeedbackForEdit, { ContentType: 'ILS', ToUserId: subject.id, CourseInstanceId: courseInstance.id })
      if (res.success) {
        const { format } = res.result
        yield put({ type: 'updateState', payload: { modalType: 'ils', ilsModalVisible: true, ilsOptions: format.children, editId: subject.id } })
      }
    },

    *onSubmitModal({ payload: data }, { take, put, call, select }) {
      const { modalType, checkedId, courseInstanceId, isChecked, editId } = yield select(state => state.classStudentList)
      // 注意在add的时候不能加id，edit的时候需要加id
      switch (modalType) {
        case 'add':
          const postData = {
            subjectId: checkedId,
            courseInstanceId,
            tryTry: isChecked,
          }
          const res = yield call(AddUserToCourseInstance, postData)
          if (res.success) {
            message.success('添加成功!')
            yield put({ type: 'query', payload: { ...queryString.parse(location.search) } })
            yield put({ type: 'classBasicInfo/query', payload: { ...queryString.parse(location.search) } })
            yield put({ type: 'updateState', payload: { addModalVisible: false } })
          } else {
            message.error('添加失败!')
          }
          break
        case 'edit':
          break
        case 'feedback':
          const postFeebackData = {
            toUserId: editId,
            courseInstanceId,
            content: data.feedback,
          }
          const feedBackRes = yield call(CreateCourseInstanceUserFeedback, postFeebackData)
          if (feedBackRes.success) {
            message.success('反馈成功!')
            yield put({ type: 'updateState', payload: { commonModalVisible: false } })
          } else {
            message.error('反馈失败!')
          }
          break
        case 'ils':
          const { ils } = data
          const postILSdata = {
            toUserId: editId,
            courseInstanceId,
            content: ils.length > 0 ? ils.pop() : '',
            contentType: 'ils'
          }
          const ILSfeedBackRes = yield call(CreateCourseInstanceUserFeedback, postILSdata)
          if (ILSfeedBackRes.success) {
            message.success('ILS进度上传成功!')
            yield put({ type: 'updateState', payload: { ilsModalVisible: false } })
            yield put({ type: 'query', payload: { ...queryString.parse(location.search) } })
          } else {
            message.error('ILS进度上传失败!')
          }

          break
        default:
          break
      }
    },

    // 跳转到详情页
    *goToUserDetail({ payload: id }, { take, put, call, select }) {
      router.push({
        pathname: basicInfo,
        search: '?userId=' + id
      })
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {

      history.listen((location) => {
        //如果pathname里面找到了
        if (location.pathname.search(classSubjectList) === 0) {
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })

    },

  },
}
