import { pageConfig, matchDynamicForm, enumSetting } from 'utils'
import { GetDetail, UpdateOrganizationUnit, RemoveUserFromOrganizationUnit,FindUsers,AddUsersToOrganizationUnit } from 'services/centerService/OrganizationUnit'
import { GetUsers, GetCurrentUserForEdit, CreateOrUpdateUser, GetRolesExceptSubject } from 'services/centerService/User'
import { message } from 'antd'
import queryString from 'query-string'


const { detail } = pageConfig
const { regionDetail } = detail

const { genderType } = enumSetting

export default {
  namespace: 'regionDetail',

  state: {
    list: [],
    regionDetailInfo: {},
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    selectPagination: {
      current: 1,
      pageSize: 10,
      total: 0
    },
    changeType: 'addInfo',
    modalVisible: false,
    content: null,
    userInfo: {},
    selectModalVisible: false,
    roleList: [],
    selectList: [],
    checkedId: null,
    checkedRoleId: null,
    selectModalLoading: true,
    searchField: null,

  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {

    *query({ payload: data }, { take, put, call, select }) {
      const { orgId } = data
      yield put({ type: 'queryBasicInfo', payload: orgId })
      yield put({ type: 'queryManager', payload: orgId })
    },

    *queryBasicInfo({ payload: organizationUnitId }, { take, put, call, select }) {
      const regionInfoRes = yield call(GetDetail, organizationUnitId)
      if (regionInfoRes.success) {
        const regionDetailInfo = regionInfoRes.result
        yield put({ type: 'updateState', payload: { regionDetailInfo } })
      }
    },

    *editRegionInfo(_, { take, put, call, select }) {
      const { regionDetailInfo } = yield select(state => state.regionDetail)

      const { displayName } = regionDetailInfo
      const emptyForm = {
        Properties: [{
          "EditorType": "Input",
          "ShowTitle": "区域名称",
          "FormKey": "displayName",
          "AdditionalData": null,
          "Value": null,
          "Setting": {
            "Required": true
          },
          "Description": null
        },
        ]
      }

      const values = { displayName }
      const regionInfoForm = matchDynamicForm(emptyForm, values)
      yield put({ type: 'updateState', payload: { content: regionInfoForm, modalType: 'editInfo', modalVisible: true } })
    },

    // 获取区域经理列表
    *queryManager({ payload: organizationUnitId }, { take, put, call, select }) {
      const { pagination } = yield select(state => state.regionDetail)

      let query = { skipCount: 0, maxResultCount: 10, organizationUnitId }
      const currentPageIndex = pagination.current
      const currentPageSize = pagination.pageSize
      query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }

      // if (Object.keys(filters).length !== 0) {
      //   for (let item in filters) {
      //     if (filters[item] !== null) {
      //       query[filterableItem[item]] = filters[item]
      //     }
      //   }
      // }

      const res = yield call(GetUsers, query)
      if (res.success) {
        const { items, totalCount } = res.result
        const list = items.map((item) => {
          const { roles, name, isActive, gender } = item
          let RolesText = ''
          roles.forEach((item) => {
            RolesText += item.roleName + ' '
          })
          return {
            ...item,
            gender: genderType[gender]['cn'],
            role: RolesText,
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { pagination: newPagination, list } })
        yield put({ type: 'queryRole', payload: organizationUnitId })
      }
    },

    // 添加新员工
    *onNewWorker(_, { take, put, call, select }) {
      const { regionDetailInfo } = yield select(state => state.regionDetail)
      const postData = { OrganizationUnitId: regionDetailInfo.id }
      const res = yield call(GetCurrentUserForEdit, postData)

      if (res.success) {
        const { roles } = res.result
        // 初始化表单组件
        const newWorkerForm = {
          Properties: [{
            "EditorType": "Input",
            "ShowTitle": "员工姓名",
            "FormKey": "employeName",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Input",
            "ShowTitle": "手机号",
            "FormKey": "phone",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "男",
                "Id": "1"
              }, {
                "Value": "女",
                "Id": "2"
              }],
              "Required": true
            },
            "ShowTitle": "性别",
            "FormKey": "gender",
            "Description": null,
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "角色",
            "FormKey": "role",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "邮箱",
            "FormKey": "emailAddress",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "密码",
            "FormKey": "password",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "再次输入密码",
            "FormKey": "rePassword",
            "Description": null
          }, {
            "EditorType": "Bool",
            "Value": null,
            "Setting": {
              "Required": true
            },
            "ShowTitle": "是否激活该员工",
            "FormKey": "isActive",
            "Description": null
          }]
        }

        // 找到需要添加内容的位置
        const roleIndex = newWorkerForm.Properties.findIndex((item) => {
          return item.FormKey === 'role'
        })
        // 如果找到
        if (roleIndex !== -1) {
          const roleOptions = roles.map((item) => {
            return { "Value": item.roleDisplayName, "Id": item.roleId }
          })
          newWorkerForm.Properties[roleIndex].Setting.DropdownOptions = roleOptions
        }
        yield put({ type: 'updateState', payload: { content: newWorkerForm, modalType: 'add', modalVisible: true } })
      }
    },

    // 编辑区域
    *onEditWorker({ payload: id }, { take, put, call, select }) {
      const { regionDetailInfo } = yield select(state => state.regionDetail)
      const postData = { id, OrganizationUnitId: regionDetailInfo.id }
      const res = yield call(GetCurrentUserForEdit, postData)
      if (res.success) {
        const { user, roles } = res.result
        // 获取res.result
        // 后端获取该用户的信息
        const emptyWorkerForm = {
          ShowTitle: "编辑员工",
          Properties: [{
            "EditorType": "Input",
            "ShowTitle": "员工姓名",
            "FormKey": "employeName",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Input",
            "ShowTitle": "手机号",
            "FormKey": "phone",
            "AdditionalData": null,
            "Value": null,
            "Setting": {
              "Required": true
            },
            "Description": null
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [{
                "Value": "男",
                "Id": "1"
              }, {
                "Value": "女",
                "Id": "2"
              }],
              "Required": true
            },
            "ShowTitle": "性别",
            "FormKey": "gender",
            "Description": null,
          }, {
            "EditorType": "Dropdown",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "角色",
            "FormKey": "role",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
              "Required": true
            },
            "ShowTitle": "邮箱",
            "FormKey": "emailAddress",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
            },
            "ShowTitle": "密码",
            "FormKey": "password",
            "Description": null
          }, {
            "EditorType": "Input",
            "Value": null,
            "Setting": {
              "DropdownOptions": [],
            },
            "ShowTitle": "再次输入密码",
            "FormKey": "rePassword",
            "Description": null
          }, {
            "EditorType": "Bool",
            "Value": null,
            "Setting": {
              "Required": true
            },
            "ShowTitle": "是否激活该员工",
            "FormKey": "isActive",
            "Description": null
          }]
        }

        // 找到需要添加内容的位置
        const roleIndex = emptyWorkerForm.Properties.findIndex((item) => {
          return item.FormKey === 'role'
        })

        // 如果找到
        if (roleIndex !== -1) {
          const roleOptions = roles.map((item) => {
            return { "Value": item.roleDisplayName, "Id": item.roleId.toString() }
          })
          emptyWorkerForm.Properties[roleIndex].Setting.DropdownOptions = roleOptions
        }
        const roleValue = roles.filter(item => {
          return item.isAssigned === true
        })
        const values = {
          employeSurName: user.surname,
          employeName: user.name,
          phone: user.phoneNumber,
          gender: user.gender,
          role: roleValue[0].roleId.toString(),
          password: user.password,
          rePassword: user.password,
          emailAddress: user.emailAddress
        }
        const onEditWorkerForm = matchDynamicForm(emptyWorkerForm, { ...user, ...values })

        yield put({ type: 'updateState', payload: { content: onEditWorkerForm, modalType: 'editWorker', modalVisible: true, userInfo: res.result } })
      }
    },

    *onSubmitModal({ payload: data }, { take, put, call, select }) {
      const { modalType, regionDetailInfo, userInfo, checkedId, checkedRoleId } = yield select(state => state.regionDetail)
      const organizationUnitId = regionDetailInfo.id
      switch (modalType) {
        case 'editInfo':
          const { displayName } = data
          const putData = { id: organizationUnitId, displayName }
          const regionInfoRes = yield call(UpdateOrganizationUnit, putData)
          if (regionInfoRes.success) {
            yield put({ type: 'updateState', payload: { modalVisible: false } })
            yield put({ type: 'queryBasicInfo', payload: organizationUnitId })
          }
          break
        case 'add':
        case 'editWorker':
          const { employeName, phone, gender, role: roleInfo, password, emailAddress, isActive } = data
          let userData = { name: employeName, phoneNumber: phone, gender, password, emailAddress, isActive }
          if (modalType === 'editWorker') {
            const userId = userInfo.user.id
            userData = { ...userData, id: userId }
          }
          const assignedRoleIds = [roleInfo]
          const postAddData = { user: userData, assignedRoleIds, organizationUnitId }
          const addRes = yield call(CreateOrUpdateUser, postAddData)
          if (addRes.success) {
            message.success('成功了!')
            yield put({ type: 'updateState', payload: { modalVisible: false } })
            yield put({ type: 'queryManager', payload: organizationUnitId })
          } else {
            message.error('失败了!')
          }
          break
        case 'addSelect':
          if (!checkedId || !checkedRoleId) {
            message.error('请选择员工并为其添加角色')
            break
          }
          const postSelectData = { userIds: [checkedId], organizationUnitId, roleIds: [checkedRoleId] }
          const currentUserRes = yield call(AddUsersToOrganizationUnit, postSelectData)
          if (currentUserRes.success) {
            message.success('添加成功!')
            yield put({ type: 'updateState', payload: { selectModalVisible: false } })
            yield put({ type: 'queryManager', payload: organizationUnitId })
          } else {
            message.success('添加失败!')
          }
          break
      }
    },

    *onRemoveWorker({ payload: id }, { take, put, call, select }) {
      const { regionDetailInfo } = yield select(state => state.regionDetail)
      const organizationUnitId = regionDetailInfo.id
      const deleteData = { organizationUnitId, UserId: id }
      const res = yield call(RemoveUserFromOrganizationUnit, deleteData)
      if (res.success) {
        message.success('移除成功!')
        yield put({ type: 'queryManager', payload: organizationUnitId })
      } else {
        message.error('移除失败!')
      }
    },

    // 选择现有员工
    *onSelectWorker(_, { take, put, call, select }) {
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { selectModalVisible: true, modalType: 'addSelect', selectPagination: defaultPagination } })
      yield put({ type: 'onSelectQuery' })
    },

    *queryRole({ payload: organizationUnitId }, { take, put, call, select }) {
      const allRoleRes = yield call(GetRolesExceptSubject, { organizationUnitId })
      if (allRoleRes.success) {
        const roleList = allRoleRes.result.items
        yield put({ type: 'updateState', payload: { roleList } })
      }
    },

    *onSelectedRole({ payload: roleId }, { take, put, call, select }) {
      yield put({ type: 'updateState', payload: { checkedRoleId: roleId } })
    },

    *onCheckboxChange({ payload: data }, { take, put, call, select }) {
      const id = data[0].id
      yield put({ type: 'updateState', payload: { checkedId: id } })
    },

    *onTabelChange({ payload: data }, { take, put, call, select }) {
      const { selectPagination } = data
      yield put({ type: 'updateState', payload: { selectPagination } })
      yield put({ type: 'onSelectQuery' })
    },

    *onSearch({ payload: data }, { take, put, call, select }) {
      const { searchField } = data || {}
      const defaultPagination = {
        current: 1,
        pageSize: 10,
        total: 0
      }
      yield put({ type: 'updateState', payload: { selectPagination: defaultPagination, searchField } })
      yield put({ type: 'onSelectQuery' })
    },

    // 搜索现有员工
    *onSelectQuery(_, { take, put, call, select }) {
      const { regionDetailInfo, selectPagination, searchField } = yield select(state => state.regionDetail)

      let query = { skipCount: 0, maxResultCount: 10, organizationUnitId: regionDetailInfo.id }
      const currentPageIndex = selectPagination.current
      const currentPageSize = selectPagination.pageSize
      query = { ...query, skipCount: (currentPageIndex - 1) * currentPageSize, maxResultCount: currentPageSize }

      if (searchField !== undefined) {
        query = { ...query, filter: searchField }
      }

      const res = yield call(FindUsers, query)
      if (res.success) {
        const { items, totalCount } = res.result
        const selectList = items.map(item => {
          return {
            ...item
          }
        })
        const newPagination = { current: currentPageIndex, pageSize: currentPageSize, total: totalCount }
        yield put({ type: 'updateState', payload: { selectPagination: newPagination, selectList, selectModalLoading: false } })
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname.search(regionDetail) !== -1) {
          dispatch({ type: 'query', payload: { ...queryString.parse(location.search) } })
        }
      })
    },
  }
}
