import { routerRedux } from 'dva/router'
import { pageConfig } from 'utils'

const { detail } = pageConfig
const { regionManagerDetail } = detail


export default {
  namespace: 'regionalManagerDetail',

  state: {
    regionalManagerInfo: {},
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {

    *query({ payload: data }, { take, put, call, select }) {

    },


  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location)=>{
        if(location.pathname.search(regionManagerDetail)!==-1){
          dispatch({ type: 'query' })
        }
      })
    },
  }
}
