import { routerRedux } from 'dva/router'
import { pageConfig } from 'utils'

const { detail } = pageConfig
const { vratDetail,vratDetailBasicInfo } = detail

export default {
  namespace: 'vratDetail',
  state: {
    baseInfoList: [
      {
        "testId": "00000",
        "sex": "male",
        "birthday": "2012-09-09",
        "grade": "1年级",
      },
      {
        "testId": "1111",
        "sex": "female",
        "birthday": "2011-06-09",
        "grade": "2年级",
      },
    ],
    baseInfoListSupport: {
      sex: {
        showText: '儿童性别',
        showType: 'Text',
      },
      birthday: {
        showText: '儿童生日',
        showType: 'Text',
      },
      grade: {
        showText: '儿童年级',
        showType: 'Text',
      },
    },
    list: [
      {
        "testId": "00000",
        "createdDate": "2018-09-09",
        "createdTime": "14:00",
        "site": "China",
        "status": "0",
      }, {
        "testId": "111111",
        "createdDate": "2018-06-09",
        "createdTime": "10:00",
        "site": "America",
        "status": "1",
      }
    ],
    listSupport: {
      createdDate: {
        showText: '创建日期',
        showType: 'Text',
      },
      createdTime: {
        showText: '创建时间',
        showType: 'Text',
      },
      site: {
        showText: '测试地点',
        showType: 'Text',
      },
      status: {
        showText: '测试状态',
        showType: 'Status',
        addtional: {
          statusArray: ['未测试', '测试中', '测试结束', '已出报告', '失败']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '未测试', value: 0 },
            { text: '测试中', value: 1 },
            { text: '测试结束', value: 2 },
            { text: '已出报告', value: 3 },
            { text: '失败', value: 4 },
          ],
        }
      },
    },
    userTestInfoList: [
      {
        "testId": "00000",
        "createdDate": "2018-09-09",
        "createdTime": "14:00",
        "site": "China",
        "status": "0",
        "type": "0"
      },{
        "testId": "11111",
        "createdDate": "2018-08-09",
        "createdTime": "15:00",
        "site": "America",
        "status": "1",
        "type": "1"
      },
    ],
    userTestInfoListSupport: {
      createdDate: {
        showText: '创建日期',
        showType: 'Text',
      },
      createdTime: {
        showText: '创建时间',
        showType: 'Text',
      },
      site: {
        showText: '测试地点',
        showType: 'Text',
      },
      status: {
        showText: '测试状态',
        showType: 'Status',
        addtional: {
          statusArray: ['未测试', '测试中', '测试结束', '已出报告', '失败']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '未测试', value: 0 },
            { text: '测试中', value: 1 },
            { text: '测试结束', value: 2 },
            { text: '已出报告', value: 3 },
            { text: '失败', value: 4 },
          ],
        }
      },
      type: {
        showText: '测试类型',
        showType: 'Status',
        addtional: {
          statusArray: ['正式测试', '其他测试']
        },
        filter: {
          hasFilter: true,
          filterOptions: [
            { text: '正式测试', value: 0 },
            { text: '其他测试', value: 1 },
          ],
        }
      }
    },
    commentsList:[
      {
        "testId": "00000",
        "recordedDate": "2018-09-09",
        "recordedTime": "14:00",
        "recorder": "Lisa",
      },{
        "testId": "111111",
        "recordedDate": "2018-09-29",
        "recordedTime": "10:00",
        "recorder": "Evan",
      },
    ],
    commentsListSupport: {
      recordedDate: {
        showText: '记录日期',
        showType: 'Text',
      },
      recordedTime: {
        showText: '记录时间',
        showType: 'Text',
      },
      recorder: {
        showText: '记录人',
        showType: 'Text',
      },
    }

  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *goToVratDetail({ payload: id }, { take, put, call, select }) {
      yield put(routerRedux.push({
        pathname: vratDetailBasicInfo,
        search: '?testId=' + id
      }))
    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
    },
  }
}
