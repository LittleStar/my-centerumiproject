import React from 'react';
import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import { Layout, Icon, message } from 'antd';
import { filterMenu } from '../../utils/utils';
import SiderMenu from '../../components/SiderMenu/SiderMenu';
import styles from '../Layout.css';

const { Content, Header, Footer, Sider } = Layout;

//todo outside later

const MenuLayout = ({ location, children, user }) => {
  const { showMenu } = user;
  if (showMenu.length === 0) return null;

  return (
    <Layout className={styles.footerStyle}>
      <SiderMenu menuData={showMenu} location={location} />

      <Content className={styles.dashboardContentWrapper}>{children}</Content>
    </Layout>
  );
};

export default connect(({ user }) => ({ user }))(MenuLayout);
