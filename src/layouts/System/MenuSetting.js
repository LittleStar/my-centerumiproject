
module.exports = {

	menu:{
		"Location.Dashboard":{
			path:'/system/manage/dashboard',
			name:'首页',
			icon:'home',
		},

		"Location.Sales":{
			name: '营销中心',
			icon: 'bank',
		 	path: '/system/manage/sales',
		},

		"Location.Sales.PotentialUser":{
			name: '储备用户',
			path: '/system/manage/sales/user',
		},

		"Location.Vip":{
			name: '会员中心',
			icon: 'user',
		  	path: '/system/manage/user',
		},

		"Location.Vip.List":{
			name: '会员列表',
			path: '/system/manage/user/vip',
		},

		"Location.Vip.Contract":{
			name: '合同管理',
			path: '/system/manage/user/contract',
		},

		"Location.TeachingCenter":{
			name: '教务中心',
			icon: 'schedule',
		  	path: '/system/manage/edu',
		},

		"Location.TeachingCenter.Schedule":{
			name: '课程表',
			path: '/system/manage/edu/class',
		},

		"Location.TeachingCenter.Plan":{
			name: '课程计划',
			path: '/system/manage/edu/plan',
		},

		"Location.TestingCenter":{
			name: '测评中心',
			icon: 'compass',
		  	path: '/system/manage/test',
		},

		"Location.VerifyCenter":{
			name: '审核中心',
			icon: 'key',
		  	path: '/system/manage/verify',
		},

		"Location.VerifyCenter.Contract":{
			name: '合同审核',
			path: '/system/manage/verify/contract',
		},

		"Location.VerifyCenter.Gift":{

		},

		"Location.VerifyCenter.Refund":{

		},

		"Location.SettingCenter":{
			name: '配置中心',
			icon: 'setting',
		  	path: '/system/manage/setting',
		},

		"Location.SettingCenter.Hr":{
			name: '人事管理',
			path: '/system/manage/setting/user',
		},

		"Location.SettingCenter.Location":{
			name: '中心管理',
			path: '/system/manage/setting/account',
		},

		"Location.SettingCenter.Course":{
			name: '课程管理',
			path: '/system/manage/setting/course',
		},

		"Location.SettingCenter.Classroom":{
			name: '教室管理',
			path: '/system/manage/setting/room',
		},

		"Location.SettingCenter.Others":{
			name: '其他',
			path: '/system/manage/setting/other',
		},
	}

}
