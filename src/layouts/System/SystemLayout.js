import React, { Fragment } from 'react';
import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import { Layout, Icon } from 'antd';
import DocumentTitle from 'react-document-title';
import styles from '../Layout.css';
import NormalFooter from '../../components/Footer/footer';
import CenterHeader from '../../components/Header/testingCenterHeader';
import { config } from '../../utils';

const { userSerivceUrl } = config;

const { Header, Content, Sider, Footer } = Layout;

const TestCenterLayout = ({ location, dispatch, children, user }) => {
  const { currentUser, permission, menu, locations } = user;

  // 原地刷新的时候
  if (Object.keys(currentUser).length === 0) {
    const returnUrl = location.pathname + location.search;
    dispatch({ type: 'user/fetch', payload: { fromUrl: returnUrl } });
    return <div />;
  }

  const getPageTitle = ()=>{
    return 'Center System'
  }

  const HeaderProps = {
    currentUser,
    locations,
    onLocationChange(id) {
      dispatch({ type: 'user/changeLocation', payload: id });
    },
    onSignOut() {
      dispatch({ type: 'user/logout' });
    },
  };

  const FooterProps = {
    links: [
      {
        key: 'HomePage',
        title: 'Home',
        href: '/',
        blankTarget: false,
      },
      {
        key: 'Company',
        title: 'Company',
        href: 'http://cognitiveleap.com/',
        blankTarget: true,
      },

      /*****
  Temp removes not supportd links
    ,
    {
      key: 'Account',
      title: 'Account System',
      href: userSerivceUrl,
      blankTarget: true,
    }, {
      key: 'Help',
      title: 'Help',
      href: null,
      blankTarget: true,
    }, {
      key: 'Privacy',
      title: 'Privacy',
      href: null,
      blankTarget: true,
    }, {
      key: 'Policy',
      title: 'Policy',
      href: null,
      blankTarget: true,
    }
    *****/
    ],

    copyright: (
      <Fragment>
        Copyright <Icon type="copyright" /> 2017-2018 Cognitive Leap Solutions, Inc.
      </Fragment>
    ),
  };

  return (
    <DocumentTitle title={getPageTitle(location.pathname)}>
      <div>
        <CenterHeader {...HeaderProps} />
        <Content>
          <div className={styles.background_layout}>
            {children}
          </div>
        </Content>
        <NormalFooter {...FooterProps} />
      </div>
    </DocumentTitle>
  );
};

export default connect(({ user }) => ({ user }))(TestCenterLayout);
