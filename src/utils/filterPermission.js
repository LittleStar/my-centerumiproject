
// export function ExistAtAllPermission(allPermission, currentPermission) {
//   var count = 0
//   var notGrantedPermissionList = []
//   var result = { exist: false, list: [] }
//   currentPermission.forEach(item => {
//     allPermission.indexOf(item) !== -1 ? count ++ : notGrantedPermissionList.push(item)
//   })
//   if (count === currentPermission.length) {
//     result['exist'] = true
//     return result
//   }
//   result['list'] = notGrantedPermissionList
//   return result
// }
// function GetCurrentPermission(allPermission, currentPermissionList) {
//   const allPermissionList = allPermission || []
//   const grantedPermissionList = []
//   currentPermissionList.forEach(item => {
//     allPermissionList.indexOf(item) !== -1 ? grantedPermissionList.push(item) : null
//   })
//   return grantedPermissionList
// }

// export function isExistAtCurrentPermission(currentPermissionList, currentPermission) {
//   return currentPermissionList.indexOf(currentPermission) !== -1 ? true : false
// }

export function isExistAtCurrentPermission(grantedPermissionList, currentPermission) {
  return grantedPermissionList.indexOf(currentPermission) !== -1 ? true : false
}

export function GetCurrentPermission(allPermission, currentPermissionList) {
  const allPermissionList = allPermission || []
  const grantedPermissionList = []
  for (var key in currentPermissionList) {
    allPermissionList.indexOf(currentPermissionList[key]) !== -1 ? grantedPermissionList.push(currentPermissionList[key]) : null
  }

  return grantedPermissionList
}
