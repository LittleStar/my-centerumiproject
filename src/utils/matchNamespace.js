const path = require('path')
const fs = require('fs')
const readline = require('readline')

const basePath = path.resolve(__dirname, '../models')
const readdir = path => fs.readdirSync(path,
  { withFileTypes: true })
const dir = readdir(basePath)

const files = []

function recursive(dir, basePath) {
  dir.forEach(element => {
    if (element.isDirectory()) {
      let childPath = path.resolve(basePath, element.name)
      recursive(readdir(childPath), childPath)
    } else {
      files.push({ dirent: element, path: basePath })
    }
  })
}
recursive(dir, path.resolve(basePath))

function join(fileObj) {
  return path.resolve(fileObj.path, fileObj.dirent.name)
}

function getFilename(filepath) {
  return path.parse(filepath).name
}

const nsList = []
files.forEach(element => {
  // const lineReader = readline.createInterface({ input: fs.createReadStream(join(element)) })
  // lineReader.on('line', (line) => {
  //   const match = line.indexOf('namespace')
  //   if (match !== -1) nsList.push(line)
  // })
  let content = fs.readFileSync(join(element), { encoding: 'utf-8' }).split('\r\n')
  content.forEach(line => {
    const match = line.indexOf('namespace')
    if (match !== -1) {
      let str = line.replace('namespace:', '').replace(',', '').replace('\'', '').replace('\'', '').trim()
      nsList.push({ filename: getFilename(join(element)), namespace: str })
    }
  })
})

// console.log(nsList)
let pass = true
nsList.forEach(element => {
  if (element.filename !== element.namespace) {
    console.log(element)
    pass = false
  }
})

if (pass) console.log('PASSED! NO DUPLICATED NAMESPACE')
console.log('Done')
