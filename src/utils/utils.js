import allPermissionObj from 'utils/urlPermission';


function getRelation(str1, str2) {
  if (str1 === str2) {
    console.warn('Two path are equal!'); // eslint-disable-line
  }
  const arr1 = str1.split('/');
  const arr2 = str2.split('/');
  if (arr2.every((item, index) => item === arr1[index])) {
    return 1;
  } else if (arr1.every((item, index) => item === arr2[index])) {
    return 2;
  }
  return 3;
}



function getRenderArr(routes) {
  let renderArr = [];
  renderArr.push(routes[0]);
  for (let i = 1; i < routes.length; i += 1) {
    let isAdd = false;
    // 是否包含
    isAdd = renderArr.every(item => getRelation(item, routes[i]) === 3);
    // 去重
    renderArr = renderArr.filter(item => getRelation(item, routes[i]) !== 1);
    if (isAdd) {
      renderArr.push(routes[i]);
    }
  }
  return renderArr;
}

/**
 * Get router routing configuration
 * { path:{name,...param}}=>Array<{name,path ...param}>
 * @param {string} path
 * @param {routerData} routerData
 */
export function getRoutes(path, routerData) {
  let routes = Object.keys(routerData).filter(
    routePath => routePath.indexOf(path) === 0 && routePath !== path
  );

  // Replace path to '' eg. path='user' /user/name => name
  routes = routes.map(item => item.replace(path, ''));
  // Get the route to be rendered to remove the deep rendering
  const renderArr = getRenderArr(routes);
  // Conversion and stitching parameters
  const renderRoutes = renderArr.map(item => {
    const exact = !routes.some(route => route !== item && getRelation(route, item) === 1);
    return {
      exact,
      ...routerData[`${path}${item}`],
      key: `${path}${item}`,
      path: `${path}${item}`,
    };
  });
  return renderRoutes;
}

export function matchDynamicForm(originDocument,valueObj){
  const {Properties} = originDocument

  Properties.forEach((item)=>{
    if(valueObj[item.FormKey]!==undefined){
      let value = valueObj[item.FormKey]
      // //在dropdown的时候需要做一下处理
      if(item.EditorType==='Dropdown'&&typeof(value)==='number'){
        value = value.toString()
      }
      item["Value"] = value
    }
  })
  return {...originDocument,Properties}
}

export function generateRequestStr(queryObj){
  if(!queryObj) return ''

  const queryKeyList = Object.keys(queryObj)
  let base = '?'
  if(queryKeyList.length===0) return ''
  else{
    queryKeyList.forEach((keyItem,index)=>{
      let queryItem = queryObj[keyItem]

      if(index!==queryKeyList.length-1)
        base+=encodeURIComponent(keyItem)+'='+encodeURIComponent(queryItem)+'&'
      else
        base+=encodeURIComponent(keyItem)+'='+encodeURIComponent(queryItem)
    })
  }
  return base
}

export function matchUserInfo(originUserInfoList, valueObj){

  originUserInfoList.forEach((item)=>{
    if(valueObj[item.itemKey]!==undefined){
      let value = valueObj[item.itemKey]
      item['itemVal'] = value
    }
  })
  return originUserInfoList
}

function MenuTreeToObj(menuList){
 return menuList.reduce((menuObj,item)=>{
    //当前版本是只要有key，就显示，默认就是true
    menuObj = {...menuObj,[item.name]:true}
    if(item.items.length>0){
      return Object.assign(menuObj,MenuTreeToObj(item.items))
    }
    return menuObj
  },{})
}


function filterMenuList(menuList,keyObj){
  let result = []
  menuList.forEach((item)=>{
    if(item.children!==undefined){
      const childrenList = filterMenuList(item.children,keyObj)
      if(childrenList.length!==0) item = {...item,children:childrenList}
    }
    if(item.permissionKey===undefined||keyObj[item.permissionKey]!==undefined){
      result.push(item)
    }
  })
  return result
}

//根据permissionList，判断url是否可以访问
export function CheckUrlPermission(url, currentPermissionList) {
  const routeMatchPermission = allPermissionObj[url]
  console.log('routeMatchPermission',routeMatchPermission)
  if(routeMatchPermission===undefined){
    return false
  }
  const bolValue = routeMatchPermission.every(item => {
    return currentPermissionList.includes(item)
  });
  return bolValue;
}


//originMenu: 原始全部menu
//menuItems: 后端返回的menu
export function filterMenu(originMenu, menuItems){

  //将树形结构转化成obj，方便后面使用
  const menuObj = MenuTreeToObj(menuItems)
  //递归获取要显示的menu
  return filterMenuList(originMenu,menuObj)
}

//originList:原始list
//permissionList：后端获取的用户权限list
//return list,filter之后
export function filterList(originList,permissionList){
  return originList.filter((item)=>{
    const {permissionKey} = item
    //如果不需要权限的话，默认显示该内容
    if(permissionKey===undefined) return true
    return permissionList.includes(permissionKey)
  })
}

//判断是否有某个指定的权限
export function checkPermissionKey(permissionKey,permissionList){
  return permissionList.includes(permissionKey)
}

// 给官方演示站点用，用于关闭真实开发环境不需要使用的特性
export function isAntdPro() {
  return window.location.hostname === 'preview.pro.ant.design';
}
