module.exports = {

	common:{

	},
	cl:{

	},
	center:{

	},
	location:{
		//编辑潜在用户
		PotentialUserEdit:'Pages.Location.Users.PotentialUser.Edit',
		//创建潜在用户
		PotentialUserCreate:'Pages.Location.Users.PotentialUser.Create',
		//编辑会员。包括班主任，会员
		VipEdit:'Pages.Location.Users.Vip.Edit',
		//分配课程顾问
		AssignSales:'Pages.Location.Users.Sales',
		//创建沟通记录，查看沟通记录
		SalesContact:'Pages.Location.Sales.Contact',
		//合同管理
		Contract:'Pages.Location.Contract',
		//申请新合同，或者合同被打回后修改
		ContractEdit:'Pages.Location.Contract.Edit',
		//取消合同
		ContractCancel:'Pages.Location.Contract.Cancel',
		//签到管理
		CheckIn:'Pages.Location.CheckIn',
		//查看课程表
		TeachingSchedule:'Pages.Location.TeachingCenter.TeachingSchedule',
		//查看班级详情
		CourseInstance:'Pages.Location.TeachingCenter.CourseInstance',
		//班级编辑/创建，包括编辑老师，教室上课时间，
		CourseInstanceEdit:'Pages.Location.TeachingCenter.CourseInstance.Edit',
		//班级用户编辑，包括添加学生，删除学生
		CourseInstanceUserEdit:'Pages.Location.TeachingCenter.CourseInstance.UserEdit',
		//查看反馈记录
		CourseInstanceFeedback:'Pages.Location.TeachingCenter.CourseInstance.Feedback',
		//创建反馈记录, 删除反馈记录
		CourseInstanceFeedbackEdit:'Pages.Location.TeachingCenter.CourseInstance.Feedback.Edit',
		//手动排课
		ManualSchedule:'Pages.Location.TeachingCenter.ManualSchedule',
		//创建/编辑/删除开课计划。编辑开课计划长期用户
		TeachingPlanEdit:'Pages.Location.TeachingCenter.TeachingPlan.Edit',
		//合同审核
		ContractVerify:'Pages.Location.VerifyCenter.Contract',
		//编辑员工信息，包括新建，编辑密码等
		SettingCenterHrEdit:'Pages.Location.SettingCenter.Hr.Edit',

	}
}
