module.exports = {

	genderType:[{
		str:'Unknown',
		cn:'',
	},{
		str:'Male',
		cn:'男',
	},{
		str:'Female',
		cn:'女',
	}],

	vipType:[{
		str:'None',
		cn:'非会员',
	},{
		str:'Vip',
		cn:'会员',
	},{
		str:'ExpiredVip',
		cn:'过期会员',
	}],

	displayDateType:[{
		str:'Year',
		cn:'年',
	},{
		str:'Month',
		cn:'月',
	},{
		str:'Week',
		cn:'周',
	},{
		str:'Day',
		cn:'天',
	},],

	coursePackageType:[{
		str:'OneToMany',
		cn:'一对多',
	},{
		str:'OneToOne',
		cn:'一对一',
	}],

	currencyType:[{
		str:'RMB',
		cn:'人民币',
	}],

	contractStatus:[{
		str:'None',
		cn:'合同草稿',
	},{
		str:'Commited',
		cn:'正在审核中',
	},{
		str:'Deny',
		cn:'被拒合同',
	},{
		str:'Confirmed',
		cn:'正式合同',
	},{
		str:'Expired',
		cn:'过期合同',
	},{
		str:'Cancelled',
		cn:'已取消合同',
  }],

  contractType: [{
    str:'New',
		cn:'新合同',
  },{
    str:'Upgrade',
		cn:'升包',
  },{
    str:'ModifyPackage ',
		cn:'更改课包',
  },]

}
