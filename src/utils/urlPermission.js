module.exports = {

  //列表页

  // 首页
  '/system/manage/dashboard': ['Pages.Location.Dashboard'],
  // '/system/manage/dashboard/operationalReport': [],
  '/system/manage/dashboard/checkin': ['Pages.Location.CheckIn'],
  '/system/manage/dashboard/checkin/notCheckin': ['Pages.Location.CheckIn'],
  '/system/manage/dashboard/courseSchedule': ['Pages.Location.TeachingCenter.TeachingSchedule'],
  //报表中心
  '/system/manage/report': [],
  //营销中心
  '/system/manage/sales/user': ['Pages.Location.Users.PotentialUser'],
  //会员中心
  '/system/manage/user/vip': ['Pages.Location.Users.Vip'],
  '/system/manage/user/contract': ['Pages.Location.Contract'],
  //教务中心
  '/system/manage/edu/class': ['Pages.Location.TeachingCenter.TeachingSchedule'],
  '/system/manage/edu/class': [],
  //测评中心
  '/system/manage/test/vrat': ['Pages.Location.TestingCenter'],
  //审核中心
  '/system/manage/verify/contract':['Pages.Location.VerifyCenter.Contract'],
  '/system/manage/verify/refund':['Pages.Location.VerifyCenter.Refund'],
  '/system/manage/verify/gift':['Pages.Location.VerifyCenter.Gift'],
  //配置中心
  '/system/manage/setting/user': ['Pages.Location.SettingCenter.Hr'],
  '/system/manage/setting/account':[],
  '/system/manage/setting/course':['Pages.Location.SettingCenter.Course'],
  '/system/manage/setting/room':['Pages.Location.SettingCenter.Classroom'],
  //管理中心
  '/system/manage/manage/organization': [],
  '/system/manage/manage/center':[],
  '/system/manage/manage/region/regionList': [],
  '/system/manage/manage/course/packageList': ['Pages.Center.Management.Course'],
  //详情页
  '/system/manage/userDetail/basicInfo': [],
};
