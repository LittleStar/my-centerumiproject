import fetch from 'dva/fetch';
import qs from 'qs'
import { notification } from 'antd';
import { hashHistory } from 'react-router';
import { getAuthHeader,redirectLogin} from './auth.js';
import download from './download'
import { baseURL } from './config'

const codeMessage = {
  200: 'OK',
  201: 'Created',
  202: 'Accepted',
  204: 'No Content',
  400: 'Bad Request',
  401: 'Unauthorized',
  403: 'Forbidden',
  404: 'Not Found',
  406: 'Not Acceptable',
  410: 'Gone',
  422: 'Object Error',
  500: 'Internal Server Error',
  502: 'Bad Gateway',
  503: 'Service Unavailable',
  504: 'Gateway Timeout',
};

function parseJSON(response) {
    return response.text().then(
        function(text) {
            const responseJson = text?JSON.parse(text):{}
            const {result,error} =  responseJson
            return {
                    result,
                    error,
                    'url':response.url,
                    'type':response.type,
                    'statusText':response.statusText,
                    'status':response.status,
                    'headers':response.headers,
            }
        })
    // const err = new Error('response not JSON format')
    // throw err;
}

function checkStatus(response) {

  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  //const errortext = codeMessage[response.status] || response.statusText;
  // show notification
  //   notification.error({
  //     message: `请求错误 ${response.status}: ${response.url}`,
  //     description: errortext,
  //   });

  throw JSON.stringify(response)

  // const error = new Error(JSON.stringify(response));
  // console.log(error)
  // error.name = response.status;
  // error.response = response;
  // //error.message = response.statusText;
  // throw error;
}

function ErrorHandle(err){

  if(isJson(err)){
    const data = JSON.parse(err)
    const { error,status } = data
    if(status===401){
      window.g_app._store.dispatch({
        type: 'user/logout',
      });
      return{success:false};
    }
    else{
      notification.error({
        message: error.message,
        description: error.details,
      });
      return{success:false,...data}
    }

  }
  //
  else{
    notification.error({
      message: 'error connection to server',
    });
    return{success:false}
  }

}




/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default function request(url, options) {
  // options.credentials='include'

  //Head
  options.headers = options.headers || {}
  if (!hasContentType(options.headers)) {
    options.headers['Content-Type'] = 'application/json'
  }
   options.headers={...options.headers,...getAuthHeader()}
  //Body
  options.body = objectToQueryString(options.body,options.headers['Content-Type'])
  return fetch(baseURL + url, options)
    .then(parseJSON)
    .then(checkStatus)
    .then((data)=>(
      {success: true, ...data})
    )
    .catch(ErrorHandle)
}

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function hasContentType(headers) {
  return Object.keys(headers).some(function(name) {
    return name.toLowerCase() === 'content-type'
  })
}

function parseErrorMessage({data}) {
  const { status, message } = data;
  if (status === 'error') {
    throw new Error(message);
  }
  return {data};
}

//Parse
function objectToQueryString(data,contenthead) {
  if(!isObject(data))
    return data;
  if(contenthead==='application/x-www-form-urlencoded')
    return qs.stringify(data);
  if(contenthead==='application/json')
    return JSON.stringify(data);
  return data;
}

function isObject(data) {
  return Object.prototype.toString.call(data) === '[object Object]'
}
