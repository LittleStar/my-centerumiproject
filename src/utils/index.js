import config from './config';
import enumSetting from './enumSetting';
import request from './request';
import downloadfile from './downloadfile';
import { getAuthHeader } from './auth';
import { matchDynamicForm, generateRequestStr, matchUserInfo } from './utils';
import pageConfig from './pageConfig';
import permissionConfig from './permissionConfig';
// import allPermissionObj from './urlPermission';
// module.exports = {
//   config,
//   request,
//   downloadfile,
// }

/**
 * @param   {String}
 * @return  {String}
 */

const queryURL = name => {
  let reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`, 'i');
  let r = window.location.search.substr(1).match(reg);
  if (r != null) return decodeURI(r[2]);
  return null;
};

//根据permissionList，判断url是否可以访问
// function CheckUrlPermission(url, currentPermissionList, allPermissionObj ) {
//   const routeMatchPermission = allPermissionObj[url]
//   const bolValue = routeMatchPermission.every(item => {
//     currentPermissionList.indexOf(item) !== -1
//   });
//   return bolValue;
// }

//返回一个基于url的redirect url
//'/system/manage/dashboard'=>'/system/manage/dashboard/courseSchedule'
//'/system/manage/dashboard/checkin'=>'/system/manage/dashboard/checkin/notCheckin'
function DefaultRedirectUrl(permissionList, url = '/') {
  return '/system/manage/dashboard/checkin/notCheckin';
}

export {
  permissionConfig,
  enumSetting,
  config,
  pageConfig,
  request,
  // CheckUrlPermission,
  DefaultRedirectUrl,
  downloadfile,
  matchDynamicForm,
  generateRequestStr,
  matchUserInfo,
  getAuthHeader,
};
