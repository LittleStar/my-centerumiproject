module.exports = {

	public: {
		loading: '/',
		login: '/login',
		notFound: '/notFound',
	},

	dashboard: {
    dashboard: '/system/manage/dashboard',
      courseSchedule: '/system/manage/dashboard/courseSchedule',
      checkin: '/system/manage/dashboard/checkin',
      notCheckin: '/system/manage/dashboard/checkin/notCheckin',
      checkinHistory: '/system/manage/dashboard/checkin/checkinHistory',
    	leftHistory: '/system/manage/dashboard/checkin/leftHistory',
    	operationalReport: '/system/manage/dashboard/operationalReport',
	},

	sale:{
  		saleUserList:'/system/manage/sales/user',
	},

	user: {
		vip: '/system/manage/user/vip',
  		contract: '/system/manage/user/contract',
	},

	detail:{
		//用户详情页
		userDetail: '/system/manage/userDetail',
    contractInfo:'/system/manage/userDetail/contractInfo',
    basicInfo:'/system/manage/userDetail/basicInfo',

		//合同详情页
		contractDetail:'/system/manage/contractDetail',

		//班级详情页
		classDetail: '/system/manage/classDetail',
		classSubjectList: '/system/manage/classDetail/studentList',
		classCourseInfo:'/system/manage/classDetail/courseInfo',

		//课程计划详情页
		classPlanDetail: '/system/manage/classPlanDetail',
		classPlanSubjectList: '/system/manage/classPlanDetail/classPlanStudentList',
		classPlanCourseInfo:'/system/manage/classPlanDetail/classPlanCourseInfo',

			//中心(店铺)详情页
		centerDetail: '/system/manage/centerDetail',
		employeePage: '/system/manage/centerDetail/employee',
		businessPage: '/system/manage/centerDetail/business',
		cdHistoryPage: '/system/manage/centerDetail/cdHistory',

		//区域经理详情页
		regionManagerDetail: '/system/manage/regionalManagerDetail',

		//区域详情页
		regionDetail: '/system/manage/regionDetail',

		//测试详情页
		vratDetail: '/system/manage/vratDetail',
		vratDetailBasicInfo:'/system/manage/vratDetail/baseInfo',

		//课程详情页
    courseDetail:'/system/manage/courseDetail',

    // 员工详情页
    employeeDetail: '/system/manage/employeeDetail',
	},

	edu: {
		classList:'/system/manage/edu/class',
		plan:'/system/manage/edu/plan',
		course: '/system/manage/edu/course',
	},

	setting:{
  		room:'/system/manage/setting/room',
      user: '/system/manage/setting/user',
      account: '/system/manage/setting/account',
	},
    report:{
      report: '/system/manage/report',
      userSource: '/system/manage/report/sa/us',
      monthCost: '/system/manage/report/te/mc',
      salesData: '/system/manage/report/sa/sales',
      teach: '/system/manage/report/te',
      appear: '/system/manage/report/te/ap',
      appearDetails: '/system/manage/report/te/ap/appearDetails',
      appearAdvisor: '/system/manage/report/te/ap/appearAdvisor',
      appearTeacher: '/system/manage/report/te/ap/appearTeacher',
      appearSales: '/system/manage/report/te/ap/appearSales',
      teacherCourse: '/system/manage/report/te/tc',
      monthPay: '/system/manage/report/pa/mp',
    },

	manage: {
		permission: '/system/manage/manage/permission',
		center: '/system/manage/manage/center',
		course: '/system/manage/manage/course',
		organization:'/system/manage/manage/organization',
		courseList:'/system/manage/manage/course/courseList',
      packageList:'/system/manage/manage/course/packageList',
      regionList: '/system/manage/manage/region/regionList',
      regionalManagerList: '/system/manage/manage/region/regionalManagerList'
	},

	verify: {
		contract:'/system/manage/verify/contract',
	}

}
