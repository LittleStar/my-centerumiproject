
import { setLocalStorage,getLocalStorage, delCookie } from './helper';

// Auth
export function getAuthHeader() {
  return (
	   {
      'Authorization': 'Bearer ' + getLocalStorage('CognitiveleapUser'),
    }
  )
}


export function logOut() {
  // delCookie({
  //   name: 'CognitiveleapUser',
  //   path: '/',
  //   domain: '.localhost',
  // });
   setLocalStorage('CognitiveleapUser','');
   
   //authenticated();
}

export function logIn(token){
	setLocalStorage('CognitiveleapUser',token);
}