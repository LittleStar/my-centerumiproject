import fetch from 'dva/fetch';
import qs from 'qs'
import { getAuthHeader, redirectLogin } from './auth.js';
import download from './download'
import { baseURL } from './config'

function parseJSON(response) {
    return response.text().then(function(text) {
        return {
            data: text ? JSON.parse(text) : {},
            status: response.status
        }
    })

}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }

    const error = new Error(JSON.stringify(response));
    throw error;
}
/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */

export default function downloadfile(url, options) {

    options.headers = options.headers || {}
    if (!hasContentType(options.headers)) {
        options.headers['Content-Type'] = 'application/json'
    }
    options.headers = {
        ...options.headers,...getAuthHeader()
    }
    //Body
    options.body = objectToQueryString(options.body, options.headers['Content-Type'])
    return fetch(baseURL + url, options)
        .then(resp => {
            var filename = resp.headers.get('Content-Disposition');
            let decodeFileName = decodeURIComponent(filename)
            const realName = getStr(decodeFileName)
            resp.blob().then(function(blob) {
                download(blob, realName);
            })
        });
}

function getStr(str) {
    const getyinihaoinfo = /UTF.*/g
    var result = str.match(getyinihaoinfo);
    const fullname = result[0]
    const filename = fullname.split('\'\'')
    return filename[1]
}

function hasContentType(headers) {
    return Object.keys(headers).some(function(name) {
        return name.toLowerCase() === 'content-type'
    })
}

function parseErrorMessage({data}) {
    const {status, message} = data;
    if (status === 'error') {
        throw new Error(message);
    }
    return {
        data
    };
}

//Parse
function objectToQueryString(data, contenthead) {
    if (!isObject(data))
        return data;
    if (contenthead === 'application/x-www-form-urlencoded')
        return qs.stringify(data);
    if (contenthead === 'application/json')
        return JSON.stringify(data);
    return data;
}

function isObject(data) {
    return Object.prototype.toString.call(data) === '[object Object]'
}
