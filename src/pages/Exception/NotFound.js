const NotFound = () => {
	console.log('not found')
  return (
    <div>
    	<h1>404 抱歉，你访问的页面不存在</h1>
    	<a href='/'>点击返回首页</a>
    </div>
  );
};

export default NotFound;
