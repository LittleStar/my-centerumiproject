
import { connect } from 'dva'
import { Button, Icon, Row, Form, Input } from 'antd'
import styles from './login.css'
// import cogleapLogo from './img/cogleaplogo.png'
import cogleapLogo from '../../components/Header/img/CogLeapLogo.png'

const FormItem = Form.Item;

const Login = ({ login, dispatch, form: { getFieldDecorator, validateFields, } }) => {

  const handleSubmit = (e) => {
    e.preventDefault()
    validateFields((err, value) => {
      if (err) {
        return
      }
      const { email, password } = value
      dispatch({ type: 'login/login', payload: value })
    })
  }

  return (
    <div className={styles.bgc}>
      <div className={styles.main}>
        <div>
          <div className={styles.marginAuto}>
            <img src={cogleapLogo} style={{ width: 280, height: 80 }} alt="logo" />
          </div>
        </div>
        <Form onSubmit={handleSubmit}>
          <FormItem
            label="邮箱/手机"
          >
            {getFieldDecorator('userNameOrEmailAddress', {
              initialValue: '',
              rules: [{ required: true, message: '请输入用户邮箱或手机' }]
            })(
              <Input
                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="请输入邮箱或手机" />
            )}

          </FormItem>
          <FormItem
            label="密码"
            style={{ marginTop: -20 }}
          >
            {
              getFieldDecorator('password', {
                initialValue: '',
                rules: [{ required: true, message: '请输入密码' }]
              })(
                <Input
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="请输入密码"
                  type='password' />
              )
            }
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit" >登录</Button>
          </FormItem>
        </Form>
      </div>
    </div>
  )
}


export default Form.create()(connect(({ login }) => ({ login }))(Login))
