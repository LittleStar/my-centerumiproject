import React from 'react';
import {connect} from 'dva'
import {Spin} from 'antd'
import styles from './LoadingPage.css'

const LoadingPage = () => {

	return (

		<div className = {styles.page_div}>
			<div className={styles.spinnerWrapper}>
				<Spin tip="Loading massive data from far away..." size="large" className={styles.spinnerWrapperText}/>
	        </div>
		</div>

	);

};

export default connect()(LoadingPage);
