
import React from 'react'
import { connect } from 'dva'
import MagicTable from '../../../../components/MagicTable/MagicTable'

const CheckinHistory = ({ dispatch, checkinHistory, loading }) => {
  const { checkinHistory: checkinHistoryLoading } = loading.models
  const { listCheckinHistory, pagination } = checkinHistory

  const listSupportCheckinHistory = {
    type: {
      showText: '用户状态',
      showType: 'Text',
    },
    checkRestUnplannedCourseCount: {
      showText: '试听',
      showType: 'Text',
    },
    subjectName: {
      showText: '儿童姓名',
      showType: 'Text',
    },
    guardiansPhone: {
      showText: '家长电话',
      showType: 'Text',
    },
    startTime: {
      showText: '上课时间',
      showType: 'Text',
      sorter: {
        hasSorter: false
      }
    },
    courseTitle: {
      showText: '课程名',
      showType: 'Text',
    },
    classroom: {
      showText: '教室',
      showType: 'Text',
    },
    teacher: {
      showText: '任课老师',
      showType: 'Text',
    },
    userStatus: {
      showText: '签到状态',
      showType: 'Status',
      addtional: {
        statusArray: ['未签到', '已签到', '已请假', '已旷课']
      },
      filter: {
        filterOptions: [
          { text: '已签到', value: 1 },
        ],
      }
    },
  }

  const TableProps = {
    loading: checkinHistoryLoading,
    key: 'CheckinHistory',
    listData: listCheckinHistory,
    listSupport: listSupportCheckinHistory,
    pagination,
    onTableChange(pagination, filters={}, sorter) {
      if(Object.keys(pagination).length!==0){
        dispatch({type: 'checkinHistory/updatePagination', payload: pagination})
      }
    },
  }

  return (
    <div>
      <MagicTable {...TableProps} />
    </div>
  )
}
export default connect(({ checkinHistory, loading }) => ({ checkinHistory, loading }))(CheckinHistory)

