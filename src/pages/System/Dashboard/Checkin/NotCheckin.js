import React from 'react'
import { connect } from 'dva'
import { Modal, Input, } from 'antd'
import MagicTable from '../../../../components/MagicTable/MagicTable'


const Search = Input.Search
const NotCheckin = ({ dispatch, notCheckin, loading }) => {
  const { notCheckin: notCheckinLoading } = loading.models

  const { listNocheckin, pagination } = notCheckin

  const listSupportNocheckin = {
    type: {
      showText: '用户状态',
      showType: 'Text',
    },
    checkRestUnplannedCourseCount: {
      showText: '试听',
      showType: 'Text',
    },
    subjectName: {
      showText: '儿童姓名',
      showType: 'Text',
    },
    guardiansPhone: {
      showText: '家长电话',
      showType: 'Text',
    },
    startTime: {
      showText: '上课时间',
      showType: 'Text',
      sorter: {
        hasSorter: false
      }
    },
    courseTitle: {
      showText: '课程名',
      showType: 'Text',
    },
    classroom: {
      showText: '教室',
      showType: 'Text',
    },
    teacher: {
      showText: '任课老师',
      showType: 'Text',
    },
    userStatus: {
      showText: '签到状态',
      showType: 'Status',
      addtional: {
        statusArray: ['未签到', '已签到', '已请假', '已旷课']
      },
      filter: {
        hasFilter: true,
        filterOptions: [
          { text: '未签到', value: 0 },
          { text: '已旷课', value: 3 },
        ],
      }
    },
  }

  const TableProps = {
    loading: notCheckinLoading,
    key: 'Checkin',
    listData: listNocheckin,
    listSupport: listSupportNocheckin,
    pagination,
    onTableChange(pagination, filters, sorter) {
      if(pagination!==undefined){
        dispatch({type: 'notCheckin/updatePagination', payload: pagination})
      }
    },
    Actions: [{
      showText: '签到',
      onClick(record) {
        Modal.confirm({
          title: '签到',
          content: `您确定要给 ${record.subject.name} 签到吗`,
          onOk() {
            const subjectId = record.subject.id
            const courseInstanceId = record.courseInstance.id
            dispatch({type: 'notCheckin/onCheckIn', payload: {subjectId, courseInstanceId}})
          }
        })
      }
    },
    {
      showText: '请假',
      onClick(record) {
        Modal.confirm({
          title: '请假',
          content: `您确定要给 ${record.subject.name} 请假吗`,
          onOk() {
            const subjectId = record.subject.id
            const courseInstanceId = record.courseInstance.id
            dispatch({type: 'notCheckin/onLeave', payload: {subjectId, courseInstanceId}})
          }
        })
      }
    },
    // {
    //   showText: '旷课',
    //   onClick(record) {
    //     Modal.confirm({
    //       title: '旷课',
    //       content: `您确定要给 ${record.subject.name} 旷课吗`,
    //       onOk() {
    //         const subjectId = record.subject.id
    //         const courseInstanceId = record.courseInstance.id
    //         dispatch({type: 'notCheckin/onAbsent', payload: {subjectId, courseInstanceId}})
    //       }
    //     })
    //   }
    // },
    ]
  }

  return (
    <div>
      <MagicTable {...TableProps} />
    </div>
  )
}
export default connect(({ notCheckin, loading }) => ({ notCheckin, loading }))(NotCheckin)
