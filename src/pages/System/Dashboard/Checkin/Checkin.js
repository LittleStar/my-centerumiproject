import React from 'react'
import { connect } from 'dva'
import moment from 'moment'
import { DatePicker, Input, Row, Col, Tabs, Button } from 'antd'
import { Route, Redirect, Switch, routerRedux } from 'dva/router'
import { getRoutes } from '../../../../utils/utils'

const TabPane = Tabs.TabPane;

const { Search } = Input

const Checkin = ({ routerData, location, children, match, dispatch, checkin }) => {

  const { selectDate, searchField, checkInType } = checkin

  //const routes = getRoutes(match.path, routerData)
  const { search } = location

  const searchProps = {
    placeholder: "搜索用户",
    onSearch(value) {
      dispatch({ type: 'checkin/updateSearchField', payload: value })
    },
    onChange(e) {
      dispatch({ type: 'checkin/updateState', payload: { searchField: e.target.value } })
    },
    value: searchField,
  }

  const datePickerProps = {
    placeholder: "选择日期",
    onChange(value) {
      dispatch({ type: 'checkin/updateDate', payload: value })
    },
    value: selectDate,
    format: 'YYYY/MM/DD',
  }

  const showDate = selectDate ? moment(selectDate).format('YYYY-MM-DD') : ''

  const onTabChange = (key) => {
    dispatch({ type: 'checkin/switchTab', payload: key })
  }

  const onClickClear = () => {
    dispatch({ type: 'checkin/clearSearch' })
  }

  const onClickToday = () => {
    dispatch({ type: 'checkin/todayCheckIn' })
  }

  return (
    <div>
      <Row gutter={16} style={{ marginTop: 20, marginBottom: 20 }}>
        <Col span={4}>
          <Search {...searchProps} />
        </Col>

        <Col span={4}>
          <DatePicker {...datePickerProps} />
        </Col>
        <Col span={4}>
          <Button onClick={onClickClear}>清空搜索</Button>
        </Col>

        <Col span={8}>
          <h3>{'当前选择日期： ' + showDate}</h3>
        </Col>
        <Col span={4}>
          <Button onClick={onClickToday}>今天签到</Button>
        </Col>

      </Row>

      <Tabs activeKey={checkInType} onChange={onTabChange}>
        <TabPane tab="未签到" key="notCheckIn" />
        <TabPane tab="已签到" key="checkIn" />
        <TabPane tab="已请假" key="left" />
      </Tabs>

      {/* <Switch>
        {routes.map(item => (
          <Route
            key={item.key}
            path={item.path}
            component={item.component}
            exact={item.exact}
          />
        ))}
        <Redirect exact from='/system/manage/dashboard/checkin' to={'/system/manage/dashboard/checkin/notCheckin' + search} />
      </Switch> */}
      {children}
    </div>
  )
}
export default connect(({ checkin }) => ({ checkin }))(Checkin)

