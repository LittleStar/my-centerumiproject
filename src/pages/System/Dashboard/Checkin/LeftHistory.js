
import React from 'react'
import { connect } from 'dva'
import { Modal } from 'antd'
import MagicTable from '../../../../components/MagicTable/MagicTable'

const LeftHistory = ({ dispatch, leftHistory, loading }) => {
  const { leftHistory: leftHistoryLoading } = loading.models
  const { list, pagination } = leftHistory

  const listSupport = {
    type: {
      showText: '用户状态',
      showType: 'Text',
    },
    checkRestUnplannedCourseCount: {
      showText: '试听',
      showType: 'Text',
    },
    subjectName: {
      showText: '儿童姓名',
      showType: 'Text',
    },
    guardiansPhone: {
      showText: '家长电话',
      showType: 'Text',
    },
    startTime: {
      showText: '上课时间',
      showType: 'Text',
      sorter: {
        hasSorter: false
      }
    },
    courseTitle: {
      showText: '课程名',
      showType: 'Text',
    },
    classroom: {
      showText: '教室',
      showType: 'Text',
    },
    teacher: {
      showText: '任课老师',
      showType: 'Text',
    },
    userStatus: {
      showText: '签到状态',
      showType: 'Status',
      addtional: {
        statusArray: ['未签到', '已签到', '已请假', '已旷课']
      },
      filter: {
        filterOptions: [
          { text: '已请假', value: 2 },
        ],
      }
    },
  }

  const TableProps = {
    loading: leftHistoryLoading,
    key: 'leftHistory',
    listData: list,
    listSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      if(pagination!==undefined){
        dispatch({type: 'leftHistory/updatePagination', payload: pagination})
      }
    },
    Actions: [{
      showText: '取消请假',
      onClick(record) {
        Modal.confirm({
          title: '取消请假',
          content: `您确定要给 ${record.subject.name} 取消请假吗`,
          onOk() {
            const subjectId = record.subject.id
            const courseInstanceId = record.courseInstance.id
            dispatch({type: 'leftHistory/onCancelLeft', payload: {subjectId, courseInstanceId}})
          }
        })
      }
    },]
  }

  return (
      <MagicTable {...TableProps} />
  )
}
export default connect(({ leftHistory, loading }) => ({ leftHistory, loading }))(LeftHistory)

