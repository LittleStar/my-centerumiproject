import React from 'react'
import { connect } from 'dva'
import { Row, Col, Card, Icon, Spin } from 'antd'
import styles from './operationalReport.less'
import WeeklyChart from 'components/Chart/WeeklyChart'


const OperationalReport = ({ operationalReport, user, loading }) => {
  const operationalReportLoading = loading.models.operationalReport

  const NumberToWeekDayKey = {
    "0": 'Sunday',
    "1": 'Monday',
    "2": 'Tuesday',
    "3": 'Wednesday',
    "4": 'Thursday',
    "5": 'Friday',
    "6": 'Saturday',
  }

  const { currentMonthInfo } = operationalReport
  const { thisWeekSalesPrice, thisWeekSalesPricePercent, thisMonthSalesPrice, thisMonthSalesPricePercent, thisMonthCostClassPrice, thisMonthSuccessNewContractCount,
    assessmentCount, thisWeekCostClassCount, thisMonthCostClassCount, thisWeekSalesPriceDetail,thisWeekLongTermClassCount,thisWeekShortTermClassCount} = currentMonthInfo


    let WeekSalesPriceDetail = thisWeekSalesPriceDetail? thisWeekSalesPriceDetail.map(item=>{
      return {...item, dayOfWeek:NumberToWeekDayKey[item.dayOfWeek] }
    }): []

  return (
    <React.Fragment>
      {
        operationalReportLoading ?
          <Spin /> :
          <React.Fragment>
            <div style={{ background: '#ECECEC', padding: '30px' }}>
              <Card title="本月统计" bordered={false}>
                <Row>
                  <Col span={6} className={styles.paddingStyle}>
                    <span className={styles.reportItem}>本周业绩</span><br />
                    <span className={styles.reportValue}>￥{thisWeekSalesPrice}元</span></Col>
                  {/* <Col span={6} className={styles.paddingStyle}>
                    <span className={styles.reportItem}>本周业绩达标率</span><br />
                    <span className={styles.reportValue}>{thisWeekSalesPricePercent}%</span></Col> */}
                  <Col span={6} className={styles.paddingStyle}>
                    <span className={styles.reportItem}>本月业绩</span><br />
                    <span className={styles.reportValue}>￥{thisMonthSalesPrice}元</span></Col>
                  {/* <Col span={6} className={styles.paddingStyle}>
                    <span className={styles.reportItem}>本月业绩达标率</span><br />
                    <span className={styles.reportValue}>{thisMonthSalesPricePercent}%</span></Col> */}
                  <Col span={6} className={styles.paddingStyle}>
                    <span className={styles.reportItem}>本月耗课金额</span><br />
                    <span className={styles.reportValue}>￥{thisMonthCostClassPrice}元</span></Col>
                  <Col span={6} className={styles.paddingStyle}>
                    <span className={styles.reportItem}>签单量</span><br />
                    <span className={styles.reportValue}>{thisMonthSuccessNewContractCount}单</span></Col>
                </Row>
              </Card>
            </div>
            <div style={{ background: '#ECECEC', padding: '30px', marginTop: -30 }}>
              <Card title="其他数据" bordered={false}>
                <Row>
                  <Col span={6} className={styles.paddingStyle}>
                    <span className={styles.reportItem}>测评量</span><br />
                    <span className={styles.reportValue}>{assessmentCount}名</span></Col>
                  <Col span={6} className={styles.paddingStyle}>
                    <span className={styles.reportItem}>本周耗课量</span><br />
                    <span className={styles.reportValue}>{thisWeekCostClassCount}节</span></Col>
                  <Col span={6} className={styles.paddingStyle}>
                    <span className={styles.reportItem}>本月耗课量</span><br />
                    <span className={styles.reportValue}>{thisMonthCostClassCount}节</span></Col>
                  <Col span={6} className={styles.paddingStyle}>
                    <span className={styles.reportItem}>班级数</span><br />
                    <span className={styles.reportValue}>长期:{thisWeekLongTermClassCount}个</span><br />
                    <span className={styles.reportValue}>临时:{thisWeekShortTermClassCount}个</span></Col>
                </Row>
              </Card>
            </div>
            <div style={{ background: '#ECECEC', padding: '30px', marginTop: -30 }}>
              <Card title={<span><Icon type="line-chart" theme="outlined" />本周业绩</span>} bordered={false}>
                <WeeklyChart data={WeekSalesPriceDetail} />
              </Card>
            </div>
          </React.Fragment>
      }
    </React.Fragment >
  )
}

export default connect(({ loading, operationalReport }) => ({ loading, operationalReport }))(OperationalReport)
