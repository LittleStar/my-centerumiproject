import React from 'react';
import { connect } from 'dva';
import { permissionConfig } from 'utils';
import NavTab from 'components/NavTab/NavTab';

const { location: locationPermission } = permissionConfig;
const { CheckIn, TeachingSchedule } = locationPermission;

const Dashboard = ({ children, location, dashboard }) => {

  const { showList } = dashboard

  if (showList.length === 0) return null;

  return (
    <div>
      <h1>首页</h1>
      <NavTab location={location} list={showList} type="card" />
      {children}
    </div>
  );
};

export default connect(({ dashboard }) => ({ dashboard }))(Dashboard);
