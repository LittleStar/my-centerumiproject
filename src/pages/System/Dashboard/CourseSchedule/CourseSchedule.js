import React from 'react'
import { connect } from 'dva'
import { Input, message } from 'antd'
import ClassSchedule from '../../../../components/ClassSchedule/ClassSchedule'
import { filterList,checkPermissionKey } from 'utils/utils'

const Search = Input.Search

const CourseSchedule = ({ dispatch, courseManage, user, loading }) => {

  const { timeList, classroomList, classScheduleList, addClassVisible, content} = courseManage
  const { permission } = user
  const courseManageloading = loading.models.courseManage

  const canLinkCourseDetail = checkPermissionKey('Pages.Location.TeachingCenter.CourseInstance',permission)

  const ClassScheduleProps = {
    loading: courseManageloading,
    timeList,
    classroomList,
    classScheduleList,
    onSelect(classItem){
      if(canLinkCourseDetail){
        dispatch({ type: 'courseManage/goToClassDetail', payload: classItem })
      }
      else{
        message.warning('对不起！您没有查看班级详情的权限！');
      }
    },
  }

  const searchProps = {
    placeholder: "搜索用户",
    onSearch(value) {
      dispatch({ type: 'courseManage/query', payload: { searchField:value } })
    }
  }

  return (
    <div style={{marginBottom: 10}}>
      {/* <Row gutter={10} justify="start" style={{marginTop: 20, marginBottom: 20}}>
        <Col span={5}>
          <Search {...searchProps}/>
        </Col>
      </Row> */}
      <ClassSchedule {...ClassScheduleProps} />
    </div>
  )
}
export default connect(({courseManage, user, loading})=>({courseManage, user, loading}))(CourseSchedule)
