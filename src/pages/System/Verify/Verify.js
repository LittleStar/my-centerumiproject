import React from 'react';
import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import { getRoutes } from '../../../utils/utils'

const Verify = ({ children }) => {
  return (
    <div>
      {children}
    </div>
  );
};

export default connect()(Verify);
