import React from 'react'
import { connect } from 'dva'
import { Input, Row, Col, Button, Tabs } from 'antd'
import MagicTable from '../../../../components/MagicTable/MagicTable'

const Search = Input.Search
const TabPane = Tabs.TabPane;

const VerifyContract = ({ dispatch, verifyContract, loading }) => {
  const { verifyContract: verifyContractLoading } = loading.models
  const { verifyType, list, pagination, currentFilter, historyFilter, sorter, searchField,unchangeModelLoading } = verifyContract

  const currentVerifySupport = {
    creationTime: {
      showText: '申请时间',
      showType: 'Time',
      addtional: {
        format: 'MM/DD/YYYY HH:mm'
      }
    },
    subjectName: {
      showText: '学生姓名',
      showType: 'Text',
    },
    contractType: {
      showText: '合同类型',
      showType: 'Status',
      addtional: {
        statusArray: ['新合同', '升包', '课包更改']
      },
      filter: {
        hasFilter: true,
        filterOptions: [
          { text: '新合同', value: 'New' },
          { text: '升包', value: 'Upgrade' },
          { text: '课包更改', value: 'ModifyPackage' },
        ],
      }
    },
    packageType: {
      showText: '课包类型',
      showType: 'Status',
      addtional: {
        statusArray: ['一对多', '一对一']
      },
      filter: {
        hasFilter: true,
        filterOptions: [
          { text: '一对多', value: 'OneToMany' },
          { text: '一对一', value: 'OneToOne' },
        ],
      }
    },
    packageCount: {
      showText: '课时数',
      showType: 'Text',
    },
    createPerson: {
      showText: '申请人',
      showType: 'Text',
    },
    originPrice: {
      showText: '课包原价',
      showType: 'Text',
    },
    discountPrice: {
      showText: '折扣金额',
      showType: 'Text',
    },
    realPayment: {
      showText: '实付金额',
      showType: 'Text',
    }
  }

  const historyVerifySupport = {
    creationTime: {
      showText: '申请时间',
      showType: 'Time',
      addtional: {
        format: 'MM/DD/YYYY HH:mm'
      }
    },
    subjectName: {
      showText: '学生姓名',
      showType: 'Text',
    },
    contractType: {
      showText: '合同类型',
      showType: 'Status',
      addtional: {
        statusArray: ['新合同', '升包', '课包更改']
      },
      filter: {
        hasFilter: true,
        filterOptions: [
          { text: '新合同', value: 'New' },
          { text: '升包', value: 'Upgrade' },
          { text: '课包更改', value: 'ModifyPackage' },
        ],
      }
    },
    status: {
      showText: '合同状态',
      showType: 'Status',
      addtional: {
        statusArray: ['合同草稿', '待审核', '拒绝合同', '通过合同', '过期合同', '取消合同']
      },
      filter: {
        hasFilter: true,
        filterOptions: [
          // { text: '合同草稿', value: 'None' },
          { text: '待审核', value: 'Commited' },
          { text: '拒绝合同', value: 'Deny' },
          { text: '通过合同', value: 'Confirmed' },
          { text: '过期合同', value: 'Expired' },
          { text: '取消合同', value: 'Cancelled ' },
        ],
      }
    },
    packageType: {
      showText: '课包类型',
      showType: 'Status',
      addtional: {
        statusArray: ['一对多', '一对一']
      },
      filter: {
        hasFilter: true,
        filterOptions: [
          { text: '一对多', value: 'OneToMany' },
          { text: '一对一', value: 'OneToOne' },
        ],
      }
    },
    packageCount: {
      showText: '课时数',
      showType: 'Text',
    },
    createPerson: {
      showText: '申请人',
      showType: 'Text',
    },
    originPrice: {
      showText: '课包原价',
      showType: 'Text',
    },
    discountPrice: {
      showText: '折扣金额',
      showType: 'Text',
    },
    realPayment: {
      showText: '实付金额',
      showType: 'Text',
    }
  }

  const listSupport = verifyType === 'current' ? currentVerifySupport : historyVerifySupport
  const listFilters = verifyType === 'current' ? currentFilter : historyFilter

  const TableProps = {
    loading: verifyContractLoading || unchangeModelLoading,
    listData: list,
    listSupport,
    pagination,
    filters: listFilters,
    sorter,
    onTableChange(pagi, fil={}, sort={}) {
      let data = { pagination, filters:listFilters, sorter }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, pagination: pagi }
      }
      if (Object.keys(fil).length !== 0) {
        data = { ...data, filters: fil }
      }
      if (Object.keys(sort).length !== 0) {
        data = { ...data, sorter: sort }
      }
      dispatch({ type: 'verifyContract/changeTable', payload: data })
    },
    Actions: [{
      showText: '查看详情',
      onClick(record) {
        dispatch({ type: 'verifyContract/goToContractDetail', payload: record.id })
      }
    }]
  }

  const searchProps = {
    placeholder: "搜索用户",
    onSearch(value) {
      const data = { pagination, currentFilter, historyFilter, sorter, searchField:value }
      dispatch({ type: 'verifyContract/updateSearchField', payload: data })
    },
    onChange(e) {
      const data = { pagination, currentFilter, historyFilter, sorter, searchField:e.target.value }
      dispatch({ type: 'verifyContract/updateSearchField', payload: data })
    },
    value:searchField,
  }

  const tabChange = (key) => {
    dispatch({ type: 'verifyContract/switchTab', payload: key })
  }

  const clearFilter = () => {
    verifyType === 'current' ? dispatch({ type: 'verifyContract/clearCurrentFilter' }) : dispatch({ type: 'verifyContract/clearHistoryFilter' })
  }

  return (
    <div>
      <h1>合同审核</h1>
      <Row style={{ marginTop: 20, marginBottom: 20 }} gutter={20}>
        <Col span={6}>
          <Search {...searchProps} />
        </Col>
        <Col span={6}>
          <Button onClick={clearFilter}>重置所有筛选</Button>
        </Col>
      </Row>
      <Tabs activeKey={verifyType} onChange={tabChange}>
        <TabPane tab="当前审核" key="current" />
        <TabPane tab="审核历史" key="history" />
      </Tabs>
      <MagicTable {...TableProps} />
    </div>
  );
};

export default connect(({ verifyContract, loading }) => ({ verifyContract, loading }))(VerifyContract)
