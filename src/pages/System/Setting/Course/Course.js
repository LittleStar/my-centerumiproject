
import React from 'react'
import { connect } from 'dva'
import { Input, Row, Col, Button } from 'antd'
import MagicTable from '../../../../components/MagicTable/MagicTable'


const Search = Input.Search

const Course = ({ dispatch, settingCourseManage }) => {
  const { list, listSupport } = settingCourseManage

  const TableProps = {
    listData: list,
    listSupport: listSupport,
    pagination: {
      current: 1,
      pageSize: 10,
      total: 37
    },
    onTableChange(pagination, filters, sorter) {
      console.log(pagination, filters, sorter)
    },
    Actions: [{
      showText: '编辑状态',
      onClick(record) {
        // const requestId = record.centerID
        // console.log('requestId', requestId)
        // dispatch({ type: 'center/goToCenterDetail', payload: requestId })
      }
    },
    {
      showText: '查看详情',
      onClick(record) {
      }
    }]
  }

  const searchProps = {
    placeholder: "搜索课程",
    onSearch(value) {
    }
  }


  return (
    <div>
      <h1>课程管理</h1>
      <Row type="flex" justify="space-between" style={{ marginBottom: 20 }}>
        <Col span={5}>
          <Search {...searchProps} />
        </Col>
      </Row>
      <MagicTable {...TableProps} />
    </div>
  );
};

export default connect(({ settingCourseManage }) => ({ settingCourseManage }))(Course)
