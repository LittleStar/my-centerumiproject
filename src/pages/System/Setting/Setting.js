import React from 'react';
import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import { getRoutes } from '../../../utils/utils';

const Test = ({children}) => {

	return (
		<div>
		{/* <h1>配置中心</h1> */}
       {children}
		</div>
	);
};

export default connect()(Test);
