
import React from 'react'
import { connect } from 'dva'
import { Input } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'
import MagicFormModal from 'components/MagicFormModal/MagicFormModal'


const Account = ({ dispatch, accountManage }) => {

  const { list, pagination, modalVisible, modalType, content } = accountManage

  // const modalTypeToTitle = (type) => {
  //   switch (type) {
  //     case 'edit':
  //       return '编辑店铺信息'
  //   }
  // }

  const listSupport = {
    creationTime: {
      showText: '创建时间',
      showType: 'Time',
      addtional: {
        format: 'YYYY/MM/DD'
      }
    },
    displayName: {
      showText: '店铺名称',
      showType: 'Text',
    },
    city: {
      showText: '所在城市',
      showType: 'Text',
    },
    address1:{
      showText: '详细地址',
      showType: 'Text',
    },
    memberCount: {
      showText: '员工数',
      showType: 'Text',
    },
    organizationUnitOperationType: {
      showText: '运营状态',
      showType: 'Status',
      addtional: {
        statusArray: ['未开业', '已开业', '已停业']
      },
      // filter: {
      //   hasFilter: false,
      //   filterOptions: [
      //     { text: '未开业', value: 'None' },
      //     { text: '已开业', value: 'Opened' },
      //     { text: '已停业', value: 'Closed' },
      //   ],
      // }
    }
  }

  // 店长编辑店铺信息暂时不需要
  // const modalProps = {
  //   title: modalTypeToTitle(modalType),
  //   visible: modalVisible,
  //   onCancel() {
  //     dispatch({ type: 'accountManage/updateState', payload: { modalVisible: false } })
  //   },
  //   onOk(value) {
  //     dispatch({ type: 'accountManage/onSubmitModal', payload: value })
  //   },
  //   content,
  // }

  const TableProps = {
    listData: list,
    listSupport: listSupport,
    pagination,
    onTableChange(pagi, fil={}) {
      let data = { pagination }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, pagination: pagi }
      }
      dispatch({ type: 'accountManage/query', payload: data })
    },
    // Actions: [{
    //   showText: '编辑',
    //   onClick(record) {
    //     console.log('record', record)
    //     dispatch({ type: 'accountManage/onEdit', payload: record.id })
    //   }
    // }]
  }


  return (
    <div>
      <h1>店铺管理</h1>
      <MagicTable {...TableProps} />
      {/* <MagicFormModal {...modalProps} /> */}
    </div>
  );
};

export default connect(({ accountManage }) => ({ accountManage }))(Account)

