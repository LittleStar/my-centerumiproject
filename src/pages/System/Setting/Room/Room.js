
import React from 'react'
import { connect } from 'dva'
import { Modal, Input, Row, Col, Button } from 'antd'
import MagicTable from '../../../../components/MagicTable/MagicTable'
import MagicModal from '../../../../components/MagicFormModal/MagicFormModal'

const Search = Input.Search

const Room = ({ dispatch, roomManage, loading }) => {

  const roomManageLoading = loading.models.roomManage

  const { list, listSupport, pagination, modalType, content, modalVisible } = roomManage

  const TableProps = {
    loading: roomManageLoading,
    listData: list,
    listSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      const data = { pagination, filters, sorter }
      dispatch({ type: 'roomManage/query', payload: data })
    },
    Actions: [{
      showText: '编辑',
      onClick(record) {
        dispatch({ type: 'roomManage/onEditRoom', payload: record.id })
      }
    }, {
      showText: '删除',
      onClick(record) {
        Modal.confirm({
          title: '删除教室',
          content: `您确定要删除教室${record.name}吗`,
          onOk() {
            dispatch({ type: 'roomManage/onDeleteRoom', payload: record.id })
          }
        })
      }
    },]
  }

  const searchProps = {
    placeholder: "搜索教室",
    onSearch(value) {
      const data = { searchField: value }
      dispatch({ type: 'roomManage/query', payload: data })
    }
  }

  const onAddRoom = () => {
    dispatch({ type: 'roomManage/onAddRoom' })
  }

  const modalProps = {
    title: modalType === 'add' ? '新教室' : '编辑教室',
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'roomManage/updateState', payload: {modalVisible:false} })
    },
    onOk(value) {
      dispatch({ type: 'roomManage/onSubmitModal', payload: value })
    },
    content
  }

  return (
    <div>
      <h1>教室管理</h1>
      <Row type="flex" justify="space-between" style={{ marginBottom: 20 }}>
        <Col span={5}>
          <Search {...searchProps} />
        </Col>
        <Col span={5}>
          <Button onClick={onAddRoom} icon="plus">新教室</Button>
        </Col>
      </Row>
      <MagicTable {...TableProps} />
      <MagicModal {...modalProps} />
    </div>
  );
};

export default connect(({ roomManage, loading }) => ({ roomManage, loading }))(Room)
