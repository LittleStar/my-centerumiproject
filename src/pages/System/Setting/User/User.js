import React from 'react'
import { connect } from 'dva'
import { permissionConfig } from 'utils'
import { Input, Row, Col, Button, message } from 'antd'
import MagicTable from '../../../../components/MagicTable/MagicTable'
import MagicModal from '../../../../components/MagicFormModal/MagicFormModal'
import { filterList, checkPermissionKey } from 'utils/utils'

const { location: locationPermission } = permissionConfig
const { SettingCenterHrEdit } = locationPermission

const Search = Input.Search

const User = ({ dispatch, settingUserManage, user, loading }) => {
  const settingUserManageLoading = loading.models.settingUserManage
  const { permission } = user
  const { list, listSupport, pagination, modalType, content, modalVisible, filter } = settingUserManage

  const formatDataToBackEndModel = (data) => {
    const { employeSurName, employeName, phone, gender, role, password, rePassword, emailAddress, isActive } = data
    const user = { name: employeName, surname: employeSurName, phoneNumber: phone, gender, password, emailAddress, isActive }
    const assignedRoleIds = [role]
    return { user, assignedRoleIds }
  }

  const validator = (value) => {
    let phoneValidRule = /^(13[0-9]|14[5-9]|15[012356789]|166|17[0-8]|18[0-9]|19[8-9])[0-9]{8}$/
    let emailValidRule = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (value.password !== value.rePassword) {
      message.error('密码输入不一致!')
      return true
    }
    if (!phoneValidRule.test(value.phone)) {
      message.error('手机号码为11位有效数字!')
      return true
    }
    if (!emailValidRule.test(value.emailAddress)) {
      message.error('邮箱验证不通过!')
      return true
    }
    return false
  }

  const tableActions = [{
    showText: '编辑',
    onClick(record) {
      dispatch({ type: 'settingUserManage/onEditWorker', payload: record.id })
    },
    permissionKey: SettingCenterHrEdit
  },{
    showText: '查看详情',
    onClick(record) {
      dispatch({ type: 'settingUserManage/goToEmployeeDetail', payload: record.id })
    },
    // permissionKey: SettingCenterHrEdit
  }]

  const Actions = filterList(tableActions, permission)

  const TableProps = {
    loading: settingUserManageLoading,
    listData: list,
    listSupport,
    pagination,
    filters: filter,
    onTableChange(pagi={}, filt={}, sort={}) {
      let data = { pagination, filter }
      if (Object.keys(pagi).length !== 0)
        data = { ...data, pagination: pagi }
      if (Object.keys(filt).length !== 0)
        data = { ...data, filter: filt }
      dispatch({ type: 'settingUserManage/query', payload: data })
    },
    Actions
  }

  const searchProps = {
    placeholder: "搜索员工姓名",
    onSearch(value) {
      const data = { pagination, filter, searchField: value }
      dispatch({ type: 'settingUserManage/query', payload: data })
    }
  }

  const onAdd = () => {
    dispatch({ type: 'settingUserManage/onNewWorker' })
  }

  const modalProps = {
    title: modalType === 'add' ? '添加新员工' : '编辑员工信息',
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'settingUserManage/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      if (validator(value)) {
        return
      } else {
        const formatData = formatDataToBackEndModel(value)
        dispatch({ type: 'settingUserManage/onSubmitModal', payload: formatData })
      }
    },
    content,
  }

  const addWorkerPermission = checkPermissionKey(SettingCenterHrEdit, permission)

  return (
    <div>
      <h1>人事管理</h1>
      <Row type="flex" justify="space-between" style={{ marginBottom: 20 }}>
        <Col span={5}>
          <Search {...searchProps} />
        </Col>
        {
          addWorkerPermission ?
            <Col span={5}>
              <Button onClick={onAdd} icon="plus">新员工</Button>
            </Col> : null
        }
      </Row>
      <MagicTable {...TableProps} />
      <MagicModal {...modalProps} />
    </div>
  );
};

export default connect(({ settingUserManage, user, loading }) => ({ settingUserManage, user, loading }))(User)
