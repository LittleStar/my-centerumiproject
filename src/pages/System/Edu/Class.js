import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import MagicFormModal from '../../../components/MagicFormModal/MagicFormModal'
import { Input, DatePicker, Row, Col, Button, Icon, message, Spin } from 'antd'
import ClassSchedule from '../../../components/ClassSchedule/ClassSchedule'
import moment from 'moment';
import { getRoutes, filterList, checkPermissionKey } from 'utils/utils'

import { permissionConfig } from 'utils'

const { location: locationPermission } = permissionConfig
const { CourseInstance, CourseInstanceEdit } = locationPermission

const Search = Input.Search

const CLASS = ({ routerData, match, location, dispatch, classes, user, loading }) => {

  const { timeList, classroomList, classScheduleList, addClassVisible, content, selectDate } = classes
  const { permission } = user
  const classesLoading = loading.models.classes

  const canLinkCourseDetail = checkPermissionKey(CourseInstance, permission)

  const showDate = selectDate ? moment(selectDate).format('YYYY-MM-DD') : ''

  const datePickerProps = {
    placeholder: "选择课表日期",
    onChange(value) {
      dispatch({ type: 'classes/query', payload: { date: value } })
    }
  }

  const ClassScheduleProps = {
    timeList,
    classroomList,
    classScheduleList,
    onSelect(classItem) {
      if (canLinkCourseDetail) {
        dispatch({ type: 'classes/goToClassDetail', payload: classItem })
      }
      else {
        message.warning('对不起！您没有查看班级详情的权限！');
      }
    },
  }

  const addBtnModalProps = {
    title: '临时添加当天班级',
    visible: addClassVisible,
    onCancel() {
      dispatch({ type: 'classes/updateState', payload: { addClassVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'classes/addClass', payload: value })
    },
    content
  }

  const onClickAdd = () => {
    dispatch({ type: 'classes/clickAdd' })
  }

  const showAddCourseInstanceBtn = checkPermissionKey(CourseInstanceEdit, permission)

  return (
    <div>
      <h1>课程表</h1>
      <Row gutter={10} style={{ marginTop: 20, marginBottom: 20 }}>
        <Col span={5}>
          <DatePicker {...datePickerProps} />
        </Col>
        <Col span={5}>
          <h3>{'当前选择日期： ' + showDate}</h3>
        </Col>
        {
          showAddCourseInstanceBtn ?
            <Col span={7}>
              <Button onClick={onClickAdd}>临时添加当日班级</Button>
            </Col> : null
        }
      </Row>
      {
        classesLoading ?
          <Spin /> :
          <ClassSchedule {...ClassScheduleProps} />
      }
      <MagicFormModal {...addBtnModalProps} />
    </div>
  );
};

export default connect(({ classes, user, loading }) => ({ classes, user, loading }))(CLASS);
