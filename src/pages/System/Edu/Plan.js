import { connect } from 'dva'
import { Button, Modal, Tabs, Row, Col, Input } from 'antd'
import MagicTable from '../../../components/MagicTable/MagicTable'
import MagicFormModal from '../../../components/MagicFormModal/MagicFormModal'
import PlanModal from './Components/PlanModal'
import moment from 'moment'
import { filterList, checkPermissionKey } from 'utils/utils'
import { permissionConfig } from 'utils'

const { location: locationPermission } = permissionConfig
const { ManualSchedule, TeachingPlanEdit } = locationPermission
const { Search } = Input
const TabPane = Tabs.TabPane;

const Plan = ({ classPlan, dispatch, loading, user }) => {

  const { permission } = user

  const classPlanLoading = loading.models.classPlan
  const { planList: list, planListSupport: listSupport, tabPlanListSupport, pagination, modalType, modalVisible, content,
    selectWeekday, manualVisible, manualContent, dropdownSelect, tabSelect, sorter, filter, searchField } = classPlan

  const timeToBirthDay = (time) => {
    return moment.utc(time.format('YYYYMMDD')).format()
  }

  const formatDataToBackEndModel = (value, modalType) => {

    switch (modalType) {
      case 'add':
      case 'edit':
        return value
      case 'manual':
        const { startTime } = value
        const modifiedstartTime = startTime ? timeToBirthDay(startTime) : null
        return { ...value, startTime: modifiedstartTime }
      default:
        return value
    }
  }

  const matchSelectedFilter = (selectWeekday) => {
    switch (selectWeekday) {
      case 'Monday':
        return mondayFilter
      case 'Tuesday':
        return tuesdayFilter
      case 'Wednesday':
        return wednesdayFilter
      case 'Thursday':
        return thursdayFilter
      case 'Friday':
        return fridayFilter
      case 'Saturday':
        return saturdayFilter
      case 'Sunday':
        return sundayFilter
      case 'all':
        return allFilter
    }
  }


  const tableActions = [
    {
      showText: '查看计划详情',
      onClick(record) {
        dispatch({ type: 'classPlan/goToClassPlanDetail', payload: record.id })
      }
    },
    {
      showText: '编辑',
      onClick(record) {
        dispatch({ type: 'classPlan/updateModalVisible', payload: true })
        dispatch({ type: 'classPlan/clickEdit', payload: record.id })
      },
      permissionKey: TeachingPlanEdit
    },
    {
      showText: '删除',
      onClick(record) {
        Modal.confirm({
          title: '你确定要删除该课程计划？',
          okText: '确定',
          okType: 'danger',
          cancelText: '取消',
          onOk() {
            dispatch({ type: 'classPlan/clickDelete', payload: record.id })
          }
        })
      },
      permissionKey: TeachingPlanEdit
    },
  ]

  const Actions = filterList(tableActions, permission)

  const TableProps = {
    listData: list,
    listSupport: tabSelect === 'all' ? tabPlanListSupport : listSupport,
    pagination,
    filters: filter,
    sorter,
    onTableChange(pagi, fil = {}, sor = {}) {
      let data = { pagination, filter, sorter, searchField }
      if (Object.keys(pagi).length !== 0)
        data = { ...data, pagination: pagi }
      if (Object.keys(fil).length !== 0)
        data = { ...data, filter: fil }
      if (Object.keys(sor).length !== 0)
        data = { ...data, sorter: sor }
      dispatch({ type: 'classPlan/changeTable', payload: data })
    },
    Actions
  }

  const FormModalProps = {
    loading: classPlanLoading,
    selectWeekday: dropdownSelect,
    onWeekDayChange(value) {
      dispatch({ type: 'classPlan/queryWeekDay', payload: value })
    },
    title: modalType === 'add' ? '新建课程计划' : '编辑课程计划',
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'classPlan/updateModalVisible', payload: false })
    },
    onOk(value) {
      const formatData = formatDataToBackEndModel(value, modalType)
      dispatch({ type: 'classPlan/onSubmitModal', payload: formatData })
    },
    content
  }

  const manualFormProps = {
    title: '排课',
    visible: manualVisible,
    onCancel() {
      dispatch({ type: 'classPlan/updateManualVisible', payload: false })
    },
    onOk(value) {
      const formatData = formatDataToBackEndModel(value, modalType)
      dispatch({ type: 'classPlan/onSubmitModal', payload: formatData })
      // dispatch({ type: 'classPlan/updateManualVisible', payload: false })
    },
    content: manualContent
  }

  const onAddPlan = () => {
    dispatch({ type: 'classPlan/clickAdd' })
  }

  const onManualSchedule = () => {
    dispatch({ type: 'classPlan/clickManualSchedule' })
  }

  const onChangeTab = (key) => {
    dispatch({ type: 'classPlan/changeTab', payload: key })
  }

  const addPlanBtnPermission = checkPermissionKey(TeachingPlanEdit, permission)
  const manualSchedulePermission = checkPermissionKey(ManualSchedule, permission)

  const clearFilter = () => {
    dispatch({ type: 'classPlan/clearFilter' })
  }

  const searchProps = {
    placeholder: "搜索课程/老师",
    onSearch(value) {
      const data = { pagination, filter, sorter, searchField: value }
      dispatch({ type: 'classPlan/changeSearchField', payload: data })
    }
  }

  return (
    <div>
      <h1>课程计划（课程将在下周生效）</h1>
      <Row type="flex" justify="end">
        {
          addPlanBtnPermission ? <Button icon="plus" onClick={onAddPlan}>添加课程计划</Button> : null
        }
        {
          manualSchedulePermission ? <Button onClick={onManualSchedule}>排课</Button> : null
        }
      </Row>
      <Tabs activeKey={selectWeekday} onChange={onChangeTab} type="card">
        <TabPane tab="星期一" key="Monday" />
        <TabPane tab="星期二" key="Tuesday" />
        <TabPane tab="星期三" key="Wednesday" />
        <TabPane tab="星期四" key="Thursday" />
        <TabPane tab="星期五" key="Friday" />
        <TabPane tab="星期六" key="Saturday" />
        <TabPane tab="星期日" key="Sunday" />
        <TabPane tab="全部" key="all" />
      </Tabs>
      <Row style={{marginBottom: 10}} gutter={10}>
        <Col span={6}>
          <Search {...searchProps} />
        </Col>
        {/* <Col span={6}>
          <Button onClick={clearFilter}>重置所有筛选</Button>
        </Col> */}
      </Row>

      <MagicTable {...TableProps} />
      <MagicFormModal {...manualFormProps} />
      <PlanModal {...FormModalProps} />
    </div>
  );
};

export default connect(({ classPlan, loading, user }) => ({ classPlan, loading, user }))(Plan);
