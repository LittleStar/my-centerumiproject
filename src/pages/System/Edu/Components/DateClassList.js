import React from 'react'
import { connect } from 'dva'
import { Input, DatePicker, Row, Col } from 'antd'
import ClassSchedule from '../../../../components/ClassSchedule/ClassSchedule'

const Search = Input.Search
const DateClassList = ({ timeList,classroomList,classScheduleList,onClickMore, onClickAdd, onSelectClass, selected, onSearch, onSearchClass, onDateChange }) => {

  const searchProps = {
    placeholder: "搜索用户/老师",
    onSearch: onSearch
  }

  const searchClassProps = {
    placeholder: "班级编号",
    onSearch: onSearchClass
  }

  const datePickerProps = {
    placeholder: "选择日期",
    onChange: onDateChange
  }

  const ClassScheduleProps = {
    timeList,
    classroomList,
    classScheduleList,
    onClickMore,
    onSelectClass,
    onClickAdd,
    selected,
  }

  return (
    <div>
      <Row gutter={10}>
        <Col span={5}>
          <Search {...searchProps}/>
        </Col>
        <Col span={5}>
          <Search {...searchClassProps}/>
        </Col>
        <Col span={5}>
          <DatePicker {...datePickerProps} />
        </Col>
      </Row>
      <ClassSchedule {...ClassScheduleProps} />
    </div>
  );
};

export default (DateClassList)

