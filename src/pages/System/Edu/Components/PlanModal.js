import React, { Fragment } from 'react'
import MagicForm from '../../../../components/MagicForm/DocumentTypeItem'
import { Modal, Select, Input, DatePicker, Row, Col } from 'antd'

const Option = Select.Option;

const PlanModal = ({ selectWeekday, onWeekDayChange, title, visible, onCancel, onOk, content, loading }) => {
  const magicFormProps = {
    loading,
    content,
    onSubmit: onOk,
    onCancel
  }

  return (
    <Modal
      visible={visible}
      title={title}
      width={800}
      footer={null}
      onCancel={onCancel}
    >
      <Row style={{marginBottom: 20}}>
        <Col xs={24} sm={6}>
          <span style={{display: 'block', textAlign: 'right', marginTop: 5, marginRight: 8, color: '#000'}}>星期: </span>
        </Col>
        <Select
          value={selectWeekday}
          onChange={onWeekDayChange}
        >
          <Option value="Sunday">星期天</Option>
          <Option value="Monday">星期一</Option>
          <Option value="Tuesday">星期二</Option>
          <Option value="Wednesday">星期三</Option>
          <Option value="Thursday">星期四</Option>
          <Option value="Friday">星期五</Option>
          <Option value="Saturday">星期六</Option>
        </Select>
      </Row>
      <MagicForm {...magicFormProps} />
    </Modal>
  )

}


export default PlanModal
