import React, { PureComponent } from 'react'
import { Table, Button, Select, Card, Icon } from 'antd'
import moment from 'moment'
import styles from './ClassPlanList.css'

const Option = Select.Option
const columnWidth = 250

const ClassList = ({ classList, onSelectClass }) => {

  if (classList !== undefined) {
    return(
      classList.map((classItem,index)=>{
        const {id,teacher,course}= classItem
        const courseInfo = course?course.title:'未指定课程'
        const teacherInfo = teacher?teacher.name:'未指定老师'
        const title = courseInfo+'-'+teacherInfo
          if(index!==classList.length-1){
            return (
              <React.Fragment key = {id+index}>
                <Button style={{ width: columnWidth - 90 }} onClick={() => onSelectClass(id)}>{title}</Button>
                <br/>
              </React.Fragment>
            )
          }
          else{
            return(
              <Button key = {id+index} style={{ width: columnWidth - 90 }} onClick={() => onSelectClass(id)}>{title}</Button>
              )
          }
        })
      )
  }
  else {
    return null
  }
}

const ClassSchedule = ({ timeList, classroomList, classScheduleList, onSelect}) => {

  // 排序时间段
  const sortTime = (time) => {
    const temp = time
    temp.sort((a, b) => {
      return (moment(a.startTime, 'HH:mm:ss').isBefore(moment(b.startTime, 'HH:mm:ss'))) ? -1 : 1
    })
    return temp
  }

  const renderTimeColumn = (text, record, index) => {
    return (
      <p>{record.time}</p>
    )
  }

  const renderClassColumn = (text, record, index, room) => {
    const classListProps = {
      classList: text,
      onSelectClass(id) {
        const classItem = text.find((item) => {
          return item.id === id
        })
        onSelect(classItem)
      }
    }

    return (
      <div className={styles.wrap}>
        <Card>
          <ClassList {...classListProps} />
        </Card>
      </div>
    )
  }

  const scheduleListToObj = (schedule) => {
    const result = {}

    schedule.forEach((item) => {
      const {classroom} = item
      const classroomId = classroom.id
      if (!Array.isArray(result[classroomId])) {
        result[classroomId] = []
      }
      result[classroomId].push({ ...item })
    })

    return result
  }

  const getShowTime = (time) => {
    return time.startTime + '-' + time.endTime
  }

  const sortedTime = sortTime(timeList)

  const showData = sortedTime.map((timeItem) => {
    const schedule = classScheduleList.filter((classItem) => {
      return classItem.startCoursePeriodId === timeItem.id
    })

    const scheduleObj = scheduleListToObj(schedule)
    const showTime = getShowTime(timeItem)
    return { key: showTime, timeKey: timeItem.key, time: showTime, ...scheduleObj }
  })


  const textAlign = styles['column-textalign']
  // 头部列
  const columns = [
    { title: '时间', width: 100, className: textAlign, key: 'time', fixed: 'left', render: (text, record, index) => renderTimeColumn(text, record, index) },
  ]

  const scrollX = classroomList.length * columnWidth + 100

  classroomList.forEach((item, index) => {
    const {name,id} = item
    columns.push({
      title: '教室 ' + name,
      dataIndex: id,
      key: id,
      width: columnWidth,
      className: textAlign,
      render: (text, record, index) => renderClassColumn(text, record, index, item)
    })
  })

  return (
    <Table columns={columns} dataSource={showData} bordered scroll={{ x: scrollX, y: 600 }} />
  )
}

export default ClassSchedule
