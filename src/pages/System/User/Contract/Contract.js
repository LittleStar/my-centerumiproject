import React from 'react'
import { connect } from 'dva'
import { Input, Row, Col, Button } from 'antd'
import MagicTable from '../../../../components/MagicTable/MagicTable'

const Search = Input.Search

const Contract = ({ dispatch, contract, loading }) => {
  const isLoading = loading.models.contract
  const { list, listSupport, pagination, filter } = contract

  const TableProps = {
    loading: isLoading,
    listData: list,
    listSupport,
    pagination,
    filters: filter,
    onTableChange(pagination, filters, sorter) {
      const data = { pagination, filters, sorter }
      dispatch({ type: 'contract/query', payload: data })
    },
    Actions: [{
      showText: '查看详情',
      onClick(record) {
        dispatch({ type: 'contract/goToContractDetail', payload: record.id })
      }
    }]
  }

  const searchProps = {
    placeholder: "搜索用户",
    onSearch(value) {
      const data = { pagination, filter, searchField: value }
      dispatch({ type: 'contract/query', payload: data })
    }
  }

  const clearFilter = () => {
    dispatch({ type: 'contract/clearFilter' })
  }

  return (
    <div>
      <h1>合同管理</h1>
      <Row style={{ marginBottom: 20 }} gutter={20}>
        <Col span={6}>
          <Search {...searchProps} />
        </Col>
        <Col span={6}>
          <Button onClick={clearFilter}>重置所有筛选</Button>
        </Col>
      </Row>
      <MagicTable {...TableProps} />
    </div>
  );
};

export default connect(({ contract, loading }) => ({ contract, loading }))(Contract)
