import React from 'react'
import { connect } from 'dva'
import moment from 'moment'
import {filterList,checkPermissionKey} from 'utils/utils'
import {permissionConfig} from 'utils'
import { Input, Row, Col, message, Button } from 'antd'
import MagicTable from '../../../../components/MagicTable/MagicTable'
import MagicFormModal from '../../../../components/MagicFormModal/MagicFormModal'

const {location:locationPermission} = permissionConfig
const {VipEdit} = locationPermission

const Search = Input.Search
const Vip = ({ dispatch, vip, loading, user }) => {

  const { permission } = user
  const isLoading = loading.models.vip

  const { list, listSupport, pagination, modalType, content, modalVisible, filters, sorter, searchField } = vip

  const timeToBirthDay = (time) => {
    return moment.utc(time.format('YYYYMMDD')).format()
  }

  const formatDataToBackEndModel = (data, modalType) => {
    switch (modalType) {
      case 'edit':
        const { guardianName, guardianPhone, guardianRole, guardianSurName, subjectBirthDay, subjectGender, subjectName, subjectSurName, subjectSource } = data
        const subject = { gender: subjectGender, name: subjectName, surname: subjectSurName, sourceId: subjectSource, birthDay: timeToBirthDay(subjectBirthDay) }
        const guardians = [{ role: guardianRole, name: guardianName, surname: guardianSurName, phoneNumber: guardianPhone }]
        return { subject, guardians }
      default:
        return data
    }
  }
  const modalTypeToTitle = (type) => {
    switch (type) {
      case 'edit':
        return '编辑会员信息'
      case 'assignTeacher':
        return '分配班主任'
      default:
        return ''
    }
  }

  const tableActions = [{
      showText: '编辑',
      onClick(record) {
        dispatch({ type: 'vip/onEditBasicInfo', payload: record })
      },
      permissionKey:VipEdit
    }, {
      showText: '分配班主任',
      onClick(record) {
        dispatch({ type: 'vip/onAssignTeacher', payload: record.id })
      },
      permissionKey:VipEdit
    }, {
      showText: '查看详情',
      onClick(record) {
        dispatch({ type: 'vip/goToUserDetail', payload: record })
      }
    },]

  const Actions = filterList(tableActions,permission)

  const TableProps = {
    loading: isLoading,
    listData: list,
    listSupport,
    pagination,
    filters,
    sorter,
    onTableChange(pagi={}, fil={}, sor={}) {
      let data = { pagination, filters, sorter }
      if(Object.keys(pagi).length!==0){
        data = { ...data, pagination: pagi }
      }
      if(Object.keys(fil).length!==0){
        data = { ...data, filters: fil }
      }
      if(Object.keys(sor).length!==0){
        data = { ...data, sorter: sor }
      }
      dispatch({ type: 'vip/query', payload: data })
    },
    Actions
  }

  const searchProps = {
    placeholder: "搜索姓名/电话",
    onSearch(value) {
      const data = { pagination, filters, sorter, searchField: value }
      dispatch({ type: 'vip/query', payload: data })
    }
  }

  const validator = (value) => {
    let phoneValidRule = /^(13[0-9]|14[5-9]|15[012356789]|166|17[0-8]|18[0-9]|19[8-9])[0-9]{8}$/
    if (!phoneValidRule.test(value.guardianPhone)) {
      message.error('手机号码为11位有效数字!')
      return true
    }
    return false
  }

  const modalProps = {
    title: modalTypeToTitle(modalType),
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'vip/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      switch (modalType) {
        case 'edit':
          if (validator(value)) {
            return
          } else {
            const formatData = formatDataToBackEndModel(value, modalType)
            dispatch({ type: 'vip/onSubmitModal', payload: formatData })
          }
          break
        case 'assignTeacher':
          const formatData = formatDataToBackEndModel(value, modalType)
          dispatch({ type: 'vip/onSubmitModal', payload: formatData })
          break
      }
    },
    content,
  }

  const clearFilter = () => {
    dispatch({ type: 'vip/clearFilter' })
  }

  return (
    <div>
      <h1>会员列表</h1>
      <Row style={{ marginBottom: 20 }} gutter={20}>
        <Col span={6}>
          <Search {...searchProps} />
        </Col>
        <Col span={6}>
          <Button onClick={clearFilter}>重置所有筛选</Button>
        </Col>
      </Row>
      <MagicTable {...TableProps} />
      <MagicFormModal {...modalProps} />
    </div>
  );
};

export default connect(({ vip, loading, user }) => ({ vip, loading, user }))(Vip)
