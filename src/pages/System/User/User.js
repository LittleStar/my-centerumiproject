import React from 'react';
import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import { getRoutes } from '../../../utils/utils';

const User = ({children, routerData,match,location,dispatch}) => {

	// const routes = getRoutes(match.path, routerData)

	return (
		<div>
		<h1>会员中心</h1>
        {/* <Switch>
          {routes.map(item => (
            <Route
              key={item.key}
              path={item.path}
              component={item.component}
              exact={item.exact}
            />
          ))}
          <Redirect exact from='/system/manage/user' to='/system/manage/user/vip' />
    </Switch> */}
    {children}
		</div>
	);
};

export default connect()(User);
