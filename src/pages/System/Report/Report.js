
import React from 'react';
import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router'
import { Input, Select, Icon } from 'antd'
import styles from './report.less'

const InputGroup = Input.Group
const Option = Select.Option
const Report = ({ children, dispatch, report }) => {

  const { organizationUnitsList, firstUnit, unitId } = report

  const showOptions = organizationUnitsList.map(unit => {
    return (
      <Option value={unit.displayName} key={unit.id}><Icon type="home" theme="twoTone" /> {unit.displayName}</Option>
    )
  })

  const handleChange = (value, option) => {
    const unitId = option.key
    dispatch({ type: 'report/changeUnit', payload: unitId })
  }

  return (
    <div>
      {/* <h1>报表中心</h1> */}
      <div className={styles.filter}>
        {/* <span style={{fontSize: 20}}>筛选中心:</span> */}
        <InputGroup compact>
          {
            firstUnit !== '' ?
              <Select style={{ width: '90%' }} defaultValue={firstUnit} onChange={handleChange}>
                {showOptions}
              </Select> : null
          }
        </InputGroup>
      </div>

      {/* <Switch>
        {routes.map(item => (
          <Route
            key={item.key}
            path={item.path}
            component={item.component}
            exact={item.exact}
          />
        ))}
        <Redirect exact from='/system/manage/report' to='/system/manage/report/sa' />
      </Switch> */}
      {children}
    </div>
  );
};

export default connect(({ report }) => ({ report }))(Report)
