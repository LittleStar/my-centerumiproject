import React from 'react'
import { connect } from 'dva'
import moment from 'moment'
import { DatePicker, Card, Icon, Input, Row, Col, Spin } from 'antd'
import UserSourseChart from 'components/Chart/UserSourseChart'
import MagicTable from 'components/MagicTable/MagicTable'
import styles from '../report.less'

const { RangePicker } = DatePicker

const UserSource = ({ dispatch, userSource, loading }) => {
  const { list, pagination, dateRange, userSourceInfo } = userSource
  const userSourceLoading = loading.models.userSource
  const { data, totalSubjectCount } = userSourceInfo

  const listSupport = {
    organizationUnitName: {
      showText: '中心名称',
      showType: 'Text',
    },
    sourceName: {
      showText: '来源渠道',
      showType: 'Text',
    },
    subjectCount: {
      showText: '储备用户数量',
      showType: 'Text',
    },
    dealCount: {
      showText: '签单数量',
      showType: 'Text',
    },
    userTransitPercent: {
      showText: '转化率（签单数量/储备用户数量）',
      showType: 'Text',
    },
    sourcePercent: {
      showText: '比例',
      showType: 'Text',
    },
  }

  const realData = data ?
    data.map(item => {
      return { name: item.source.value, value: item.percent }
    }) : []

  const datePickerProps = {
    onChange(value) {
      const defaultSelectedDate = null
      const changedDate = value.length === 0 ? defaultSelectedDate : { startTime: value[0], endTime: value[1] }
      dispatch({ type: 'userSource/updateDate', payload: changedDate })
    },
    defaultValue: dateRange === null ? [] : [dateRange.startTime, dateRange.endTime]
  }

  const userSourceListProps = {
    loading: userSourceLoading,
    listData: list,
    listSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      const data = { pagination, filters, sorter }
      dispatch({ type: 'userSource/query', payload: data })
    }
  }

  return (
    <div>
      <h1>客户来源</h1>
      <RangePicker {...datePickerProps} /><br/>
      {
        userSourceLoading ?
          <Spin /> :
          <React.Fragment>
            <Card title={<span><Icon type="pie-chart" theme="outlined" />客户来源统计表</span>} style={{ marginTop: 20 }} type="inner">
              <UserSourseChart
                data={realData}
              />
            </Card>
            <div className={styles.tableStyle}>
              <MagicTable {...userSourceListProps} />
            </div>
          </React.Fragment>
      }

    </div>
  )
}

export default connect(({ loading, userSource }) => ({ loading, userSource }))(UserSource);
