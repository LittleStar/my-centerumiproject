
import React from 'react'
import { connect } from 'dva'
import moment from 'moment'
import { DatePicker, Input, Row, Col, Button } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'


const { RangePicker } = DatePicker
const { Search } = Input

const SalesData = ({ dispatch, loading, salesData }) => {

  const salesDataLoading = loading.models.salesData
  const { list, pagination, searchField, dateRange } = salesData

  const listSupport = {
    organizationUnitName: {
      showText: '中心名称',
      showType: 'Text',
    },
    salesName: {
      showText: '负责课程顾问',
      showType: 'Text',
    },
    subjectCount: {
      showText: '储备用户数量',
      showType: 'Text',
    },
    dealCount: {
      showText: '签单数量',
      showType: 'Text',
    },
    userTransitPercent: {
      showText: '转化率（签单数量/储备用户数量）',
      showType: 'Text',
    },
  }

  const datePickerProps = {
    onChange(value) {
      const defaultSelectedDate = null
      const changedDate = value.length === 0 ? defaultSelectedDate : { startTime: value[0], endTime: value[1] }
      dispatch({ type: 'salesData/updateDate', payload: changedDate })
    },
    defaultValue: dateRange === null ? [] : [dateRange.startTime, dateRange.endTime]
  }

  const searchProps = {
    placeholder: "搜索课程顾问姓名",
    onSearch(value) {
      dispatch({ type: 'salesData/updateSearchField', payload: value })
    },
    onChange(e) {
      dispatch({ type: 'salesData/updateState', payload: { searchField: e.target.value } })
    },
    value: searchField,
  }

  const salesDataProps = {
    loading: salesDataLoading,
    listData: list,
    listSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      const data = { pagination, filters, sorter }
      dispatch({ type: 'salesData/query', payload: data })
    }
  }

  const onClickClear = () => {
    dispatch({ type: 'salesData/clearSearch' })
  }

  return (
    <div>
      <h1>课程顾问数据</h1>
      <Row type="flex" justify="start" gutter={10} style={{ marginBottom: 20 }}>
        <Col span={9}><RangePicker {...datePickerProps} /></Col>
        <Col span={6}><Search {...searchProps} /></Col>
        <Col span={8}><Button onClick={onClickClear}>清空搜索</Button></Col>
      </Row>
      <MagicTable {...salesDataProps} />
    </div>
  );
};

export default connect(({ loading, salesData }) => ({ loading, salesData }))(SalesData);
