
import React from 'react';
import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';

const Report = ({routerData,match,children,location,dispatch}) => {

	return (
		<div>
		{/* <h1>课程顾问报表</h1> */}
        {/* <Switch>
          {routes.map(item => (
            <Route
              key={item.key}
              path={item.path}
              component={item.component}
              exact={item.exact}
            />
          ))}
          <Redirect exact from='/system/manage/report/sa' to='/system/manage/report/sa/us' />
    </Switch> */}
    {children}
		</div>
	);
};

export default connect()(Report);
