import React from 'react'
import { connect } from 'dva'
import moment from 'moment'
import { DatePicker, Input, Row, Col } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'
import styles from '../report.less'

const { MonthPicker } = DatePicker
const { Search } = Input

const MonthPay = ({ dispatch, monthPay, loading }) => {

  const { list, listSupport, pagination, selectDate, searchField, filters, sorter } = monthPay

  const monthPayLoading = loading.models.monthPay

  const datePickerProps = {
    defaultValue: selectDate,
    onChange(value) {
      const defaultSelectedDate = moment()
      const changedDate = !value ? defaultSelectedDate : value
      dispatch({ type: 'monthPay/updateDate', payload: changedDate })
    }
  }

  const monthPayProps = {
    loading: monthPayLoading,
    listData: list,
    listSupport,
    pagination,
    onTableChange(pagi={}, filt={}, sort={}) {
      let data = { pagination, filters, sorter }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, pagination: pagi }
      }
      if (Object.keys(filt).length !== 0) {
        data = { ...data, filters: filt }
      }
      if (Object.keys(sort).length !== 0) {
        data = { ...data, sorter: sort }
      }
      dispatch({ type: 'monthPay/query', payload: data })
    }
  }

  return (
    <div>
      <h1>月度运营收入统计表</h1>
      <Row type="flex" justify="start" gutter={10} style={{marginBottom:10}}>
        <Col span={6}><MonthPicker {...datePickerProps} /></Col>
      </Row>
      <MagicTable {...monthPayProps} />

    </div>
  );
};

export default connect(({ monthPay, loading }) => ({ monthPay, loading }))(MonthPay)
