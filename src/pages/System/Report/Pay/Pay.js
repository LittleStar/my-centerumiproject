
import React from 'react';
import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import { getRoutes } from '../../../../utils/utils';

const Report = ({children, routerData,match,location,dispatch}) => {

	// const routes = getRoutes(match.path, routerData)

	return (
		<div>
		{/* <h1>营收报表</h1> */}
        {/* <Switch>
          {routes.map(item => (
            <Route
              key={item.key}
              path={item.path}
              component={item.component}
              exact={item.exact}
            />
          ))}
          <Redirect exact from='/system/manage/report/pa' to='/system/manage/report/pa/mp' />
    </Switch> */}
    {children}
		</div>
	);
};

export default connect()(Report);
