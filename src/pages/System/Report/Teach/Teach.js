
import React from 'react'
import { connect } from 'dva'
import { Route, Redirect, Switch, routerRedux } from 'dva/router'


const Teach = ({ routerData, match, children, location, dispatch }) => {


  return (
    <div>
      {/* <h1>教务报表</h1> */}
      {/* <Switch>
        {routes.map(item => (
          <Route
            key={item.key}
            path={item.path}
            component={item.component}
            exact={item.exact}
          />
        ))}
        <Redirect exact from='/system/manage/report/te' to='/system/manage/report/te/mc' />
      </Switch> */}
      {children}
    </div>
  );
};

export default connect()(Teach)
