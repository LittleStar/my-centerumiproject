import React from 'react'
import { connect } from 'dva'
import moment from 'moment'
import { DatePicker, Input, Row, Col, Button } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'
import styles from '../report.less'

const { MonthPicker } = DatePicker
const { Search } = Input

const TeacherCourse = ({ dispatch, teacherCourse, loading, }) => {

  const { list, listSupport, pagination, selectDate, searchField, filters, sorter } = teacherCourse
  const teacherCourseLoading = loading.models.teacherCourse

  const datePickerProps = {
    defaultValue: selectDate,
    onChange(value) {
      const defaultSelectedDate = moment()
      const changedDate = !value ? defaultSelectedDate : value
      dispatch({ type: 'teacherCourse/updateDate', payload: changedDate })
    }
  }

  const searchProps = {
    placeholder: "搜索老师姓名",
    onSearch(value) {
      dispatch({ type: 'teacherCourse/updateSearchField', payload: value })
    },
    onChange(e) {
      dispatch({ type: 'teacherCourse/updateState', payload: { searchField: e.target.value } })
    },
    value: searchField,
  }

  const onClickClear = () => {
    dispatch({ type: 'teacherCourse/clearSearch' })
  }


  const teacherCourseProps = {
    loading: teacherCourseLoading,
    listData: list,
    listSupport,
    pagination,
    onTableChange(pagi={}, filt={}, sort={}) {
      let data = { pagination, filters, sorter }
      if(Object.keys(pagi).length!==0){
        data = { ...data, pagination: pagi }
      }
      if(Object.keys(filt).length!==0){
        data = { ...data, filters: filt }
      }
      if(Object.keys(sort).length!==0){
        data = { ...data, sorter: sort }
      }
      dispatch({ type: 'teacherCourse/query', payload: data })
    },
  }
  return (
    <div>
      <h1>教师排课课时统计</h1>
      <Row type="flex" justify="start" gutter={10}>
        <Col span={6}><MonthPicker {...datePickerProps} /></Col>
        <Col span={6}><Search {...searchProps} /></Col>
        <Col span={8}><Button onClick={onClickClear}>清空搜索</Button></Col>
      </Row>
      <div className={styles.tableStyle}>
        <MagicTable {...teacherCourseProps} />
      </div>
    </div>
  );
};

export default connect(({ teacherCourse, loading, }) => ({ teacherCourse, loading }))(TeacherCourse)
