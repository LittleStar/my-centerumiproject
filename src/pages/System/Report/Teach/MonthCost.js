import React from 'react'
import { connect } from 'dva'
import moment from 'moment'
import { DatePicker, Input, Row, Col, Button } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'
import styles from '../report.less'


const { MonthPicker } = DatePicker
const { Search } = Input

const MonthCost = ({ dispatch, monthCost, loading }) => {

  const { list, totalCostCourseCount, totalCostCoursePrice, pagination, selectDate, searchField, filters, sorter } = monthCost
  const monthCostLoading = loading.models.monthCost

  const listSupport = {
    centerName: {
      showText: '中心名称',
      showType: 'Text',
      // addtional: {
      //   statusArray: ['上海古北', '北京朝阳']
      // },
      // filter: {
      //   hasFilter: true,
      //   filterOptions: [
      //     { text: '上海古北', value: 0 },
      //     { text: '北京朝阳', value: 1 },
      //   ],
      // }
    },
    creationTime: {
      showText: '申请时间',
      showType: 'Time',
      addtional: {
        format: 'MM/DD/YYYY HH:mm'
      }
    },
    subjectName: {
      showText: '儿童姓名',
      showType: 'Text',
    },
    guardiansPhone: {
      showText: '家长电话',
      showType: 'Text',
    },
    period: {
      showText: '有效期',
      showType: 'Text',
      sorter: {
        hasSorter: false
      }
    },
    realPayment: {
      showText: '合同实付金额',
      showType: 'Text',
    },
    packageCount: {
      showText: '课时数',
      showType: 'Text',
    },
    unitPrice: {
      showText: '单位课时单价',
      showType: 'Text',
    },
    startCourseCount: {
      showText: '月初剩余课时数',
      showType: 'Text',
    },
    costCourseCount: {
      showText: '本月耗课课时',
      showType: 'Text',
    },
    costCoursePrice: {
      showText: '本月耗课金额',
      showType: 'Text',
    }
  }

  const datePickerProps = {
    defaultValue: selectDate,
    onChange(value) {
      const defaultSelectedDate = moment()
      const changedDate = !value ? defaultSelectedDate : value
      dispatch({ type: 'monthCost/updateDate', payload: changedDate })
    },
  }

  const searchProps = {
    placeholder: "搜索儿童姓名",
    onSearch(value) {
      dispatch({ type: 'monthCost/updateSearchField', payload: value })
    },
    onChange(e) {
      dispatch({ type: 'monthCost/updateState', payload: { searchField: e.target.value } })
    },
    value: searchField,
  }

  const onClickClear = () => {
    dispatch({ type: 'monthCost/clearSearch' })
  }

  const monthCostProps = {
    loading: monthCostLoading,
    listData: list,
    listSupport,
    pagination,
    filters,
    onTableChange(pagi={}, filt={}, sort={}) {
      let data = { pagination, filters, sorter }
      if(Object.keys(pagi).length!==0){
        data = { ...data, pagination: pagi }
      }
      if(Object.keys(filt).length!==0){
        data = { ...data, filters: filt }
      }
      if(Object.keys(sort).length!==0){
        data = { ...data, sorter: sort }
      }
      dispatch({ type: 'monthCost/query', payload: data })
    },
    Actions: [{
      showText: '查看详情',
      onClick(record) {
      dispatch({ type: 'monthCost/goToUserDetail', payload: record.subject.id })

      }
    }]
  }

  return (
    <div>
      <h1>月度耗课统计</h1>
      <Row type="flex" justify="start" gutter={10}>
        <Col span={6}><MonthPicker {...datePickerProps} /></Col>
        <Col span={6}><Search {...searchProps} /></Col>
        <Col span={8}><Button onClick={onClickClear}>清空搜索</Button></Col>
      </Row>
      <div className={styles.tableStyle}>
        <h3 style={{marginLeft: 15, marginTop: 10}}> {`本月耗课课时总计: ${totalCostCourseCount}`} </h3>
        <h3 style={{marginLeft: 15, marginTop: 10}}> {`本月耗课金额的总计: ${totalCostCoursePrice}`} </h3>
        <MagicTable {...monthCostProps} />
      </div>
    </div>
  );
};

export default connect(({ loading, monthCost }) => ({ loading, monthCost }))(MonthCost)
