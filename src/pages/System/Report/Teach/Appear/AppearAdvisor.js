import React from 'react'
import { connect } from 'dva'
import MagicTable from 'components/MagicTable/MagicTable'


const AppearAdvisor = ({ dispatch, appearAdvisor, loading }) => {
  const appearAdvisorLoading = loading.models.appearAdvisor

  const { appearAdvisorList, pagination, filters, sorter } = appearAdvisor
  const listSupport = {
    year: {
      showText: '年份',
      showType: 'Text',
    },
    month: {
      showText: '月份',
      showType: 'Text',
    },
    headTeacherName: {
      showText: '班主任姓名',
      showType: 'Text',
    },
    shouldCostCourseCount: {
      showText: '应出勤会员课时总计',
      showType: 'Text',
    },
    costCourseCount: {
      showText: '实出勤会员课时总计',
      showType: 'Text',
    },
    percent: {
      showText: '会员出勤率',
      showType: 'Text',
    }
  }

  const TableProps = {
    loading: appearAdvisorLoading,
    key: 'appearAdvisor',
    listData: appearAdvisorList,
    listSupport,
    pagination,
    onTableChange(pagi={}, filt={}, sort={}) {
      let data = { pagination, filters, sorter }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, pagination: pagi }
      }
      if (Object.keys(filt).length !== 0) {
        data = { ...data, filters: filt }
      }
      if (Object.keys(sort).length !== 0) {
        data = { ...data, sorter: sort }
      }
      dispatch({ type: 'appearAdvisor/updatePagination', payload: data })
    },
  }

return (
  <div>
    <MagicTable {...TableProps} />
  </div>
)
}
export default connect(({ appearAdvisor, loading }) => ({ appearAdvisor, loading }))(AppearAdvisor)
