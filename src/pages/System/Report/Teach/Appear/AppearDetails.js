import React from 'react'
import { connect } from 'dva'
import MagicTable from 'components/MagicTable/MagicTable'


const AppearDetails = ({ dispatch, appearDetails, loading }) => {
  const appearDetailsLoading = loading.models.appearDetails

  const { appearDetailsList, pagination, filters, sorter, } = appearDetails

  const listSupport = {
    year: {
      showText: '年份',
      showType: 'Text',
    },
    month: {
      showText: '月份',
      showType: 'Text',
    },
    salesName: {
      showText: '顾问名称',
      showType: 'Text',
    },
    teacherName: {
      showText: '班主任名称',
      showType: 'Text',
    },
    subjectName: {
      showText: '学员姓名',
      showType: 'Text',
    },
    shouldCostCourseCount: {
      showText: '应耗课时',
      showType: 'Text',
    },
    costCourseCount: {
      showText: '实耗课时',
      showType: 'Text',
    },
  }

  const TableProps = {
    loading: appearDetailsLoading,
    key: 'AppearDetails',
    listData: appearDetailsList,
    listSupport,
    pagination,
    onTableChange(pagi={}, filt={}, sort={}) {
      console.log(pagi, filt, sort)
      let data = { pagination, filters, sorter }
      if(Object.keys(pagi).length !== 0){
        data = {...data, pagination: pagi }
      }
      if(Object.keys(filt).length !== 0){
        data = {...data, filters: filt }
      }
      if(Object.keys(sort).length !== 0){
        data = {...data, sorter: sort }
      }
      dispatch({ type: 'appearDetails/changeTable', payload: data })
    },
  }

return (
  <div>
    <MagicTable {...TableProps} />
  </div>
)
}
export default connect(({ appearDetails, loading }) => ({ appearDetails, loading }))(AppearDetails)
