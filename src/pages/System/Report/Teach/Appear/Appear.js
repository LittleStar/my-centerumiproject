import React, { Children } from 'react'
import { connect } from 'dva'
import moment from 'moment'
import { DatePicker, Input, Row, Col, Button, Tabs } from 'antd'
import { Route, Redirect, Switch, routerRedux } from 'dva/router'
import MagicTable from 'components/MagicTable/MagicTable'
// import { getRoutes } from 'utils/utils'
import NavTab from 'components/NavTab/NavTab'
import styles from '../../report.less'


const { MonthPicker } = DatePicker
const { Search } = Input
const TabPane = Tabs.TabPane

const Appear = ({ children, routerData, location, match, dispatch, appear, loading, }) => {

  const { selectDate, searchField, checkInType } = appear
  // const routes = getRoutes(match.path, routerData)
  const { search } = location

  const datePickerProps = {
    defaultValue: selectDate,
    onChange(value) {
      const defaultSelectedDate = moment()
      const changedDate = !value ? defaultSelectedDate : value
      dispatch({ type: 'appear/updateDate', payload: changedDate })
    }
  }

  const searchProps = {
    placeholder: "搜索姓名",
    onSearch(value) {
      dispatch({ type: 'appear/updateSearchField', payload: value })
    },
    onChange(e) {
      dispatch({ type: 'appear/updateState', payload: { searchField: e.target.value } })
    },
    value: searchField,
  }

  const onClickClear = () => {
    dispatch({ type: 'appear/clearSearch' })
  }

  const onTabChange = (key) => {
    dispatch({ type: 'appear/switchTab', payload: key })
  }

  return (
    <div>
      <h1>会员耗课统计</h1>
      <Row type="flex" justify="start" gutter={10}>
        <Col span={6}><MonthPicker {...datePickerProps} /></Col>
        <Col span={6}><Search {...searchProps} /></Col>
        <Col span={8}><Button onClick={onClickClear}>清空搜索</Button></Col>
      </Row>
      <div className={styles.tableStyle}>
        <Tabs activeKey={checkInType} onChange={onTabChange}>
          <TabPane tab="细节表" key="details" />
          <TabPane tab="老师表" key="teacher" />
          <TabPane tab="班主任表" key="headTeacher" />
          <TabPane tab="课程顾问表" key="sales" />
        </Tabs>
        {/* <Switch>
          {routes.map(item => (
            <Route
              key={item.key}
              path={item.path}
              component={item.component}
              exact={item.exact}
            />
          ))}
          <Redirect exact from='/system/manage/report/te/ap' to={'/system/manage/report/te/ap/appearDetails' + search} />
        </Switch> */}
        {children}
      </div>
    </div>
  );
};

export default connect(({ appear, loading }) => ({ appear, loading }))(Appear)
