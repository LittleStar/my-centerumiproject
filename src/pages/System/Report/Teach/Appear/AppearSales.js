import React from 'react'
import { connect } from 'dva'
import { Modal, Input, Row, Col, DatePicker } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'


const Search = Input.Search
const AppearSales = ({ dispatch, appearSales, loading }) => {
  const appearSalesLoading = loading.models.appearSales

  const { appearSalesList, pagination, filters, sorter } = appearSales

  const listSupport = {
    year: {
      showText: '年份',
      showType: 'Text',
    },
    month: {
      showText: '月份',
      showType: 'Text',
    },
    salesName: {
      showText: '课程顾问姓名',
      showType: 'Text',
    },
    shouldCostCourseCount: {
      showText: '应出勤会员课时总计',
      showType: 'Text',
    },
    costCourseCount: {
      showText: '实出勤会员课时总计',
      showType: 'Text',
    },
    percent: {
      showText: '会员出勤率',
      showType: 'Text',
    }
  }

  const TableProps = {
    loading: appearSalesLoading,
    key: 'appearSales',
    listData: appearSalesList,
    listSupport,
    pagination,
    onTableChange(pagi={}, filt={}, sort={}) {
      let data = { pagination, filters, sorter }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, pagination: pagi }
      }
      if (Object.keys(filt).length !== 0) {
        data = { ...data, filters: filt }
      }
      if (Object.keys(sort).length !== 0) {
        data = { ...data, sorter: sort }
      }
      dispatch({ type: 'appearSales/changeTable', payload: data })
    },
  }

  return (
    <div>
      <MagicTable {...TableProps} />
    </div>
  )
}
export default connect(({ appearSales, loading }) => ({ appearSales, loading }))(AppearSales)
