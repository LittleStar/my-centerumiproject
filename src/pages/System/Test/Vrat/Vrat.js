
import React from 'react'
import { connect } from 'dva'
import { Input, Row, Col, Button } from 'antd'
import MagicTable from '../../../../components/MagicTable/MagicTable'


const Search = Input.Search
const Vrat = ({ dispatch, testVrat }) => {

  const { list, listSupport } = testVrat
  const TableProps = {
    listData: list,
    listSupport: listSupport,
    pagination: {
      current: 1,
      pageSize: 10,
      total: 37
    },
    onTableChange(pagination, filters, sorter) {
      console.log(pagination, filters, sorter)
    },
    Actions: [{
      showText: '查看详情',
      onClick(record) {
        const requestId = record.testId
        dispatch({ type: 'testVrat/goToVratDetail', payload: requestId })
      }
    },{
      showText: '查看报告',
      onClick(record) {
        console.log(record)
        // const requestId = record.CourseID
        // dispatch({ type: 'course/goToCourseDetail', payload: requestId })
      }
    },
  ]
  }

  const searchProps = {
    placeholder: "搜索用户",
    onSearch(value) {
      console.log(value)
    }
  }
  return (

    <div>
      <h1>VRAT测评</h1>
      <Row type="flex" justify="space-between" style={{ marginBottom: 20 }}>
        <Col span={5}>
          <Search {...searchProps} />
        </Col>
        <Col span={5}>
          <Button icon="plus">新测试</Button>
        </Col>
      </Row>
      <MagicTable {...TableProps} />
    </div>
  );
};

export default connect(({ testVrat }) => ({ testVrat }))(Vrat)
