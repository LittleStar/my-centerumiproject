import { message, Input, DatePicker, Button, Row, Col, Upload, Icon, Spin } from 'antd'
import { connect } from 'dva'
import { filterList, checkPermissionKey } from 'utils/utils'
import { config,permissionConfig, getAuthHeader } from 'utils'
import moment from 'moment'
import MagicFormModal from '../../../../components/MagicFormModal/MagicFormModal'
import MagicTable from '../../../../components/MagicTable/MagicTable'

const { baseURL } = config
const UploadBatchImport = '/api/User/ExcelBatchImport'

const { location: locationPermission } = permissionConfig
const { PotentialUserEdit, ContractEdit, AssignSales, PotentialUserCreate, uploadStatus } = locationPermission

const Search = Input.Search
const { RangePicker } = DatePicker

const User = ({ saleUser, dispatch, loading, user }) => {

  const { permission } = user

  const isLoading = loading.models.saleUser
  const { list, listSupport, pagination, modalType, content, modalVisible, filters, hasSpin } = saleUser

  const timeToBirthDay = (time) => {
    return moment.utc(time.format('YYYYMMDD')).format()
  }

  const formatDataToBackEndModel = (data, modalType) => {
    switch (modalType) {
      case 'add':
      case 'edit':
        // const check = validator(data)
        // if (check) return null
        const { GuardianName, GuardianPhone, GuardianRole, GuardianSurName, SubjectBirthDay, SubjectGender, SubjectName, SubjectSource, SubjectSurName, Description } = data
        const subject = { gender: SubjectGender, name: SubjectName, surname: SubjectSurName, sourceId: SubjectSource, birthDay: timeToBirthDay(SubjectBirthDay), description: Description }
        const guardians = [{ role: GuardianRole, name: GuardianName, surname: GuardianSurName, phoneNumber: GuardianPhone }]
        return { subject, guardians }
      case 'apply':
        const { contractPics } = data
        const pictures = contractPics.map((item) => {
          const { response } = item
          const { result } = response
          return result[0].id
        })
        return { ...data, pictures }
      case 'assign':
      default:
        return data
    }
  }

  const modalTypeToTitle = (type) => {
    switch (type) {
      case 'add':
        return '添加新用户'
      case 'edit':
        return '编辑用户信息'
      case 'assign':
        return '分配课程顾问'
      case 'apply':
        return '付费申请'
      default:
        return ''
    }
  }

  const searchProps = {
    placeholder: "搜索用户/电话",
    onSearch(value) {
      const data = { pagination, filters, searchField: value }
      dispatch({ type: 'saleUser/query', payload: data })
    }
  }

  const btnNewProps = {
    onClick() {
      dispatch({ type: 'saleUser/updateState', payload: { modalVisible: true } })
      dispatch({ type: 'saleUser/onAddUser' })
    }
  }

  const validator = (value) => {
    let phoneValidRule = /^(13[0-9]|14[5-9]|15[012356789]|166|17[0-8]|18[0-9]|19[8-9])[0-9]{8}$/
    if (!phoneValidRule.test(value.GuardianPhone)) {
      message.error('手机号码为11位有效数字!')
      return true
    }
    return false
  }

  const modalProps = {
    loading: isLoading,
    title: modalTypeToTitle(modalType),
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'saleUser/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      switch (modalType) {
        case 'add':
        case 'edit':
          if (validator(value)) {
            return
          } else {
            const formatData = formatDataToBackEndModel(value, modalType)
            dispatch({ type: 'saleUser/onSubmitModal', payload: formatData })
          }
          break
        case 'apply':
        case 'assign':
          const formatData = formatDataToBackEndModel(value, modalType)
          dispatch({ type: 'saleUser/onSubmitModal', payload: formatData })
          break
      }
    },
    content,
  }

  const tableActions = [{
    showText: '编辑',
    onClick(record) {
      dispatch({ type: 'saleUser/onEditUser', payload: record.id })
    },
    permissionKey: PotentialUserEdit,
  }, {
    showText: '付费申请',
    onClick(record) {
      dispatch({ type: 'saleUser/onApply', payload: record.id })
    },
    permissionKey: ContractEdit,
  }, {
    showText: '分配课程顾问',
    onClick(record) {
      dispatch({ type: 'saleUser/onAssignSalesman', payload: record.id })
    },
    permissionKey: AssignSales
  }, {
    showText: '个人详情页',
    onClick(record) {
      dispatch({ type: 'saleUser/goToUserDetail', payload: record.id })
    }
  },]

  const Actions = filterList(tableActions, permission)

  const userListProps = {
    loading: isLoading,
    listData: list,
    listSupport,
    pagination,
    filters,
    onTableChange(pagi, filt = {}, sorter = {}) {
      let data = { pagination, filters }
      if (Object.keys(pagi).length !== 0)
        data = { ...data, pagination: pagi }
      if (Object.keys(filt).length !== 0)
        data = { ...data, filters: filt }
      dispatch({ type: 'saleUser/query', payload: data })
    },
    Actions
  }

  const uploadProps = {
    action: baseURL + UploadBatchImport,
    headers: {
      ...getAuthHeader()
    },
    showUploadList: false,
    onChange(info) {
      if (info.file.status === 'uploading') {
        dispatch({ type: 'saleUser/updateState', payload: { hasSpin: true } })
      }
      if (info.file.status === 'done') {
        const { response } = info.file
        if (response.success) {
          const { result } = response
          if (result.success) {
            const { successCount } = result
            message.success('全部用户导入成功，成功添加了' + successCount + '人')
            dispatch({ type: 'saleUser/updateState', payload: { hasSpin: false } })
            dispatch({ type: 'saleUser/query' })
          }
          else {
            const { returnFile, successCount } = result
            if (successCount === 0) {
              message.warning('全部用户数据导入失败')
            }
            else {
              message.warning('有部分用户数据导入失败，成功添加了' + successCount + '人')
            }
            dispatch({ type: 'saleUser/query' })
            dispatch({ type: 'saleUser/getResponseFile', payload: returnFile })
          }
        }
        else {
          const { error } = response
          message.error(error.message)
        }
      }
    }
  }

  const downloadBtnclick = () => {
    dispatch({ type: 'saleUser/getTemplete' })
  }

  const clearFilter = () => {
    dispatch({ type: 'saleUser/clearFilter' })
  }

  const addNewUserVisible = checkPermissionKey(PotentialUserCreate, permission)

  return (
    <div>
      <h1>储备用户</h1>
      <Row type="flex" justify="space-between" style={{ marginTop: 30, marginBottom: 30 }} gutter={10}>
        <Col span={5}>
          <Search {...searchProps} />
        </Col>
        <Col span={5}>
          <Button onClick={clearFilter}>重置所有筛选</Button>
        </Col>
        <Col span={14}>
          {
            addNewUserVisible ? <Button icon="plus" {...btnNewProps}>添加新用户</Button> : null
          }
          {
            addNewUserVisible ? <Button icon="download" onClick={downloadBtnclick}>批量导入模板</Button> : null
          }
          {
            addNewUserVisible ?
              <Upload {...uploadProps}>
                <Button>
                  <Icon type="upload" /> 上传
              </Button>
              {hasSpin?<Spin tip="上传中..."/>: null}
              </Upload> : null
          }

        </Col>
      </Row>
      <MagicFormModal {...modalProps} />
      <MagicTable {...userListProps} />
    </div>
  );
};

export default connect(({ saleUser, loading, user }) => ({ saleUser, loading, user }))(User)
