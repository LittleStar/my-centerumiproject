
import React from 'react';
import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';

const Sales = ({routerData,match, children, location,dispatch}) => {


	return (
		<div>
        {/* <Switch>
          {routes.map(item => (
            <Route
              key={item.key}
              path={item.path}
              component={item.component}
              exact={item.exact}
            />
          ))}
          <Redirect exact from='/system/manage/sales' to='/system/manage/sales/user' />
    </Switch> */}
    {children}
		</div>
	);
};

export default connect()(Sales);
