import React from 'react'
import {filterList,checkPermissionKey} from 'utils/utils'
import {enumSetting,permissionConfig} from 'utils'
import { Route, Redirect, Switch } from 'dva/router'
import { Card, Spin } from 'antd'
import { connect } from 'dva'
import NavTab from 'components/NavTab/NavTab'
import CardInfo from 'components/CardInfo/CardInfo'


const {location:locationPermission} = permissionConfig
const {TeachingPlanEdit} = locationPermission

const ClassPlanDetail = ({ children, match, location, dispatch, permissionList, classPlanBasicInfo, user, loading }) => {
  const { classPlanBasicInfo: classPlanBasicInfoLoading } = loading.models
  const weekDayMenu = {
    "0": '周日',
    "1": '周一',
    "2": '周二',
    "3": '周三',
    "4": '周四',
    "5": '周五',
    "6": '周六',
  }
  const activeMenu = {
    true: '激活',
    false: '未激活'
  }

  const { editClassModalVisible, editFormContent, planInfo } = classPlanBasicInfo
  const { permission } = user

  const { search } = location

  const list = [
    {
      tabName: '学生列表',
      path: '/system/manage/classPlanDetail/classPlanStudentList',
      permissionKey: 'edu.classDetail.studentList',
    },
  ]

  const editBtnModalProps = {
    title: '班级基本信息',
    visible: editClassModalVisible,
    onCancel() {
      dispatch({ type: 'classPlanBasicInfo/updateState', payload: { editClassModalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'classPlanBasicInfo/submitEdit', payload: value })
      dispatch({ type: 'classPlanBasicInfo/updateState', payload: { editClassModalVisible: false } })
    },
    content: editFormContent
  }

  const InfoToShowList = (info) => {

    let result = []
    const { classroom, course, teacher, planValue, startCoursePeriod, effectiveTime, expireTime, isActive } = info

    if(classroom===undefined) return []

    result.push({name:'上课时间（/周）',itemVal:weekDayMenu[planValue]})
    result.push({name:'上课时间（/天）',itemVal:startCoursePeriod.startTime+'-'+startCoursePeriod.endTime})
    result.push({name:'课程',itemVal: course?course.title:''})
    result.push({name:'教室',itemVal: classroom?classroom.name:''})
    result.push({name:'教师',itemVal:teacher?teacher.name:''})
    result.push({name:'班级状态',itemVal: activeMenu[isActive]})

    return result
  }

  const showInfo = InfoToShowList(planInfo)

  return (
    <div>
      <h1>长期课程详情页</h1>
      <div style={{ backgroundColor: '#F0F2F5', padding: 15, marginBottom: 20 }}>
        <Card>
          <Spin spinning={classPlanBasicInfoLoading} delay={200}>
            <h2>课程基本信息</h2>
            <CardInfo cardData={showInfo} />
          </Spin>
        </Card>
      </div>
      <NavTab
        location={location}
        list={list}
      />
      {children}
    </div>
  )
}

export default connect(({ classPlanBasicInfo, user, loading }) => ({ classPlanBasicInfo, user, loading }))(ClassPlanDetail)
