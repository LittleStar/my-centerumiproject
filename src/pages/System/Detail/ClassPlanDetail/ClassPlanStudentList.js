import React, { Fragment } from 'react'
import { connect } from 'dva'
import {getRoutes,filterList,checkPermissionKey} from 'utils/utils'
import {enumSetting,permissionConfig} from 'utils'
import { Button, Modal, Input, Row, Col } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'

const {location:locationPermission} = permissionConfig
const {TeachingPlanEdit} = locationPermission

const Search = Input.Search
const classPlanStudentList = ({ classPlanStudentList, dispatch, loading,user }) => {

  const {permission} = user
  const isLoading = loading.models.classPlanStudentList
  const { studentList, studentListSupport, vipList, vipListSupport, pagination, modalVisible } = classPlanStudentList


  const tableActions= [{
      showText: '移除',
      onClick(record) {
        dispatch({type: 'classPlanStudentList/onRemove', payload: record.id })
      },
      permissionKey:TeachingPlanEdit
    },]

  const Actions = filterList(tableActions,permission)

  const studentTableProps = {
    loading: isLoading,
    listData: studentList,
    listSupport: studentListSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      console.log(pagination, filters, sorter)
    },
    Actions
  }

  const AddStudent = () => {
    dispatch({ type: 'classPlanStudentList/onAddStudent' })
  }

  const addModalProps = {
    title: '添加长期学生',
    width: 800,
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'classPlanStudentList/updateState', payload: { modalVisible: false } })
    },
    onOk() {
      dispatch({ type: 'classPlanStudentList/onSubmitModal' })
    },
    destroyOnClose: true
  }

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      dispatch({ type: 'classPlanStudentList/onCheckboxChange', payload: selectedRows })
    }
  }

  const modalTableProps = {
    loading: isLoading,
    listData: vipList,
    listSupport: vipListSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      const data = { pagination, filters, sorter }
      dispatch({ type: 'classPlanStudentList/modalQuery', payload: data })
    },
    rowSelection,
  }

  const modalSearchProps = {
    placeholder: "搜索儿童姓名或电话",
    onSearch(value) {
      const data = { searchField: value }
      dispatch({ type: 'classPlanStudentList/modalQuery', payload: data })
    }
  }

  const addPlanStudent = (
    <Fragment>
      <Row type="flex" justify="space-between">
        <Col span={8}>
          <Search style={{ width: 200 }} {...modalSearchProps} />
        </Col>
      </Row>
      <MagicTable {...modalTableProps} />
    </Fragment>
  )

  const addPlanStudentBtnShow = checkPermissionKey(TeachingPlanEdit,permission)

  return (
    <div>
      {
        addPlanStudentBtnShow?
        <Row type="flex" justify="space-between">
          <Col span={8}>
            <Button icon="plus" onClick={AddStudent}>添加长期学生</Button>
          </Col>
        </Row>:null
      }
      <MagicTable {...studentTableProps} />
      <Modal {...addModalProps}>{addPlanStudent}</Modal>
    </div>
  );
};

export default connect(({ classPlanStudentList, loading,user }) => ({ classPlanStudentList, loading,user }))(classPlanStudentList)
