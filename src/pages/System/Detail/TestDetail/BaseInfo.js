
import React from 'react'
import { connect } from 'dva'
import MagicTable from 'components/MagicTable/MagicTable'


const BaseInfo = ({ dispatch, vratDetail }) => {

  const { baseInfoList, baseInfoListSupport } = vratDetail
  const TableProps = {
    listData: baseInfoList,
    listSupport: baseInfoListSupport,
    pagination: {
      current: 1,
      pageSize: 10,
      total: 37
    },
    onTableChange(pagination, filters, sorter) {
      console.log(pagination, filters, sorter)
    },
    Actions: [{
      showText: '',
      onClick(record) {
        console.log(record)
      }
    },]
  }

  return (

    <div>
      <MagicTable {...TableProps} />
    </div>
  );
};

export default connect(({ vratDetail }) => ({ vratDetail }))(BaseInfo)
