
import React from 'react'
import { connect } from 'dva'
import { Button } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'


const CommentsList = ({ dispatch, vratDetail }) => {

  const { commentsList, commentsListSupport } = vratDetail

  const TableProps = {
    listData: commentsList,
    listSupport: commentsListSupport,
    pagination: {
      current: 1,
      pageSize: 10,
      total: 37
    },
    onTableChange(pagination, filters, sorter) {
      console.log(pagination, filters, sorter)
    },
    Actions: [{
      showText: '查看详情',
      onClick(record) {
        console.log(record)
      }
    },]
  }

  return (

    <div>
      <Button style={{ marginBottom: 20 }} icon="plus">添加</Button>
      <MagicTable {...TableProps} />
    </div>
  );
};

export default connect(({ vratDetail }) => ({ vratDetail }))(CommentsList)

