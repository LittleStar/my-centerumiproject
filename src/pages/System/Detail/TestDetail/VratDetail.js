

import React from 'react'
import { Route, Redirect, Switch, routerRedux } from 'dva/router'
import NavTab from 'components/NavTab/NavTab'


const VratDetail = ({ children, location }) => {

  const {search} = location

  const list = [
    {
      tabName: '基本信息',
      path: '/system/manage/vratDetail/baseInfo',
    },
    {
      tabName: '测试信息',
      path: '/system/manage/vratDetail/userTestInfo',
    },
    {
      tabName: '观察记录',
      path: '/system/manage/vratDetail/commentsList',
    },
  ]


  return (
    <div>
      <h1>测试详情页</h1>
      <NavTab
        location={location}
        list={list}
      />
      {children}
    </div>
  )
}
export default (VratDetail)
