import React from 'react'
import { connect } from 'dva'
import { Row, Col, Button, Tabs, Card, Spin, Icon, Input } from 'antd'
import { getRoutes } from 'utils/utils'
import CardInfo from 'components/CardInfo/CardInfo'

const TabPane = Tabs.TabPane

const RegionalManagerDetail = ({ routerData, match, location, dispatch, permissionList, regionalManagerDetail, loading, user }) => {
  const { search } = location

  // const { permission } = user
  // const centerDetailLoading = loading.models.centerDetail
  const routes = getRoutes(match.path, routerData)
  const { regionalManagerInfo } = regionalManagerDetail

  const unchangeableInfoToShowList = (info) => {
    let result = []
    // 创建日期 ， 创建时间， 店铺ID
    // const { guardians, gender, birthDay, name, source, creatorUser, description } = info
    // if (name === undefined) return []
    // result.push({ name: '姓名', itemVal: name })
    // result.push({ name: '性别', itemVal: genderType[gender]['cn'] })
    // result.push({ name: '生日', itemVal: moment(birthDay).format('MM-DD-YYYY') })
    return result
  }
  const changeableInfoToShowList = (info) => {
    let result = []
    // 店铺名 ， 所在城市， 地址
    return result
  }

  const unchangeableInfo = unchangeableInfoToShowList(regionalManagerInfo)
  const bothChangeableInfo = changeableInfoToShowList(regionalManagerInfo)


  return (
    <div>
      <h1>区域经理详情页</h1>
      <div style={{ backgroundColor: '#F0F2F5', padding: 15, marginBottom: 20 }}>
        <Card>
          <h2>区域经理基本信息</h2>
          <Row type="flex" justify="space-around" gutter={16}>
            <Col span={12}>
              <Card >
                <CardInfo cardData={unchangeableInfo} />
              </Card>
            </Col>
            <Col span={12}>
              <Card>
                <CardInfo cardData={bothChangeableInfo} />
                <Button><Icon type="edit" />编辑</Button>
              </Card>
            </Col>
          </Row>
        </Card>
      </div>
    </div>
  )
}
export default connect(({ regionalManagerDetail, loading, user }) => ({ regionalManagerDetail, loading, user }))(RegionalManagerDetail)
