import React from 'react'
import { connect } from 'dva'
import { Row, Col, Button, Card, Spin, Icon, Input, Modal, Select } from 'antd'
import moment from 'moment'
import CardInfo from 'components/CardInfo/CardInfo'
import MagicFormModal from 'components/MagicFormModal/MagicFormModal'
import MagicTable from 'components/MagicTable/MagicTable'

const { Search } = Input
const Option = Select.Option

const RegionDetail = ({ dispatch, permissionList, regionDetail, loading, user }) => {

  // const { permission } = user
  const centerDetailLoading = loading.models.regionDetail
  const { regionDetailInfo, pagination, content, modalType, modalVisible, list, selectList, selectModalVisible, selectPagination, roleList, selectModalLoading } = regionDetail

  const regionalManagerListSupport = {
    creationTime: {
      showText: '入职日期',
      showType: 'Time',
      addtional: {
        format: 'MM/DD/YYYY HH:mm'
      }
    },
    // assignDate: {
    //   showText: '任命日期',
    //   showType: 'Time',
    //   addtional: {
    //     format: 'MM/DD/YYYY HH:mm'
    //   }
    // },
    id: {
      showText: '员工编号',
      showType: 'Text',
    },
    name: {
      showText: '员工姓名',
      showType: 'Text',
    },
    // controlRegion: {
    //   showText: '管辖区域',
    //   showType: 'Text',
    // },
    phoneNumber: {
      showText: '手机号',
      showType: 'Text',
    },
    gender: {
      showText: '性别',
      showType: 'Text',
      // addtional: {
      //   statusArray: ['未知', '男', '女']
      // },
      // filter: {
      //   hasFilter: true,
      //   filterOptions: [
      //     { text: '未知', value: 'Unknown' },
      //     { text: '男', value: 'Male' },
      //     { text: '女', value: 'Female' },
      //   ],
      // }
    },
    status: {
      showText: '员工状态',
      showType: 'Status',
      addtional: {
        statusArray: ['未入职', '已入职', '请假中', '已离职']
      },
      filter: {
        hasFilter: true,
        filterOptions: [
          { text: '未入职', value: 0 },
          { text: '已入职', value: 1 },
          { text: '请假中', value: 2 },
          { text: '已离职', value: 3 },
        ],
      }
    },
  }

  const selectListSupport = {
    id: {
      showText: '员工编号',
      showType: 'Text',
    },
    name: {
      showText: '员工姓名',
      showType: 'Text',
    },
    phoneNumber: {
      showText: '手机号',
      showType: 'Text',
    },
  }

  const modalTypeToTitle = (type) => {
    switch (type) {
      case 'editInfo':
        return '编辑区域信息'
      case 'add':
        return '添加区域经理'
      case 'editWorker':
        return '编辑区域经理信息'
      case 'addSelect':
        return '选择现有员工'
    }
  }
  const regionInfoToShowList = (info) => {
    const { creationTime, displayName, id } = info
    let result = []
    // 创建日期， 店铺ID
    if (creationTime === undefined) return []
    result.push({ name: '创建日期 ', itemVal: moment(creationTime).format('MM-DD-YYYY') })
    result.push({ name: '区域ID', itemVal: id })
    result.push({ name: '区域名', itemVal: displayName })
    return result
  }

  const regionalManagerToShowList = (info) => {
    let result = []
    // 入职日期  任命日期  任命日期 员工姓名 管辖区域 手机号 性别 员工状态
    return result
  }

  const regionData = regionInfoToShowList(regionDetailInfo)
  // const regionalManagerData = regionalManagerToShowList(regionDetailInfo)

  const onEditRegionInfo = () => {
    dispatch({ type: 'regionDetail/editRegionInfo' })
  }

  const onEditRegionalManagerInfo = () => {
    dispatch({ type: 'regionDetail/editRegionalManagerInfo' })
  }

  const modalProps = {
    title: modalTypeToTitle(modalType),
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'regionDetail/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      console.log('value', value)
      dispatch({ type: 'regionDetail/onSubmitModal', payload: value })
    },
    content,
  }

  const onClickAdd = () => {
    dispatch({ type: 'regionDetail/onNewWorker' })
  }

  const TableProps = {
    loading: centerDetailLoading,
    listData: list,
    listSupport: regionalManagerListSupport,
    pagination,
    onTableChange(pagi, filt = {}) {
      let data = { pagination }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, pagination: pagi }
      }
      dispatch({ type: 'regionDetail/query', payload: data })
    },
    Actions: [{
      showText: '编辑',
      onClick(record) {
        console.log('record', record)
        dispatch({ type: 'regionDetail/onEditWorker', payload: record.id })
      }
    }, {
      showText: '移除',
      onClick(record) {
        dispatch({ type: 'regionDetail/onRemoveWorker', payload: record.id })
      }
    },]
  }

  const onSelectUser = () => {
    dispatch({ type: 'regionDetail/onSelectWorker' })
  }

  const selectModalProps = {
    title: modalTypeToTitle(modalType),
    width: 800,
    visible: selectModalVisible,
    onCancel() {
      dispatch({ type: 'regionDetail/updateState', payload: { selectModalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'regionDetail/onSubmitModal' })
    },
    destroyOnClose: true,
  }

  const onHandleChange = (roleId) => {
    console.log(roleId)
    dispatch({ type: 'regionDetail/onSelectedRole', payload: roleId })
  }

  const optionsList = (
    roleList.map((item, index) => {
      return <Option key={index} value={item.id}>{item.displayName}</Option>
    })
  )

  const rowSelection = {
    type: 'radio',
    onChange: (selectedRowKeys, selectedRows) => {
      dispatch({ type: 'regionDetail/onCheckboxChange', payload: selectedRows })
    }
  }

  const selectTableProps = {
    loading: selectModalLoading,
    listData: selectList,
    listSupport: selectListSupport,
    pagination: selectPagination,
    onTableChange(pagi) {
      let data = { selectPagination }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, selectPagination: pagi }
      }
      dispatch({ type: 'regionDetail/onTabelChange', payload: data })
    },
    rowSelection,
  }

  const modalSearchProps = {
    placeholder: "搜索姓名或电话",
    onSearch(value) {
      const data = { searchField: value }
      dispatch({ type: 'regionDetail/onSearch', payload: data })
    }
  }

  const selectUserProps = (
    <React.Fragment>
      <Row type="flex" justify="start" gutter={10}>
        <Col span={8}>
          <Search style={{ width: 200 }} {...modalSearchProps} />
        </Col>
        <Col span={8}>
          <span>分配角色: </span>
          <Select defaultValue="请选择角色" onChange={onHandleChange}>{optionsList}</Select>
        </Col>
      </Row>
      <MagicTable {...selectTableProps} />
    </React.Fragment>
  )



  return (
    <div>
      <h1>区域详情页</h1>
      <div style={{ backgroundColor: '#F0F2F5', padding: 15, marginBottom: 20 }}>
        <Card>
          <h2>区域信息</h2>
          <Row type="flex" justify="space-around" gutter={16}>
            <Col span={23}>
              <Card>
                <CardInfo cardData={regionData} />
                <Button onClick={onEditRegionInfo}><Icon type="edit" />编辑</Button>
              </Card>
            </Col>
          </Row>
          {/* <h2>区域经理信息</h2>
          <Row type="flex" justify="space-around" gutter={16}>
            <Col span={23}>
              <Card>
                <CardInfo cardData={regionalManagerData} />
                <Button onClick={onEditRegionalManagerInfo}><Icon type="edit" />编辑</Button>
              </Card>
            </Col>
          </Row> */}
        </Card>
      </div>
      <Row type="flex" gutter={10} style={{ marginBottom: 10 }}>
        <Col>
          <Button icon="plus" onClick={onClickAdd}>添加区域经理</Button>
        </Col>
        <Col>
          <Button onClick={onSelectUser}><Icon type="usergroup-add" />选择员工到组织</Button>
        </Col>
      </Row>
      <MagicFormModal {...modalProps} />
      <MagicTable {...TableProps} />
      <Modal {...selectModalProps}>{selectUserProps}</Modal>
    </div>
  )
}
export default connect(({ regionDetail, loading, user }) => ({ regionDetail, loading, user }))(RegionDetail)
