import React from 'react'
import { connect } from 'dva'
import moment from 'moment'
import {filterList,checkPermissionKey} from 'utils/utils'
import {enumSetting,permissionConfig} from 'utils'
import { Card, Row, Col, Button, Modal, Spin } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'
import MagicModal from 'components/MagicFormModal/MagicFormModal'
import CardInfo from 'components/CardInfo/CardInfo'
import styles from './tabStyle.css'


const {location:locationPermission} = permissionConfig
const {ContractCancel,ContractEdit,ContractVerify} = locationPermission

const { contractStatus: contractStatusSetting, contractType: contractTypeSetting, currencyType: currencyTypeSetting } = enumSetting

const ContractInfoDetail = ({ contractInfoDetail, dispatch, loading, user }) => {

  const {permission} = user

  const { contractInfoDetail: contractInfoDetailLoading } = loading.models

  const coursePackageMenu = ['一对多', '一对一']
  const periodMenu = ['年', '个月', '周', '天']

  const { historyList, historyListSupport, modalVisible, modalType, content, pagination, userContract } = contractInfoDetail

  const {pictures, status, coursePackage, contractType, creationTime, responseUser, paymentPrice, discount, currency, effectiveTime, expireTime } = userContract

  const typeToModalTitle = (type) => {
    switch (type) {
      case 'deny':
        return '拒绝申请'
      case 'cancel':
        return '取消申请'
      case 'edit':
        return '修改合同(如果要修改课包类型可以先取消该申请再重新申请)'
      default:
        return ''
    }
  }

  const typeToBackEndData = (value,type)=>{
    switch(type){
      case 'edit':
        const {contractPics} = value
        const pictures = contractPics.map((item)=>{
          const {id,response} = item
          if(id!==undefined) return id
          else if(response!==undefined) return response.result[0].id
          else return ''
        })
        return {...value,pictures}
      case 'deny':
      default:
        return value
    }
  }

  //todo page loading
  if (status === undefined) return null

  const clickDeny = () => {
    dispatch({ type: 'contractInfoDetail/onDenyContract' })
  }

  const clickConfirm = () => {
    Modal.confirm({
      title: '您确定要审核通过该合同吗？',
      onOk() {
        dispatch({ type: 'contractInfoDetail/confirmContract' })
      }
    })
  }

  const clickEdit = () => {
    dispatch({ type: 'contractInfoDetail/onModifyContract' })
  }

  const clickCancel = () => {
    dispatch({ type: 'contractInfoDetail/onCencelContract' })

    // Modal.confirm({
    //   title: '您确定要取消该合同吗？',
    //   onOk() {
    //     dispatch({ type: 'contractInfoDetail/cancelContract' })
    //   }
    // })
  }

  const verifyPermission = checkPermissionKey(ContractVerify,permission)
  const cancelPermission = checkPermissionKey(ContractCancel,permission)
  const editContractPermission = checkPermissionKey(ContractEdit,permission)

  const statusToActions = (contractStatus) => {
    switch (contractStatus) {
      //commited
      case 1:
        return (
          <React.Fragment>
          {
            verifyPermission?
            <React.Fragment>
              <Button onClick={clickDeny}>拒绝</Button>
              <Button onClick={clickConfirm}>审核通过</Button>
             </React.Fragment>:null
          }
          {
            cancelPermission?<Button onClick={clickCancel}>取消申请</Button>:null
          }
          </React.Fragment>
        )
      //deny
      case 2:
        return (
          <React.Fragment>
          {
            editContractPermission?<Button onClick={clickEdit}>修改合同</Button>:null
          }
          {
            cancelPermission?<Button onClick={clickCancel}>取消申请</Button>:null
          }
          </React.Fragment>
        )
        break

      //None
      case 0:
        return (
          <React.Fragment>
          {
            cancelPermission?<Button onClick={clickCancel}>取消申请</Button>:null
          }
          </React.Fragment>
        )

      //confirmed，现在合同通过了之后就什么都不能改了
      case 3:
        return null

      default:
        return null
    }
  }

  const historyProps = {
    loading:contractInfoDetailLoading,
    listData: historyList,
    listSupport: historyListSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      const data = { pagination, filters, sorter }
      dispatch({ type: 'contractInfoDetail/query', payload: data })
    },
  }

  const modifyContractProps = {
    loading:contractInfoDetailLoading,
    title: typeToModalTitle(modalType),
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'contractInfoDetail/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      const postData = typeToBackEndData(value,modalType)
      dispatch({ type: 'contractInfoDetail/onSubmitModal', payload: postData })
      dispatch({ type: 'contractInfoDetail/updateState', payload: { modalVisible: false } })
    },
    content,
  }

  const showImg = (imgUrl) => {
    return (
      <a href={imgUrl} target='_blank'>
        <img src={imgUrl} alt="合同原件图片" style={{ width: 50, height: 50 }} />
      </a>
    )
  }

  const showPictures = (imgList)=>{
    return imgList.map((item)=>{
      return(
        <a key={item.id} href={item.url} target='_blank'>
          <img src={item.url} alt="合同原件图片" style={{ width: 50, height: 50 }} />
        </a>
        )
    })
  }

  const InfoToShowList = (info) => {

    let result = []
    const { creationTime, coursePackage, subject, contractType, responseUser, discount, paymentPrice, currency, price, pictures, contractPictureId, orderId } = info
    const { coursePackageType, count, period, displayDateType } = coursePackage
    if (subject === undefined) return []

    result.push({ name: '申请时间', itemVal: moment(creationTime).format('MM-DD-YYYY') })
    result.push({ name: '学生姓名', itemVal: subject.name })
    result.push({ name: '合同类型', itemVal: contractTypeSetting[contractType]['cn'] })
    result.push({ name: '申请人', itemVal: responseUser.name })
    result.push({ name: '课包类型', itemVal: coursePackageMenu[coursePackageType] })
    result.push({ name: '课时数', itemVal: count + '节' })
    result.push({ name: '课包原价', itemVal: price + ' ' + currencyTypeSetting[currency]['str'] })
    result.push({ name: '折扣金额', itemVal: discount + ' ' + currencyTypeSetting[currency]['str'] })
    result.push({ name: '实付金额', itemVal: paymentPrice + ' ' + currencyTypeSetting[currency]['str'] })
    result.push({ name: '有效期', itemVal: period + ' ' + periodMenu[coursePackageType] })
    result.push({ name: '附件', itemVal: showPictures(pictures) })
    result.push({ name: '订单号', itemVal: orderId ? orderId : null })

    return result
  }
  const showInfo = InfoToShowList(userContract)
  return (
    <div>
      <h1>合同详情页</h1>
      <div className={styles.tabBg} >
        <Card style={{ marginBottom: 10 }}>
          <h2>合同详细信息</h2>
          <Card>
            <Spin spinning={contractInfoDetailLoading} delay={800}>
              <CardInfo cardData={showInfo} />
            </Spin>
          </Card>
          <h3>{'当前合同状态： ' + contractStatusSetting[status]['cn']}</h3>
          <Row type="flex" justify="space-around">
            <Col span={4}></Col>
            <Col span={4}></Col>
            <Col span={4}></Col>
            <Col span={4}></Col>
          </Row>

          <div>{statusToActions(status)}</div>
        </Card>
      </div>

      <div className={styles.tabBg} >
        <Card style={{ marginBottom: 10 }}>
          <h2>修改历史</h2>
          <MagicTable {...historyProps} />
        </Card>
      </div>
      <MagicModal {...modifyContractProps} />
    </div>
  )
}

export default connect(({ contractInfoDetail, loading, user }) => ({ contractInfoDetail, loading, user}))(ContractInfoDetail)
