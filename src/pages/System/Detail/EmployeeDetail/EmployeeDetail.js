
import React from 'react'
import { connect } from 'dva'
import MagicTable from 'components/MagicTable/MagicTable'


const EmployeeDetail = ({ dispatch, employeeDetail, loading, user }) => {

  const { list, pagination } = employeeDetail
  const roleChangeListSupport = {
    userName: {
      showText: '姓名',
      showType: 'Text',
    },
    creationTime: {
      showText: '修改日期',
      showType: 'Time',
      addtional: {
        format: 'MM/DD/YYYY HH:mm'
      }
    },
    creatorUser: {
      showText: '任命人',
      showType: 'Text',
    },
    oldRole: {
      showText: '旧角色',
      showType: 'Text',
    },
    newRole: {
      showText: '新角色',
      showType: 'Text',
    },
    reason: {
      showText: '变更原因',
      showType: 'Text',
    },
  }

  const employeeDetailLoading = loading.models.employeeDetail

  const TableProps = {
    loading: employeeDetailLoading,
    listData: list,
    listSupport: roleChangeListSupport,
    pagination,
    onTableChange(pagi, filt = {}) {
      let data = { pagination }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, pagination: pagi }
      }
      dispatch({ type: 'employeeDetail/tableChange', payload: data })
    }
  }

  return (
    <div>
      <h1>员工详情页</h1>
      <h3>角色变更历史</h3>
      <MagicTable {...TableProps} />

    </div>


  )
}

export default connect(({ employeeDetail, loading }) => ({ employeeDetail, loading }))(EmployeeDetail)
