import React from 'react'
import { connect } from 'dva'
import  moment  from 'moment'
import styles from './tabStyle.css'
import BasicInfoCard from 'components/UserDetail/BasicInfoCard'
import MagicFormModal from 'components/MagicFormModal/MagicFormModal'

import {filterList,checkPermissionKey} from 'utils/utils'
import {enumSetting,permissionConfig} from 'utils'

const {location:locationPermission} = permissionConfig
const {AssignSales,VipEdit} = locationPermission

const { displayDateType, coursePackageType } = enumSetting

const BasicInfo = ({ dispatch, basicInfo, loading, user }) => {

  const { permission } = user

  const { basicInfo:basicInfoLoading } = loading.models

  const { modalType, content, modalVisible, userInfo, sales, headteacher, currentCoursePackage} = basicInfo

  const modalTypeToTitle = (type) => {
    switch (type) {
      case 'assignSales':
        return '分配课程顾问'
      case 'assignTeacher':
        return '分配班主任'
      case 'addFreeCourse':
        return '添加赠课'
      default:
        return ''
    }
  }
  const formatDataToBackEndModel = (data, modalType) => {
    switch (modalType) {
      case 'assignSales':
      case 'assignTeacher':
      case 'addFreeCourse': // 或许需要重新处理数据再提交
      default:
        return data
    }
  }


  const showAssignSalesBtn = checkPermissionKey(AssignSales,permission)
  const showHeadTeacherBtn = checkPermissionKey(VipEdit,permission)

  const salesShowList = (sales)=>{
    if(!sales){
      return [{name:'课程顾问信息',itemVal:'暂未分配课程顾问'}]
    }
    else{
      const {name,phoneNumber} = sales
      return[{name:'课程顾问姓名',itemVal:name},{name:'课程顾问电话',itemVal:phoneNumber}]
    }
  }
  const teacherShowlist = (headteacher)=>{
    if(!headteacher) return[{name:'班主任信息',itemVal:'暂未分配班主任'}]
    else{
      const {name, phoneNumber} = headteacher
      return[{name:'班主任姓名',itemVal:name},{name:'班主任电话',itemVal:phoneNumber}]
    }
  }
  const coursePackageShow = ({currentCoursePackage,userInfo})=>{
    if(!currentCoursePackage) return []
    else{
      const {count,coursePackageType:currentPackageType,displayDateType:displayDate,period} = currentCoursePackage
      const {restAllCourseCount,expireTime} = userInfo
      return [
        {name:'总课时数',itemVal:count},
        {name:'剩余课时数',itemVal:restAllCourseCount},
        {name:'课包类型',itemVal:coursePackageType[currentPackageType]['cn']},
        {name:'到期日期',itemVal:moment(expireTime).format('YYYY-MM-DD')}
      ]
    }
  }

  const saleCardProps = {
    titleInfo: '课程顾问信息',
    infoList: salesShowList(sales),
    btnInfo:showAssignSalesBtn?(sales?'重新分配':'分配'):false ,
    loading:basicInfoLoading,
    clickBtn() {
      dispatch({ type: 'basicInfo/onAssignSales' })
    }
  }
  const teacherCardProps = {
    titleInfo: '班主任信息',
    infoList: teacherShowlist(headteacher),
    btnInfo: showAssignSalesBtn?(headteacher?'重新分配':'分配'):false,
    loading:basicInfoLoading,
    clickBtn() {
      dispatch({ type: 'basicInfo/onAssignTeacher' })
    }
  }
  const courseCardProps = {
    titleInfo: '课程信息',
    infoList: coursePackageShow({currentCoursePackage,userInfo}),
    loading:basicInfoLoading,
  }


  const showBasicCard = (userInfo)=>{
    if(!userInfo) return null
    switch(userInfo.type){
      case 0:
        return <BasicInfoCard {...saleCardProps} />
      case 1:
        return (
          <React.Fragment>
            <BasicInfoCard {...saleCardProps} />
            <BasicInfoCard {...teacherCardProps} />
            <BasicInfoCard {...courseCardProps} />
          </React.Fragment>
          )
      default:
        return null
    }
  }

  const modalProps = {
    loading: basicInfoLoading,
    title: modalTypeToTitle(modalType),
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'basicInfo/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      const formatData = formatDataToBackEndModel(value, modalType)
      dispatch({ type: 'basicInfo/onSubmitModal', payload: formatData })
    },
    content,
  }

  return (
    <div className={styles.tabBg} >
      {showBasicCard(userInfo)}
      <MagicFormModal {...modalProps} />
    </div>
  )
}

export default connect(({ basicInfo, loading, user }) => ({ basicInfo, loading, user }))(BasicInfo)
