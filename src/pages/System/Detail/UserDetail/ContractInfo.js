import React from 'react'
import { Card, Button, Row, Col, Input } from 'antd'
import { connect } from 'dva'
import styles from './tabStyle.css'
import MagicTable from 'components/MagicTable/MagicTable'
import MagicFormModal from 'components/MagicFormModal/MagicFormModal'

import {filterList,checkPermissionKey} from 'utils/utils'
import {enumSetting,permissionConfig} from 'utils'

const {location:locationPermission} = permissionConfig
const {ContractEdit} = locationPermission

const Search = Input.Search

const ContractInfo = ({ contractInfo, dispatch, loading,user }) => {

  const {permission} = user

  const { contractInfo: contractInfoLoading } = loading.models
  const { contractList, contractListSupport, pagination, modalType, content, modalVisible, userId, userInfo } = contractInfo

  const modalTypeToTitle = (type) => {
    switch (type) {
      case 'New':
        return '续费'
      case 'ModifyPackage':
        return '更改课包'
      case 'Upgrade':
        return '升包'
      default:
        return ''
    }
  }

  const formatDataToBackEndModel = (data, modalType) => {
    switch (modalType) {
      case 'New':
      case 'ModifyPackage':
      case 'Upgrade':
      default:
        const {contractPics} = data
        const pictures = contractPics.map((item)=>{
          const {response} = item
          const {result} = response
          return result[0].id
        })
        return {...data,pictures}
    }
  }

  const TableProps = {
    loading: contractInfoLoading,
    listData: contractList,
    listSupport: contractListSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      const data = { pagination, filters, sorter, userId }
      dispatch({ type: 'contractInfo/query', payload: data })
    },
    Actions: [{
      showText: '查看详情',
      onClick(record) {
        dispatch({ type: 'contractInfo/goToContractInfoDetail', payload: record.id })
      }
    },]
  }

  const modalProps = {
    title: modalTypeToTitle(modalType),
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'contractInfo/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      const formatData = formatDataToBackEndModel(value, modalType)
      dispatch({ type: 'contractInfo/onSubmitModal', payload: formatData })
    },
    content,
  }

  const onRenewal = () => {
    dispatch({ type: 'contractInfo/onNewContract', payload: 'New' })
  }

  const onChangePackage = () => {
    dispatch({ type: 'contractInfo/onNewContract', payload: 'ModifyPackage' })
  }

  const onUpgradePackage = () => {
    dispatch({ type: 'contractInfo/onNewContract', payload: 'Upgrade' })
  }

  const showContractBtns = checkPermissionKey(ContractEdit,permission)
  const isVip = userInfo?userInfo.type===1:false

  return (
    <div className={styles.tabBg}>
      <Card style={{ marginBottom: 10 }}>
        <h2>合同列表</h2>
        {
          (showContractBtns&&isVip)?
          <Row type="flex" justify="end">
              <Col>
                <Button type="primary" onClick={onRenewal}>续费</Button>
              </Col>
              <Col style={{ marginLeft: 5 }}>
                <Button type="primary" onClick={onChangePackage}>更改课包</Button>
              </Col>
              <Col style={{ marginLeft: 5, marginBottom: 10 }}>
                <Button type="primary" onClick={onUpgradePackage}>升包</Button>
              </Col>
            </Row>:null
        }
        <MagicTable {...TableProps} />
        <MagicFormModal {...modalProps} />
      </Card>
    </div>
  )
}

export default connect(({ contractInfo, loading,user }) => ({ contractInfo, loading,user }))(ContractInfo)
