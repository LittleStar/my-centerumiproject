import React from 'react'
import { connect } from 'dva'
import { Route, Redirect, Switch, routerRedux } from 'dva/router'
import { Card, Button, Tabs, Modal } from 'antd'

import BasicInfoCard from 'components/UserDetail/BasicInfoCard'
import MagicTable from 'components/MagicTable/MagicTable'
import MagicModal from 'components/MagicFormModal/MagicFormModal'
import ILSModal from 'components/ILSFeedbackModal/ILSFeedbackModal'


import styles from './tabStyle.css'

import { filterList, checkPermissionKey } from 'utils/utils'
import { enumSetting, permissionConfig } from 'utils'

const { location: locationPermission } = permissionConfig
const { VipEdit, CourseInstance } = locationPermission

const TabPane = Tabs.TabPane;

const CourseInfo = ({ dispatch, courseInfo, loading, user }) => {

  const { permission } = user

  const isLoading = loading.models.courseInfo
  const { modalType, content, pagination, modalVisible, currentTeacher, courseList, courseType, courseLoading,
    feedbackModalVisible, feedbackList, feedbackPagination, editFeedbackModalVisible, fbModalType, selectedILS, ilsOptions, ILSModalVisible } = courseInfo

  const finishedListSupport = {
    userStatus: {
      showText: '上课状态',
      showType: 'Text',
      // addtional: {
      //   statusArray: ['未签到', '完成课程', '请假', '旷课']
      // },
      // filter: {
      //   hasFilter: false,
      //   filterOptions: [
      //     // { text: '未签到', value: 0 },
      //     { text: '完成课程', value: 1 },
      //     { text: '请假', value: 2 },
      //     { text: '旷课', value: 3 },
      //   ],
      // }
    },
    actualTime: {
      showText: '上课日期',
      showType: 'Time',
      addtional: {
        format: 'YYYY/MM/DD'
      }
    },
    coursePeriod: {
      showText: '上课时间',
      showType: 'Text',
    },
    teacherName: {
      showText: '上课老师',
      showType: 'Text',
    },
    classroomName: {
      showText: '上课教室',
      showType: 'Text',
    },
    courseName: {
      showText: '课程名称',
      showType: 'Text',
    },
    creatorUser: {
      showText: '操作人',
      showType: 'Text',
    },
    ILSFeedback: {
      showText: 'ILS反馈',
      showType: 'Text',
    },
  }

  const unfinishedListSupport = {
    userStatus: {
      showText: '上课状态',
      showType: 'Text',
      // addtional: {
      //   statusArray: ['未签到', '完成课程', '请假', '旷课']
      // },
      // filter: {
      //   hasFilter: true,
      //   filterOptions: [
      //     { text: '未签到', value: 0 },
      //     // { text: '完成课程', value: 1 },
      //     // { text: '请假', value: 2 },
      //     // { text: '旷课', value: 3 },
      //   ],
      // }
    },
    actualTime: {
      showText: '上课日期',
      showType: 'Time',
      addtional: {
        format: 'YYYY/MM/DD'
      }
    },
    coursePeriod: {
      showText: '上课时间',
      showType: 'Text',
    },
    teacherName: {
      showText: '上课老师',
      showType: 'Text',
    },
    classroomName: {
      showText: '上课教室',
      showType: 'Text',
    },
    courseName: {
      showText: '课程名称',
      showType: 'Text',
    },
    creatorUser: {
      showText: '操作人',
      showType: 'Text',
    },
    ILSFeedback: {
      showText: 'ILS反馈',
      showType: 'Text',
    },
  }

  const feedbackListSupport = {
    feedbackContent: {
      showText: '课程反馈内容',
      showType: 'Text',
    },
  }

  const modalTypeToTitle = (type) => {
    switch (type) {
      case 'assignTeacher':
        return '分配班主任'
      case 'addFreeCourse':
        return '添加赠课'
      case 'checkFeedback':
        return '查看反馈'
      case 'editFeedback':
        return '编辑反馈'
      default:
        return ''
    }
  }

  const teacherShowlist = (headteacher) => {
    if (!headteacher) return [{ name: '班主任', itemVal: '暂未分配' }]
    else {
      const { name, phoneNumber } = headteacher
      return [{ name: '班主任姓名', itemVal: name }, { name: '班主任电话', itemVal: phoneNumber }]
    }
  }

  const assignTeacherBtnShow = checkPermissionKey(VipEdit, permission)

  const teacherCardProps = {
    loading: isLoading,
    titleInfo: '班主任信息',
    infoList: teacherShowlist(currentTeacher),
    btnInfo: assignTeacherBtnShow ? (currentTeacher ? '重新分配' : '分配') : false,
    clickBtn() {
      dispatch({ type: 'courseInfo/onAssignTeacher' })
    }
  }

  const teacherModalProps = {
    loading: isLoading,
    title: modalTypeToTitle(modalType),
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'courseInfo/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'courseInfo/onSubmitModal', payload: value })
    },
    content,
  }

  const feedbackModalProps = {
    title: modalTypeToTitle(modalType),
    visible: feedbackModalVisible,
    onCancel() {
      dispatch({ type: 'courseInfo/updateState', payload: { feedbackModalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'courseInfo/onSubmitModal', payload: value })
    },
    destroyOnClose: true,
    footer: null,
  }

  const operateFbModalProps = {
    title: modalTypeToTitle(fbModalType),
    visible: editFeedbackModalVisible,
    onCancel() {
      dispatch({ type: 'courseInfo/updateState', payload: { editFeedbackModalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'courseInfo/onSubmitModal', payload: value })
    },
    destroyOnClose: true,
    content,
  }

  const courseTableActions = [{
    showText: '查看班级详情',
    onClick(record) {
      dispatch({ type: 'courseInfo/goToCourseDetail', payload: record.courseInstance.id })
    },
    permissionKey: CourseInstance
  }, {
    showText: '查看课程反馈',
    onClick(record) {
      const data = { courseInstanceId: record.courseInstance.id, contentType: 'text'}
      dispatch({ type: 'courseInfo/checkFeedback', payload: data })
    },
    // permissionKey:
  }, {
    showText: '查看ILS反馈',
    onClick(record) {
      const data = { courseInstanceId: record.courseInstance.id, contentType: 'ils'}
      dispatch({ type: 'courseInfo/checkFeedback', payload: data })
    },
    // permissionKey:
  },]

  const Actions = filterList(courseTableActions, permission)

  const courseTableProps = {
    loading: courseLoading,
    listData: courseList,
    listSupport: courseType === 'finished' ? finishedListSupport : unfinishedListSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      if (pagination !== undefined) {
        dispatch({ type: 'courseInfo/switchPagination', payload: pagination })
      }
    },
    Actions
  }

  const tabChange = (key) => {
    dispatch({ type: 'courseInfo/switchTab', payload: key })
  }

  const feedbackTableProps = {
    listData: feedbackList,
    listSupport: feedbackListSupport,
    pagination: feedbackPagination,
    Actions: [{
      showText: '编辑',
      onClick(record) {
        dispatch({ type: 'courseInfo/editFeedback', payload: record })
      }
    }, {
      showText: '删除',
      onClick(record) {
        // const {feedbackId, courseId } = record
        Modal.confirm({
          title: '你确定要删除该课程反馈？',
          okText: '确定',
          okType: 'danger',
          cancelText: '取消',
          onOk() {
            dispatch({ type: 'courseInfo/deleteFeedback', payload: record })
          }
        })
      }
    }]
  }

  const showFeedback = (
    <MagicTable {...feedbackTableProps} />
  )

  const ilsModalProps = {
    title: 'ILS进度反馈',
    visible: ILSModalVisible,
    onCancel() {
      dispatch({ type: 'courseInfo/updateState', payload: { ILSModalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'courseInfo/onSubmitModal', payload: value })
    },
    courseOptions:ilsOptions,
    selectedILS,
  }

  return (
    <div className={styles.tabBg}>
      <BasicInfoCard {...teacherCardProps} />

      <Card style={{ marginBottom: 10 }}>
        <h2>课程列表</h2>
        <Tabs activeKey={courseType} onChange={tabChange}>
          <TabPane tab="已结束课程" key="finished" />
          <TabPane tab="未完成" key="unfinished" />
        </Tabs>
        <MagicTable {...courseTableProps} />
      </Card>

      <MagicModal {...teacherModalProps} />
      <MagicModal {...operateFbModalProps} />
      <Modal {...feedbackModalProps}>{showFeedback}</Modal>
      <ILSModal {...ilsModalProps} />
    </div>
  )
}

export default connect(({ courseInfo, loading, user }) => ({ courseInfo, loading, user }))(CourseInfo)
