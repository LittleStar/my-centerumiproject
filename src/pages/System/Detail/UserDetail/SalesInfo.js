import React from 'react'
import { connect } from 'dva'
import { Card, Button } from 'antd'
import styles from './tabStyle.css'
import BasicInfoCard from 'components/UserDetail/BasicInfoCard'
import MagicFormModal from 'components/MagicFormModal/MagicFormModal'
import MagicTable from 'components/MagicTable/MagicTable'

import {filterList,checkPermissionKey} from 'utils/utils'
import {enumSetting,permissionConfig} from 'utils'

const {location:locationPermission} = permissionConfig
const {AssignSales,SalesContact} = locationPermission

const SalesInfo = ({ dispatch, saleInfo, loading,user }) => {

  const {permission} = user

  const isLoading = loading.models.saleInfo
  const { modalType, content, modalVisible, currentSale, commList, commListSupport, pagination, userId } = saleInfo

  const salesShowList = (sales)=>{
    if(!sales){
      return [{name:'课程顾问信息',itemVal:'暂未分配课程顾问'}]
    }
    else{
      const {name,phoneNumber} = sales
      return[{name:'课程顾问姓名',itemVal:name},{name:'课程顾问电话',itemVal:phoneNumber}]
    }
  }

  const modalTypeToTitle = (type) => {
    switch (type) {
      case 'assignSales':
        return '分配课程顾问'
      case 'addRecord':
        return '新建沟通记录'
      case 'editRecord':
        return '编辑沟通记录'
      default:
        return ''
    }
  }

  const assignSalesBtnShow = checkPermissionKey(AssignSales,permission)

  const saleCardProps = {
    loading: isLoading,
    titleInfo: '课程顾问信息',
    infoList: salesShowList(currentSale),
    btnInfo:assignSalesBtnShow?(currentSale ? '重新分配' : '分配'):false,
    clickBtn() {
      dispatch({ type: 'saleInfo/onAssignSales' })
    }
  }

  const CommTableProps = {
    listData: commList,
    listSupport: commListSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      dispatch({ type: 'saleInfo/query', payload: {pagination, filters, sorter,userId} })
    },
  }

  const modalProps = {
    loading: isLoading,
    title: modalTypeToTitle(modalType),
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'saleInfo/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'saleInfo/onSubmitModal', payload: value })
    },
    content,
  }

  // 新建沟通记录
  const onAddRecord = () => {
      dispatch({ type: 'saleInfo/onAddRecord'})
  }

  const showSalescontact = checkPermissionKey(SalesContact,permission)

  return (
    <div className={styles.tabBg}>
      <BasicInfoCard {...saleCardProps} />
      {
        showSalescontact?
        <Card style={{ marginBottom: 10 }}>
          <h2>沟通记录</h2>
          <Button style={{ marginBottom: 10 }} icon="plus" onClick={onAddRecord}>沟通记录</Button>
          <MagicTable {...CommTableProps} />
        </Card>:null
      }
      <MagicFormModal {...modalProps} />
    </div>
  )
}

export default connect(({ saleInfo, loading,user }) => ({ saleInfo, loading,user }))(SalesInfo)
