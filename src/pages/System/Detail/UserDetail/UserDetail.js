import React, { Fragment } from 'react'
import { Route, Redirect, Switch, routerRedux } from 'dva/router'
import { Row, Col, Button, Avatar, Card, Spin } from 'antd'
import { connect } from 'dva'
import moment from 'moment'
import styles from './tabStyle.css'
import NavButton from 'components/NavButton/NavButton'
import NavTab from 'components/NavTab/NavTab'
import CardInfo from 'components/CardInfo/CardInfo'
import MagicModal from 'components/MagicFormModal/MagicFormModal'

import { filterList, checkPermissionKey } from 'utils/utils'
import { enumSetting, permissionConfig } from 'utils'

const { location: locationPermission } = permissionConfig
const { Contract } = locationPermission

const { genderType, vipType } = enumSetting

const UserDetail = ({ children, routerData, match, location, dispatch, permissionList, userDetail, loading, user }) => {
  const { permission } = user

  const { userDetail: userDetailLoading } = loading.models

  const { search } = location

  const fullDetailList = [
    {
      tabName: '信息概况',
      path: '/system/manage/userDetail/basicInfo',
    },
    {
      tabName: '课程顾问相关',
      path: '/system/manage/userDetail/salesInfo',
    },
    {
      tabName: '课程信息',
      path: '/system/manage/userDetail/courseInfo',
    },
    {
      tabName: '合同信息',
      path: '/system/manage/userDetail/contractInfo',
      permissionKey: Contract
    },
    {
      tabName: '测评信息',
      path: '/system/manage/userDetail/vratInfo',
    },
  ]

  const notVipDetailList = [
    {
      tabName: '信息概况',
      path: '/system/manage/userDetail/basicInfo',
    },
    {
      tabName: '课程顾问相关',
      path: '/system/manage/userDetail/salesInfo',
    },
    {
      tabName: '合同信息',
      path: '/system/manage/userDetail/contractInfo',
      permissionKey: Contract
    },
    {
      tabName: '测评信息',
      path: '/system/manage/userDetail/vratInfo',
    },
  ]

  const ExpiredVipDetailList = [
    {
      tabName: '课程信息',
      path: '/system/manage/userDetail/courseInfo',
    },
    {
      tabName: '合同信息',
      path: '/system/manage/userDetail/contractInfo',
      permissionKey: Contract
    },
    {
      tabName: '测评信息',
      path: '/system/manage/userDetail/vratInfo',
    },
  ]


  const showDetailList = (userType) => {
    switch (userType) {
      case 0:
        return filterList(notVipDetailList, permission)
      case 1:
        return filterList(fullDetailList, permission)
      case 2:
        return filterList(ExpiredVipDetailList, permission)
      default:
        return []
    }
  }

  const { userInfo } = userDetail
  let pathList = showDetailList(userInfo.type)
  if(pathList.length===0) return null

  let defaultLink = pathList[0].path


  const InfoToShowList = (info) => {
    let result = []
    const { guardians, gender, birthDay, name, source, creatorUser, description } = info

    if (name === undefined) return []

    result.push({ name: '姓名', itemVal: name })
    result.push({ name: '性别', itemVal: genderType[gender]['cn'] })
    result.push({ name: '生日', itemVal: moment(birthDay).format('MM-DD-YYYY') })

    if (guardians.length > 0) {
      //获取第一个家长信息
      const { name: guardianName, role, phoneNumber } = guardians[0]
      result.push({ name: '家长姓名', itemVal: guardianName })
      result.push({ name: '家长电话', itemVal: phoneNumber })
      result.push({ name: '亲子关系', itemVal: role.displayName })
    }

    result.push({ name: '用户来源', itemVal: source ? source.value : '' })
    result.push({ name: '添加人', itemVal: creatorUser ? creatorUser.name : '' })
    result.push({ name: '备注', itemVal: description })

    return result
  }

  const showInfo = InfoToShowList(userInfo)

  const vipTypeToAvatar = (type) => {
    switch (type) {
      case 1:
      case 'Vip':
        return <Avatar size="large" className={styles.vipStyle}>会员</Avatar>
      case 2:
      case 'ExpiredVip':
        return <Avatar size="large" className={styles.nonmemberStyle}>过期会员</Avatar>
      default:
        return <Avatar size="large" className={styles.nonmemberStyle}>非会员</Avatar>
    }
  }


  return (
    <div style={{ padding: '10px' }}>
      <h1>个人详情页</h1>
      <div style={{ backgroundColor: '#F0F2F5', padding: 15, marginBottom: 20 }}>
        <Card>
          <Spin spinning={userDetailLoading} delay={800}>
            <Row type="flex" justify="space-around">
              <Col span={18} >
                <h2>个人基本信息</h2>
                <CardInfo cardData={showInfo} />
              </Col>
              <Col span={6}>
                {vipTypeToAvatar(userInfo.type)}
              </Col>
            </Row>
          </Spin>
        </Card>
      </div>
      <NavTab
        location={location}
        list={showDetailList(userInfo.type)}
        type="card"
      />
      {children}
    </div>
  )
}

export default connect(({ userDetail, loading, user }) => ({ userDetail, loading, user }))(UserDetail)
