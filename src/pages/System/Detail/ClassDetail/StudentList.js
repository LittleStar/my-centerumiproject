import React, { Fragment } from 'react'
import { connect } from 'dva'
import {getRoutes,filterList,checkPermissionKey} from 'utils/utils'
import {enumSetting,permissionConfig} from 'utils'
import { Button, Modal, Checkbox, Input, Row, Col } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'
import MagicFormModal from 'components/MagicFormModal/MagicFormModal'
import ILSModal from 'components/ILSFeedbackModal/ILSFeedbackModal'

const {location:locationPermission} = permissionConfig
const {CourseInstanceUserEdit,CourseInstanceFeedbackEdit} = locationPermission

const Search = Input.Search
const StudentList = ({ classStudentList, dispatch, loading, user }) => {

  const {permission} = user
  const isLoading = loading.models.classStudentList

  const { studentList, studentListSupport, userList, userListSupport, pagination, addModalVisible, commonModalVisible, modalType, content, ilsModalVisible, ilsOptions, modalPagination} = classStudentList

  const modalTypeToTitle = (type) => {
    switch (type) {
      case 'add':
        return '添加当日学生'
      case 'feedback':
        return '填写课堂反馈'
      default:
        return ''
    }
  }

  const tableActions = [
      {
        showText: '个人详情页',
        onClick(record) {
          dispatch({ type: 'classStudentList/goToUserDetail', payload: record.subject.id })
        }
      }, {
        showText: '课堂反馈',
        onClick(record) {
          dispatch({ type: 'classStudentList/onCreateFeedback', payload: record })
        },
        permissionKey:CourseInstanceFeedbackEdit
      },{
        showText: 'ILS进度',
        onClick(record){
          dispatch({ type: 'classStudentList/onOpenILS', payload: record })
        }
      }
    ]

  const Actions = filterList(tableActions,permission)

  const TableProps = {
    loading: isLoading,
    listData: studentList,
    listSupport: studentListSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      const data = { pagination,filters, sorter }
      // dispatch({ type: 'classStudentList/query', payload: data })
    },
    Actions
  }

  const AddStudent = () => {
    dispatch({ type: 'classStudentList/onAddStudent' })
  }

  const modalSearchProps = {
    placeholder: "搜索儿童姓名或电话",
    onSearch(value) {
      const data = { searchField: value }
      dispatch({ type: 'classStudentList/modalQuery', payload: data })
    }
  }

  const addModalProps = {
    title: modalTypeToTitle(modalType),
    width: 800,
    visible: addModalVisible,
    cancelText: '取消',
    okText: '确定',
    onCancel() {
      dispatch({ type: 'classStudentList/updateState', payload: { addModalVisible: false } })
    },
    onOk() {
      dispatch({ type: 'classStudentList/onSubmitModal' })
    },
    destroyOnClose: true,
  }

  const ilsModalProps = {
    title: 'ILS进度反馈',
    visible: ilsModalVisible,
    onCancel() {
      dispatch({ type: 'classStudentList/updateState', payload: { ilsModalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'classStudentList/onSubmitModal', payload: value })
    },
    courseOptions:ilsOptions,
  }


  const commonModalProps = {
    title: modalTypeToTitle(modalType),
    visible: commonModalVisible,
    onCancel() {
      dispatch({ type: 'classStudentList/updateState', payload: { commonModalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'classStudentList/onSubmitModal', payload: value })
    },
    content,
  }

  const rowSelection = {
    type: 'radio',
    onChange: (selectedRowKeys, selectedRows) => {
      dispatch({ type: 'classStudentList/onCheckboxChange', payload: selectedRows })
    }
  }

  const onChange = (e) => dispatch({ type: 'classStudentList/onTrialLesson', payload: e.target.checked })

  const modalTableProps = {
    loading: isLoading,
    listData: userList,
    listSupport: userListSupport,
    pagination: modalPagination,
    onTableChange(modalPagination, filters, sorter) {
      const data = { modalPagination, filters, sorter }
      dispatch({ type: 'classStudentList/modalQuery', payload: data })
    },
    rowSelection,
  }

  const addTempStudent = (
    <Fragment>
      <Row type="flex" justify="space-between">
        <Col span={8}>
          <Search style={{ width: 200 }} {...modalSearchProps} />
        </Col>
        <Col span={8}>
          <Checkbox onChange={onChange}>是否以试听形式加入</Checkbox>
        </Col>
      </Row>
      <MagicTable {...modalTableProps} />
    </Fragment>
  )

  const addSubjectBtnShow = checkPermissionKey(CourseInstanceUserEdit,permission)

  return (
    <div>
      {
        addSubjectBtnShow?<Button icon="plus" onClick={AddStudent}>添加当日学生</Button>:null
      }
      <MagicTable {...TableProps} />
      <Modal {...addModalProps}>{addTempStudent}</Modal>
      <MagicFormModal {...commonModalProps} />
      <ILSModal {...ilsModalProps} />
    </div>
  );
};

export default connect(({ classStudentList, loading, user }) => ({ classStudentList, loading, user }))(StudentList)
