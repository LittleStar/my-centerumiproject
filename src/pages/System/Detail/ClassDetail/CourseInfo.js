import { connect } from 'dva'

const CourseInfo = ({classCourse}) => {
  return(
    <div>
      <h1>课程内容</h1>
      <div>课程信息?很多小课？名字是什么？</div>
      <div>课件，教案信息显示/下载</div>
    </div>
  )
}

export default connect(({classCourse}) => ({classCourse}))(CourseInfo)
