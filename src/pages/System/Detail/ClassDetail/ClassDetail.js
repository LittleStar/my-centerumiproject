import React from 'react'
import { Route, Redirect, Switch, } from 'dva/router'
import {filterList,checkPermissionKey} from 'utils/utils'
import {enumSetting,permissionConfig} from 'utils'
import { Button, Row, Col, Card, Spin, Icon } from 'antd'
import { connect } from 'dva'
import moment from 'moment'
import NavTab from 'components/NavTab/NavTab'
import CardInfo from 'components/CardInfo/CardInfo'
import MagicFormModal from 'components/MagicFormModal/MagicFormModal'


const {location:locationPermission} = permissionConfig
const {CourseInstanceEdit,CourseInstanceUserEdit,CourseInstanceFeedbackEdit} = locationPermission

const ClassDetail = ({ children, match, location, dispatch, classBasicInfo, user, loading }) => {
  const { classBasicInfo: classBasicInfoLoading } = loading.models

  const { modalVisible, content, classInfo, modalType } = classBasicInfo
  const { permission } = user

  const { search } = location

  const list = [
    {
      tabName: '学生列表',
      path: '/system/manage/classDetail/studentList',
    },
    {
      tabName: '课程内容',
      path: '/system/manage/classDetail/courseInfo',
    },
  ]

  const editBtnProps = {
    onClick() {
      dispatch({ type: 'classBasicInfo/clickEdit' })
    }
  }

  const modalProps = {
    loading: classBasicInfoLoading,
    title: '临时修改班级（当前仅能临时修改教室，老师）',
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'classBasicInfo/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'classBasicInfo/onSubmitModal', payload: value })
    },
    content,
  }

  const InfoToShowList = (info) => {
    let result = []
    const { classroom, course, teacher, actualTime, startCoursePeriod, subjects } = info
    if (classroom === undefined) return []
    result.push({ name: '课程名', itemVal: course?course.title:'' })
    result.push({ name: '教室', itemVal: classroom?classroom.name:'' })
    result.push({ name: '教师', itemVal: teacher?teacher.name:'' })
    result.push({ name: '上课时间', itemVal: moment(actualTime).format('YYYY-MM-DD')+' '+ startCoursePeriod.startTime+'-'+startCoursePeriod.endTime})
    result.push({ name: '当日班级人数', itemVal: subjects.length })

    return result
  }
  const showInfo = InfoToShowList(classInfo)

  const showCourseEditBtn = checkPermissionKey(CourseInstanceEdit,permission)

  return (
    <div>
      <h1>班级详情页</h1>
      <div style={{ backgroundColor: '#F0F2F5', padding: 15, marginBottom: 20 }}>
        <Card>
          <Spin spinning={classBasicInfoLoading} delay={200}>
            <Row type="flex" justify="space-between">
              <Col span={8}>
                <h2>班级基本信息</h2>
              </Col>
              {
                showCourseEditBtn?
                <Col span={3}>
                  <Button {...editBtnProps}><Icon type="edit"></Icon>编辑</Button>
                </Col>:null
              }
            </Row>
            <MagicFormModal {...modalProps} />
            <CardInfo cardData={showInfo} />
          </Spin>
        </Card>
      </div>

      <NavTab
        location={location}
        list={list}
      />
      {/* <Switch>
        {routes.map(item => (
          <Route
            key={item.key}
            path={item.path}
            component={item.component}
            exact={item.exact}
          />
        ))}

        <Redirect exact from='/system/manage/classDetail' to={'/system/manage/classDetail/studentList' + search} />
      </Switch> */}
      {children}
    </div>
  )
}

export default connect(({ classBasicInfo, user, loading }) => ({ classBasicInfo, user, loading }))(ClassDetail)
