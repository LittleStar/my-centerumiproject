import React from 'react'
import { connect } from 'dva'
import MagicTable from 'components/MagicTable/MagicTable'


const Employee = ({ dispatch, employee, loading }) => {

  const { list, pagination } = employee

  const listSupport = {
    firstDay: {
      showText: '入职日期',
      showType: 'Text',
    },
    assignDay: {
      showText: '任命日期',
      showType: 'Text',
    },
    employeeNo: {
      showText: '员工编号',
      showType: 'Text',
    },
    employeeName: {
      showText: '员工姓名',
      showType: 'Text',
    },
    phoneNum: {
      showText: '手机号',
      showType: 'Text',
    },
    status: {
      showText: '性别',
      showType: 'Status',
      addtional: {
        statusArray: ['男', '女']
      },
      filter: {
        hasFilter: true,
        filterOptions: [
          { text: '男', value: 0 },
          { text: '女', value: 1 },
        ],
      }
    },
    role: {
      showText: '角色',
      showType: 'Text',
    },
  }

  const TableProps = {
    listData: list,
    listSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      console.log(pagination, filters, sorter)
    },
  }

  return (
    <div>
      <h1>员工列表</h1>
      <MagicTable {...TableProps} />
    </div>
  )
}
export default connect(({ employee, loading }) => ({ employee, loading }))(Employee)
