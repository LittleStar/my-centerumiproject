import React from 'react'
import { connect } from 'dva'
import moment from 'moment'
import { Row, Col, Button, Tabs, Card, Spin, Icon, Input, Modal, Select } from 'antd'
import CardInfo from 'components/CardInfo/CardInfo'
import MagicTable from 'components/MagicTable/MagicTable'
import MagicFormModal from 'components/MagicFormModal/MagicFormModal'

const TabPane = Tabs.TabPane
const { Search } = Input
const Option = Select.Option

const CenterDetail = ({ dispatch, centerDetail, loading, user }) => {

  // const { permission } = user
  const { list, centerDetailInfo, changeType, pagination, filters, roleList, modalType, modalVisible, content,
    selectPagination, selectModalVisible, selectList,selectModalLoading } = centerDetail
  const centerDetailLoading = loading.models.centerDetail
  const operationTypeList = ['未开业', '已开业', '已停业']
  const businessListSupport = {
    changeTime: {
      showText: '修改日期',
      showType: 'Time',
      addtional: {
        format: 'MM/DD/YYYY HH:mm'
      }
    },
    newValue: {
      showText: '修改人',
      showType: 'Text',
    },
    perationType: {
      showText: '运营状态',
      showType: 'Text',
    },
    reason: {
      showText: '原因',
      showType: 'Text',
    },
  }
  const CDHistoryListSupport = {
    creationTime: {
      showText: '上任日期',
      showType: 'Time',
      addtional: {
        format: 'MM/DD/YYYY HH:mm'
      }
    },
    creatorUser: {
      showText: '任命人',
      showType: 'Text',
    },
    cdName: {
      showText: '店长名',
      showType: 'Text',
    },
    phoneNumber: {
      showText: '手机号',
      showType: 'Text',
    },
    reason: {
      showText: '更换原因',
      showType: 'Text',
    },
  }
  const employeeListSupport = {
    creationTime: {
      showText: '入职日期',
      showType: 'Time',
      addtional: {
        format: 'MM/DD/YYYY HH:mm'
      }
    },
    // assignDay: {
    //   showText: '任命日期',
    //   showType: 'Time',
    //   addtional: {
    //     format: 'MM/DD/YYYY HH:mm'
    //   }
    // },
    id: {
      showText: '员工编号',
      showType: 'Text',
    },
    name: {
      showText: '员工姓名',
      showType: 'Text',
    },
    phoneNumber: {
      showText: '手机号',
      showType: 'Text',
    },
    gender: {
      showText: '性别',
      showType: 'Text',
      // addtional: {
      //   statusArray: ['男', '女']
      // },
    },
    role: {
      showText: '角色',
      showType: 'Text',
      filter: {
        hasFilter: true,
        filterOptions: [],
      }
    },
  }

  const selectListSupport = {
    id: {
      showText: '员工编号',
      showType: 'Text',
    },
    name: {
      showText: '员工姓名',
      showType: 'Text',
    },
    phoneNumber: {
      showText: '手机号',
      showType: 'Text',
    },
  }

  const modalTypeToTitle = (type) => {
    switch (type) {
      case 'editOperation':
        return '编辑运营状态'
      case 'editInfo':
        return '编辑店铺信息'
      case 'changeRole':
        return '变更角色'
      case 'add':
        return '添加新员工'
      case 'addSelect':
        return '选择现有员工'
    }
  }


  const unchangeableInfoToShowList = (info) => {
    const { creationTime, displayName, id } = info
    let result = []
    // 创建日期， 店铺ID
    if (creationTime === undefined) return []
    result.push({ name: '创建日期 ', itemVal: moment(creationTime).format('MM-DD-YYYY') })
    result.push({ name: '店铺ID', itemVal: id })
    return result
  }

  const bothChangeableInfoToShowList = (info) => {
    const { address1, city, displayName } = info
    let result = []
    // 店铺名 ， 所在城市， 地址
    result.push({ name: '店铺名', itemVal: displayName })
    result.push({ name: '所在城市', itemVal: city ? city : '请编辑' })
    result.push({ name: '地址', itemVal: address1 ? address1 : '请编辑' })
    return result
  }

  const operationTypeToShowList = (info) => {
    const { organizationUnitOperationType } = info
    let result = []
    // 运营状态
    result.push({ name: '运营状态', itemVal: operationTypeList[organizationUnitOperationType] })
    return result
  }

  const unchangeableInfo = unchangeableInfoToShowList(centerDetailInfo)
  const bothChangeableInfo = bothChangeableInfoToShowList(centerDetailInfo)
  const operationTypeInfo = operationTypeToShowList(centerDetailInfo)


  const onTabChange = (key) => {
    dispatch({ type: 'centerDetail/switchTab', payload: key })
  }

  const onChangeOperation = () => {
    dispatch({ type: 'centerDetail/changeOperation' })
  }

  const onEditCenterInfo = () => {
    dispatch({ type: 'centerDetail/editCenterInfo' })
  }

  const listSupportByType = (type) => {
    switch (type) {
      case 'employee':
        const roleOptions = roleList.map((item) => {
          return { text: item.displayName, value: item.id }
        })
        employeeListSupport.role.filter.filterOptions = roleOptions
        return employeeListSupport
      case 'business':
        return businessListSupport
      case 'cdHistory':
        return CDHistoryListSupport
    }
  }

  const showActionByType = (type) => {
    switch (type) {
      case 'employee':
        return [{
          showText: '编辑角色',
          onClick(record) {
            dispatch({ type: 'centerDetail/onEditRole', payload: record })
          }
        }, {
          showText: '移除',
          onClick(record) {
            dispatch({ type: 'centerDetail/onRemoveUser', payload: record })
          }
        },]
      default:
        break
    }
  }

  const TableProps = {
    loading: centerDetailLoading,
    listData: list,
    listSupport: listSupportByType(changeType),
    pagination,
    filters,
    onTableChange(pagi, fil = {}) {
      let data = { pagination, filters }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, pagination: pagi }
      }
      if (Object.keys(fil).length !== 0) {
        data = { ...data, filters: fil }
      }
      dispatch({ type: 'centerDetail/changeTable', payload: data })
    },
    Actions: showActionByType(changeType)
  }

  const rowSelection = {
    type: 'radio',
    onChange: (selectedRowKeys, selectedRows) => {
      dispatch({ type: 'centerDetail/onCheckboxChange', payload: selectedRows })
    }
  }

  const selectTableProps = {
    loading: selectModalLoading,
    listData: selectList,
    listSupport: selectListSupport,
    pagination: selectPagination,
    onTableChange(pagi) {
      let data = { selectPagination }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, selectPagination: pagi }
      }
      dispatch({ type: 'centerDetail/onTabelChange', payload: data })
    },
    rowSelection,
  }

  const selectModalProps = {
    title: modalTypeToTitle(modalType),
    width: 800,
    visible: selectModalVisible,
    onCancel() {
      dispatch({ type: 'centerDetail/updateState', payload: { selectModalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'centerDetail/onSubmitModal' })
    },
    destroyOnClose: true,
  }

  const modalProps = {
    title: modalTypeToTitle(modalType),
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'centerDetail/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'centerDetail/onSubmitModal', payload: value })
    },
    content,
  }

  const onAdd = () => {
    dispatch({ type: 'centerDetail/onNewWorker' })
  }

  const onSelectUser = () => {
    dispatch({ type: 'centerDetail/onSelectWorker' })
  }

  const modalSearchProps = {
    placeholder: "搜索姓名或电话",
    onSearch(value) {
      const data = { searchField: value }
      dispatch({ type: 'centerDetail/onSearch', payload: data })
    }
  }

  const onHandleChange = (roleId) => {
    dispatch({ type: 'centerDetail/onSelectedRole', payload: roleId })
  }

  const optionsList = (
    roleList.map((item, index) => {
      return <Option key={index} value={item.id}>{item.displayName}</Option>
    })
  )

  const selectUserProps = (
    <React.Fragment>
      <Row type="flex" justify="start" gutter={10}>
        <Col span={8}>
          <Search style={{ width: 200 }} {...modalSearchProps} />
        </Col>
        <Col span={8}>
          <span>分配角色: </span>
          <Select defaultValue="请选择角色" onChange={onHandleChange}>{optionsList}</Select>
        </Col>
      </Row>
      <MagicTable {...selectTableProps} />
    </React.Fragment>
  )

  return (
    <div>
      <h1>店铺详情页</h1>
      <div style={{ backgroundColor: '#F0F2F5', padding: 15, marginBottom: 20 }}>
        <Card>
          <h2>店铺基本信息</h2>
          <Row type="flex" justify="space-around" gutter={16}>
            <Col span={8}>
              <Card >
                <CardInfo cardData={unchangeableInfo} />
              </Card>
            </Col>
            <Col span={8}>
              <Card>
                <CardInfo cardData={operationTypeInfo} />
                <Button onClick={onChangeOperation}><Icon type="edit" />编辑</Button>
              </Card>
            </Col>
            <Col span={8}>
              <Card>
                <CardInfo cardData={bothChangeableInfo} />
                <Button onClick={onEditCenterInfo}><Icon type="edit" />编辑</Button>
              </Card>
            </Col>
          </Row>
        </Card>
      </div>
      <Tabs activeKey={changeType} onChange={onTabChange}>
        <TabPane tab="员工表" key="employee" />
        <TabPane tab="运营状态表" key="business" />
        <TabPane tab="历任店长表" key="cdHistory" />
      </Tabs>
      {changeType === 'employee' ?
        <Row type="flex" gutter={10} style={{ marginBottom: 10 }}>
          <Col>
            <Button onClick={onAdd}><Icon type="plus" />添加新员工</Button>
          </Col>
          <Col>
            <Button onClick={onSelectUser}><Icon type="usergroup-add" />选择员工到组织</Button>
          </Col>
        </Row>
        : null}
      <MagicTable {...TableProps} />
      <Modal {...selectModalProps}>{selectUserProps}</Modal>
      <MagicFormModal {...modalProps} />
    </div>
  )
}
export default connect(({ centerDetail, loading, user }) => ({ centerDetail, loading, user }))(CenterDetail)
