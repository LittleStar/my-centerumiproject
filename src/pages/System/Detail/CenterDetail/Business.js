import React from 'react'
import { connect } from 'dva'
import MagicTable from 'components/MagicTable/MagicTable'


const Business = ({ dispatch, business, loading }) => {

  const { list, pagination } = business

  const listSupport = {
    modifyDate: {
      showText: '修改日期',
      showType: 'Text',
    },
    editor: {
      showText: '修改人',
      showType: 'Text',
    },
    status: {
      showText: '运营状态',
      showType: 'Text',
    },
    reason: {
      showText: '原因',
      showType: 'Text',
    },
  }

  const TableProps = {
    listData: list,
    listSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      console.log(pagination, filters, sorter)
    },
  }
  return (
    <div>
      <h1>运营状态列表</h1>
      <MagicTable {...TableProps} />
    </div>
  )
}
export default connect(({ business, loading }) => ({ business, loading }))(Business)
