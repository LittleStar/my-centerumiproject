import React from 'react'
import { connect } from 'dva'
import MagicTable from 'components/MagicTable/MagicTable'


const CDHistory = ({ dispatch, cdHistory, loading }) => {

  const { list, pagination } = cdHistory

  const listSupport = {
    startDate: {
      showText: '上任日期',
      showType: 'Text',
    },
    appointor: {
      showText: '任命人',
      showType: 'Text',
    },
    cdName: {
      showText: '店长名',
      showType: 'Text',
    },
    phoneNo: {
      showText: '手机号',
      showType: 'Text',
    },
    reason: {
      showText: '更换原因',
      showType: 'Text',
    },
  }

  const TableProps = {
    listData: list,
    listSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      console.log(pagination, filters, sorter)
    },
  }
  return (
    <div>
      <h1>历任店长列表</h1>
      <MagicTable {...TableProps} />
    </div>
  )
}
export default connect(({ cdHistory, loading }) => ({ cdHistory, loading }))(CDHistory)
