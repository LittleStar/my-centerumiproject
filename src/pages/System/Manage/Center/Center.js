import React from 'react'
import { connect } from 'dva'
import { Input, DatePicker, Row, Col, Button } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'
import MagicFormModal from 'components/MagicFormModal/MagicFormModal'


const Search = Input.Search

const Center = ({ dispatch, center, loading }) => {
  const centerLoading = loading.models.center
  const { list, modalVisible, modalType, content, pagination } = center

  const modalTypeToTitle = (type) => {
    switch (type) {
      case 'add':
        return '添加新店铺'
    }
  }

  const listSupport = {
    creationTime: {
      showText: '创建时间',
      showType: 'Time',
      addtional: {
        format: 'YYYY/MM/DD'
      }
    },
    displayName: {
      showText: '店铺名称',
      showType: 'Text',
    },
    city: {
      showText: '所在城市',
      showType: 'Text',
    },
    totalAddress:{
      showText: '详细地址',
      showType: 'Text',
    },
    memberCount: {
      showText: '员工数',
      showType: 'Text',
    },
    organizationUnitOperationType: {
      showText: '运营状态',
      showType: 'Status',
      addtional: {
        statusArray: ['未开业', '已开业', '已停业']
      },
      // filter: {
      //   hasFilter: false,
      //   filterOptions: [
      //     { text: '未开业', value: 'None' },
      //     { text: '已开业', value: 'Opened' },
      //     { text: '已停业', value: 'Closed' },
      //   ],
      // }
    }
  }

  const TableProps = {
    listData: list,
    listSupport: listSupport,
    pagination,
    onTableChange(pagi, fil={}) {
      let data = { pagination }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, pagination: pagi }
      }
      dispatch({ type: 'center/query', payload: data })
    },
    Actions: [{
      showText: '查看详情',
      onClick(record) {
        dispatch({ type: 'center/goToCenterDetail', payload: record.id })
      }
    }]
  }

  const searchProps = {
    placeholder: "搜索店长/店铺",
    onSearch(value) {
      const data = { searchField: value }
      dispatch({ type: 'center/query', payload: data })
    }
  }

  const addCenter = () => {
    dispatch({ type: 'center/updateState', payload: { modalVisible: true } })
    dispatch({ type: 'center/newCenter' })
  }

  const modalProps = {
    loading: centerLoading,
    title: modalTypeToTitle(modalType),
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'center/updateState', payload: { modalVisible: false } })
    },
    onOk() {
      const formatData = {}
      dispatch({ type: 'center/onSubmitModal', payload: formatData })
    },
    content,
  }


  return (
    <div>
      <h1>店铺管理</h1>
      <Row type="flex" justify="space-between" style={{ marginBottom: 20 }}>
        <Col span={5}>
          <Search {...searchProps} />
        </Col>
        {/* <Col span={5}>
          <Button icon="plus" onClick={addCenter}>新建分店</Button>
        </Col> */}
      </Row>
      <MagicTable {...TableProps} />
      <MagicFormModal {...modalProps} />
    </div>
  );
};

export default connect(({ center, loading }) => ({ center, loading }))(Center)
