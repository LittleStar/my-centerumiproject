import React from 'react'
import { connect } from 'dva'
import moment from 'moment'
import { Button, Modal } from 'antd'
import MagicTable from '../../../../components/MagicTable/MagicTable'
import MagicFormModal from '../../../../components/MagicFormModal/MagicFormModal'

const PackageList = ({ dispatch, managePackage }) => {

    const { packageList, packageListSupport, pagination, modalType, content, modalVisible } = managePackage

    const modalProps = {
        title: modalType==='add'?'创建课包':'编辑课包',
        visible: modalVisible,
        onCancel() {
            dispatch({ type: 'managePackage/updateState', payload: { modalVisible: false } })
        },
        onOk(value) {
          //const formatData = formatDataToBackEndModel(value)
          dispatch({ type: 'managePackage/onSubmitModal', payload: value})
        },
        content,
    }

    const TableProps = {
        listData: packageList,
        listSupport: packageListSupport,
        pagination,
        onTableChange(pagination, filters, sorter) {
            console.log(pagination, filters, sorter)
        },
        Actions: [{
            showText: '编辑',
            onClick(record) {
              dispatch({ type: 'managePackage/onEditPackage', payload: record.id })
            }
        }, {
            showText: '删除',
            onClick(record) {
              Modal.confirm({
                title: `你确定要删除课时数为${record.count},有效期为${record.period}, 类型为${record.coursePackageType}的课包吗？`,
                okText: '确定',
                okType: 'danger',
                cancelText: '取消',
                onOk() {
                  dispatch({ type: 'managePackage/onDeletePackage', payload: record.id })
                }
              })
            }
        },]
    }

    const onClickAdd = () => {
      dispatch({ type: 'managePackage/onAddPackage'})
    }

    return (
        <div>
            <Button icon="plus" onClick={onClickAdd}>创建课包</Button>
            <MagicFormModal {...modalProps} />
            <MagicTable {...TableProps} />
        </div>
    )
}

export default connect(({ managePackage }) => ({ managePackage }))(PackageList)
