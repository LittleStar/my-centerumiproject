import React from 'react';
import { connect } from 'dva';
import { Input, Row, Col, Button } from 'antd';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import { getRoutes } from '../../../../utils/utils';
import MagicTable from '../../../../components/MagicTable/MagicTable';
import NavTab from '../../../../components/NavTab/NavTab';

const Search = Input.Search;
const Course = ({ children, location }) => {

  const list = [
    {
      tabName: '课包列表',
      path: '/system/manage/manage/course/packageList',
    },
    {
      tabName: '课程列表',
      path: '/system/manage/manage/course/courseList',
    },
  ];

  return (
    <div>
      <h1>课程管理</h1>
      <NavTab location={location} list={list} />
      {children}
    </div>
  );
};

export default Course;
