import React from 'react'
import { connect } from 'dva'
import { Modal, Input, Row, Col, Button } from 'antd'
import { Route, Redirect, Switch, routerRedux } from 'dva/router'
import { getRoutes } from '../../../../utils/utils'
import MagicTable from '../../../../components/MagicTable/MagicTable'
import MagicFormModal from '../../../../components/MagicFormModal/MagicFormModal'

const CourseList = ({ dispatch, manageCourse }) => {

  const { courseList, courseListSupport, pagination, modalType, content, modalVisible, filters,sorter } = manageCourse

  const modalProps = {
    title: modalType === 'add' ? '添加课程' : '编辑课程',
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'manageCourse/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'manageCourse/onSubmitModal', payload: value })
    },
    content
  }

  const onClickAdd = () => {
    dispatch({ type: 'manageCourse/onAddCourse' })
  }

  const TableProps = {
    listData: courseList,
    listSupport: courseListSupport,
    pagination,
    filters,
    onTableChange(pagi={}, filt={}, sort={}) {
      let data = { pagination, filters, sorter }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, pagination: pagi }
      }
      if (Object.keys(filt).length !== 0) {
        data = { ...data, filters: filt }
      }
      if (Object.keys(sort).length !== 0) {
        data = { ...data, sorter: sort }
      }
      dispatch({ type: 'manageCourse/changeTable', payload: data })

    },
    Actions: [{
      showText: '编辑',
      onClick(record) {
        dispatch({ type: 'manageCourse/onEditCourse', payload: record.id })
      }
    }, {
      showText: '删除',
      onClick(record) {
        Modal.confirm({
          title: '你确定要删除' + record.title + '课程？',
          okText: '确定',
          okType: 'danger',
          cancelText: '取消',
          onOk() {
            dispatch({ type: 'manageCourse/onDeleteCourse', payload: record.id })
          }
        })
      }
    }, {
      showText: '查看详情',
      onClick(record) {
        dispatch({ type: 'manageCourse/goToCourseDetail', payload: record.id })
      }
    },]
  }

  return (
    <div>
      <Button icon="plus" onClick={onClickAdd}>添加课程</Button>
      <MagicFormModal {...modalProps} />
      <MagicTable {...TableProps} />
    </div>
  )
}

export default connect(({ manageCourse }) => ({ manageCourse }))(CourseList)
