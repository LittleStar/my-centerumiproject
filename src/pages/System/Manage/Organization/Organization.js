import React from 'react'
import { connect } from 'dva'
import { Input, DatePicker, Row, Col, Button, Modal } from 'antd'
import MagicFormModal from 'components/MagicFormModal/MagicFormModal'
import OrganizationTree from 'components/OrganizationTree/OrganizationTree'

const Search = Input.Search

const Organization = ({ dispatch, organization, loading }) => {

  const organizationLoading = loading.models.organization
  const { organizationList, modalVisible, content, modalType, parentId,currentClickId } = organization

  const modalTypeToTitle = (type) => {
    switch (type) {
      case 'addOutside':
        return '添加店铺/区域'
      case 'addInsid':
        return '为区域添加子店铺/区域'
      case 'move':
        return '转区域'
    }
  }

  const formatDataToBackEndModel = (data, type) => {
    switch (type) {
      case 'addOutside':
        return {
          displayName: data.displayName, city: data.city, organizationUnitType: data.organizationUnitType, address1: data.address, isActive: data.isActive
        }
      case 'addInsid':
        return {
          displayName: data.displayName, city: data.city, organizationUnitType: data.organizationUnitType,
          address1: data.address, isActive: data.isActive, parentId,
        }
      case 'move':
        const { regions } = data
        return {
          newParentId: regions, id: currentClickId
        }
      default:
        return data
    }
  }

  const addLocationOrRegion = () => {
    dispatch({ type: 'organization/onNewLocationOrRegion' })
  }

  const modalProps = {
    title: modalTypeToTitle(modalType),
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'organization/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      const formatData = formatDataToBackEndModel(value, modalType)
      dispatch({ type: 'organization/onSubmitModal', payload: formatData })
    },
    content,
  }


  const treeProps = {
    organizationList,
    onGoToLocationDetail(id) {
      dispatch({ type: 'organization/goToCenterDetail', payload: id })
    },
    onGoToRegionDetail(id) {
      dispatch({ type: 'organization/goToRegionDetail', payload: id })
    },
    onAddLocationToRegion(id) {
      dispatch({ type: 'organization/onNewLocation', payload: id })
    },
    onDeleteLocationOrRegion(id) {
      const locationOrRegionInfo = organizationList.find(item => {
        return item.id === id
      })
      const { displayName, organizationUnitType } = locationOrRegionInfo
      const showOrganizationUnitType = organizationUnitType === 0 ? '店铺' : '区域'
      Modal.confirm({
        title: '删除区域/店铺',
        content: `您确定要删除${displayName}${showOrganizationUnitType}吗`,
        onOk() {
          dispatch({ type: 'organization/onDelete', payload: id })
        }
      })
    },
    onMoveLocation(id) {
      dispatch({ type: 'organization/onMove', payload: id })
    }

  }

  return (
    <div>
      <h1>架构管理</h1>
      <Row type="flex" justify="space-between" style={{ marginBottom: 20 }}>
        <Col span={5}>
          <Button icon="plus" onClick={addLocationOrRegion}>新建分店/区域</Button>
        </Col>
      </Row>
      <OrganizationTree {...treeProps} />
      <MagicFormModal {...modalProps} />
    </div>
  );
};

export default connect(({ organization, loading }) => ({ organization, loading }))(Organization)
