import React from 'react';
import { connect } from 'dva';
import { Input, Row, Col, Button } from 'antd';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import MagicTable from 'components/MagicTable/MagicTable';
import NavTab from 'components/NavTab/NavTab';


const Region = ({ children, location, }) => {

  const list = [
    {
      tabName: '区域列表',
      path: '/system/manage/manage/region/regionList',
    },
    // {
    //   tabName: '区域经理列表',
    //   path: '/system/manage/manage/region/regionalManagerList',
    // },
  ];

  return (
    <div>
      <h1>区域管理</h1>
      <NavTab location={location} list={list} />
      {children}
    </div>
  );
};

export default Region;
