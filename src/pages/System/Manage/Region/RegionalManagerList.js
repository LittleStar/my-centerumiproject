
import React from 'react'
import { connect } from 'dva'
import { Input, Row, Col, Button } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'
import MagicFormModal from 'components/MagicFormModal/MagicFormModal'


const Search = Input.Search
const RegionalManagerList = ({ dispatch, regionalManagerList, loading }) => {

  const regionalManagerListLoading = loading.models.regionalManagerList


  const regionManagerListSupport = {
    startDate: {
      showText: '入职日期',
      showType: 'Time',
      addtional: {
        format: 'MM/DD/YYYY HH:mm'
      }
    },
    assignDate: {
      showText: '任命日期',
      showType: 'Time',
      addtional: {
        format: 'MM/DD/YYYY HH:mm'
      }
    },
    employeeNo: {
      showText: '员工编号',
      showType: 'Text',
    },
    employeeName: {
      showText: '员工姓名',
      showType: 'Text',
    },
    controlRegion: {
      showText: '管辖区域',
      showType: 'Text',
    },
    phoneNo: {
      showText: '手机号',
      showType: 'Text',
    },
    sex: {
      showText: '性别',
      showType: 'Status',
      addtional: {
        statusArray: ['男', '女']
      },
      filter: {
        hasFilter: true,
        filterOptions: [
          { text: '男', value: 0 },
          { text: '女', value: 1 },
        ],
      }
    },
    status: {
      showText: '员工状态',
      showType: 'Status',
      addtional: {
        statusArray: ['未入职', '已入职', '请假中', '已离职']
      },
      filter: {
        hasFilter: true,
        filterOptions: [
          { text: '未入职', value: 0 },
          { text: '已入职', value: 1 },
          { text: '请假中', value: 2 },
          { text: '已离职', value: 3 },
        ],
      }
    },
  }
  const { list, pagination, modalVisible, content, filters, sorter } = regionalManagerList

  const onClickAdd = () => {
    dispatch({ type: 'regionalManagerList/onAddRegionManager' })
  }

  const TableProps = {
    loading: regionalManagerListLoading,
    listData: list,
    listSupport: regionManagerListSupport,
    pagination,
    onTableChange(pagi, filt = {}, sort = {}) {
      let data = { pagination, filters, sorter }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, pagination: pagi }
      }
      if (Object.keys(filt).length !== 0) {
        data = { ...data, filters: filt }
      }
      if (Object.keys(sort).length !== 0) {
        data = { ...data, sorter: sort }
      }
      dispatch({ type: 'regionalManagerList/query', payload: data })
    },
    Actions: [{
      showText: '查看详情',
      onClick(record) {
        // 传值
        dispatch({ type: 'regionalManagerList/goToRegionDetail', payload: 1 })
      }
    },]
  }

  const modalProps = {
    title: '添加区域经理',
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'regionalManagerList/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      //const formatData = formatDataToBackEndModel(value)
      // dispatch({ type: 'managePackage/onSubmitModal', payload: value})
    },
    content,
  }

  return (
    <div>
      <Button icon="plus" onClick={onClickAdd}>添加区域经理</Button>
      <MagicFormModal {...modalProps} />
      <MagicTable {...TableProps} />
    </div>
  );
};

export default connect(({ regionalManagerList, loading }) => ({ regionalManagerList, loading }))(RegionalManagerList)
