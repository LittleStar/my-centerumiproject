
import React from 'react'
import { connect } from 'dva'
import { Input, Row, Col, Button } from 'antd'
import MagicTable from 'components/MagicTable/MagicTable'
import MagicFormModal from 'components/MagicFormModal/MagicFormModal'


const Search = Input.Search
const RegionList = ({ dispatch, regionList, loading }) => {

  const regionListLoading = loading.models.regionList

  const regionListSupport = {
    creationTime: {
      showText: '创建时间',
      showType: 'Time',
      addtional: {
        format: 'YYYY/MM/DD'
      }
    },
    displayName: {
      showText: '区域名',
      showType: 'Text',
    },
    // regionalManager: {
    //   showText: '区域经理',
    //   showType: 'Text',
    // },
    // number: {
    //   showText: '管辖数',
    //   showType: 'Text',
    // },
  }
  const { list, pagination, modalType, modalVisible, content } = regionList

  const onClickAdd = () => {
    dispatch({ type: 'regionList/onAddRegion' })
  }

  const TableProps = {
    loading: regionListLoading,
    listData: list,
    listSupport: regionListSupport,
    pagination,
    onTableChange(pagi, filt = {}) {
      let data = { pagination }
      if (Object.keys(pagi).length !== 0) {
        data = { ...data, pagination: pagi }
      }
      dispatch({ type: 'regionList/query', payload: data })
    },
    Actions: [
      // {
      //   showText: '编辑',
      //   onClick(record) {
      //     dispatch({ type: 'regionList/onEditRegion' })
      //   }
      // },
      {
        showText: '查看详情',
        onClick(record) {
          dispatch({ type: 'regionList/goToRegionDetail', payload: record.id })
        }
      },]
  }

  const modalProps = {
    title: modalType === 'add' ? '添加区域' : '编辑区域',
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'regionList/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      //const formatData = formatDataToBackEndModel(value)
      // dispatch({ type: 'managePackage/onSubmitModal', payload: value})
    },
    content,
  }

  return (
    <div>
      {/* <Button icon="plus" onClick={onClickAdd}>添加区域</Button> */}
      <MagicFormModal {...modalProps} />
      <MagicTable {...TableProps} />
    </div>
  );
};

export default connect(({ regionList, loading }) => ({ regionList, loading }))(RegionList)
