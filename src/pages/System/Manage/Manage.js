import React from 'react';
import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import { getRoutes } from '../../../utils/utils';

const Manage = ({children,match,location,dispatch}) => {

	// const routes = getRoutes(match.path, routerData)

	return (
		<div>
		{/* <h1>管理中心</h1> */}
        {/* <Switch>
          {routes.map(item => (
            <Route
              key={item.key}
              path={item.path}
              component={item.component}
              exact={item.exact}
            />
          ))}
          <Redirect exact from='/system/manage/manage' to='/system/manage/manage/organization' />
    </Switch> */}
    {children}
		</div>
	);
};

export default connect()(Manage);
