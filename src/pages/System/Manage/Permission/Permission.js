import { connect } from 'dva';
import { Button } from 'antd'
import MagicTable from '../../../../components/MagicTable/MagicTable'
import MagicFormModal from '../../../../components/MagicFormModal/MagicFormModal'


const Permission = ({ dispatch, permission }) => {

  const { list, listSupport, pagination, modalType, content, modalVisible } = permission

  const TableProps = {
    listData: list,
    listSupport: listSupport,
    pagination,
    onTableChange(pagination, filters, sorter) {
      console.log(pagination, filters, sorter)
    },
    Actions: [{
      showText: '编辑',
      onClick(record) {
        console.log(record)
      }
    }, {
      showText: '删除',
      onClick(record) {
        console.log(record)
      }
    }]
  }

  const onAddRole = () => {
    dispatch({ type: 'permission/onAddRole' })
  }

  const modalProps = {
    title: modalType === 'add' ? '新建角色' : '编辑角色',
    visible: modalVisible,
    onCancel() {
      dispatch({ type: 'permission/updateState', payload: { modalVisible: false } })
    },
    onOk(value) {
      dispatch({ type: 'permission/onSubmitModal', payload: value })
    },
    content,
  }


  return (
    <div>
      <h1>权限管理</h1>
      <Button style={{ marginBottom: 20 }} icon="plus" onClick={onAddRole}>新建角色</Button>
      <MagicTable {...TableProps} />
      <MagicFormModal {...modalProps} />
    </div>
  );
};

export default connect(({ permission }) => ({ permission }))(Permission)
