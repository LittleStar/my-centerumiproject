import { request} from '../../utils'

const	PostCreateOrUpdateClassroom='/api/services/app/Classroom/CreateOrUpdateClassroom'
const	PostGetClassrooms='/api/services/app/Classroom/FindAll'
const	GetDeleteClassroom= '/api/services/app/Classroom/DeleteClassroom?Id={Id}'
const	GetGetDetail='/api/services/app/Classroom/GetDetail?Id={Id}'


export async function CreateOrUpdateClassroom(data) {
	return request(PostCreateOrUpdateClassroom, {
		method: "POST",
		body: data,
	});
}

export async function GetClassrooms(data) {
	return request(PostGetClassrooms, {
		method: "POST",
		body: data,
	});
}

export async function GetDetail(Id='') {
	return request(GetGetDetail.replace('{Id}',Id), {
		method: "GET",
	});
}
export async function DeleteClassroom(Id='') {
  return request(GetDeleteClassroom.replace('{Id}', Id), {
    method: "DELETE"
  })
}
