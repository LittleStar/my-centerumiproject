import { request } from '../../utils'

const	GetGetUserAvailableContract='/api/services/app/UserContract/GetUserAvailableContract?userId={userId}'
const	PostApplyForContract='/api/services/app/UserContract/ApplyForContract'
const	PostCommitContract='/api/services/app/UserContract/CommitContract'
const	PostUserContractList='/api/services/app/UserContract/UserContractList'
const	GetGetContractForEdit='/api/services/app/UserContract/GetContractForEdit?Id={Id}'
const	PostConfirm='/api/services/app/UserContract/Confirm?userContractId={userContractId}'
const	PostDeny='/api/services/app/UserContract/Deny'
const	PostCancel='/api/services/app/UserContract/Cancel'
const   PutUpdateContract='/api/services/app/UserContract/UpdateContract'
const   PostChangeHistory='/api/services/app/UserContract/ChangeHistory'


export async function ApplyForContract(data) {
	return request(PostApplyForContract, {
		method: "POST",
		body: data,
	});
}

export async function CommitContract(data) {
	return request(PostCommitContract, {
		method: "POST",
		body: data,
	});
}

export async function UserContractList(data) {
	return request(PostUserContractList, {
		method: "POST",
		body: data,
	});
}

export async function GetUserAvailableContract(userId='') {
	return request(GetGetUserAvailableContract.replace('{userId}',userId), {
		method: "GET",
	});
}

export async function GetContractForEdit(Id) {
	return request(GetGetContractForEdit.replace('{Id}',Id), {
		method: "GET",
	});
}

export async function Confirm(userContractId) {
	return request(PostConfirm.replace('{userContractId}',userContractId), {
		method: "POST",
	});
}

export async function UpdateContract(data) {
	return request(PutUpdateContract, {
		method: "PUT",
		body: data,
	});
}

export async function Deny(data) {
	return request(PostDeny, {
		method: "POST",
		body: data,
	});
}

export async function Cancel(data) {
	return request(PostCancel, {
    method: "POST",
    body: data,
	});
}

export async function ChangeHistory(data) {
	return request(PostChangeHistory, {
		method: "POST",
		body: data,
	});
}
