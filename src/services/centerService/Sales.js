import { request, generateRequestStr } from '../../utils'

const	PostCreateContact='/api/services/app/Sales/CreateContact'
const	GetGetContactRecord='/api/services/app/Sales/GetContactRecord'

export async function GetContactRecord(data) {
	const queryStr = generateRequestStr(data)
	return request(GetGetContactRecord+queryStr, {
		method: "GET",
	});
}

export async function CreateContact(data) {
  return request(PostCreateContact, {
    method: "POST",
    body: data,
  });
}