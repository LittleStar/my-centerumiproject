import { request } from '../../utils'

//Course
const  PostCreateOrUpdateCourse='/api/services/app/Course/CreateOrUpdateCourse'
const  PostFindAllCourse='/api/services/app/Course/FindAll'
const  GetGetCourseDetail='/api/services/app/Course/GetDetail?Id={Id}'
const  DeleteCourse='/api/services/app/Course/DeleteCourse?Id={Id}'

//Package
const  PostCreateOrUpdatePackage='/api/services/app/Course/CreateOrUpdatePackage'
const  PostFindAllPackage='/api/services/app/Course/PackagesAll'
const  GetGetPackageDetail='/api/services/app/Course/GetPackageDetail?Id={Id}'
const  DeletePackage='/api/services/app/Course/DeletePackage?Id={Id}'


const PostCreateOrUpdateRole= '/api/services/app/Role/CreateOrUpdateRole'

export async function CreateOrUpdateCourse(data) {
  return request(PostCreateOrUpdateCourse, {
    method: "POST",
    body: data,
  });
}

export async function FindAllCourse(data) {
  return request(PostFindAllCourse, {
    method: "POST",
    body: data,
  });
}

export async function GetCourseDetail(Id) {
  return request(GetGetCourseDetail.replace('{Id}', Id), {
    method: "GET",
  });
}

export async function CourseDelete(Id) {
  return request(DeleteCourse.replace('{Id}', Id), {
    method: "DELETE",
  });
}

export async function CreateOrUpdatePackage(data) {
  return request(PostCreateOrUpdatePackage, {
    method: "POST",
    body: data,
  });
}

export async function FindAllPackage(data) {
  return request(PostFindAllPackage, {
    method: "POST",
    body: data,
  });
}

export async function GetPackageDetail(Id) {
  return request(GetGetPackageDetail.replace('{Id}', Id), {
    method: "GET",
  });
}

export async function PackageDelete(Id) {
  return request(DeletePackage.replace('{Id}', Id), {
    method: "DELETE",
  });
}

export async function CreateOrUpdateRole(data) {
  return request(PostCreateOrUpdateRole, {
    method: "POST",
    body: data,
  });
}

