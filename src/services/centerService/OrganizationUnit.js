import { request, generateRequestStr } from '../../utils'

const PostFindOrganizationUnits = '/api/services/app/OrganizationUnit/FindOrganizationUnits'
const GetGetOrganizationUnitUsers = '/api/services/app/OrganizationUnit/GetOrganizationUnitUsers'
const PostCreateOrganizationUnit = '/api/services/app/OrganizationUnit/CreateOrganizationUnit'
const DeleteDeleteOrganizationUnit = '/api/services/app/OrganizationUnit/DeleteOrganizationUnit'
const PostMoveOrganizationUnit = '/api/services/app/OrganizationUnit/MoveOrganizationUnit'
const GetGetDetail = '/api/services/app/OrganizationUnit/GetDetail?Id={Id}'
const PostChangeOperationType = '/api/services/app/OrganizationUnit/ChangeOperationType'
const PostHistory = '/api/services/app/OrganizationUnit/History'
const PostUserRoleRecords = '/api/services/app/OrganizationUnit/UserRoleRecords'
const PutUpdateOrganizationUnit = '/api/services/app/OrganizationUnit/UpdateOrganizationUnit'
const DELETERemoveUserFromOrganizationUnit = '/api/services/app/OrganizationUnit/RemoveUserFromOrganizationUnit'
const PostFindUsers = '/api/services/app/OrganizationUnit/FindUsers'
const PostAddUsersToOrganizationUnit = '/api/services/app/OrganizationUnit/AddUsersToOrganizationUnit'


export async function FindOrganizationUnits(data) {
  return request(PostFindOrganizationUnits, {
    method: "POST",
    body: data,
  })
}

export async function GetOrganizationUnitUsers(data) {
  const queryStr = generateRequestStr(data)
  return request(GetGetOrganizationUnitUsers + queryStr, {
    method: "GET",
  })
}

export async function CreateOrganizationUnit(data) {
  return request(PostCreateOrganizationUnit, {
    method: "POST",
    body: data,
  })
}

export async function DeleteOrganizationUnit(data) {
  const queryStr = generateRequestStr(data)
  return request(DeleteDeleteOrganizationUnit + queryStr, {
    method: "DELETE",
  })
}

export async function MoveOrganizationUnit(data) {
  return request(PostMoveOrganizationUnit, {
    method: "POST",
    body: data,
  })
}

export async function GetDetail(Id) {
  return request(GetGetDetail.replace('{Id}', Id), {
    method: "GET",
  })
}

export async function ChangeOperationType(data) {
  return request(PostChangeOperationType, {
    method: "POST",
    body: data,
  })
}

export async function History(data) {
  return request(PostHistory, {
    method: "POST",
    body: data,
  })
}

export async function UserRoleRecords(data) {
  return request(PostUserRoleRecords, {
    method: "POST",
    body: data,
  })
}

export async function UpdateOrganizationUnit(data) {
  return request(PutUpdateOrganizationUnit, {
    method: "PUT",
    body: data,
  })
}

export async function RemoveUserFromOrganizationUnit(data) {
  const queryStr = generateRequestStr(data)
  return request(DELETERemoveUserFromOrganizationUnit + queryStr, {
    method: "DELETE",
  })
}

export async function FindUsers(data) {
  return request(PostFindUsers, {
    method: "POST",
    body: data,
  })
}

export async function AddUsersToOrganizationUnit(data) {
  return request(PostAddUsersToOrganizationUnit, {
    method: "POST",
    body: data,
  })
}

