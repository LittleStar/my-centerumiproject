
import { request, downloadfile, generateRequestStr } from '../../utils'

//TokenAuth
const Authenticate = '/api/TokenAuth/Authenticate'

//Session
const GetUserInfo = '/api/services/app/Session/GetCurrentLoginInformations'

//User
const GetUserPermissions = '/api/services/app/User/GetCurrentUserPermissions'
const GetSubjectForEdit = '/api/services/app/User/GetSubjectForEdit?Id={Id}'
const PostCreateOrUpdateSubject = '/api/services/app/User/CreateOrUpdateSubject'
const GetGetUsers = '/api/services/app/User/FindUsers'
const PostFindSubjects = '/api/services/app/User/FindSubjects'
const GetSubjectForSales = '/api/services/app/User/GetSubjectForSales?Id={Id}'
const PostCreateOrUpdateUser = '/api/services/app/User/CreateOrUpdateUser'
const GetUserForEdit = '/api/services/app/User/GetUserForEdit'
const PostLinkSubjectSales = '/api/services/app/User/LinkSubjectSales'
const GetSubjectForHeadteacher = '/api/services/app/User/GetSubjectForHeadteacher?Id={Id}'
const PostLinkSubjectHeadTeacher = '/api/services/app/User/LinkSubjectHeadTeacher'
const GetGetSubjectDetail = '/api/services/app/User/GetSubjectDetail?Id={Id}'
const GetGetCurrentUserOrganizationUnits = '/api/services/app/UserOrganization/GetCurrentUserOrganizationUnits'
const PostChangeOrganizationUnit = '/api/TokenAuth/ChangeOrganizationUnit?organizationUnitId={organizationUnitId}'
const PostFindVip = '/api/services/app/User/FindVip'
const PostFindPotentialUser = '/api/services/app/User/FindPotentialUser'
const PostUserRoleChange = '/api/services/app/User/UserRoleChange'
const GetGetAllPermissions = '/api/services/app/Permission/GetAllPermissions'


//File
const GetExcelBatchImportTemplete = '/api/User/ExcelBatchImportTemplete'
const PostDownloadTempFile = '/api/File/DownloadTempFile'

//Menu
const GetMenu = '/api/Navigation/GetMenu'

//Role
const GetGetRolesExceptSubject = '/api/services/app/Role/GetRolesExceptSubject'


// login
export async function PostAuthenticate(data) {
  const headers = { 'Abp.TenantId': '1' }
  return request(Authenticate, {
    method: "POST",
    body: data,
    headers
  });
}

export async function GetCurrentUserInfo() {
  return request(GetUserInfo, {
    method: "GET",
  });
}

export async function GetCurrentUserOrganizationUnits() {
  return request(GetGetCurrentUserOrganizationUnits, {
    method: "GET",
  });
}

export async function ChangeOrganizationUnit(organizationUnitId) {
  return request(PostChangeOrganizationUnit.replace('{organizationUnitId}', organizationUnitId), {
    method: "POST",
  });
}

export async function ExcelBatchImportTemplete() {
  return downloadfile(GetExcelBatchImportTemplete, {
    method: "GET",
  });
}

export async function DownloadTempFile(data) {
  return downloadfile(PostDownloadTempFile, {
    method: "POST",
    body: data,
  });
}


export async function FindSubjects(data) {
  return request(PostFindSubjects, {
    method: "POST",
    body: data,
  });
}

export async function GetCurrentUserPermissions() {
  return request(GetUserPermissions, {
    method: "GET",
  });
}

export async function GetMenuPermission() {
  return request(GetMenu, {
    method: "GET",
  });
}

export async function GetSubjectEditInfo(Id = '') {
  return request(GetSubjectForEdit.replace('{Id}', Id), {
    method: "GET",
  });
}


export async function GetSubjectDetail(Id) {
  return request(GetGetSubjectDetail.replace('{Id}', Id), {
    method: "GET",
  });
}

export async function CreateOrUpdateSubject(data) {
  return request(PostCreateOrUpdateSubject, {
    method: "POST",
    body: data,
  });
}

export async function GetUsers(data) {
  return request(GetGetUsers, {
    method: "POST",
    body: data,
  });
}

export async function GetCurrentSubjectForSales(Id = '') {
  return request(GetSubjectForSales.replace('{Id}', Id), {
    method: "GET",
  })
}

export async function GetCurrentSubjectForHeadteacher(Id = '') {
  return request(GetSubjectForHeadteacher.replace('{Id}', Id), {
    method: "GET",
  })
}

export async function CreateOrUpdateUser(data) {
  return request(PostCreateOrUpdateUser, {
    method: "POST",
    body: data
  })
}

export async function GetCurrentUserForEdit(data) {
  const queryStr = generateRequestStr(data)
  return request(GetUserForEdit + queryStr, {
    method: "GET",
  })
}

export async function LinkSubjectSales(data) {
  return request(PostLinkSubjectSales, {
    method: "POST",
    body: data
  })
}

export async function LinkSubjectHeadTeacher(data) {
  return request(PostLinkSubjectHeadTeacher, {
    method: "POST",
    body: data
  })
}

export async function FindVip(data) {
  return request(PostFindVip, {
    method: "POST",
    body: data,
  });
}

export async function FindPotentialUser(data) {
  return request(PostFindPotentialUser, {
    method: "POST",
    body: data,
  });
}

export async function GetRolesExceptSubject(data) {
  const queryStr = generateRequestStr(data)
  return request(GetGetRolesExceptSubject + queryStr, {
    method: "GET",
  })
}

export async function GetAllPermissions(data) {
  return request(GetGetAllPermissions, {
    method: "GET",
  })
}

export async function UserRoleChange(data) {
  return request(PostUserRoleChange, {
    method: "POST",
    body: data,
  });
}
