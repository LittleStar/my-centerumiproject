import { request, generateRequestStr } from '../../utils'

const	GetGetUserNotifications='/api/services/app/Notification/GetUserNotifications'
const	PostSetNotificationAsRead='/api/services/app/Notification/SetNotificationAsRead'
const	PostSetAllNotificationsAsRead='/api/services/app/Notification/SetAllNotificationsAsRead'


export async function SetNotificationAsRead(data) {
	return request(PostSetNotificationAsRead, {
		method: "POST",
		body: data,
	});
}

export async function SetAllNotificationsAsRead(data) {
	return request(PostSetAllNotificationsAsRead, {
		method: "POST",
		body: data,
	});
}

export async function GetUserNotifications(data={}) {
  const queryStr = generateRequestStr(data)
  return request(GetGetUserNotifications + queryStr, {
    method: "GET",
  });
}
