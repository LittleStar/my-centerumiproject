import { request, generateRequestStr } from '../../utils'

const  GetGetSchedule='/api/services/app/Teaching/GetSchedule'
const  GetGetCoursePlanForEdit='/api/services/app/Teaching/GetCoursePlanForEdit'
const  GetGetAllActiveCoursePeriod='/api/services/app/Teaching/GetAllActiveCoursePeriod?DayOfWeek={DayOfWeek}'
const  PostCreateOrUpdateCoursePlan='/api/services/app/Teaching/CreateOrUpdateCoursePlan'
const  GetGetCoursePlan='/api/services/app/Teaching/GetCoursePlan'
const  DeleteDeleteCoursePlan='/api/services/app/Teaching/DeleteCoursePlan?Id={Id}'
const  PostManuallySchedule='/api/services/app/Teaching/ManuallySchedule'
const  PostAddUserToCourseInstance='/api/services/app/Teaching/AddUserToCourseInstance'
const  PostAddToCoursePlan= '/api/services/app/Teaching/AddToCoursePlan'
const  GetGetCoursePlanUsers= '/api/services/app/Teaching/GetCoursePlanUsers'
const  GetGetCourseInstanceForEdit= '/api/services/app/Teaching/GetCourseInstanceForEdit'
const  PostCreateOrUpdateCourseInstance= '/api/services/app/Teaching/CreateOrUpdateCourseInstance'
const  GetGetCoursePlanInfo= '/api/services/app/Teaching/GetCoursePlanInfo?Id={Id}'
const  GetGetCourseInstanceInfo= '/api/services/app/Teaching/GetCourseInstanceInfo?Id={Id}'
const  GetGetCourseInstanceUsers= '/api/services/app/Teaching/GetCourseInstanceUsers?Id={Id}'
const  PostCheckInManageList= '/api/services/app/Teaching/CheckInManageList'
const  PostCreateCourseInstanceUserFeedback= '/api/services/app/Teaching/CreateCourseInstanceUserFeedback'
const  GetGetCourseInstanceUserFeedbackForEdit='/api/services/app/Teaching/GetCourseInstanceUserFeedbackForEdit'
const  PostChangeUserCourseInstance= '/api/services/app/Teaching/ChangeUserCourseInstance'
const  DeleteRemoveFromCoursePlan= '/api/services/app/Teaching/RemoveFromCoursePlan'
const  GetGetUserCourseInstanceList='/api/services/app/Teaching/GetUserCourseInstanceList'
const  PostFindCoursePlan='/api/services/app/Teaching/FindCoursePlan'
const  PostFindCourseInstanceFeedback= '/api/services/app/Teaching/FindCourseInstanceFeedback'
const  PutUpdateCourseInstanceUserFeedback= '/api/services/app/Teaching/UpdateCourseInstanceUserFeedback'
const  DeleteDeleteCourseInstanceFeedback= '/api/services/app/Teaching/DeleteCourseInstanceFeedback'


export async function GetSchedule(data) {
  // data.date = encodeURIComponent(data.date)
  const queryStr = generateRequestStr(data)
	return request(GetGetSchedule+queryStr, {
		method: "GET",
	});
}

export async function GetCoursePlanForEdit(data) {
	const queryStr = generateRequestStr(data)
	return request(GetGetCoursePlanForEdit+queryStr, {
		method: "GET",
	});
}

export async function GetCourseInstanceUserFeedbackForEdit(data) {
  const queryStr = generateRequestStr(data)
  return request(GetGetCourseInstanceUserFeedbackForEdit+queryStr, {
    method: "GET",
  });
}

export async function GetUserCourseInstanceList(data) {
  const queryStr = generateRequestStr(data)
  return request(GetGetUserCourseInstanceList+queryStr, {
    method: "GET",
  });
}

export async function GetAllActiveCoursePeriod(DayOfWeek) {
	return request(GetGetAllActiveCoursePeriod.replace('{DayOfWeek}',DayOfWeek), {
		method: "GET",
	});
}


export async function CreateOrUpdateCoursePlan(data) {
  return request(PostCreateOrUpdateCoursePlan, {
    method: "POST",
    body: data,
  });
}

export async function FindCoursePlan(data) {
  return request(PostFindCoursePlan, {
    method: "POST",
    body: data,
  });
}

export async function GetCoursePlan(data) {
	const queryStr = generateRequestStr(data)
	return request(GetGetCoursePlan+queryStr, {
		method: "GET",
	});
}

export async function DeleteCoursePlan(Id) {
  return request(DeleteDeleteCoursePlan.replace('{Id}', Id), {
    method: "DELETE",
  });
}

export async function ManuallySchedule(data) {
  return request(PostManuallySchedule, {
    method: "POST",
    body: data,
  });
}

export async function AddUserToCourseInstance(data) {
  return request(PostAddUserToCourseInstance, {
    method: "POST",
    body: data,
  });
}

export async function AddToCoursePlan(data) {
  return request(PostAddToCoursePlan, {
    method: "POST",
    body: data,
  });
}

export async function GetCoursePlanUsers(data) {
  const queryStr = generateRequestStr(data)
	return request(GetGetCoursePlanUsers+queryStr,{
		method: "GET",
  })
}

export async function GetCourseInstanceForEdit(data) {
  const queryStr = generateRequestStr(data)
  return request(GetGetCourseInstanceForEdit+queryStr, {
    method: "GET",
  });
}

export async function GetCoursePlanInfo(Id) {
  return request(GetGetCoursePlanInfo.replace('{Id}', Id), {
    method: "GET",
  });
}

export async function GetCourseInstanceInfo(Id) {
  return request(GetGetCourseInstanceInfo.replace('{Id}', Id), {
    method: "GET",
  });
}

export async function GetCourseInstanceUsers(Id) {
  return request(GetGetCourseInstanceUsers.replace('{Id}', Id), {
    method: "GET",
  });
}

export async function CheckInManageList(data) {
  return request(PostCheckInManageList, {
    method: "POST",
    body: data,
  });
}

export async function ChangeUserCourseInstance(data) {
  return request(PostChangeUserCourseInstance, {
    method: "POST",
    body: data,
  });
}

export async function RemoveFromCoursePlan(data) {
  const queryStr = generateRequestStr(data)
  return request(DeleteRemoveFromCoursePlan+queryStr, {
    method: "DELETE",
  });
}

export async function CreateOrUpdateCourseInstance(data) {
  return request(PostCreateOrUpdateCourseInstance, {
    method: "POST",
    body: data,
  });
}

export async function CreateCourseInstanceUserFeedback(data) {
  return request(PostCreateCourseInstanceUserFeedback, {
    method: "POST",
    body: data,
  });
}

export async function FindCourseInstanceFeedback(data) {
  return request(PostFindCourseInstanceFeedback, {
    method: "POST",
    body: data,
  });
}

export async function UpdateCourseInstanceUserFeedback(data) {
  return request(PutUpdateCourseInstanceUserFeedback, {
    method: "PUT",
    body: data,
  });
}

export async function DeleteCourseInstanceFeedback(data) {
  const queryStr = generateRequestStr(data)
  return request(DeleteDeleteCourseInstanceFeedback+queryStr, {
    method: "DELETE",
  });
}

