import { request, generateRequestStr } from '../../utils'

const  PostOperationalStatistics= '/api/services/app/LocationStatistics/OperationalStatistics'
const  PostUserSourceStatistics= '/api/services/app/LocationStatistics/UserSourceStatistics'
const  PostSalesStatistics= '/api/services/app/LocationStatistics/SalesStatistics'
const  PostClassStatistics= '/api/services/app/LocationStatistics/ClassStatistics'
const  PostTeacherTeachingStatistics= '/api/services/app/LocationStatistics/TeacherTeachingStatistics'
const  PostSubjectCourseStatictis='/api/services/app/LocationStatistics/SubjectCourseStatictis'
const  GetGetAvailableOrganizationUnits='/api/services/app/LocationStatistics/GetAvailableOrganizationUnits'
const  PostMonthlySalesStatistics= '/api/services/app/LocationStatistics/MonthlySalesStatistics'

export async function OperationalStatistics(data) {
  return request(PostOperationalStatistics, {
    method: "POST",
    body: data,
  })
}

export async function UserSourceStatistics(data) {
  return request(PostUserSourceStatistics, {
    method: "POST",
    body: data,
  })
}

export async function SalesStatistics(data) {
  return request(PostSalesStatistics, {
    method: "POST",
    body: data,
  })
}

export async function ClassStatistics(data) {
  return request(PostClassStatistics, {
    method: "POST",
    body: data,
  })
}

export async function TeacherTeachingStatistics(data) {
  return request(PostTeacherTeachingStatistics, {
    method: "POST",
    body: data,
  })
}

export async function SubjectCourseStatictis(data) {
  return request(PostSubjectCourseStatictis, {
    method: "POST",
    body: data,
  })
}

export async function GetAvailableOrganizationUnits() {
	return request(GetGetAvailableOrganizationUnits,{
		method: "GET",
  })
}

export async function MonthlySalesStatistics(data) {
  return request(PostMonthlySalesStatistics, {
    method: "POST",
    body: data,
  })
}


