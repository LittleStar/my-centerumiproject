export default [
  // login
  {
    path: '/',
    component: './LoadingPage/LoadingPage',
  },
  {
    path: '/login',
    component: './LoginPage/Login',
  },
  // center layout
  {
    path: '/system',
    component: '../layouts/System/SystemLayout',
    name: 'SystemLayout',
    routes: [
      { path: '/system', redirect: '/system/manage' },
      {
        path: '/system/manage',
        name: 'MenuLayout',
        component: '../layouts/System/SystemMenu',
        routes: [
          //首页
          //{ path: '/system/manage', redirect: '/system/manage/dashboard' },
          // dashboard
          {
            path: '/system/manage/dashboard',
            name: 'Dashboard',
            component: './System/Dashboard/Dashboard',
            routes: [
              {
                path: '/system/manage/dashboard/checkin',
                name: 'Dashboard-checkin',
                component: './System/Dashboard/Checkin/Checkin',
                routes: [
                  {
                    path: '/system/manage/dashboard/checkin',
                    redirect: '/system/manage/dashboard/checkin/notCheckin',
                  },
                  {
                    path: '/system/manage/dashboard/checkin/notCheckin',
                    name: 'Dashboard-notCheckin',
                    component: './System/Dashboard/Checkin/NotCheckin',
                  },
                  {
                    path: '/system/manage/dashboard/checkin/checkinHistory',
                    name: 'Dashboard-checkinHistory',
                    component: './System/Dashboard/Checkin/CheckinHistory',
                  },
                  {
                    path: '/system/manage/dashboard/checkin/leftHistory',
                    name: 'Dashboard-LeftHistory',
                    component: './System/Dashboard/Checkin/LeftHistory',
                  },
                ],
              },
              {
                path: '/system/manage/dashboard/courseSchedule',
                name: 'Dashboard-courseSchedule',
                component: './System/Dashboard/CourseSchedule/CourseSchedule',
              },
              {
                path: '/system/manage/dashboard/operationalReport',
                name: 'Dashboard-operationalReport',
                component: './System/Dashboard/OperationalReport/OperationalReport',
              },
            ],
          },

          //报表中心
          {
            path: '/system/manage/report',
            name: 'report',
            component: './System/Report/Report',
            routes: [
              { path: '/system/manage/report', redirect: '/system/manage/report/sa' },
              {
                path: '/system/manage/report/sa',
                name: 'report-sales',
                component: './System/Report/Sales/Sales',
                routes: [
                  { path: '/system/manage/report/sa', redirect: '/system/manage/report/sa/us' },
                  {
                    path: '/system/manage/report/sa/us',
                    name: 'report-sales-userSource',
                    component: './System/Report/Sales/UserSource',
                  },
                  {
                    path: '/system/manage/report/sa/sales',
                    name: 'report-sales-salesData',
                    component: './System/Report/Sales/SalesData',
                  },
                ],
              },

              {
                path: '/system/manage/report/te',
                name: 'report-teach',
                component: './System/Report/Teach/Teach',
                routes: [
                  { path: '/system/manage/report/te', redirect: '/system/manage/report/te/mc' },
                  {
                    path: '/system/manage/report/te/mc',
                    name: 'report-teach-monthCost',
                    component: './System/Report/Teach/MonthCost',
                  },
                  {
                    path: '/system/manage/report/te/tc',
                    name: 'report-teach-teacherCourse',
                    component: './System/Report/Teach/TeacherCourse',
                  },
                  {
                    path: '/system/manage/report/te/ap',
                    name: 'report-teach-appear',
                    component: './System/Report/Teach/Appear/Appear',
                    routes: [
                      {
                        path: '/system/manage/report/te/ap',
                        redirect: '/system/manage/report/te/ap/appearDetails',
                      },
                      {
                        path: '/system/manage/report/te/ap/appearDetails',
                        name: 'report-teach-appear-appearDetails',
                        component: './System/Report/Teach/Appear/AppearDetails',
                      },
                      {
                        path: '/system/manage/report/te/ap/appearAdvisor',
                        name: 'report-teach-appear-appearAdvisor',
                        component: './System/Report/Teach/Appear/AppearAdvisor',
                      },
                      {
                        path: '/system/manage/report/te/ap/appearTeacher',
                        name: 'report-teach-appear-appearTeacher',
                        component: './System/Report/Teach/Appear/AppearTeacher',
                      },
                      {
                        path: '/system/manage/report/te/ap/appearSales',
                        name: 'report-teach-appear-appearSales',
                        component: './System/Report/Teach/Appear/AppearSales',
                      },
                    ],
                  },
                ],
              },

              {
                path: '/system/manage/report/pa',
                name: 'report-teach',
                component: './System/Report/Pay/Pay',
                routes: [
                  { path: '/system/manage/report/pa', redirect: '/system/manage/report/pa/mp' },
                  {
                    path: '/system/manage/report/pa/mp',
                    name: 'report-pay-monthPay',
                    component: './System/Report/Pay/MonthPay',
                  },
                ],
              },
            ],
          },

          // 课程顾问中心
          {
            path: '/system/manage/sales',
            name: 'sales',
            component: './System/Sales/Sales',
            routes: [
              { path: '/system/manage/sales', redirect: '/system/manage/sales/user' },
              {
                path: '/system/manage/sales/user',
                name: 'sales-user',
                component: './System/Sales/User/User',
              },
            ],
          },

          // 会员中心
          {
            path: '/system/manage/user',
            name: 'User',
            component: './System/User/User',
            routes: [
              { path: '/system/manage/user', redirect: '/system/manage/user/vip' },
              {
                path: '/system/manage/user/vip',
                name: 'Vip',
                component: './System/User/Vip/Vip',
              },
              {
                path: '/system/manage/user/contract',
                name: 'contract',
                component: './System/User/Contract/Contract',
              },
            ],
          },

          //教务中心
          {
            path: '/system/manage/edu',
            name: 'Edu',
            component: './System/Edu/Edu',
            routes: [
              { path: '/system/manage/edu', redirect: '/system/manage/edu/class' },
              {
                path: '/system/manage/edu/class',
                name: 'Class',
                component: './System/Edu/Class',
              },
              {
                path: '/system/manage/edu/plan',
                name: 'ClassPlan',
                component: './System/Edu/Plan',
              },
            ],
          },

          //测评中心
          {
            path: '/system/manage/test',
            name: 'Edu',
            component: './System/Test/Test',
            routes: [
              { path: '/system/manage/test', redirect: '/system/manage/test/vrat' },
              {
                path: '/system/manage/test/vrat',
                name: 'Test-VRAT',
                component: './System/Test/Vrat/Vrat',
              },
            ],
          },

          //审核中心
          {
            path: '/system/manage/verify',
            name: 'Verify',
            component: './System/Verify/Verify',
            routes: [
              { path: '/system/manage/verify', redirect: '/system/manage/verify/contract' },
              {
                path: '/system/manage/verify/contract',
                name: 'Verify-Contract',
                component: './System/Verify/Contract/Contract',
              },
              {
                path: '/system/manage/verify/refund',
                name: 'Verify-Refund',
                component: './System/Verify/Refund/Refund',
              },
              {
                path: '/system/manage/verify/gift',
                name: 'Verify-Gift',
                component: './System/Verify/Gift/Gift',
              },
            ],
          },

          //配置中心
          {
            path: '/system/manage/setting',
            name: 'Setting',
            component: './System/Setting/Setting',
            routes: [
              { path: '/system/manage/setting', redirect: '/system/manage/setting/user' },
              {
                path: '/system/manage/setting/user',
                name: 'Setting-User',
                component: './System/Setting/User/User',
              },
              {
                path: '/system/manage/setting/account',
                name: 'Setting-Account',
                component: './System/Setting/Account/Account',
              },
              {
                path: '/system/manage/setting/course',
                name: 'Setting-Course',
                component: './System/Setting/Course/Course',
              },
              {
                path: '/system/manage/setting/room',
                name: 'Setting-Room',
                component: './System/Setting/Room/Room',
              },
              {
                path: '/system/manage/setting/other',
                name: 'Setting-Other',
                component: './System/Setting/Other/Other',
              },

            ],
          },

          //管理中心
          {
            path: '/system/manage/manage',
            name: 'Manage',
            component: './System/Manage/Manage',
            routes: [
              { path: '/system/manage/manage', redirect: '/system/manage/manage/organization' },
              {
                path: '/system/manage/manage/organization',
                name: 'Manage-Organization',
                component: './System/Manage/Organization/Organization',
              },
              {
                path: '/system/manage/manage/center',
                name: 'Manage-Center',
                component: './System/Manage/Center/Center',
              },
              {
                path: '/system/manage/manage/region',
                name: 'Manage-Region',
                component: './System/Manage/Region/Region',
                routes: [
                  { path: '/system/manage/manage/region', redirect: '/system/manage/manage/region/regionList'},
                  {
                    path: '/system/manage/manage/region/regionList',
                    name: 'Manage-Region-regionList',
                    component: './System/Manage/Region/RegionList',
                  }
                ]
              },
              {
                path: '/system/manage/manage/course',
                name: 'Manage-Course',
                component: './System/Manage/Course/Course',
                routes: [
                  { path: '/system/manage/manage/course', redirect: '/system/manage/manage/course/packageList'},
                  {
                    path: '/system/manage/manage/course/packageList',
                    name: 'Manage-CoursePackageList',
                    component: './System/Manage/Course/PackageList',
                  },
                  {
                    path: '/system/manage/manage/course/courseList',
                    name: 'Manage-CourseCourseList',
                    component: './System/Manage/Course/CourseList',
                  }
                ]
              },
            ],
          },

          //详情页
          //用户详情页
          {
            path: '/system/manage/userDetail',
            name: 'UserDetail',
            component: './System/Detail/UserDetail/UserDetail',
            routes: [
              {
                path: '/system/manage/userDetail/basicInfo',
                name: 'BasicInfo',
                component: './System/Detail/UserDetail/BasicInfo',
              },
              {
                path: '/system/manage/userDetail/salesInfo',
                name: 'salesInfo',
                component: './System/Detail/UserDetail/SalesInfo',
              },
              {
                path: '/system/manage/userDetail/contractInfo',
                name: 'contractInfo',
                component: './System/Detail/UserDetail/ContractInfo',
              },
              {
                path: '/system/manage/userDetail/courseInfo',
                name: 'courseInfo',
                component: './System/Detail/UserDetail/CourseInfo',
              },
              {
                path: '/system/manage/userDetail/vratInfo',
                name: 'vratInfo',
                component: './System/Detail/UserDetail/TestInfo',
              },
            ],
          },
          //合同详情页
          {
            path: '/system/manage/contractDetail',
            name: 'contractDetail',
            component: './System/Detail/ContractDetail/ContractInfoDetail',
          },
          //班级详情页
          {
            path: '/system/manage/classDetail',
            name: 'Class',
            component: './System/Detail/ClassDetail/ClassDetail',
            routes: [
              {
                path: '/system/manage/classDetail/studentList',
                name: 'StudentList',
                component: './System/Detail/ClassDetail/StudentList',
              },
              {
                path: '/system/manage/classDetail/courseInfo',
                name: 'CourseInfo',
                component: './System/Detail/ClassDetail/CourseInfo',
              },
            ]
          },
           //长期计划详情页
          {
            path: '/system/manage/classPlanDetail',
            name: 'ClassPlanDetail',
            component: './System/Detail/ClassPlanDetail/ClassPlanDetail',
            routes: [
              {
                path: '/system/manage/classPlanDetail/classPlanStudentList',
                name: 'ClassPlanStudentList',
                component: './System/Detail/ClassPlanDetail/ClassPlanStudentList',
              },
              // {
              //   path: '/system/manage/classPlanDetail/classPlanCourseInfo',
              //   name: 'ClassPlanCourseInfo',
              //   component: './System/Detail/ClassPlanDetail/ClassPlanCourseInfo',
              // }
            ]
          },
          //中心详情页
          {
            path: '/system/manage/centerDetail',
            name: 'ClassManage-Center',
            component: './System/Detail/CenterDetail/CenterDetail',
          },
          //测试详情页
          {
            path: '/system/manage/vratDetail',
            name: 'Manage-Vrat',
            component: './System/Detail/TestDetail/VratDetail',
            routes: [
              {
                path: '/system/manage/vratDetail/baseInfo',
                name: 'Manage-Vrat-baseInfo',
                component: './System/Detail/TestDetail/BaseInfo',
              },
              {
                path: '/system/manage/vratDetail/userTestInfo',
                name: 'Manage-Vrat-userTestInfo',
                component: './System/Detail/TestDetail/UserTestInfo',
              },
              {
                path: '/system/manage/vratDetail/commentsList',
                name: 'Manage-Vrat-commentsList',
                component: './System/Detail/TestDetail/CommentsList',
              },

            ]
          },
          //课程详情页
          {
            path: '/system/manage/courseDetail',
            name: 'Manage-Vrat-commentsList',
            component: './System/Detail/CourseDetail/CourseDetail',
          },
          //区域经理详情页
          {
            path: '/system/manage/regionDetail',
            name: 'Manage-Region-regionDetail',
            component: './System/Detail/RegionDetail/RegionDetail',
          },
          // 员工详情页
          {
            path: '/system/manage/employeeDetail',
            name: 'Manage-setting-employeeDetail',
            component: './System/Detail/EmployeeDetail/EmployeeDetail',
          },
        ],
      },
      {
        component: './Exception/NotFound',
      },
    ],
  },
  {
    component: './Exception/NotFound',
  },
];
